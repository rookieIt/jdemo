package cn.willbj.dubbo.zk.demo.consumer;

import cn.willbj.dubbo.zk.demo.DemoService;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Application {
    /**
     * In order to make sure multicast registry works, need to specify '-Djava.net.preferIPv4Stack=true' before
     * launch the application
     */
    public static void main(String[] args) throws Exception{
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring/dubbo-consumer.xml");
        context.start();
        DemoService demoService = context.getBean("demoService", DemoService.class);
        String hello = demoService.sayHello("world");
        System.out.println("result: " + hello);

        // by_will
        Application application = new Application();
        application.test();
        System.in.read();
    }

    public void test() throws InterruptedException {
        String temp = "a";
        for (;;){
            test1(temp);
        }
    }

    public void test1(String temp1) throws InterruptedException {
        Thread.sleep(5000L);
        System.out.println(temp1);
    }

}
