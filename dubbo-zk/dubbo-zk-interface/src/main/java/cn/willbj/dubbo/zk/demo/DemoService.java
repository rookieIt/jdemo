package cn.willbj.dubbo.zk.demo;

public interface DemoService {

    String sayHello(String name);

}