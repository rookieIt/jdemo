package cn.jdemo.sql.datasource;

/**
 * @see javax.sql.DataSource
 * @see java.sql.Connection
 *
 * 通过driver和drivermanager获取connection
 * @see java.sql.Driver
 * @see java.sql.DriverManager
 * DriverManager是驱动的管理类，用它来连接数据库比Driver更为方便，
 * 只需要利用反射加载好数据库的驱动程序然后再调用其中的getConnection()方法即可完成数据库的连接，
 * 因为可以通过改变传入getConnection()方法的参数来返回不同的数据库连接，所以它可以管理不同的驱动程序
 *
 * @description
 * @date 2021/2/12
 */
public class AllInOne {
}
