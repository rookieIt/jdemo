package cn.jdemo.cron.scheduledExecutorService;

import java.util.concurrent.*;

/**
 * @see ScheduledExecutorService
 * @see ScheduledExecutorService#schedule(Runnable, long, TimeUnit) 
 * @see ScheduledExecutorService#schedule(Callable, long, TimeUnit)
 *
 * @see ScheduledExecutorService#scheduleAtFixedRate(Runnable, long, long, TimeUnit)
 * @see ScheduledExecutorService#scheduleWithFixedDelay(Runnable, long, long, TimeUnit)
 *
 * @date 2020/12/30
 */
public class Demo01 {
    public static void main(String[] args) {
        // test01();
        // test02();
        test00();
    }

    /**
     * 输出内容:
     *  空
     */
    public static void test00() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() << 1);
        ScheduledFuture<?> schedule = scheduledExecutorService.schedule(() -> {
            System.out.println("java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit)");
        }, 1000, TimeUnit.MILLISECONDS);
        if(!scheduledExecutorService.isShutdown()){
            scheduledExecutorService.shutdownNow(); // 立即停止
        }
    }

    /**
     * 输出内容
     * java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit)
     * 
     * @see ScheduledExecutorService#schedule(Runnable, long, TimeUnit) 
     */
    public static void test01() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() << 1);
        ScheduledFuture<?> schedule = scheduledExecutorService.schedule(() -> {
            System.out.println("java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit)");
        }, 1000, TimeUnit.MILLISECONDS);
        if(!scheduledExecutorService.isShutdown()){
            // scheduledExecutorService.shutdownNow(); 立即停止
            scheduledExecutorService.shutdown();//执行完毕后停止
        }
    }

    /**
     * 输出内容
     * java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit)
     * 
     * @see ScheduledExecutorService#schedule(Callable, long, TimeUnit) 
     */
    public static void test02() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() << 1);
        ScheduledFuture<?> schedule = scheduledExecutorService.schedule(() -> {
            return "java.util.concurrent.ScheduledExecutorService.schedule(java.lang.Runnable, long, java.util.concurrent.TimeUnit)";
        }, 1000, TimeUnit.MILLISECONDS);

        if(!scheduledExecutorService.isShutdown()){
            // scheduledExecutorService.shutdownNow(); 立即停止
            scheduledExecutorService.shutdown();//执行完毕后停止
        }

        try {
            System.out.println(schedule.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
