/**
 * java自身含有三种种定时任务执行方式
 *
 * @see java.util.Timer 缺点:(1)单线程任务调度(2)Timer是基于绝对时间的，对系统时间比较敏感(3)不会捕获异常,
 * @see java.util.concurrent.ScheduledExecutorService 解决了上述两个问题(1)线程池任务调度(2)基于相对时间
 * @see java.util.concurrent.DelayQueue 延迟队列，基于PriorityQueue按照时间升序
 *
 * @see java.util.concurrent.ScheduledExecutorService#schedule(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit)
 *      返回值ScheduledFuture<V>
 * 
 * @see java.util.concurrent.Executors#newScheduledThreadPool(int) 
 * @see java.util.concurrent.Executors#newScheduledThreadPool(int, java.util.concurrent.ThreadFactory)
 *
 *
 * *****************************************************************************
 * ScheduledExecutorService 每个任务一个线程，然后自身判断调度
 * *****************************************************************************
 * netty的时间轮算法
 * 1) 海量任务
 * 2) 适用于处理耗时较短的任务
 * 3) 任务的新增和取消都是 O(1) 时间复杂度
 * 问题
 * 1) 单层级时间轮 --> 空推进问题
 * 解决方式
 * 1) DelayQueue 代替 do-while 循环进行调度,当某一个 slot 没有到期任务可执行时，会阻塞等待，解决空推进问题
 * 2) 多层级时间轮 --> 以时分秒的方式计算，来减少空间冗余，解决空推进问题
 * @see io.netty.util.HashedWheelTimer
 *
 *
 * *****************************************************************************
 *
 * 补充内容；
 * Runnable、Callable、Future和FutureTask关系
 * @see java.lang.Runnable
 * @see java.util.concurrent.Callable
 * @see java.util.concurrent.Future 对具体的Runnable或者Callable的执行结果进一步操作
 *
 * @see java.util.concurrent.FutureTask RunnableFuture实现类: 可对Runnbale或者Callable的结果进行消费
 * @see java.util.concurrent.ScheduledFuture Future实现类: 可对Runnbale或者Callable的结果进行消费
 *
 * @see java.util.concurrent.Future#cancel(boolean mayInterruptIfRunning) 若为true则执行中的任务可被中断，否则在进行的任务被允许完成
 * @see java.util.concurrent.Future#isCancelled()  如果此任务在正常完成之前被取消，则返回{@code true}
 * @see java.util.concurrent.Future#isDone() 是否完成
 * @see java.util.concurrent.Future#get() 
 * @see java.util.concurrent.Future#get(long timeout, java.util.concurrent.TimeUnit) 最长等待时间
 *
 * @date 2020/12/30
 */
package cn.jdemo.cron;