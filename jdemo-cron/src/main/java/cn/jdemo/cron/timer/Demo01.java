package cn.jdemo.cron.timer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @see Timer
 *
 * 构造方法
 * @see Timer#Timer()
 * @see Timer#Timer(String) 
 * @see Timer#Timer(boolean) 
 * @see Timer#Timer(String, boolean)
 *
 * @see Timer#schedule(TimerTask, Date time) 执行任务的时间
 * @see Timer#schedule(TimerTask, long delay) 在执行任务之前的毫秒数
 * @see Timer#schedule(TimerTask, long delay, long period) period,连续执行任务之间的时间（以毫秒为单位）
 * @see Timer#schedule(TimerTask, Date firstTime, long period) firstTime,第一次执行任务时间
 * 
 * @see Timer#scheduleAtFixedRate(TimerTask, long delay, long period)
 * @see Timer#scheduleAtFixedRate(TimerTask, Date firstTime, long period)
 *
 * @see Timer#queue 计时器,任务队列
 * @see Timer#thread 计时器,任务调度线程
 *
 * 补充:schedule和scheduleAtFixedRate的区别在于，如果指定开始执行的时间在当前系统运行时间之前，
 *      scheduleAtFixedRate会把已经过去的时间也作为周期执行，而schedule不会把过去的时间算上;
 *
 * @description
 * @date 2020/12/30
 */
public class Demo01 {
    public static void main(String[] args) {
        // test01();
        // test02();
        test03();
    }
    public static void test01() {
        Timer test01 = new Timer("test01", false);
        test01.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("java.util.Timer#");
            }
        }, 1000);
    }

    public static void test02() {
        Timer test01 = new Timer("test01", false);
        test01.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("java.util.Timer#");
            }
        }, 1000,1000);
    }

    public static void test03() {
        Timer test01 = new Timer("test01", false);
        test01.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("java.util.Timer#");
            }
        }, 1000,1000);
    }

}
