# JDemo

#### 迁往新地址[原地址保留]
>https://gitee.com/oswillbj/jdemo.git

#### 介绍
1. 体系学习

2. 版本历史记录

版本|更新人|更新日期|备注
--- |---|------|--------
Ashe|will|2020-12-25|init
Ashe|will|2021-01-18|阶段性汇总
#### 版本介绍
1. 参照springcloud版本控制:https://www.pianshen.com/article/90181863839/
2. 大版本
    - 2.1) Ashe
    - 2.2) Bard
    - 2.3) Camille
    - 2.4) Darius
    - 2.5) Ekko
    - ... ...
3. 小版本
    - 3.1) SNAPSHOT：快照版本，随时可能修改
    - 3.2) (MileStone)，M1表示第1个里程碑版本，一般同时标注PRE，表示预览版版
    - 3.3) SR：Service Release，SR1表示第1个正式版本，一般同时标注GA(GenerallyAvailable)，表示稳定版本      

#### 软件架构
软件架构说明

#### 模块说明
|     模块     |      说明      |
| ---         |     ---        |
| jdemo-aop   |     aop        |
| jdemo-brief |     待重新整理内容       |
| jdemo-juc   |     juc开发       |
| jdemo-io   |     java.io包下api源码了解并简单使用       |
| jdemo-unsafe    |unsafe的了解并简单使用       |
| jdemo-util    |util的汇总leya       |
| dubbo-zk    |dubbo+zookeeper简应用       |
| dubbo-zk    |dubbo+zookeeper简应用       |
| dubbo-zk    |dubbo+zookeeper简应用       |
可以根据需求对每个模块单独引入，也可以通过引入`jdemo-all`方式引入所有模块。

#### 使用说明

1.  参照说明文档以及对应的模块说明，系统总结学习

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 文档借鉴

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
