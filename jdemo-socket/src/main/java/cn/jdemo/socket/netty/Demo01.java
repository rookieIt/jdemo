package cn.jdemo.socket.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Demo01 {
    public static void main(String[] args) throws InterruptedException {
        //设置线程组
        EventLoopGroup bossGroup = new NioEventLoopGroup(3);
        EventLoopGroup workerGroup = new NioEventLoopGroup(8);

        // 1.启动引导
        ServerBootstrap bootstrap = new ServerBootstrap();

        // 2.
        bootstrap.group(bossGroup, workerGroup)
                // 使用NioServerSocketChannel作为服务器的通道实现
                .channel(NioServerSocketChannel.class)
                /*
                  1).初始化服务器连接队列大小，服务端处理客户端连接请求是顺序处理的,所以同一时间只能处理一个客户端连接。
                  2).多个客户端同时来的时候,服务端将不能处理的客户端连接请求放在队列中等待处理
                  连接队列大小
                 */
                .option(ChannelOption.SO_BACKLOG, 1024)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        //对workerGroup的SocketChannel设置处理器
                        socketChannel.pipeline().addLast(new CustomNettyServerHandler());
                    }
                });
        System.out.println("netty server start。。");

        // 3.
        // 绑定一个端口并且同步, 生成了一个ChannelFuture异步对象，通过isDone()等方法可以判断异步事件的执行情况
        // 启动服务器(并绑定端口)，bind是异步操作，sync方法是等待异步操作执行完毕
        ChannelFuture cf = bootstrap.bind(9000).sync();


        // 给cf注册监听器，监听我们关心的事件
            /*cf.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (cf.isSuccess()) {
                        System.out.println("监听端口9000成功");
                    } else {
                        System.out.println("监听端口9000失败");
                    }
                }
            });*/
        // 等待服务端监听端口关闭，closeFuture是异步操作
        // 通过sync方法同步等待通道关闭处理完毕，这里会阻塞等待通道关闭完成，内部调用的是Object的wait()方法
        cf.channel().closeFuture().sync();

        //关闭资源
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
