package cn.jdemo.socket.zeroCopy;

import java.nio.ByteBuffer;

/**
 * @see java.nio.ByteBuffer#allocate(int) 
 * @see java.nio.ByteBuffer#allocateDirect(int)
 *
 */
public class DirectBufferTest {
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(10*1000);
        ByteBuffer allocate = ByteBuffer.allocate(1024*1024*1024);
        ByteBuffer.allocateDirect(1024*1024*1024);
        Thread.sleep(50*1000);
    }
}
