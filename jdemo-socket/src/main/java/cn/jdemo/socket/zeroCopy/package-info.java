/**
 * <a href="https://mp.weixin.qq.com/s/BV3dfFRFEYUcTs1SUPb2GQ"> kafka/rocketmq/java 零拷贝总结</>
 *
 *
 * <a href="https://blog.csdn.net/beiduofen2011/article/details/124339878">java bio,nio(内存映射/零拷贝),aio（直接IO）</>
 * 1.缓存io(bio/nio)-- 内核加载磁盘数据到pageCache
 *  mmap
 *  sendfile
 *
 * 2.直接io(aio)--内核直接加载磁盘数据，不经过pageCache
 *
 * 3.directBuffer: 用户空间(堆外)--分配的直接内存
 *
 * 4.directBuffer 与 直接io,概念上没有直接关系
 */
package cn.jdemo.socket.zeroCopy;