/**
 * C10k问题
 * nio简介 nio是java New IO的简称(并不只是指非阻塞IO)
 * @see java.nio.channels.Channel
 * @see java.nio.channels.FileChannel 文件
 * @see java.nio.channels.DatagramChannel UDP
 * @see java.nio.channels.ServerSocketChannel 监听新TCP连接，对每一个新连接都会创建一个SocketChannel。
 * @see java.nio.channels.SocketChannel tcp
 *
 * @see java.nio.channels.ServerSocketChannel#open()
 * @see java.nio.channels.Selector#open()
 * @see java.nio.channels.spi.AbstractSelectableChannel#register(java.nio.channels.Selector, int)
 * @see java.nio.channels.Selector#select()
 * @see java.nio.channels.Selector#selectedKeys()
 */
package cn.jdemo.socket.nio;