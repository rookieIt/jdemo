/**
 * bio
 * @see java.net.ServerSocket
 * @see java.net.ServerSocket#accept() 阻塞
 * @see java.net.ServerSocket#close()
 *
 * @see java.net.Socket
 * @see java.net.Socket#getInputStream() 阻塞
 * @see java.net.Socket#getOutputStream() 阻塞
 * @see java.net.Socket#connect(java.net.SocketAddress, int timeOut)
 *
 */
package cn.jdemo.socket.bio;