package cn.jdemo.socket.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @see DatagramSocket
 * @see DatagramPacket
 */
public class Recv {
    public static void main(String[] args) {
        Recv.recv();
    }

    /**
     * 基于UDP服务器端程序的编写
     */
    public static void recv() {
        try {
            //创建数据报套接字对象，绑定端口号为6000
            DatagramSocket ds = new DatagramSocket(6000);
            //构建数据包接收数据：
            //创建字节数组
            byte[] buf = new byte[10];
            //创建数据包对象，它的长度不能超过数组的长度，我们把它设为100
            DatagramPacket dp = new DatagramPacket(buf, 10);
            //接收数据
            ds.receive(dp);
            //打印数据
            //getLength方法返回实际接收数；getData方法返回数据，返回格式为字节数组
            System.out.println(new String(buf, 0, dp.getLength()));

            //关闭数据报套接字
            ds.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
