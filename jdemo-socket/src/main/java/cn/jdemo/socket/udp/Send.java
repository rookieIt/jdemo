package cn.jdemo.socket.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Send {
    public static void main(String[] args) {
        Send.send();
    }
    /**
     * 基于UDP客户端程序的编写
     */
    public static void send() {
        try {
            //创建一个数据报对象。
            DatagramSocket ds = new DatagramSocket();
            //要发送的数据
            String str = "Hello,this is zhangsan";
            //构造一个发送数据包：
            //InetAddress.getByName("localhost"):获得本地ip地址
            //端口号指定为6000
            DatagramPacket dp = new DatagramPacket(
                    str.getBytes(),
                    str.length(),
                    InetAddress.getByName("localhost"),
                    6000);
            //发送数据包
            ds.send(dp);

            //关闭数据报套接字
            ds.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
