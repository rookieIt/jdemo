package cn.jdemo.socket.rmi.server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RmiServer {
    public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException {
        LocateRegistry.createRegistry(8881);

        IBusiness business = new BusinessImpl();
        Naming.bind("rmi://localhost:8881/business",business);

        System.out.println("Hello, RMI Server!");
    }


}
