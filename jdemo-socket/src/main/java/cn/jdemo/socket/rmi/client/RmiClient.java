package cn.jdemo.socket.rmi.client;

import cn.jdemo.socket.rmi.server.IBusiness;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RmiClient {
    public static void main(String[] args) throws MalformedURLException, NotBoundException, RemoteException {
        IBusiness business = (IBusiness) Naming.lookup("rmi://localhost:8881/business");

        String resp = business.echo("xingoo");
        System.out.println(resp);
    }
}
