package cn.jdemo.socket.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class BusinessImpl extends UnicastRemoteObject implements IBusiness{

    protected BusinessImpl() throws RemoteException {
    }

    @Override
    public String echo(String message) throws RemoteException{
        return "hello,"+message;
    }
}
