package cn.jdemo.socket.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBusiness extends Remote {
    String echo(String message) throws RemoteException;
}
