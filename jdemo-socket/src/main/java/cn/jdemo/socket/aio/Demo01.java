package cn.jdemo.socket.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Demo01 {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        AsynchronousServerSocketChannel asynchronousServerSocketChannel = AsynchronousServerSocketChannel.open();
        asynchronousServerSocketChannel.bind(new InetSocketAddress(8002));
        while (true){
            Future<AsynchronousSocketChannel> accept = asynchronousServerSocketChannel.accept();
            AsynchronousSocketChannel asynchronousSocketChannel = accept.get();
            ByteBuffer byteBuffer = ByteBuffer.allocate(8);
            int read = asynchronousSocketChannel.read(byteBuffer).get();
            if(read!=-1 && read!= 0){
                System.out.println(new String(byteBuffer.array(),0, read));
            }
        }
    }
}
