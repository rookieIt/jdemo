/**
 * aio(nio2.0) 异步非阻塞
 * @see java.nio.channels.AsynchronousServerSocketChannel
 *
 * 异步io
 * <a href="https://zhuanlan.zhihu.com/p/115912936"></>
 */
package cn.jdemo.socket.aio;