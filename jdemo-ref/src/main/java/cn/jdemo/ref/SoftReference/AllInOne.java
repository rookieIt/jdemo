package cn.jdemo.ref.SoftReference;

/**
 * @see java.lang.ref.SoftReference
 *
 * soft reference和weak reference一样, 但被GC回收的时候需要多一个条件: 当系统内存不足时(GC是如何判定系统内存不足?
 * 是否有参数可以配置这个 threshold(临界点/阈值) ?), soft reference指向的object才会被回收.
 * 正因为有这个特性, soft reference比weak reference更加适合做cache objects的reference. 因为它可以尽可能的 retain cached objects, 减少重建他们所需的时间和消耗.
 * @description
 * @date 2021/1/8
 */
public class AllInOne {
}
