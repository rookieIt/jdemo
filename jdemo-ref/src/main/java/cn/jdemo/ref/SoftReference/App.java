package cn.jdemo.ref.SoftReference;

import java.lang.ref.SoftReference;

public class App {
    /**
     * 输出内容:
     * [B@5680a178
     * null
     * Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
     * 	at cn.jdemo.ref.SoftReference.App.main(App.java:21)
     *
     */
    public static void main(String[] args) {
        SoftReference<byte[]> softReference = null;
        try{
            byte[] buff = new byte[2*1024*1024];
            softReference = new SoftReference<>(buff);
            System.out.println(softReference.get());
            buff = null;
            byte[] buff2 = new byte[3*1024 * 1024];
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (softReference!=null){
                System.out.println(softReference.get());
            }
        }
    }
}
