package cn.jdemo.ref.StrongReference;

/**
 * 设置 -Xms2M -Xmx3M
 */
public class Demo01 {
    public static void main(String[] args) {
        testStrongReference2();
    }

    /**
     * 输出:
     * Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
     * 	at cn.jdemo.ref.StrongReference.Demo01.testStrongReference2(Demo01.java:14)
     * 	at cn.jdemo.ref.StrongReference.Demo01.main(Demo01.java:9)
     */
    private static void testStrongReference2() {
        // 当 new byte为 3M 时，堆内存溢出
        byte[] buff = new byte[1024 * 1024 * 3];
    }

    private static void testStrongReference() {
        // 当 new byte为 1M 时，程序运行正常
        byte[] buff = new byte[1024 * 1024 * 1];
    }
}
