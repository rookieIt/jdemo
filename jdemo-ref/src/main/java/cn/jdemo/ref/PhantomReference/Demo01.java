package cn.jdemo.ref.PhantomReference;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class Demo01 {
    public static void main(String[] args) throws InterruptedException {
        byte[] buff = new byte[1024 * 1024];
        ReferenceQueue<Object> objectReferenceQueue = new ReferenceQueue<>();
        PhantomReference<byte[]> sr = new PhantomReference<>(buff,objectReferenceQueue);
        System.out.println(sr.get());
        System.out.println(objectReferenceQueue.remove());
    }
}
