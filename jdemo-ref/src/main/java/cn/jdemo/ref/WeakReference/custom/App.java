package cn.jdemo.ref.WeakReference.custom;

public class App {
    public static void main(String[] args) {
        MyThread myThread = new MyThread(()->{
            MyThreadLocal<String> local = new MyThreadLocal<>();
            String my = local.getMy();
            System.out.println(my);
        });
        myThread.start();
    }
}
