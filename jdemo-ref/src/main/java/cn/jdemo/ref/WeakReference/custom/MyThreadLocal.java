package cn.jdemo.ref.WeakReference.custom;

import java.util.HashMap;
import java.util.Map;

public class MyThreadLocal<T> extends ThreadLocal{

    public T getMy(){
        Thread thread = Thread.currentThread();
        if (!(thread instanceof MyThread)){
            return (T) super.get();
        }
        System.out.println("enter into mythread...");
        MyThread myThread = (MyThread) thread;
        MyThreadLocalMap mythreadLocals = myThread.mythreadLocals;
        if (mythreadLocals == null){
            myThread.mythreadLocals = new MyThreadLocalMap(this,"随便");
        }
        mythreadLocals = myThread.mythreadLocals;
        return (T) mythreadLocals.get(this);
    }
    static class MyThreadLocalMap{

        Map map = new HashMap<MyThreadLocal,Object>();
        MyThreadLocalMap(MyThreadLocal<?> firstKey, Object firstValue) {
            System.out.println(firstKey);
            map.put(firstKey,firstValue);
        }

        public Object get(MyThreadLocal<?> tMyThreadLocal) {
            return map.get(tMyThreadLocal);
        }
    }
}
