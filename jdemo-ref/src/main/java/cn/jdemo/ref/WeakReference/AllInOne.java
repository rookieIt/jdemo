package cn.jdemo.ref.WeakReference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * @see java.lang.ref.WeakReference
 * @see WeakReference#get()
 * @see WeakReference#WeakReference(Object)
 * @see WeakReference#WeakReference(Object, ReferenceQueue)
 * @see WeakReference#queue
 *
 * 问题
 * 两个强引用对象就生成了，好吧，那么这个时候我做一下修改：
 * A a = new A();
 * B b = new B(a);
 * B的默认构造函数上是需要一个A的实例作为参数的，那么这个时候 A和B就产生了依赖，也可以说a和b产生了依赖
 *
 * a是对象A的引用，b是对象B的引用，对象B同时还依赖对象A，那么这个时候我们认为从对象B是可以到达对象A的。
 * 于是我又修改了一下代码
 * A a = new A();
 * B b = new B(a);
 * a = null;
 * A对象的引用a置空了，a不再指向对象A的地址，我们都知道当一个对象不再被其他对象引用的时候，是会被GC回收的，
 * 很显然即使a=null，那么A对象也是不可能被回收的，因为B依然依赖与A，在这个时候，造成了内存泄漏！
 *
 * 解决的问题
 * 当 a=null ，这个时候A只被弱引用依赖，那么GC会立刻回收A这个对象，这就是弱引用的好处！
 * 他可以在你对对象结构和拓扑不是很清晰的情况下，帮助你合理的释放对象，造成不必要的内存泄漏！！
 *
 * WeakReference的一个特点是它何时被回收是不可确定的, 因为这是由GC运行的不确定性所确定的.
 * 所以, 一般用weak reference引用的对象是有价值被cache, 而且很容易被重新被构建, 且很消耗内存的对象.
 *
 * ReferenceQueue
 * 在weak reference指向的对象被回收后, weak reference本身其实也就没有用了.
 * java提供了一个ReferenceQueue来保存这些所指向的对象已经被回收的reference.
 * 用法是在定义WeakReference的时候将一个ReferenceQueue的对象作为参数传入构造函数.
 *
 * @date 2021/1/8
 */
public class AllInOne {
}
