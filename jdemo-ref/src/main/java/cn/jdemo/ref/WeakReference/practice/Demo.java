package cn.jdemo.ref.WeakReference.practice;

public class Demo {
    /**
     * java.lang.Object@5fdef03a
     * null
     */
    public static void main(String[] args) throws InterruptedException {
        MyWeakRef a = new MyWeakRef(new Object());
        System.out.println(a.get());

        System.gc();
        Thread.sleep(2000);

        System.out.println(a.get());
    }
}
