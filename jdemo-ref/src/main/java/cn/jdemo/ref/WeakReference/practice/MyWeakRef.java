package cn.jdemo.ref.WeakReference.practice;

import java.lang.ref.WeakReference;

public class MyWeakRef extends WeakReference<Object> {
    public MyWeakRef(Object referent) {
        super(referent);
    }
}
