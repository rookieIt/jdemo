package cn.jdemo.juc.brief.sdk.bean.copier.test;

import java.util.Date;

import cn.jdemo.juc.brief.sdk.bean.copier.util.BeanCopyUtil;
import cn.jdemo.juc.brief.sdk.bean.copier.anno.AnnoCopyAlias;


public class Test {
	
	public void test() {
		YYY yyy = new YYY("test", 100000l, new Date(),"1000");
		XXX xxx = new XXX();
		System.out.println(xxx.toString());
		try {
			BeanCopyUtil.beanCopy(yyy, xxx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(xxx.toString());
		
	}
	public static void main(String[] args) {
		Test test = new Test();
		test.test();
	}
	public class XXX{
		@AnnoCopyAlias("str02")
		private String str01;
		@AnnoCopyAlias("long02")
		private Long long01;
		private Date date01;
		private Date date02;
		private Long long001;
		public String getStr01() {
			return str01;
		}
		public void setStr01(String str01) {
			this.str01 = str01;
		}
		public Long getLong01() {
			return long01;
		}
		public void setLong01(Long long01) {
			this.long01 = long01;
		}
		public Date getDate01() {
			return date01;
		}
		public void setDate01(Date date01) {
			this.date01 = date01;
		}
		
		public Date getDate02() {
			return date02;
		}
		public void setDate02(Date date02) {
			this.date02 = date02;
		}
		
		@Override
		public String toString() {
			return "XXX [str01=" + str01 + ", long01=" + long01 + ", date01=" + date01 + ", date02=" + date02
					+ ", long001=" + long001 + "]";
		}
		public Long getLong001() {
			return long001;
		}
		public void setLong001(Long long001) {
			this.long001 = long001;
		}
	}
	public class YYY{
		private String str02;
		private Long long02;
		private Date date02;
		private String long001;
		
		public YYY(String str02, Long long02, Date date02, String long001) {
			super();
			this.str02 = str02;
			this.long02 = long02;
			this.date02 = date02;
			this.long001 = long001;
		}

		public String getStr02() {
			return str02;
		}
		public void setStr02(String str02) {
			this.str02 = str02;
		}
		public Long getLong02() {
			return long02;
		}
		public void setLong02(Long long02) {
			this.long02 = long02;
		}
		public Date getDate02() {
			return date02;
		}
		public void setDate02(Date date02) {
			this.date02 = date02;
		}
		public String getLong001() {
			return long001;
		}
		public void setLong001(String long001) {
			this.long001 = long001;
		}
		
	}
}
