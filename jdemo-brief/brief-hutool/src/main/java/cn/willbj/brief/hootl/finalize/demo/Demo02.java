package cn.willbj.brief.hootl.finalize.demo;

/**
 * finalize()方法是Object类中提供的一个方法，在GC准备释放对象所占用的内存空间之前，它将首先调用finalize()方法
 */
public class Demo02 {
	
	public static void main(String[] args) {
		Demo02 demo02 = new Demo02();
		demo02 = null;
		System.gc();// 输出:Finalizer-->finalize()
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Finalizer-->finalize()");
		super.finalize();
	}
}
