package cn.willbj.brief.rt.clone;

import java.io.*;

/**
 * 深拷贝：对象需要序列化
 * @description
 * @date 2020/11/30
 */
public class CloneUtil {

    public  static  <T> T cloneObj(T t) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(t);
        oos.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bais);
        Object object = in.readObject();
        in.close();
        return (T) object;
    }
}
