package cn.willbj.brief.hootl.itr.demo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Demo02 {
	public static void main(String[] args) {
		List<Object> list = new ArrayList<>();
		list.add("a");
		System.out.println(list.size());// 1
		Object[] array = list.toArray(new Object[2]);
		System.out.println(array[0]);// a
		System.out.println(array[1]);// null
		
		Iterator<Object> iterator = list.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());// a
		}
	}

}
