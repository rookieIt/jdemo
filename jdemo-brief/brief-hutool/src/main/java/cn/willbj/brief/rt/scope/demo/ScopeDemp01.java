package cn.willbj.brief.rt.scope.demo;

import cn.willbj.brief.rt.scope.demo01.defaultDemo01;
import cn.willbj.brief.rt.scope.demo01.privateDemo01;
import cn.willbj.brief.rt.scope.demo01.protectedDemo01;

/**
 * 作用域	本类	本包范围内	子孙类（只代表其他包中的子孙类）	其他包
 * public   可以     可以	        可以	                     可以
 * protected 可以	 可以	        可以	                    不可以
 * default	可以	 可以	        不可以	                    不可以
 * private	可以	 不可以	        不可以	                    不可以
 */
public class ScopeDemp01 {
    public static void main(String[] args) {
        defaultDemo01 defaultDemo01 = new defaultDemo01();
        // 无法调用 defaultDemo01.
        privateDemo01 privateDemo01 = new privateDemo01();
        // 无法调用 privateDemo01.
        protectedDemo01 protectedDemo01 = new protectedDemo01();
        // 无法调用 protectedDemo01.
    }
}
