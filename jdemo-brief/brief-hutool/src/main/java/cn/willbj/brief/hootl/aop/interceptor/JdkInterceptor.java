package cn.willbj.brief.hootl.aop.interceptor;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cn.willbj.brief.hootl.aop.aspects.Aspect;

public class JdkInterceptor implements InvocationHandler,Serializable{
	
	private static final long serialVersionUID = 1L;

	private Object target;
	
	private Aspect aspect;
	
	public JdkInterceptor(Object target, Aspect aspect) {
		super();
		this.target = target;
		this.aspect = aspect;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		aspect.before(target,method,args);
		Object ret = null;
		
		try {
			method.setAccessible(true);
			ret = method.invoke(target,args);
			/**
			 * 不能在真实代理类上调用方法，因为这样后构成一个递归死循环调用。真实代理类可以用于方法返回。
			 * 
			 * 1. 可以使用反射获取代理对象的信息（也就是proxy.getClass().getName()）。
			 * 2. 可以将代理对象返回以进行连续调用，这就是proxy存在的目的，因为this并不是代理对象。
			 */
			// ret = method.invoke(proxy,args);
		} catch (InvocationTargetException e) {
			aspect.afterException(target,method,args,e.getTargetException());
		}
		aspect.after(target,method,args,ret);
		
		return ret;
	}
}
