package cn.willbj.brief.hootl.concuurent.util;

/**
 * 
 两个线程交替打印0~100的奇偶数：
偶线程：0
奇线程：1
偶线程：2
奇线程：3
```
 *
 */
public class Demo02 {
	static class SoulutionTask implements Runnable{
        static int value = 0;
        @Override
        public void run() {
            while (value <= 100){
                synchronized (SoulutionTask.class){
                	SoulutionTask.class.notify();
                    System.out.println(Thread.currentThread().getName() + ":" + value++);
                    try {
                        SoulutionTask.class.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    public static void main(String[] args) {
    	// System.out.println(new String("abc") == new String("abc"));
    	new Thread(new SoulutionTask(), "偶数").start();
    	new Thread(new SoulutionTask(), "奇数").start();
        
    }
}
