/**
 *
 * @FunctionalInterface 注解的使用
 *  1.必须注解在接口上
 *  2.被注解的接口有且只有一个抽象方法
 *  3.被注解的接口可以有默认方法/静态方法，或者重写Object的方法
 *
 * @date 2020/11/14
 */
package cn.willbj.brief.rt.jdk8.stream.functionalInterface;