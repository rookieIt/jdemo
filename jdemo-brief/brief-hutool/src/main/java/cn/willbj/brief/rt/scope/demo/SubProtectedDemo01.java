package cn.willbj.brief.rt.scope.demo;

import cn.willbj.brief.rt.scope.demo01.protectedDemo01;

/**
 * 作用域	本类	本包范围内	子孙类（只代表其他包中的子孙类）	其他包
 * public   可以     可以	        可以	                     可以
 * protected 可以	 可以	        可以	                    不可以
 * default	可以	 可以	        不可以	                    不可以
 * private	可以	 不可以	        不可以	                    不可以
 */
public class SubProtectedDemo01 extends protectedDemo01 {
    public static void main(String[] args) {
        SubProtectedDemo01 subProtectedDemo01 = new SubProtectedDemo01();
        subProtectedDemo01.test01();//跨包，子孙类可以调用
    }

    /**
     * 跨包，子孙类可以重写test01(),并且作用域可以提高
     * @see protectedDemo01#test01()
     */
    @Override
    public void test01() {
        System.out.println("子包");
        super.test01();
    }


    /**
     * 跨包，子孙类可以重写test01()
     * @see protectedDemo01#test01()
     */
    /*@Override
    protected void test01() {
        System.out.println("子包");
        super.test01();
    }*/
}
