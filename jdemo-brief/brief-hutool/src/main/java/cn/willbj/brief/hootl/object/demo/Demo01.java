package cn.willbj.brief.hootl.object.demo;

/**
 * 验证 == 比较的是hashcode值 还是 对象的具体引用地址
 * 
 * 		答: 比较的是对象的具体引用地址
 * 
 * @see Object#hashCode()
 */
public class Demo01 {
	public static void main(String[] args) {
		
		long bitLen = 4l;
		System.out.println(-1l ^ (-1l << bitLen));
		
		System.out.println("*********************");
		
		long cuurTime = 1596680146591L;
		
		String hexString = Long.toHexString(cuurTime);
		System.out.println("16进制: " + hexString);
		String binaryString = Long.toBinaryString(cuurTime);
		System.out.println("2进制: " + binaryString);// 41位
		String octalString = Long.toOctalString(cuurTime);
		System.out.println("8进制: " + octalString);
		
		
		System.out.println("*********************");
		Demo01.test01();
		System.out.println("*********************");
		Demo01.test02();
		System.out.println("*********************");
		System.out.println("null == null: "+(null == null));
	}
	
	public static void test01() {
		Dog zz = new Dog("zz");
		Dog yy = new Dog("yy");
		System.out.println(zz);
		System.out.println("toString(): "+ zz.toString());
		System.out.println(yy);
	}
	
	/**
	 * == 比较的是具体地址的应用,不是hashcode
	 */
	public static void test02() {
		Dog02 zz = new Dog02("zz");
		Dog02 yy = new Dog02("yy");
		System.out.println(zz);
		System.out.println("toString(): "+ zz.toString());
		System.out.println(yy);
		
		System.out.println("zz == yy?: "+(zz == yy));
		/*
		 * [@link Object#equals]
		 */
		System.out.println("zz.equals(yy)?: "+(zz.equals(yy)));
	}
}
