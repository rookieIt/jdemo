package cn.willbj.brief.hootl.aop.demo;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * 
 *
 */
public class Demo01 {
	public static void main(String[] args) {
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(Man.class);
		enhancer.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
				// Object invoke = proxy.invoke(obj, args); // java.lang.StackOverflowError
				Object invoke = proxy.invokeSuper(obj, args);
				invoke +="xx";
				return invoke;
			}
		});
		
		Man create = (Man) enhancer.create();
		/*Class[] argumentTypes = new Class[] {String.class};
		Object[] arguments = new Object[] {"yy"};
		Man create = (Man) enhancer.create(argumentTypes, arguments);*/
		create.show();
		System.out.println(create.print("zz"));
	}
}
