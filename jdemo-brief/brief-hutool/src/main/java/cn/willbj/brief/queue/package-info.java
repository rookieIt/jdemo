/**
 * 来源:https://mp.weixin.qq.com/s/zrGlx4AQaYAp6jFhuXIqow
 * @see java.util.Queue
 * 添加
 * @see java.util.Queue#add(java.lang.Object) 添加
 * @see java.util.Queue#offer(java.lang.Object) 提供|添加，业务侧重点
 * 移除对头并返回
 * @see java.util.Queue#remove()
 * @see java.util.Queue#poll()
 * 查看对头
 * @see java.util.Queue#element()
 * @see java.util.Queue#peek()
 *
 *
 * 队列划分
 * 阻塞队列
 * @see java.util.concurrent.ArrayBlockingQueue
 * @see java.util.concurrent.LinkedBlockingQueue
 * @see java.util.concurrent.BlockingQueue 接口
 * @see java.util.concurrent.BlockingDeque 接口
 *
 * 双端队列:指队列的头部和尾部都可以同时入队和出队的数据结构
 * @see java.util.ArrayDeque 非阻塞+双端
 * @see java.util.concurrent.LinkedBlockingDeque 阻塞+双端
 *
 * 优先队列:优先级高的元素先出队。(优先队列是根据二叉堆实现的;二叉堆分为两种类型：一种是最大堆一种是最小堆。以上展示的是最大堆，在最大堆中，任意一个父节点的值都大于等于它左右子节点的值。)
 * @see java.util.PriorityQueue
 * @see java.util.concurrent.PriorityBlockingQueue
 *
 * 延迟队列（DelayQueue）: 是基于优先队列 PriorityQueue 实现的，它可以看作是一种以时间为度量单位的优先的队列，当入队的元素到达指定的延迟时间之后方可出队。
 * @see java.util.concurrent.DelayQueue
 *
 * 其他队列
 * 在 Java 的队列中有一个比较特殊的队列 SynchronousQueue，
 * 它的特别之处在于它内部没有容器，每次进行 put() 数据后（添加数据），必须等待另一个线程拿走数据后才可以再次添加数据，它的使用示例如下
 * @see java.util.concurrent.SynchronousQueue
 * @see java.util.concurrent.ConcurrentLinkedQueue 通过cas保证，多线程安全
 *
 * ############# Disruptor 无锁队列
 * <a href="https://blog.csdn.net/tutouchengxuyua/article/details/113283307"></>
 *
 *
 * 普通队列中的常用方法有以下这些：
 *
 * offer()：添加元素，如果【队列已满，直接返回false】；
 * put()：添加元素，如果【队列已满，阻塞等待插入】；
 * add()：添加元素，如果【队列已满，抛出 IllegalStateException 异常】；
 *
 * poll()：删除并返回队头元素，【当队列为空，返回 null】；
 * take()：删除并返回队头元素，【当队列为空，则会阻塞等待】；
 * remove()：删除并返回队头元素【当队列为空，抛出 NoSuchElementException 异常】；
 *
 * element()：查询队头元素，但不会进行删除【当队列为空，抛出 NoSuchElementException 异常】；
 * peek()：查询队头元素，但不会进行删除【当队列为空，返回 null】；
 *
 * 注意：一般情况下 offer() 和 poll() 方法配合使用，
 *      put() 和 take() 阻塞方法配合使用，
 *      add() 和 remove() 方法会配合使用，
 *      程序中常用的是 offer() 和 poll() 方法，因此这两个方法比较友好，不会报错。
 *
 */
package cn.willbj.brief.queue;