package cn.willbj.brief.hootl.extend.demo;


public class Demo01 extends cn.willbj.brief.hootl.extend.demo.Demo02{

	@Override
	void executeTest() {
		System.out.println("xxx");
	}

	public static void main(String[] args) {
		Demo03 demo03 = new Demo01();
		demo03.execute();
		
		Demo03 demo03Vo = new Demo02() {
			@Override
			void executeTest() {
				System.out.println("yyy");
			}
		};
		demo03Vo.execute();
	}
}
