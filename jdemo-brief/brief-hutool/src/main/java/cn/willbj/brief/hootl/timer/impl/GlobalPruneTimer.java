/*
package cn.willbj.brief.hootl.timer.impl;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;

public enum GlobalPruneTimer {
	
	*/
/**
	 * 单例模式(编译在反编译后，可看见其基于class继承枚举类)
	 *//*

	INSTANCE;
	
	*/
/**
	 * 定时器
	 *//*

	private ScheduledExecutorService pruneTimer;
	
	*/
/**
	 * 缓存任务计数
	 *//*

	private final AtomicInteger cacheTaskNumber = new AtomicInteger(1);
	
	private GlobalPruneTimer() {
		create();
	}
	
	*/
/**
	 * 创建定时器
	 *//*

	public void create() {
		if (null != pruneTimer) {
			shutdownNow();
		}
		this.pruneTimer = new ScheduledThreadPoolExecutor(16, r -> ThreadUtil.newThread(r, StrUtil.format("Pure-Timer-{}", cacheTaskNumber.getAndIncrement())));
	}

	*/
/**
	 * 销毁全局定时器
	 *//*

	public void shutdown() {
		if (null != pruneTimer) {
			pruneTimer.shutdown();
		}
	}

	*/
/**
	 * 销毁全局定时器
	 *
	 * @return 销毁时未被执行的任务列表
	 *//*

	public List<Runnable> shutdownNow() {
		if (null != pruneTimer) {
			return pruneTimer.shutdownNow();
		}
		return null;
	}
	
	*/
/**
	 * 启动定时任务
	 *
	 * @param task  任务
	 * @param delay 周期
	 * @return {@link ScheduledFuture}对象，可手动取消此任务
	 *//*

	public ScheduledFuture<?> schedule(Runnable task, long delay) {
		return this.pruneTimer.scheduleAtFixedRate(task, delay, delay, TimeUnit.MILLISECONDS);
	}
}
*/
