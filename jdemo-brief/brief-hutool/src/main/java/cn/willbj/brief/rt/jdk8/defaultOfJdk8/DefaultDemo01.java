package cn.willbj.brief.rt.jdk8.defaultOfJdk8;

/**
 * java8新特性：接口默认方法
 *
 * @date 2020/11/14
 */
public interface DefaultDemo01 {

    String getStr();

    abstract void testAbs();

    static void testStatic(){
        System.out.println("testStatic");
    }

    default void test01(){
        System.out.println("test01");
    }

    default void test02(){
        System.out.println("test02");
    }

}

class Test implements DefaultDemo01{

    public static void main(String[] args) {
        DefaultDemo01.testStatic();
        DefaultDemo01 demo01 = new Test();
        demo01.test01();
        demo01.test02();
        demo01.getStr();

    }

    @Override
    public String getStr() {
        return null;
    }

    @Override
    public void testAbs() {
        System.out.println("Test testAbs");
    }

    @Override
    public void test01() {
        System.out.println("Test test01");
    }
}