package cn.willbj.brief.SPI;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * SPI的样例
 */
public class Demo {
    public static void main(String[] args) {
        test01();
    }

    public static void test01(){
        ServiceLoader<IUploadDemo> uploadCDN = ServiceLoader.load(IUploadDemo.class);
        Iterator<IUploadDemo> iterator = uploadCDN.iterator();
        while(iterator.hasNext()){
            iterator.next();
        }
        /*
         * 输出:
         * upload to Demo01 cdn
         * upload to Demo02 cdn
         */
        for (IUploadDemo u : uploadCDN) {
            u.upload("filePath");
        }
    }
}
