package cn.willbj.brief.hootl.lock.demo;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * 死锁--thread dump
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/10/20
 */
public class DeadLock01 {

    public static void main(String[] args) {
        Thread thread01 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (DeadLock01.Dead01.class){
                    try {
                        System.out.println("线程1获取01");
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (DeadLock01.Dead02.class){
                        System.out.println("线程1获取02");
                    }

                }
            }
        });

        Thread thread02 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (DeadLock01.Dead02.class){
                    try {
                        System.out.println("线程2获取02");
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (DeadLock01.Dead01.class){
                        System.out.println("线程2获取01");
                    }
                }
            }
        });

        thread01.start();
        thread02.start();

    }
    class  Dead01{

    }
    class  Dead02{

    }
}
