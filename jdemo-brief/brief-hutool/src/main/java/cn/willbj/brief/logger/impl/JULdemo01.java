package cn.willbj.brief.logger.impl;

import org.slf4j.LoggerFactory;

import java.util.logging.Logger;

/**
 * java 原生日志
 */
public class JULdemo01 {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger("JULdemo01");
        logger.info("xxxxxx");

        org.slf4j.Logger logger1 = LoggerFactory.getLogger(JULdemo01.class );
        logger1.info("yyyyyy");
    }
}
