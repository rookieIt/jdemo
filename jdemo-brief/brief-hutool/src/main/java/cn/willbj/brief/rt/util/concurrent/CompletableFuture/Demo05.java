package cn.willbj.brief.rt.util.concurrent.CompletableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 【对象方法】
 *
 * thenAccept 消费处理结果
 * 接收任务的处理结果，并消费处理，无返回结果。
 *
 * 链接：https://www.jianshu.com/p/6bac52527ca4
 * @date 2020/12/1
 */
public class Demo05 {

    private static final ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);

    public static void main(String[] args) throws Exception {
        Demo05.test01();
    }


    /**
     * 执行顺序:
     * pool-1-thread-1 : num1
     * 获取上个任务的返回值: return_value
     *
     */
    public static void test01()throws Exception{
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(2000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : num1");
            return "return_value";
        }, es);

        future.thenAccept((x)->{
            System.out.println("获取上个任务的返回值: "+x);
        });
    }

}
