package cn.willbj.brief.spring.demo;

import java.util.Arrays;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @see org.springframework.beans.factory.BeanFactory
 * 		是Spring bean容器的根接口.提供获取bean,是否包含bean,是否单例与原型,获取bean类型,bean 别名的api.
 * 
 * @see org.springframework.beans.factory.HierarchicalBeanFactory#getParentBeanFactory()
 * 		spring支持父子容器:提供父容器的访问功能
 * 
 * @see org.springframework.beans.factory.config.AutowireCapableBeanFactory
 * 		定义了5种装配策略：集成其他框架功能
			不自动注入：AUTOWIRE_NO
			使用BeanName策略注入：AUTOWIRE_BY_NAME
			使用类型装配策略：AUTOWIRE_BY_TYPE
			使用构造器装配策略：AUTOWIRE_CONSTRUCTOR
			自动装配策略：AUTOWIRE_AUTODETECT
	
 * @see org.springframework.beans.factory.ListableBeanFactory
 * 		提供容器内bean实例的枚举功能.这边不会考虑父容器内的实例.
 */
public class Demo02 {
	
	public static void main(String[] args) {
		/* 父子容器示例 */
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans2.xml");
		ClassPathXmlApplicationContext applicationContext02 = 
					new ClassPathXmlApplicationContext(new String[] {"beans.xml"}, applicationContext);
		
		BeanFactory parentBeanFactory = applicationContext02.getParentBeanFactory();
		
		System.out.println(applicationContext.containsBean("C"));//false
		System.out.println(applicationContext.containsBeanDefinition("C"));//false
		System.out.println(applicationContext.getBeanDefinitionCount());//2
		System.out.println(applicationContext02.getBeanDefinitionCount());//3
		System.out.println(Arrays.toString(applicationContext02.getBeanDefinitionNames()));// [A, B, C]
		System.out.println(Arrays.toString(applicationContext.getBeanNamesForType(C.class)));//[]
		System.out.println(Arrays.toString(applicationContext02.getBeanNamesForType(C.class)));//[C]
		
		System.out.println("*****************************************************************");
		
		System.out.println(applicationContext.getBean("A"));//cn.willbj.brief.spring.demo.A@10bdf5e5
		System.out.println(applicationContext02.getBean("A"));//cn.willbj.brief.spring.demo.A@6e1ec318
		System.out.println(parentBeanFactory.getBean("A"));//cn.willbj.brief.spring.demo.A@10bdf5e5
	}
}
