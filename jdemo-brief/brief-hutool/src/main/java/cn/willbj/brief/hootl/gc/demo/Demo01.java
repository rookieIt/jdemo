package cn.willbj.brief.hootl.gc.demo;

import java.util.Properties;

/**
 * -XX:+PrintGC
 * -XX:+PrintGCDetails
 *
 */
public class Demo01 {
	public static void main(String[] args) {
		System.out.println("******************程序参数******************");
		for (String sysArg : args) {
			System.out.println(sysArg);
		}
		System.out.println("******************虚拟机参数******************");
		
		Properties properties = System.getProperties();
		System.out.println(properties.getProperty("test.boot.library.path"));
		
		System.out.println("************************************");
		/*for (int i = 0; i < 10000000; i++) {
			A a = new Demo01().new A();
			System.out.println(a);
		}*/
		/*while(true){
			int[] arr = new int[1024];
		}*/
	}
	
	class A{
		
	}
}
