package cn.willbj.brief.spring.demo;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Demo01 {
	public static void main(String[] args) {
		XmlBeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("beans.xml"));
		A A = (cn.willbj.brief.spring.demo.A) beanFactory.getBean("A");
		B b = A.getB();
		System.out.println(b);
		
		C C = (cn.willbj.brief.spring.demo.C) beanFactory.getBean("C");
		C.test01();
		
		B B = (cn.willbj.brief.spring.demo.B) beanFactory.getBean("B");
	}
	
	
}
