package cn.willbj.brief.hootl.classloader.demo;

/**
 * 
 * 打印结果: 
 *     sun.misc.Launcher$AppClassLoader@6d06d69c
 * 	   sun.misc.Launcher$ExtClassLoader@70dea4e
 * 	   null
 * @author will
 */
public class Demo01 {
	public static void main(String[] args) {
		ClassLoader classLoader = Demo01.class.getClassLoader();
		Demo01.getParent(classLoader);//递归打印
	}
	
	public static void getParent(ClassLoader classLoader) {
		System.out.println(classLoader);
		if (classLoader== null) {
			return;
		}
		getParent(classLoader.getParent());
	}
}
