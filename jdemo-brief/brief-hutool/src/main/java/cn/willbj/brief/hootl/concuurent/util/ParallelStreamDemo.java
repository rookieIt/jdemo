package cn.willbj.brief.hootl.concuurent.util;

import java.util.ArrayList;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Stream;

public class ParallelStreamDemo {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }
        
        Stream<Integer> parallelStream = list.parallelStream();
        
        LongAdder sum = new LongAdder();
        parallelStream.forEach(temp -> {
        	sum.add(temp);
        });
        
        System.out.println(sum);
	}
}
