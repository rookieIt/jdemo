package cn.willbj.brief.rt.jdk8.stream.functionalInterface;

/**
 *
 * @description
 * @date 2020/11/14
 */
@FunctionalInterface
public interface FunctionalDemo01<T> {
    T get();
}

class Car {
    public static Car create(final FunctionalDemo01<Car> supplier) {
        return supplier.get();
    }

    public static void collide(final Car car) {
        System.out.println("Collided " + car.toString());
    }

    public void follow(final Car another) {
        System.out.println("Following the " + another.toString());
    }

    public void repair() {
        System.out.println("Repaired " + this.toString());
    }

    public static void main(String[] args) {
        Car car = Car.create(Car::new);

    }
}
