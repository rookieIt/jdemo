package cn.willbj.brief.hootl.aop.interceptor;

import cn.willbj.brief.hootl.aop.aspects.Aspect;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 *
 * @description
 * @date 2020/11/23
 */
public class CglibInterceptor implements MethodInterceptor {

    Object object = null;
    Aspect aspect = null;

    public CglibInterceptor(Object object, Aspect aspect){
        this.object = object;
        this.aspect = aspect;
    }

    @Override
    public Object intercept(Object prox, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("**************拦截中*******************");
        // return proxy.invoke(object,args);// 调用invoke方法，需要具体的对象，不可以首映代理对象obj
        return proxy.invokeSuper(prox,args);
    }
}
