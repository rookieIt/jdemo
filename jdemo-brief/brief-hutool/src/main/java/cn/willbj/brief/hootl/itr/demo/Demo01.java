package cn.willbj.brief.hootl.itr.demo;

import java.io.Serializable;
import java.util.Iterator;

public class Demo01 {
	
	public static void main(String[] args) {
		Students students = new Demo01(). new Students();
		
		Student[] temp = new Student[10];
		for (int i = 0; i < 10; i++) {
			temp[i] = new Demo01().new Student(String.valueOf(i), "学生" + String.valueOf(i));
        }
		students.setStudents(temp);
		
        for (Student student : students) {
            System.out.println(student.getNo() + ":" + student.getName());
        }
	}
	
	class Students implements Iterable<Student>{
		
		// 存储所有学生类的数组
	    private Student[] students;

		public Student[] getStudents() {
			return students;
		}
		public void setStudents(Student[] students) {
			this.students = students;
		}

		@Override
		public Iterator<Student> iterator() {
			return new StudentIterator();
		}
		
		private class StudentIterator implements Iterator<Student>{

			 // 当前迭代元素的下标
	        private int index = 0;

	        // 判断是否还有下一个元素，如果迭代到最后一个元素就返回false
	        public boolean hasNext() {
	            return index != students.length;
	        }

	        @Override
	        public Student next() {
	            return students[index++];
	        }

	        // 这里不支持，抛出不支持操作异常
	        public void remove() {
	            throw new UnsupportedOperationException();
	        }
		}
	}
	
	class Student implements Serializable{
		private static final long serialVersionUID = 1L;
		private String no;
		private String name;
		
		
		public Student(String no, String name) {
			super();
			this.no = no;
			this.name = name;
		}
		public String getNo() {
			return no;
		}
		public void setNo(String no) {
			this.no = no;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
	}
}
