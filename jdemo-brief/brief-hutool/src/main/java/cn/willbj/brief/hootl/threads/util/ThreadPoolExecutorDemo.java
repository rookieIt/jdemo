/*
package cn.willbj.brief.hootl.threads.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.thread.ThreadUtil;

*/
/**
 * @see java.util.concurrent.Executors
 *
 *//*

public class ThreadPoolExecutorDemo {
	public static void main(String[] args) {
		ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 10, 1l, TimeUnit.SECONDS,
				new ArrayBlockingQueue<>(100),new ThreadPoolExecutor.CallerRunsPolicy());
		
		for (int i = 0; i < 10; i++) {
            //创建WorkerThread对象（WorkerThread类实现了Runnable 接口）
			Runnable worker = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					ThreadUtil.sleep(500);
				}
			};
            //执行Runnable
            executor.execute(worker);
        }
		
		
		//终止线程池
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
	}

}
*/
