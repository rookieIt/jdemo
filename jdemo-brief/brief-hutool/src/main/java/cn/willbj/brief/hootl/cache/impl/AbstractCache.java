/*
package cn.willbj.brief.hootl.cache.impl;

import java.util.Map;
import java.util.concurrent.locks.StampedLock;

import cn.hutool.core.lang.func.Func0;
import cn.willbj.brief.hootl.cache.itf.Cache;

public abstract class AbstractCache<K, V> implements Cache<K, V>{
	
	private static final long serialVersionUID = 1L;
	
	protected Map<K, CacheObj<K, V>> cacheMap;
	
	*/
/**
	 * 返回缓存容量，<code>0</code>表示无大小限制
	 *//*

	protected int capacity;
	*/
/**
	 * 缓存失效时长， <code>0</code> 表示无限制，单位毫秒
	 *//*

	protected long timeout;
	
	*/
/**
	 * 每个对象是否有单独的失效时长，用于决定清理过期对象是否有必要。
	 *//*

	protected boolean existCustomTimeout;
	
	private final StampedLock lock = new StampedLock();
	
	public void put(K key, V obj, long timeout) {
		final long stamp = lock.writeLock();
		try {
			this.putWithOutLock(key, obj, timeout);
		} finally {
			lock.unlockWrite(stamp);
		}
		
	}
	private void putWithOutLock(K key, V obj, long timeout) {
		final CacheObj<K, V> value = new CacheObj<K, V>(key, obj, timeout);
		if (timeout != 0l) {
			existCustomTimeout = true;
		}
		if (isFull()) {
			pruneCache();
		}else {
			cacheMap.put(key, value);
		}
	}
	
	public boolean isFull() {
		return (capacity > 0) && (cacheMap.size() >= capacity);
	}
	
	*/
/**
	 * 精简
	 *//*

	public final int prune() {
		return pruneCache();
	}
	*/
/**
	 * 精简规则自定义
	 * @return
	 *//*

	protected abstract int pruneCache();
	

	public void put(K key, V object) {
		put(key, object, timeout);
	}
	
	public boolean containsKey(K key) {
		long stamp = lock.readLock();
		try {
			final CacheObj<K, V> cacheObj = cacheMap.get(key);
			if (cacheObj == null) {
				return false;
			}
			if(!cacheObj.isExpire()) {
				return true; // 命中
			}
		} finally {
			lock.unlockRead(stamp);
		}
		// 过期移除
		remove(key);
		return false;
	}
	
	*/
/**
	 * 移除操作
	 *//*

	public void remove(K key) {
		long stamp = lock.writeLock();
		try {
			this.removeWithOutLock(key);
		}finally {
			lock.unlockWrite(stamp);
		}
	}
	private CacheObj<K, V> removeWithOutLock(K key) {
		CacheObj<K, V> co = cacheMap.remove(key);
		return co;
	}
	
	public int size() {
		return cacheMap.size();
	}

	
	public boolean isEmpty() {
		return cacheMap.isEmpty();
	}
	
	public V get(K key) {
		return get(key, true);
	}
	
	public V get(K key, Func0<V> supplier) {
		return null;
	}
	
	@Override
	public V get(K key, boolean isUptLastAccess) {
		long stamp = lock.readLock();
		try {
			return this.getWithOutLock(key, isUptLastAccess);
		} finally {
			lock.unlockRead(stamp);
		}
	}
	private V getWithOutLock(K key, boolean isUptLastAccess) {
		final CacheObj<K, V> co = cacheMap.get(key);
		if (co == null) {
			return null;
		}
		if (co.isExpire()) {
			return null;
		}
		return co.get(isUptLastAccess);
	}
}

*/
