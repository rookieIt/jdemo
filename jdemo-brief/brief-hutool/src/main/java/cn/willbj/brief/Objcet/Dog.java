package cn.willbj.brief.Objcet;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/11/4
 */
public class Dog extends Animal {
    private  String jump;

    public String getJump() {
        return jump;
    }

    public void setJump(String jump) {
        this.jump = jump;
    }
}
