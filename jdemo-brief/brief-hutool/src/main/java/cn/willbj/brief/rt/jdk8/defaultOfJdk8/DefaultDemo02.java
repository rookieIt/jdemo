package cn.willbj.brief.rt.jdk8.defaultOfJdk8;

/**
 * java8新特性：接口默认方法
 *
 * @date 2020/11/14
 */
public interface DefaultDemo02 {
    default void test01(){
        System.out.println(" DefaultDemo02 test01");
    }
}
interface DefaultDemo0201 {
    default void test01(){
        System.out.println(" DefaultDemo0201 test01");
    }
}

/*
class Test implements DefaultDemo02,DefaultDemo0201{

}*/
