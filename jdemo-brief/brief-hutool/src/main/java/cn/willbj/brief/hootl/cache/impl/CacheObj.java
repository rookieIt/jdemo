package cn.willbj.brief.hootl.cache.impl;

import java.io.Serializable;

public class CacheObj<K,V> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final K key;
	private final V value;
	/** 对象存活时长，0表示永久存活*/
	private final long timeout;
	
	/** 上次访问时间 */
	private long lastAccess; 
	
	public CacheObj(K key, V value, long timeout) {
		this.key = key;
		this.value = value;
		this.timeout = timeout;
		this.lastAccess = System.currentTimeMillis();
	}

	public boolean isExpire() {
		if (timeout==0) {
			return false;
		}
		return System.currentTimeMillis()-lastAccess > timeout ? true:false;
	}

	public V get(boolean isUptLastAccess) {
		if (isUptLastAccess) {
			lastAccess = System.currentTimeMillis();
		}
		return value;
	}
}
