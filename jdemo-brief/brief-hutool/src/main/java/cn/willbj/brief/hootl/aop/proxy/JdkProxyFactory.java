package cn.willbj.brief.hootl.aop.proxy;

import java.lang.reflect.Proxy;

import cn.willbj.brief.hootl.aop.aspects.Aspect;
import cn.willbj.brief.hootl.aop.interceptor.JdkInterceptor;

public class JdkProxyFactory {
	
	public <T> T proxy(T target, Aspect aspect) {
		Object newProxyInstance = Proxy.newProxyInstance(target.getClass().getClassLoader(), 
				target.getClass().getInterfaces(), new JdkInterceptor(target, aspect));
		return (T) newProxyInstance;
	}
	
}
