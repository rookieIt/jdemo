package cn.willbj.brief.hootl.reflect.demo;

import java.lang.reflect.Field;

/**
 * 
 * getField 是可以获取到父类子类的public字段
 * 
 * getDeclaredField 是可以获取本类的所有字段
 * 
 * setAccessible -- true时可以直接方位私有成员变量
 *	
 */
public class Demo01 {
	
	public static void main(String[] args) {
		Demo01.test01();
		//Demo01.test02();
		Demo01.test0201();
		//Demo01.test03();
		Demo01.test0301();
		
	}
	
	public static void test01(){
		try {
			
			// getField是可以获取到父类的共有字段的，而getDeclaredField只能获取本类所有字段
			// getDeclaredField是可以获取一个类的所有字段. getField只能获取类的public 字段.
			Class<?> forName = Class.forName("cn.willbj.brief.hootl.reflect.demo.Person");
			Field nameField = forName.getDeclaredField("name");
			// System.out.println(nameField.isAccessible());// false
			nameField.setAccessible(true);
			// System.out.println(nameField.isAccessible());// true
			
			Person person = new Person();
			person.setName("name");
			Object obj = nameField.get(person);
			System.out.println(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return java.lang.NoSuchFieldException: xx
	 */
	public static void test02(){
		try {
			
			// getField是可以获取到父类的共有字段的，而getDeclaredField只能获取本类所有字段
			// getDeclaredField是可以获取一个类的所有字段. getField只能获取类的public 字段.
			Class<?> forName = Class.forName("cn.willbj.brief.hootl.reflect.demo.Person");
			Field xx = forName.getDeclaredField("xx");
			System.out.println("父类[getDeclaredField]:"+xx);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return java.lang.NoSuchFieldException: xx
	 */
	public static void test0201(){
		try {
			
			// getField是可以获取到父类的共有字段的，而getDeclaredField只能获取本类所有字段
			// getDeclaredField是可以获取一个类的所有字段. getField只能获取类的public 字段.
			Class<?> forName = Class.forName("cn.willbj.brief.hootl.reflect.demo.Person");
			Field xx = forName.getField("xx");
			System.out.println("父类[getField private]:"+xx);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return java.lang.NoSuchFieldException: yy
	 */
	public static void test03(){
		try {
			
			// getField是可以获取到父类的共有字段的，而getDeclaredField只能获取本类所有字段
			// getDeclaredField是可以获取一个类的所有字段. getField只能获取类的public 字段.
			Class<?> forName = Class.forName("cn.willbj.brief.hootl.reflect.demo.Person");
			Field yy = forName.getDeclaredField("yy");
			System.out.println("父类[getDeclaredField]:"+yy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void test0301(){
		try {
			
			// getField是可以获取到父类的共有字段的，而getDeclaredField只能获取本类所有字段
			// getDeclaredField是可以获取一个类的所有字段. getField只能获取类的public 字段.
			Class<?> forName = Class.forName("cn.willbj.brief.hootl.reflect.demo.Person");
			Field yy = forName.getField("yy");
			System.out.println("父类[getField public]:"+yy);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
