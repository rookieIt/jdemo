package cn.willbj.brief.hootl.aop.demo;

import cn.willbj.brief.hootl.aop.aspects.SimpleAspect;
import cn.willbj.brief.hootl.aop.proxy.JdkProxyFactory;

public class Demo03 {
	public static void main(String[] args) {
		
		JdkProxyFactory jdkProxyFactory = new JdkProxyFactory();
		
		// 接收引用对象必须是父接口？
		Animal proxy = jdkProxyFactory.proxy(new Cat(), new SimpleAspect());
		proxy.print("正则");
		Long gasdetectionid = null;
		gasdetectionid = Long.parseLong("");
		System.out.println(gasdetectionid);

	}
}
