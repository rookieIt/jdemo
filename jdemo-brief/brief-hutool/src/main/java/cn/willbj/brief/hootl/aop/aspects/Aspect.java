package cn.willbj.brief.hootl.aop.aspects;

import java.lang.reflect.Method;

public interface Aspect {
	public boolean before(Object target, Method method, Object[] args);
	
	public boolean after(Object target, Method Method, Object[] args, Object ret);
	
	public boolean afterException(Object target, Method Method, Object[] args, Throwable throwable);
}
