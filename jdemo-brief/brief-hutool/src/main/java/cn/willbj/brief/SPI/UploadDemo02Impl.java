package cn.willbj.brief.SPI;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/11/5
 */
public class UploadDemo02Impl implements  IUploadDemo{
    @Override
    public void upload(String url) {
        System.out.println("upload to Demo02 cdn");
    }
}
