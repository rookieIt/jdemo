package cn.willbj.brief.hootl.singleten.demo;

/**
 * 
 * jad反编译代码
 * 
 * <p>
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EnumSignleten.java

package cn.willbj.brief.hootl.singleten.demo;

import java.io.PrintStream;

public final class EnumSignleten extends Enum
{

    private EnumSignleten(String s, int i)
    {
        super(s, i);
    }

    public void doSomething()
    {
        System.out.println("doSomething");
    }

    public static EnumSignleten[] values()
    {
        EnumSignleten aenumsignleten[];
        int i;
        EnumSignleten aenumsignleten1[];
        System.arraycopy(aenumsignleten = ENUM$VALUES, 0, aenumsignleten1 = new EnumSignleten[i = aenumsignleten.length], 0, i);
        return aenumsignleten1;
    }

    public static EnumSignleten valueOf(String s)
    {
        return (EnumSignleten)Enum.valueOf(cn/willbj/brief/hootl/singleten/demo/EnumSignleten, s);
    }

    public static final EnumSignleten INSTANCE;
    private static final EnumSignleten ENUM$VALUES[];

    static 
    {
        INSTANCE = new EnumSignleten("INSTANCE", 0);
        ENUM$VALUES = (new EnumSignleten[] {
            INSTANCE
        });
    }
}
 *
 */
public enum EnumSignleten {

	INSTANCE;

	public void doSomething() {
		System.out.println("doSomething");
	}
}
