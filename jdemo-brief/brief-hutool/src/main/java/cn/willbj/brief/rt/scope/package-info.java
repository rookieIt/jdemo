/**
 * 作用域	本类	本包范围内	子孙类（只代表其他包中的子孙类）	其他包
 * public   可以     可以	        可以	                     可以
 * protected 可以	 可以	        可以	                    不可以
 * default	可以	 可以	        不可以	                    不可以
 * private	可以	 不可以	        不可以	                    不可以
 */
package cn.willbj.brief.rt.scope;