package cn.willbj.brief.hootl.aop.cglib;

import cn.willbj.brief.hootl.aop.aspects.Aspect;
import cn.willbj.brief.hootl.aop.aspects.SimpleAspect;
import cn.willbj.brief.hootl.aop.demo.Man;
import cn.willbj.brief.hootl.aop.interceptor.CglibInterceptor;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class Demo {
    /**
     * 纯接口,开始增强...
     * 纯接口,完成增强...
     * hello,abc.
     */
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("纯接口,开始增强...");
                String rpcResult = "hello,"+objects[0];
                System.out.println("纯接口,完成增强...");
                String rpcWrapper = rpcResult + ".";
                return rpcWrapper;
            }
        });
        enhancer.setSuperclass(IamInterface.class);

        IamInterface iamInterface = (IamInterface) enhancer.create();
        String result = iamInterface.sayHello("abc");
        System.out.println(result);
    }
}
