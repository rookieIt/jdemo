package cn.willbj.brief.hootl.aop.aspects;

import java.lang.reflect.Method;

public class SimpleAspect implements Aspect{

	@Override
	public boolean before(Object target, Method method, Object[] args) {
		System.out.println(method.getName()+" before "+"************");
		return true;
	}

	@Override
	public boolean after(Object target, Method method, Object[] args, Object ret) {
		System.out.println(method.getName()+" after "+"************");
		return true;
	}

	@Override
	public boolean afterException(Object target, Method method, Object[] args, Throwable throwable) {
		System.out.println(method.getName()+" afterException "+"************");
		return true;
	}

}
