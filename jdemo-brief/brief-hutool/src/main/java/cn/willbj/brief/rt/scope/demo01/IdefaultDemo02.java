package cn.willbj.brief.rt.scope.demo01;

/**
 *
 */
public interface IdefaultDemo02 {
    void test01();

    /**
     * 私有方法接口不能编译
     */
    // private void test02();
}
