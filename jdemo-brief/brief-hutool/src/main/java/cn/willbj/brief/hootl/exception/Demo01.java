package cn.willbj.brief.hootl.exception;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version
 * @description
 * @date 2020/9/24
 */
public class Demo01 {
    public static void main(String[] args) {
        Demo01 demo01 = new Demo01();
        demo01.test01();// RuntimeException及其子类非检查异常
        try {
            demo01.test02();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private  void test01() throws RuntimeException{
        System.out.println("RuntimeException...");
    }
    private  void test02() throws Exception{
        System.out.println("RuntimeException...");
    }
}
