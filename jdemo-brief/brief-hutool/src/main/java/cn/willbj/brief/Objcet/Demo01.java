package cn.willbj.brief.Objcet;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/11/4
 */
public class Demo01 {
    public static void main(String[] args) {
        Demo01 demo01 = new Demo01();
        Animal animal = demo01.test01("dog");
        System.out.println(animal.getType());
        if (animal instanceof  Cat){
            Cat cat = (Cat) animal;
            System.out.println(cat.getRun());
        }
        if (animal instanceof  Dog){
            Dog dog = (Dog) animal;
            System.out.println(dog.getJump());
        }
    }

    public Animal test01(String type){
        if ("cat".equals(type)){
            Cat cat = new Cat();
            cat.setType(type);
            cat.setRun("cat run...");
            return cat;
        }else{
            Dog dog = new Dog();
            dog.setType(type);
            dog.setJump("dog jump...");
            return dog;
        }
    }
}
