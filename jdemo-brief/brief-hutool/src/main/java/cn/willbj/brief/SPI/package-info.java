/**
 * 1.什么是SPI
 *      SPI全称Service Provider Interface，是Java提供的一套用来被第三方实现或者扩展的接口，
 *      它可以用来启用框架扩展和替换组件。 SPI的作用就是为这些被扩展的API寻找服务实现。
 *
 * 2.SPI和API的使用场景
 *     API （Application Programming Interface）在大多数情况下，
 *     都是实现方制定接口并完成对接口的实现，调用方仅仅依赖接口调用，且无权选择不同实现。
 *     从使用人员上来说，API 直接被应用开发人员使用。
 *
 *     SPI （Service Provider Interface）是调用方来制定接口规范，提供给外部来实现，
 *     调用方在调用时则选择自己需要的外部实现。
 *     从使用人员上来说，SPI 被框架扩展人员使用。
 *
 */
package cn.willbj.brief.SPI;