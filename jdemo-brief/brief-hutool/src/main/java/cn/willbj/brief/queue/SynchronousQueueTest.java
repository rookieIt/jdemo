package cn.willbj.brief.queue;

import java.util.Date;
import java.util.concurrent.SynchronousQueue;

/**
 * 其他队列
 * 在 Java 的队列中有一个比较特殊的队列 SynchronousQueue，
 * 它的特别之处在于它内部没有容器，每次进行 put() 数据后（添加数据），必须等待另一个线程拿走数据后才可以再次添加数据
 */
public class SynchronousQueueTest {
    public static void main(String[] args) {
        SynchronousQueue queue = new SynchronousQueue();

        // 入队
        new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                try {
                    System.out.println(new Date() + "，元素入队");
                    queue.put("Data " + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        // 出队
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    System.out.println(new Date() + "，元素出队：" + queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
