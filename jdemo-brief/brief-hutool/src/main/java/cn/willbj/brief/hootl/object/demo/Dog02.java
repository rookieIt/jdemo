package cn.willbj.brief.hootl.object.demo;

public class Dog02 {
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Dog02(String name) {
		super();
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		return 1;
	}
	
}
