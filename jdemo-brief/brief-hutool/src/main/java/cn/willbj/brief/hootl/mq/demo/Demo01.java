package cn.willbj.brief.hootl.mq.demo;

public class Demo01 {
	public static void main(String[] args) {
		Demo01.consume();
	}
	
	public static void send(){
		try {
			MqClient.produce("SEND:will");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void consume(){
		try {
			String consume = MqClient.consume();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
