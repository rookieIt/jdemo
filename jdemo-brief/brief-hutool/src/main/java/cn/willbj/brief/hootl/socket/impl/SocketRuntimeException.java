package cn.willbj.brief.hootl.socket.impl;

public class SocketRuntimeException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public SocketRuntimeException(Throwable cause) {
		super(cause.getMessage(), cause);
	}
	
	public SocketRuntimeException(String message) {
		super(message);
	}

}
