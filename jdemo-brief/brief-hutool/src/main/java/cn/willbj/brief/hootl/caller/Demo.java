package cn.willbj.brief.hootl.caller;

/**
 * StackTraceElement的用法
 * <p>
 * 通过StackTrace方式获取调用者。此方式效率最低，不推荐使用
 */
public class Demo {
	
    public static void main(String[] args) {
        new TestM().outerMethod();
    }
    
    public void methodA(){
        methodB();
    }
    
    public void methodB(){
        methodC();
    }

    /**
     * -------getStackTrace : java.lang.Thread.getStackTrace(Thread.java:1559)
     * -------methodC : cn.willbj.brief.hootl.caller.Demo.methodC(Demo.java:23)
     * -------methodB : cn.willbj.brief.hootl.caller.Demo.methodB(Demo.java:19)
     * -------methodA : cn.willbj.brief.hootl.caller.Demo.methodA(Demo.java:15)
     * -------outerMethod : cn.willbj.brief.hootl.caller.TestM.outerMethod(Demo.java:33)
     * -------main : cn.willbj.brief.hootl.caller.Demo.main(Demo.java:11)
     */
    public void methodC(){
        StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
        for(StackTraceElement s: stacks){
            System.out.println("-------"+s.getMethodName()+" : "+s);
        }
    }
}


class TestM {
    public void outerMethod(){
        new Demo().methodA();
    }
}