package cn.willbj.brief.hootl.aop.demo;

import cn.willbj.brief.hootl.aop.aspects.Aspect;
import cn.willbj.brief.hootl.aop.aspects.SimpleAspect;
import cn.willbj.brief.hootl.aop.interceptor.CglibInterceptor;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 *
 * @description
 * @date 2020/11/23
 */
public class CglibDemo {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        Aspect aspect = new SimpleAspect();
        enhancer.setCallback(new CglibInterceptor(new Man(),aspect));
        enhancer.setSuperclass(Man.class);
        Man o = (Man) enhancer.create();
        o.print("abc");

        Man man = new Man();
        man.print("xyz");
    }
}
