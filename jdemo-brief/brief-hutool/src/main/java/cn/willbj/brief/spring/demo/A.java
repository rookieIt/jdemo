package cn.willbj.brief.spring.demo;

public class A {
	private B b;
	
	public B getB() {
		return b;
	}
	public void setB(B b) {
		this.b = b;
	}
	
	private void initA(){
		System.out.println("A init ...");
	}
	
	private void destroyA(){
		System.out.println("A destroy ...");
	}
	
	/**
	 * 不配置，通过代码装配
	 */
	private C c;

	public C getC() {
		return c;
	}
	public void setC(C c) {
		System.out.println("A中通过代码注册c...");
		this.c = c;
	}
	
}
