package cn.willbj.brief.rt.scope.demo;

import cn.willbj.brief.rt.scope.demo01.IdefaultDemo02;

/**
 * 作用域	本类	本包范围内	子孙类（只代表其他包中的子孙类）	其他包
 * public   可以     可以	        可以	                     可以
 * protected 可以	 可以	        可以	                    不可以
 * default	可以	 可以	        不可以	                    不可以
 * private	可以	 不可以	        不可以	                    不可以
 */

/**
 * 对接口没有上述限制
 */
public class SubDefaultDemo02 implements IdefaultDemo02 {

    @Override
    public void test01() {

    }
}
