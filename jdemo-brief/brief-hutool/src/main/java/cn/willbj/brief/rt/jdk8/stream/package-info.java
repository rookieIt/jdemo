/**
 * java8新特性
 *      Java函数式接口（Function Predicate UnaryOperator BinaryOperator Supplier Consumer）
 *          @FunctionalInterface 注解标注
 *      @Link https://blog.csdn.net/gentelmantsao/article/details/106918416
 *
 * @author zhangliuwei
 * @date 2020/11/14
 */
package cn.willbj.brief.rt.jdk8.stream;