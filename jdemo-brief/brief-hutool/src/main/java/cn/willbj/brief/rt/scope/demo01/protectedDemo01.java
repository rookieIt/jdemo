package cn.willbj.brief.rt.scope.demo01;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/11/7
 */
public class protectedDemo01 {

    protected void  test01(){
        System.out.println("cn.willbj.brief.hootl.rt.scope.demo01.protectedDemo01.test01");
    }
}
