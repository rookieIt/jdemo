package cn.willbj.brief.rt.jdk8.stream;

import java.util.stream.Stream;

/**
 *
 * @date 2020/11/14
 */
public class StramDemo01 {

    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        integerStream.forEach(System.out::println);

        System.out.println("*****************************************************");

        // stream只能使用一次
        /*Stream<Integer> integerStream1 = integerStream.filter(x -> x > 2);
        // 报错: stream has already been operated upon or closed
        integerStream1.forEach(System.out::println);*/
        Stream<Integer> integerStream1 = Stream.of(1, 2, 3, 4, 5);
        Stream<Integer> integerStream2 = integerStream1.filter(x -> x > 2);
        integerStream2.forEach(System.out::println);

        System.out.println("*****************************************************");

        Stream<String> build = Stream.<String>builder().add("a").add("b").add("c").build();

        System.out.println("*****************************************************");

        Stream<String> build2 = Stream.<String>builder().add("a").add("b").add("c").build();
    }
}
