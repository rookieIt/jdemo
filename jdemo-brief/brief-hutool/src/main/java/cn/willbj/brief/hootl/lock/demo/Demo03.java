package cn.willbj.brief.hootl.lock.demo;

public class Demo03 implements Runnable {
	private int tickets = 200; // 定义火车票
	private Demo01 lock = new Demo01();// 自定义锁

	@Override
	public void run() {
		while (true) {
			try {
				// 加锁
				lock.lock();
				// ***************业务处理******************
				if (tickets > 0) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName() + "正在出售第" + (tickets--) + "张票");
				} else {
					break;
				}
				// *****************************************
			} finally {
				// 释放锁
				lock.unlock();
			}
		}
	}

	public static void main(String[] args) {
		Demo03 demo03 = new Demo03();
		new Thread(demo03).start();
		new Thread(demo03).start();
		new Thread(demo03).start();
	}

}
