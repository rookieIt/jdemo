package cn.willbj.brief.hootl.socket.demo;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Demo02 {
	
	public static void main(String[] args) {
		
		try {
			ServerSocket ss = new ServerSocket(3306);
			while (true) {
				Socket accept = ss.accept();
				// 当第一个客户端连接进来过后就会执行这句话,
		        System.out.println("accept方法通过了....");
		        
		        // 这个程序如果没有第二个客户端连进,那以下的代码不会执行到的.
		        Socket socket2 = ss.accept();
		        System.out.println("accept2方法通过了....");
				
				/**
				 * 如果要创建多客端的服务器,就要多次调用accept()方法,同要用多线程来做
				 * ,Socket连接里面的输入输出流也是阻塞的
				 */
				/*new Thread(new Runnable() {
					@Override
					public void run() {
						PrintStream pstream;
						System.out.println("Connected");
						try {
							pstream = new PrintStream (accept.getOutputStream());
							for (int i = 100; i >= 0; i--) {
					            pstream.println(i + " bottles of beer on the wall");
					         }
					         pstream.close();
					         accept.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}).start();*/
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	

}
