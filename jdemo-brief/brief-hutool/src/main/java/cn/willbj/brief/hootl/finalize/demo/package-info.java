/**
 * finalize()方法是Object类中提供的一个方法，在GC准备释放对象所占用的内存空间之前，它将首先调用finalize()方法
 * 
 * 9. 尽量不要使用finalize方法
	实际上，将资源清理放在finalize方法中完成是非常不好的选择，由于GC的工作量很大，尤其是回收Young代内存时，大都会引起应用程序暂停，
	所以再选择使用finalize方法进行资源清理，会导致GC负担更大，程序运行效率更差。
 */
package cn.willbj.brief.hootl.finalize.demo;