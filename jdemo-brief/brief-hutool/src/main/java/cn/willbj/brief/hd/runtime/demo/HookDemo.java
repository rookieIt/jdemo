package cn.willbj.brief.hd.runtime.demo;

/**
 * 使用场景：
 	* 程序正常退出
    * 使用System.exit()
    * 终端使用Ctrl+C触发的中断
    * 系统关闭
    * OutofMemory宕机
    * 使用Kill pid杀死进程（使用kill -9是不会被调用的）
 *
 */
public class HookDemo {
	
	public static void main(String[] args) {
		System.out.println(HookDemo.class.getName());
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("钩子hook");
			}
		}));
		Runtime.getRuntime().exit(0);
		// System.exit(1);
		
		System.out.println("!!!!!!!!!!");
	}
	
	
}
