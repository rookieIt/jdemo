package cn.willbj.brief.hootl.singleten.demo;

public class StaticSignleten {

	private static class SingletonHolder {
		private static StaticSignleten instance = new StaticSignleten();
	}

	private StaticSignleten() {

	}

	public static StaticSignleten getInstance() {
		return SingletonHolder.instance;
	}
}
