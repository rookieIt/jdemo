package cn.willbj.brief.rt.clone;

import java.io.Serializable;

/**
 * 浅拷贝:clone接口
 * @description
 * @date 2020/11/28
 */
public class PDemo implements Cloneable, Serializable {

    private String id;
    private String code;
    private PDemo2 pDemo2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public PDemo2 getpDemo2() {
        return pDemo2;
    }

    public void setpDemo2(PDemo2 pDemo2) {
        this.pDemo2 = pDemo2;
    }

    @Override
    public  Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class PDemo2 implements Serializable{
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
