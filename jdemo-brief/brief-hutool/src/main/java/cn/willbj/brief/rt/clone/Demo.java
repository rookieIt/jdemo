package cn.willbj.brief.rt.clone;

/**
 * 浅拷贝demo
 * @description
 * @date 2020/11/28
 */
public class Demo {

    public static void main(String[] args) throws CloneNotSupportedException {
        PDemo pDemo = new PDemo();
        pDemo.setCode("1");
        pDemo.setId(new String("id1"));
        PDemo2 pDemo2 = new PDemo2();
        pDemo2.setName("a");
        pDemo.setpDemo2(pDemo2);
        System.out.println("第一次"+pDemo+":"+pDemo.getCode()+"-"+pDemo.getpDemo2()+":"+pDemo.getpDemo2().getName());
        System.out.println("第一次"+pDemo+":"+pDemo.getId()+"-"+pDemo.getpDemo2()+":"+pDemo.getpDemo2().getName());
        PDemo clone = (PDemo) pDemo.clone();
        clone.setCode("2");
        clone.setId(new String("id2"));
        clone.getpDemo2().setName("b");
        System.out.println(clone+":"+clone.getCode()+"-"+clone.getpDemo2()+":"+clone.getpDemo2().getName());
        System.out.println(clone+":"+clone.getId()+"-"+clone.getpDemo2()+":"+clone.getpDemo2().getName());

        System.out.println("*********************");

        System.out.println("第二次"+pDemo+":"+pDemo.getCode()+"-"+pDemo.getpDemo2()+":"+pDemo.getpDemo2().getName());
        System.out.println("第二次"+pDemo+":"+pDemo.getId()+"-"+pDemo.getpDemo2()+":"+pDemo.getpDemo2().getName());
    }
}
