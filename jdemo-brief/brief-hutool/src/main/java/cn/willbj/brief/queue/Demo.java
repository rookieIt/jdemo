package cn.willbj.brief.queue;

import java.util.concurrent.ArrayBlockingQueue;

public class Demo {
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 长度:3
     * 1
     * 2
     * 3
     * 长度:0
     */
    private static void test01() throws InterruptedException {
        ArrayBlockingQueue<String> strings = new ArrayBlockingQueue<String>(3);
        strings.add("1");
        strings.put("2");
        strings.add("3");
        strings.offer("4");
        // strings.put("4");// 阻塞,直到插入
        // strings.add("4");// Queue full
        System.out.println("长度:"+strings.size());
        for (String string : strings) {
            System.out.println(strings.poll());
        }
        // System.out.println(strings.element());// java.util.NoSuchElementException
        // System.out.println(strings.take());// 为空会阻塞
        System.out.println(strings.peek());// 为空返回null
        // System.out.println(strings.remove());// java.util.NoSuchElementException
        System.out.println(strings.poll());// 为空返回null
        System.out.println("长度:"+strings.size());
    }
}
