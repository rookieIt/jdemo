package cn.willbj.brief.hootl.itr.demo;

import java.util.Iterator;

public class CustomArrayList<T> implements Iterable<T>{
    T[] ts;

    @SafeVarargs
    public final void addT(T... ts) {
        this.ts = ts;
    }

    @Override
    public Iterator<T> iterator() {
        return new CustomIterator();
    }

    class CustomIterator implements Iterator<T>{


        // 当前迭代元素的下标
        private int index = 0;

        // 判断是否还有下一个元素，如果迭代到最后一个元素就返回false
        @Override
        public boolean hasNext() {
            return index != ts.length;
        }

        @Override
        public T next() {
            return ts[index++];
        }
    }
}
