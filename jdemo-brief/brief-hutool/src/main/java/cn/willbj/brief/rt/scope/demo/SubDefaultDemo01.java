package cn.willbj.brief.rt.scope.demo;

import cn.willbj.brief.rt.scope.demo01.defaultDemo01;

/**
 * 作用域	本类	本包范围内	子孙类（只代表其他包中的子孙类）	其他包
 * public   可以     可以	        可以	                     可以
 * protected 可以	 可以	        可以	                    不可以
 * default	可以	 可以	        不可以	                    不可以
 * private	可以	 不可以	        不可以	                    不可以
 */
public class SubDefaultDemo01 extends defaultDemo01 {
    public static void main(String[] args) {
        SubDefaultDemo01 subDefaultDemo01 = new SubDefaultDemo01();
        // 无法调用 subDefaultDemo01.
    }

    /**
     * 跨包无法重写test01方法
     * @see defaultDemo01#test01()
     */
    // test01
}
