package cn.willbj.brief.hootl.singleten.demo;

import java.io.Serializable;

import org.apache.commons.lang3.SerializationUtils;

public class SerizeSignleten implements Serializable{

	    private static class SingletonHolder {
	        private static SerizeSignleten instance = new SerizeSignleten();
	    }

	    private SerizeSignleten() {

	    }

	    public static SerizeSignleten getInstance() {
	        return SingletonHolder.instance;
	    }

	    public static void main(String[] args) {
	    	SerizeSignleten instance = SerizeSignleten.getInstance();
	        byte[] serialize = SerializationUtils.serialize(instance);
	        SerizeSignleten newInstance = SerializationUtils.deserialize(serialize);
	        System.out.println(instance == newInstance);
	    }

}
