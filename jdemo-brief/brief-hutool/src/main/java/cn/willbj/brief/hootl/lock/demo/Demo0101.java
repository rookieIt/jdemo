package cn.willbj.brief.hootl.lock.demo;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自定义锁
 * @author user
 *
 */
public class Demo0101 {
	public static void main(String[] args) {
		
		
		int NCPU = 2;
		int HEAD_SPINS =  (NCPU  > 1) ? 1 << 6 : 0;
		
		System.out.println(HEAD_SPINS);

		
		AtomicReference<Thread> atomicReference = new AtomicReference<Thread>();
		Thread t01 = atomicReference.get();
		System.out.println(t01);
		
		Thread current = Thread.currentThread();
		boolean compareAndSet = atomicReference.compareAndSet(null, current);
		System.out.println(compareAndSet);
		
		Thread t02 = atomicReference.get();
		System.out.println(t02);
		System.out.println(atomicReference.toString());
	}
}


