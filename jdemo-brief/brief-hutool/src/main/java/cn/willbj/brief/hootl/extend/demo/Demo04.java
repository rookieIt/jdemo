package cn.willbj.brief.hootl.extend.demo;

/**
 * Copyright: Copyright (c) 2020 北京海顿中科技术有限公司
 *
 * @author zhangliuwei
 * @version 3.0.21.20200930-tzsh
 * @description
 * @date 2020/9/24
 */
public class Demo04 {
    public static void main(String[] args) {
        A a = new Demo04().new A();
        a.test1();
        a.test2();

        System.out.println("********************");

        A b = new Demo04().new B();
        b.test1();// A -- test2
                  // B -- test3
    }

    class A{
        public void test1(){
            test2();
            test3();
        }

        private final void test2(){
            System.out.println("A -- test2");
        }

        public void test3(){
            System.out.println("A -- test3");
        }
    }
    class B extends A{
        private final void test2(){
            System.out.println("B -- test2");
        }

        @Override
        public void test3(){
            System.out.println("B -- test3");
        }
    }
}
