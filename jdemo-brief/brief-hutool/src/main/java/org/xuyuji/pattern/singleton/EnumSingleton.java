package org.xuyuji.pattern.singleton;

/**
 * 不但可以防止利用反射强行构建单例对象，而且可以在枚举类对象被反序列化的时候，保证反序列的返回结果是同一对象。
 */
public enum EnumSingleton {
	INSTANCE;
}
