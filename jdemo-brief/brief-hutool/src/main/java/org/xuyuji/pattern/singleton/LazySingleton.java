package org.xuyuji.pattern.singleton;

import java.lang.reflect.Field;

/**
 * 懒汉式问题:没有final修饰，被反射修改
 */
public class LazySingleton {

	private static LazySingleton INSTANCE;

	private LazySingleton() {
	}

	public static LazySingleton getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new LazySingleton();
		}
		return INSTANCE;
	}

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		System.out.println(LazySingleton.getInstance());// org.xuyuji.pattern.singleton.LazySingleton@60e53b93
		System.out.println(LazySingleton.getInstance());// org.xuyuji.pattern.singleton.LazySingleton@60e53b93

		// 反射INSTANCE置空
		Class<LazySingleton> clazz = LazySingleton.class;
		Field f = clazz.getDeclaredField("INSTANCE");
		f.setAccessible(true);
		f.set(LazySingleton.getInstance(), null);

		System.out.println(LazySingleton.getInstance());// 重新获取 org.xuyuji.pattern.singleton.LazySingleton@1d44bcfa
	}
}
