package org.xuyuji.pattern.singleton;

public class ThreadLocalSingletonDemo01 {

	/**
	 * 测试:基于线程的单例模式
	 * @param args
	 */
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i=0;i<10;i++) {
					ThreadLocalSingleton instance = ThreadLocalSingleton.getInstance();
					System.out.println(instance);//org.xuyuji.pattern.singleton.ThreadLocalSingleton@2de3f50d
				}
				
			}
		}).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				ThreadLocalSingleton instance = ThreadLocalSingleton.getInstance();
				System.out.println(instance);//org.xuyuji.pattern.singleton.ThreadLocalSingleton@2ed71ced
			}
		}).start();
	}
}
