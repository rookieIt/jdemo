package cn.jdemo.juc.highquality.rpc_brief.client;

import cn.jdemo.juc.highquality.rpc_brief.server.HelloService;
import cn.jdemo.juc.highquality.rpc_brief.server.IHello;

public class RpcClient {
	public static void main(String[] args) {
		RpcClientProxy rpcClientProxy = new RpcClientProxy();
		try {
			IHello hello = rpcClientProxy.rpcServer(HelloService.class);
			// IHello hello = rpcClientProxy.rpcServer(IHello.class);
			
			String s = hello.sayHello("zz");
	        System.out.println(s);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
