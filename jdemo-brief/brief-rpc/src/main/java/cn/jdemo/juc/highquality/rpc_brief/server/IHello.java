package cn.jdemo.juc.highquality.rpc_brief.server;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/30 16:45.
 * Version 1.0
 */
public interface IHello {
	String sayHello(String info);
}
