package cn.jdemo.juc.highquality.rpc_brief.server;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/30 16:47.
 * Version 1.0
 */
public class HelloService implements IHello {
    public String sayHello(String info) {
        String result = "hello : " + info;
        System.out.println(result);
        return result;
    }
}
