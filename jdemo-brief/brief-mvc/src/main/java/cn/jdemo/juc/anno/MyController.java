package cn.jdemo.juc.anno;

import java.lang.annotation.*;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/16 10:38.
 * Version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyController {
}
