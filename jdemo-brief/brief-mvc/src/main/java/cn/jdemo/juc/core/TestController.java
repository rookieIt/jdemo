package cn.jdemo.juc.core;

import cn.jdemo.juc.anno.MyController;
import cn.jdemo.juc.anno.MyRequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/16 14:31.
 * Version 1.0
 */
@MyController
@MyRequestMapping("/test")
public class TestController {

    @MyRequestMapping(value = "/doTest",other = "test")
    public void test1(HttpServletRequest req, HttpServletResponse response,String param){
        System.out.println(param);
        try {
            response.getWriter().write( "doTest method success! param:"+param);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
