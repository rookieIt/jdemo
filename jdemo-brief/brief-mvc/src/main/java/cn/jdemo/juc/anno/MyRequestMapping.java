package cn.jdemo.juc.anno;

import java.lang.annotation.*;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/16 11:14.
 * Version 1.0
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRequestMapping {
    /**
     * 默认值
     * @return
     */
    String value() default "";

    String other() default "";
}
