/**
 * 注解: 低耦合、可拓展;
 *
 * 元数据从metadata一词译来，就是“关于数据的数据”。
 *
 * Annotation和Annotation类型：
 *
 * 　　Annotation：
 * 　　   Annotation使用了在java5.0所带来的新语法，它的行为十分类似public、final这样的修饰符。
 *       每个Annotation具有一个名字和成员个数>=0。每个Annotation的成员具有被称为name=value对的名字和值（就像javabean一样），name=value装载了Annotation的信息。
 *
 * 　　Annotation类型：
 * 　　   Annotation类型定义了Annotation的名字、类型、成员默认值。一个Annotation类型可以说是一个特殊的java接口，它的成员变量是受限制的，
 *       而声明Annotation类型时需要使用新语法。当我们通过java反射api访问Annotation时，返回值将是一个实现了该 annotation类型接口的对象，通过访问这个对象我们能方便的访问到其Annotation成员。
 *
 * 注解的分类：
 * 　　根据注解参数的个数，我们可以将注解分为三类：
 * 　　　　1.标记注解:一个没有成员定义的Annotation类型被称为标记注解。这种Annotation类型仅使用自身的存在与否来为我们提供信息。比如后面的系统注解@Override;
 * 　　　　2.单值注解
 * 　　　　3.完整注解　　
 *
 * 　　根据注解使用方法和用途，我们可以将Annotation分为三类：
 * 　　　　1.JDK内置系统注解
 * 　　　　2.元注解
 * 　　　　3.自定义注解
 *
 * 系统内置标准注解：
 * 　　注解的语法比较简单，除了@符号的使用外，他基本与Java固有的语法一致，JavaSE中内置三个标准注解（它在字节码层面工作），定义在java.lang中：
 * @see java.lang.Override 用于修饰此方法覆盖了父类的方法;
 * @see java.lang.Deprecated 用于修饰已经过时的方法;
 * @see java.lang.SuppressWarnings 用于通知java编译器禁止特定的编译警告。
 *      SuppressWarnings注解的常见参数值的简单说明：
 * 　　　　1.deprecation：使用了不赞成使用的类或方法时的警告；
 * 　　　　2.unchecked：执行了未检查的转换时的警告，例如当使用集合时没有用泛型 (Generics) 来指定集合保存的类型;
 * 　　　　3.fallthrough：当 Switch 程序块直接通往下一种情况而没有 Break 时的警告;
 * 　　　　4.path：在类路径、源文件路径等中有不存在的路径时的警告;
 * 　　　　5.serial：当在可序列化的类上缺少 serialVersionUID 定义时的警告;
 * 　　　　6.finally：任何 finally 子句不能正常完成时的警告;
 * 　　　　7.all：关于以上所有情况的警告。
 *
 *
 * @date 2021/1/7
 */
package cn.jdemo.anno;