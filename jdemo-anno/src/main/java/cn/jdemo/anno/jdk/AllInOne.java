package cn.jdemo.anno.jdk;

/**
 * jdk自带的: javax包下，java对其定义，没有解释执行，spring之类的解释执行了
 * @see javax.annotation.PostConstruct
 * @see javax.annotation.PreDestroy
 *
 * @description
 * @date 2021/2/12
 */
public class AllInOne {
}
