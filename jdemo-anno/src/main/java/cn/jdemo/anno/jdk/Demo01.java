package cn.jdemo.anno.jdk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * javax包下，java对其定义，没有解释执行，spring之类的解释执行了
 * @description
 * @date 2021/2/12
 */
public class Demo01 {
    private static final Logger logger = LoggerFactory.getLogger(Demo01.class);

    @PostConstruct
    public void customInit(){
        logger.info("PostConstruct info：{}","构造函数初始化后立刻执行...");
    }

    public static void main(String[] args) {
        test01();
    }

    private static void test01() {
        Demo01 demo01 = new Demo01();
    }

}
