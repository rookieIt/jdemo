package cn.jdemo.test.gm;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class App {
    public static void main(String[] args) {
        BigDecimal bigDecimal = new BigDecimal("8888888888888890.99");
        String authStandard = "10%";
        BigDecimal authStandardBd;
        if (authStandard.indexOf("%") != -1){
            authStandard = authStandard.replace("%","");
            authStandardBd = new BigDecimal(authStandard).divide(new BigDecimal(100),2,RoundingMode.HALF_UP);
        }else{
            authStandardBd = new BigDecimal(authStandard).setScale(2, RoundingMode.HALF_UP);
        }
        System.out.println(authStandardBd);
    }
}

