package cn.jdemo.jdk8.date;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * ********************************************jdk8之前********************************************
 * @see java.util.Date 其方法不易于国际化,大部分被废弃
 * @see java.util.Date#Date(int, int, int, int, int, int) 看源码可知,年份是从1900开始的,而月份是从0开始的
 * @see java.sql.Date 继承了java.util.Date
 * @see System#currentTimeMillis() 返回当前时间与1970年1月1日0:0:0之间以毫秒为单位的时间差
 *
 * ***格式化
 * @see java.text.SimpleDateFormat
 * @see java.text.SimpleDateFormat#SimpleDateFormat(String pattern) 
 * @see java.text.SimpleDateFormat#SimpleDateFormat(String pattern, Locale locale)
 * @see Locale#getDefault()// Locale 表示地区,不同的区域，时间表示方式都不同
 * @see Locale#Locale(String language, String country) 
 * @see java.text.SimpleDateFormat#format(Date)
 * @see java.text.SimpleDateFormat#parse(String source)  返回Date,将字符串解析成时间
 *
 * ***日期
 * @see java.util.Calendar
 * @see java.util.Calendar#getInstance()
 * @see java.util.Calendar#getInstance(TimeZone, Locale) TimeZone时区,Locale地区(语言)环境
 *
 * ********************************************jdk8********************************************
 * 可变性: 像日期和时间这样的类应该是不可变的,---->返回一个值,原来的对象不变;
 * 偏移性: Date中的年份是从1900开始的,而月份是从0开始的,日期表示需要减 new Date(2020-1900,9-1,8) 这样才可以表示2020年9月8日;
 * 格式化: 格式化日期只对Date有用,Calendar则不行;
 * 线程不安全: 不能处理闰秒等;
 *
 * Java8 吸收了Joda-Time精华,开启了新的API,新的java.time包含了如下子类
 *
 * 2.1 java.time基础包
 * @see java.time.LocalDate
 * @see java.time.LocalTime
 * @see java.time.LocalDateTime
 *
 * 时区
 * @see java.time.ZoneId
 * @see java.time.ZoneOffset
 * @see java.time.ZonedDateTime
 * // @see java.time.ZoneRegion
 *
 * @see java.time.Duration 持续时间
 *
 * 2.1 JDK8 的新日期的API
 * java.time -包含值对象的基础包
 * java.time.chrono - 提供不同日历系统的访问
 * java.time.format - 格式化和解析时间和日期
 * java.time.temporal - 包含底层框架和扩展特性
 * java.time.zone - 包含时区支持的类
 *
 * 时间线上的一个瞬时点,这可能用于记录应用程序的事件时间戳
 * Instant的精度可以达到纳秒级
 *
 * GMT: 格林威治时间
 * UTC: 标准时间
 * ISO: 标准时间
 * CST: 北京时间
 * 时间加Z：代表标准时间（一般和UTC时间一致）
 *
 * @date 2021/1/4
 */
public class AllInOne {
}
