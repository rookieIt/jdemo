package cn.jdemo.jdk8.date.clock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Clock;
import java.time.ZoneId;

/**
 *
 * @see java.time.Clock 时钟(抽象类)
 * 此处提供的时钟实现基于{@link System＃currentTimeMillis（）}。
 * 该方法几乎无法保证时钟的准确性。需要更精确时钟的应用程序必须使用另一个外部时钟（例如NTP服务器）自己实现此抽象类。
 *
 *
 * @see java.time.Clock.SystemClock Clock内部实现
 *
 * @description
 * @date 2021/1/29
 */
public class Demo {
    private static final Logger logger = LoggerFactory.getLogger(Demo.class);

    public static void main(String[] args) {
        test01();
        test02();
        test03();
        test04();
    }

    /**
     * 输出内容:
     * 17:19:06.912 [main] INFO  cn.jdemo.jdk8.date.clock.Demo - default zone: Z
     */
    private static void test01() {
        Clock clock = Clock.systemUTC();// 格林威治时区 new SystemClock(ZoneOffset.UTC);
        logger.info("default zone: {}",clock.getZone().getId());// getId() == toString()
    }

    /**
     * 输出内容:
     * 17:19:06.987 [main] INFO  cn.jdemo.jdk8.date.clock.Demo - default zone: Asia/Shanghai
     */
    private static void test02() {
        Clock clock = Clock.systemDefaultZone();
        logger.info("default zone: {}",clock.getZone().getId());
    }

    /**
     * @see cn.jdemo.jdk8.date.zoneId.Demo#test02() 时区列表
     * 17:19:06.988 [main] INFO  cn.jdemo.jdk8.date.clock.Demo - set zone: Asia/Hong_Kong
     */
    private static void test03() {
        ZoneId zoneId = ZoneId.of("Asia/Hong_Kong");
        Clock clock = Clock.system(zoneId);
        logger.info("set zone: {}",clock.getZone().getId());
    }

    /**
     * 输出内容:
     * Exception in thread "main" java.time.zone.ZoneRulesException: Unknown time-zone ID: xxx
     */
    private static void test04() {
        ZoneId zoneId = ZoneId.of("xxx");
        Clock system = Clock.system(zoneId);
    }
}
