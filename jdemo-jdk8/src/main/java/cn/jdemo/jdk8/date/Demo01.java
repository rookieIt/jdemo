package cn.jdemo.jdk8.date;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * @see LocalDate#now()
 * @see LocalDate#now(Clock clock) 从指定的时钟获取当前日期。
 * @see LocalDate#now(ZoneId zone) 从指定的时区获取当前日期
 *
 * @see LocalDateTime#now(ZoneId)
 * @see LocalDateTime#now(Clock)
 *
 * @see LocalTime#now(Clock)
 * @see LocalTime#now(Clock)
 *
 * @description
 * @date 2021/1/4
 */
public class Demo01 {
    private static final Logger logger = LoggerFactory.getLogger(Demo01.class);

    public static void main(String[] args) {
        test01();
        test03();
    }

    /**
     * 系统默认时区（当前上海时区）
     *
     * 输出内容:
     * 17:33:44.846 [main] INFO  cn.jdemo.jdk8.date.Demo01 - 2021-33-29,05:01:44
     * 17:33:44.849 [main] INFO  cn.jdemo.jdk8.date.Demo01 - 2021-33-29,05:01:44
     * 17:33:44.849 [main] INFO  cn.jdemo.jdk8.date.Demo01 - 2021-33-29,05:01:44
     * 17:33:44.849 [main] INFO  cn.jdemo.jdk8.date.Demo01 - 2021-33-29,05:01:44
     */
    public static void test01() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime now1 = LocalDateTime.now(Clock.systemDefaultZone());
        LocalDateTime now2 = LocalDateTime.now(ZoneId.systemDefault());
        LocalDateTime now3 = LocalDateTime.now(ZoneId.of("Asia/Shanghai"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd,hh:MM:ss");
        logger.info(now.format(formatter));
        logger.info(now1.format(formatter));
        logger.info(now2.format(formatter));
        logger.info(now3.format(formatter));
    }

    /**
     * 输出内容:
     * 默认时区(上海)时间: 16:40:20.433
     * 世界时: 08:40:20.433
     * Asia/Shanghai: 16:40:20.433
     */
    public static void test03() {
        System.out.println("默认时区(上海)时间: "+LocalTime.now());
        Clock clock = Clock.systemUTC();
        System.out.println("世界时: "+LocalTime.now(clock));
        ZoneId zone = ZoneId.systemDefault();
        System.out.println(zone.getId()+": "+LocalTime.now(zone));
    }

}
