package cn.jdemo.jdk8.date.localdate;

import java.time.LocalDate;

/**
 *
 * @description
 * @date 2021/2/22
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
    }

    private static void test01() {
        LocalDate now = LocalDate.now();
        System.out.println(now.toString());
    }
}
