package cn.jdemo.jdk8.date.zoneId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneId;

/**
 * @see java.time.ZoneId 时区
 *
 * @see java.time.ZoneOffset 与 格林威治(Greenwich)/UTC 的时区偏移
 *
 * @description
 * @date 2021/1/29
 */
public class Demo {

    private static final Logger logger = LoggerFactory.getLogger(Demo.class);

    public static void main(String[] args) {
        // test01();
        test02();
    }

    /**
     * 输出内容:
     * 16:12:00.758 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - default zoneid: Asia/Shanghai
     */
    public static void test01() {
        ZoneId zoneId = ZoneId.systemDefault();
        logger.info("default zoneid: {}",zoneId.toString());
    }

    /**
     * 输出内容:
     * ...
     * 16:14:16.091 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Etc/GMT+9
     * 16:14:16.091 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Etc/GMT+8
     * 16:14:16.091 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Africa/Nairobi
     * 16:14:16.098 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Asia/Shanghai
     * 16:14:16.100 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Asia/Chongqing
     * 16:14:16.103 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Asia/Harbin
     * 16:14:16.111 [main] INFO  cn.jdemo.jdk8.date.zoneId.Demo - AvailableZoneIds str: Asia/Hong_Kong
     * ...
     */
    public static void test02() {
        ZoneId.getAvailableZoneIds().stream()
                .forEach(zoneIdStr ->logger.info("AvailableZoneIds str: {}",zoneIdStr));
    }

}
