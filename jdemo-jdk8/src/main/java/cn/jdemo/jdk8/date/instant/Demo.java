package cn.jdemo.jdk8.date.instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

/**
 * @see java.time.Instant 瞬时点/时间戳
 *
 * @description
 * @date 2021/1/29
 */
public class Demo {

    private static final Logger logger = LoggerFactory.getLogger(Demo.class);

    public static void main(String[] args) {
        test01();
    }

    private static void test01() {
        Instant now = Instant.now();
        logger.info(now.toString());// 2021-01-29T09:47:18.761Z
    }
}
