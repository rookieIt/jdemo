package cn.jdemo.jdk8.functionalInterface;

import java.util.function.*;

/**
 * jdk8的其他基础函数式接口
 *
 * @description
 * @date 2020/12/4
 */
public class Demo02 {

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        DoubleBinaryOperator doubleBinaryOperator = (left,right) ->{
            System.out.println(left+right);
            return left + right;
        };
    }
}
