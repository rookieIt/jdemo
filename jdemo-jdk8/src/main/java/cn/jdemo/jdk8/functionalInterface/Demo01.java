package cn.jdemo.jdk8.functionalInterface;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * 四种jdk8的基础函数式接口
 *
 * @description
 * @date 2020/12/4
 */
public class Demo01 {

    public static void main(String[] args) {
        test();
    }

    public static void test(){
        // 消费型函数接口
        Consumer<String> consumer = (str)->{
            System.out.println(str);
        };
        consumer.accept("消费型函数接口");

        // 供给型函数接口
        Supplier<String> supplier = ()->{
            return  "供给型函数接口";
        };
        System.out.println(supplier.get());

        // 断言型函数接口
        Predicate<String> predicate = (str) -> {
            System.out.println(str);
            return true;
        };
        predicate.test("断言型函数接口");

        // Function函数型接口
        Function<String,Supplier<String>> function = (str)->{
            Supplier<String> temp= ()->{
                return  str;
            };
            return temp;
        };
        Supplier<String> apply = function.apply("Function函数型接口");
        System.out.println(apply.get());

    }
}
