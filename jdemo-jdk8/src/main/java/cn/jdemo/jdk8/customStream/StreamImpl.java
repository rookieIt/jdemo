/*
package cn.jdemo.jdk8.customStream;

public class StreamImpl<T> implements Stream<T> {
    private T value;
    private boolean empty = false;
    private NextValueEval<T> nextItemEval;

    @Override
    public <R> StreamImpl<R> map(Function<R, T> mapper) {
        return null;
    }

    @Override
    public <R, A> R collect(Collector<T, A, R> collector) {
        return null;
    }

    public static class NextValueEval<T> {
        private final EvalFunction<T> evalFunction;

        public NextValueEval(EvalFunction<T> evalFunction) {
            this.evalFunction = evalFunction;
        }

        StreamImpl<T> eval() {
            return evalFunction.apply();
        }
    }

    public static class Builder<T> {
        private final StreamImpl<T> stream;

        public Builder() {
            this.stream = new StreamImpl<>();
        }

        public Builder<T> head(T value) {
            this.stream.value = value;
            return this;
        }

        public Builder<T> empty(boolean empty) {
            this.stream.empty = empty;
            return this;
        }

        public Builder<T> nextItemEvalProcess(EvalFunction<T> evalFunction) {
            this.stream.nextItemEval = new NextValueEval<>(evalFunction);
            return this;
        }

        public StreamImpl<T> build() {
            return stream;
        }
    }

    private StreamImpl<T> eval() {
        return this.nextItemEval.eval();
    }

    private boolean isEmpty() {
        return empty;
    }

    public static <T> StreamImpl<T> asStream(List<T> list) {
        return StreamImpl.<T>builder().nextItemEvalProcess(() -> getListStream(list.iterator())).build();
    }


    public static <T> StreamImpl<T> getListStream(Iterator<T> iterator) {
        if (!iterator.hasNext()) {
            return Stream.emptyStream();
        }

        return StreamImpl.<T>builder()
                .head(iterator.next())
                .nextItemEvalProcess(() -> getListStream(iterator))
                .build();
    }

    private static <R, T> StreamImpl<R> map(Function<R, T> mapper, StreamImpl<T> stream) {
        if (stream.isEmpty()) {
            return Stream.emptyStream();
        }
        R value = mapper.apply(stream.value);
        return StreamImpl.<R>builder()
                .head(value)
                .nextItemEvalProcess(() -> map(mapper, stream.eval())).build();
    }


    @Override
    public <R> StreamImpl<R> map(Function<R, T> mapper) {
        return StreamImpl.<R>builder().nextItemEvalProcess(() ->
                map(mapper, this.eval())).build();
    }


*/
