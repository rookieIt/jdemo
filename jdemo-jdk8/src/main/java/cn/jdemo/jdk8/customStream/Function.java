package cn.jdemo.jdk8.customStream;

@FunctionalInterface
public interface Function<R, T> {
    /**
     * 根据输入返回结果
     *
     * @param t 输入
     * @return 输出
     */
    R apply(T t);
}