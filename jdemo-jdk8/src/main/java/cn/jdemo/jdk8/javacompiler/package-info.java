/**
 * jdk6的特性之一
 *
 * 1 Java SE5及以前版本中只能通过tools.jar中的com.sun.tools.javac包来调用Java编译器，
 *   但由于tools.jar不是标准的Java库，在使用时必须要设置这个jar的路径。
 *
 * 2 在Java SE6中为我们提供了标准的包来操作Java编译器，这就是javax.tools包。
 *
 * JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
 * JavaCompiler中最核心的方法是run()。通过这个方法能编译java源代码。
 *
 * int run(InputStream in, OutputStream out, OutputStream err, String... arguments)
 *
 * JDK 6 的编译器 API 的另外一个强大之处在于，它可以编译的源文件的形式并不局限于文本文件。JavaCompiler 类依靠文件管理服务可以编译多种形式的源文件。比如直接由内存中的字符串构造的文件，或者是从数据库中取出的文件。这种服务是由 JavaFileManager 类提供的。
 *
 * 在Java SE6中最佳的方法是使用StandardJavaFileManager类。这个类能非常好地控制输入、输出，并且能通过DiagnosticListener得到诊断信息，而DiagnosticCollector类就是listener的实现。新的 JDK 定义了 javax.tools.FileObject 和 javax.tools.JavaFileObject 接口。任何类，只要实现了这个接口，就可以被 JavaFileManager 识别。
 *
 * 3 JDK 6 的编译器 API 的另外一个强大之处在于，它可以编译的源文件的形式并不局限于文本文件。JavaCompiler 类依靠文件管理服务可以编译多种形式的源文件。比如直接由内存中的字符串构造的文件，或者是从数据库中取出的文件。这种服务是由 JavaFileManager 类提供的。
 *
 * 在Java SE6中最佳的方法是使用StandardJavaFileManager类。这个类能非常好地控制输入、输出，并且能通过DiagnosticListener得到诊断信息，而DiagnosticCollector类就是listener的实现。新的 JDK 定义了 javax.tools.FileObject 和 javax.tools.JavaFileObject 接口。任何类，只要实现了这个接口，就可以被 JavaFileManager 识别。
 *
 */
package cn.jdemo.jdk8.javacompiler;