package cn.jdemo.jdk8.optional;

import java.util.Optional;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class Demo01 {
    public static void main(String[] args) throws Exception {
        test01();
    }

    public static void test01() throws Exception {
        Optional.ofNullable(null).orElseThrow(()->new Exception("不存在"));
    }

}
