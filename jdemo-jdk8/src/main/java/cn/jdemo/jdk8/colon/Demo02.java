package cn.jdemo.jdk8.colon;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * 流函数 与 ::的使用
 *
 * (1)使用方式 类名::(静态)方法名
 * (2)返回函数，参数是(静态)方法参数
 *
 * @see java.util.stream.Stream#forEach(Consumer action)
 *
 * @date 2021/1/19
 */
public class Demo02 {

    private final static Logger logger = Logger.getLogger("Demo01.print");

    public static void main(String[] args) {
        // test01();
        Function<String, String> test03 = Demo02::test03;
        System.out.println(test03.apply("123"));// 1231
    }

    /**
     * 输出内容:
     * content:x
     * content:y
     * content:z
     */
    public static void test01() {
        Consumer<String> test02 = Demo02::test02;
        String[] strings={"x","y","z"};
        Arrays.stream(strings).forEach(test02);
    }

    /**
     * 输出内容:
     * content:x
     * content:y
     * content:z
     */
    public static void test02() {
        String[] strings={"x","y","z"};
        Arrays.stream(strings).forEach(Demo02::test02);
    }

    public static void test02(String str) {
        logger.info("content:"+str);
    }

    public static String test03(String str) {
        logger.info("content:"+str);
        return str+1;
    }
}

