package cn.jdemo.jdk8.colon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * :: 与 new 关键字的使用
 *
 * (1)使用方式: 类名::new
 *
 * (2)返回函数 R Method(Param... p) ,其中R 代表类实例对象，参数为构造函数参数
 *
 * @date 2021/1/19
 */
public class Demo03_new {

    private final static Logger logger = LoggerFactory.getLogger(Demo03_new.class);

    public static void main(String[] args) {
        test01();
        test02();
        test03();
        test04();
    }

    /**
     * 输出内容(无参数):
     * 空
     */
    public static void test01() {
        Supplier<Demo03_new> aNew = Demo03_new::new;
    }

    /**
     * 输出内容(无参数):
     * cn.jdemo.jdk8.colon.Demo03_new.Demo03_new():no paramter
     */
    public static void test02() {
        Supplier<Demo03_new> aNew = Demo03_new::new;
        aNew.get();
    }

    /**
     * 输出内容(单参数):
     * 空
     */
    public static void test03() {
        Function<String,Demo03_new> aNew = Demo03_new::new;
    }

    /**
     * 输出内容(单参数):
     * cn.jdemo.jdk8.colon.Demo03_new.Demo03_new(java.lang.String):one paramter
     */
    public static void test04() {
        Function<String,Demo03_new> aNew = Demo03_new::new;
        aNew.apply("one paramter");
    }

    /**
     * 构造函数
     */
    Demo03_new(){
        logger.info("cn.jdemo.jdk8.colon.Demo03_new.Demo03_new():{}","no paramter");
    }

    Demo03_new(String name){
        logger.info("cn.jdemo.jdk8.colon.Demo03_new.Demo03_new(java.lang.String):{}",name);
    }
}


