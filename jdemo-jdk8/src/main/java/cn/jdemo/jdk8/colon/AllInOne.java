package cn.jdemo.jdk8.colon;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * jdk8的双冒号(::),结合函数式编程使用
 *
 * <a href='https://zhuanlan.zhihu.com/p/36012093/'></>
 *
 * @date 2021/1/4
 */
public class AllInOne {
}
