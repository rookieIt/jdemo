package cn.jdemo.jdk8.colon.test;

public class Obj {
    private Boolean checked;
    Obj(Boolean checked){
        this.checked = checked;
    }
    public Boolean getChecked() {
        return checked;
    }
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
