package cn.jdemo.jdk8.colon;

import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 *
 * 自定义函数 与 :: 的使用
 *
 * (1)使用方式, 类实例对象::(非静态)方法名
 * (2)返回函数 R Method(Param... p)，若有返回值则R为返回对象，若有参数则参数为 p
 *
 * @date 2021/1/19
 */
public class Demo01 {

    /**
     * java自带的logger
     */
    private final static Logger logger = Logger.getLogger("Demo01.print");

    public static void main(String[] args) {
        // test01();
        // test02();
        test03();
    }

    /**
     * 输出内容:
     * content:will do ...
     */
    public static void test01() {
        Temp temp = new Temp();
        Supplier<String> todo = temp::todo;
        logger.info("content:"+todo.get());
    }

    /**
     * 输出内容:
     * content:doing ...
     */
    public static void test02() {
        Supplier<String> doing = Temp::doing;
        logger.info("content:"+doing.get());
    }

    public static void test04() {
        Supplier<String> doing = () -> "return";
        logger.info("content:"+doing.get());
    }

    /**
     * 输出内容:
     * content:accept doing...
     */
    public static void test03() {
        Consumer<String> doing = Temp::doing;
        doing.accept("accept doing...");
    }


}
class Temp{
    private final static Logger logger = Logger.getLogger("Temp.print");
    String todo(){
        return "will do ...";
    }

    static String doing(){
        return "doing ...";
    }

    static void doing(String s) {
        logger.info("content:"+s);
    }
}
