package cn.jdemo.jdk8.colon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * (1)使用方式,类名::(非静态)方法名
 *
 * (2) 返回函数，第一个参数是类实例对象，之后的参数是方法本身的参数
 *
 * (3) 返回函数后，可再次生成新的功能函数
 *
 * @date 2021/1/26
 */
public class Demo04 {

    private static final Logger logger = LoggerFactory.getLogger(Demo04.class);

    public static void main(String[] args) {
        // test01();
        // test02();
        test03();
    }

    /**
     * 输出内容:
     * 21:55:42.225 [main] INFO  cn.jdemo.jdk8.colon.Demo04 - data： i am data
     */
    public static void test01() {
        BiConsumer<Demo04, String> setData = Demo04::setData;
        setData.accept(new Demo04(), "i am data");
    }

    /**
     * 输出内容:
     * 22:16:19.872 [main] INFO  cn.jdemo.jdk8.colon.Demo04 - data： custom,10
     * 22:16:19.876 [main] INFO  cn.jdemo.jdk8.colon.Demo04 - 第二次打印: custom
     */
    public static void test02() {
        CustomFunc<Demo04,String,String,Integer> customFunc = Demo04::setTwo;
        String custom = customFunc.method(new Demo04(), "custom", 10);
        logger.info("第二次打印: "+custom);
    }

    /**
     * 输出内容
     * 11:30:33.014 [main] INFO  cn.jdemo.jdk8.colon.Demo04 - data： custom02,100
     * 11:30:33.017 [main] INFO  cn.jdemo.jdk8.colon.Demo04 - 基于功能函数:custom02
     */
    public static void test03() {
        CustomFunc<Demo04,String,String,Integer> customFunc = Demo04::setTwo;
        Function<Demo04,String> function = instance -> customFunc.method(instance, "custom02", 100);
        String apply = function.apply(new Demo04());
        logger.info("基于功能函数:"+apply);
    }



    public void setData(String data) {
        logger.info("data： "+data);
    }

    public String setTwo(String data,Integer value) {
        logger.info("data： "+data+","+value);
        return data;
    }

    @FunctionalInterface
    interface CustomFunc<I,R,P1,P2>{
       R method(I i,P1 p,P2 p2);
    }
}
