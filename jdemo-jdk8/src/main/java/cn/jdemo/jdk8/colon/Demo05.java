package cn.jdemo.jdk8.colon;

import java.util.function.Consumer;

public class Demo05 {
    public static void main(String[] args) {
        test01();
        System.out.println("---------------");
        Demo05 demo = new Demo05();
        demo.test02();
        System.out.println("---------------");
        test03();
    }

    private static void test01() {
        Objects objects = new Objects();
        Consumer<String> consumer = objects::setStr;
        consumer.accept("hello");
    }

    private void test02() {
        Thread zz = new Thread(() -> {
            System.out.println("zz:"+this.getClass());
        });
        zz.start();
    }

    private static void test03() {
    }
}
