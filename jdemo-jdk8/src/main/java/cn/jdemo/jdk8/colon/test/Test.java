package cn.jdemo.jdk8.colon.test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Test {
    public static void main(String[] args) {
        List<Obj> objs = Arrays.asList(new Obj(true), new Obj(false));
        Predicate<Obj> t = Obj::getChecked;
        boolean test = t.test(new Obj(true));
        System.out.println(test);

        Set<Obj> collect = objs.stream().filter(Obj::getChecked).collect(Collectors.toSet());
        System.out.println(collect.size());
    }
}
