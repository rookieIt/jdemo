/**
 * @see java.lang.Math#ceil(double) 向上取
 * @see java.lang.Math#floor(double) 向下取
 * @see java.lang.Math#round(float/double) 四舍五入
 * @see java.lang.Math#pow(x, y) ;x的y次幂
 * @see java.lang.Math#PI
 * @see java.lang.Math#abs(int/float/double)
 * @see java.lang.Math#random() 返回一个 0 到 1 ([0, 1))之间的伪随机数。
 * @see java.lang.Math#log(double)   Math.log(4) / Math.log(2) 以2为底的,4的对数
 * @see java.lang.Math#sqrt(double) 返回其平方根
 */
package cn.jdemo.jdk8.math;