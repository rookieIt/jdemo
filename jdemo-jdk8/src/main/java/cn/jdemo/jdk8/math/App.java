package cn.jdemo.jdk8.math;

public class App {
    public static void main(String[] args) {
        test01();
        System.out.println("---------------------------------");
        test02();
        System.out.println("---------------------------------");
        test04();
    }

    public static void test01(){
        int a = 10;
        int b = 3;
        System.out.println(Math.pow(a,b));//1000.0 ,10的3次方
        System.out.println(Math.round((double)a/b));//3 ,四舍五入
        System.out.println(Math.abs(-10));//10
        System.out.println(Math.random());//0.9396042620404716 返回一个 0 到 1 ([0, 1))之间的伪随机数。
    }

    public static void test02(){
        int a = 10;
        int b = 3;
        System.out.println(Math.log10(a));// 1.0
        System.out.println(Math.log(a) / Math.log(b));// 2.095903274289385 ,以b为底的,a的对数
        System.out.println(Math.log(16) / Math.log(4));// 2.0 ,以4为底的,16的对数
        System.out.println(Math.sqrt(16));// 4.0 ,返回16的平方根
    }

    public static void test04(){
        int a = 13;
        int b = 3;
        System.out.println(a/b);// 4
        System.out.println((double)a/b);// 4.333333333333333
        System.out.println((float)a/b);// 4.3333335
        System.out.println(Math.ceil((double)a/b));// 5.0 向上取
        System.out.println(Math.ceil((float)a/b));// 5.0 向上取
        System.out.println(Math.floor((double)a/b));// 4.0 向下取
        System.out.println(Math.floor((float)a/b));// 4.0 向下取
    }
}
