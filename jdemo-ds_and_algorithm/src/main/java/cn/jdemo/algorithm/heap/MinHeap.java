package cn.jdemo.algorithm.heap;

public class MinHeap extends Heap<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1.compareTo(o2);
    }
}
