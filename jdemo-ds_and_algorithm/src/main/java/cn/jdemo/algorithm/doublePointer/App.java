package cn.jdemo.algorithm.doublePointer;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright: Copyright (c) 2021 北京海顿中科技术有限公司
 *
 * @description
 *  <p>(1) 字符串基于空字符串切割，遍历最长子串(不考虑)</>
 *  <p>(2) 双指针，滑动窗口，子串计算 -- 时间复杂度O(n)</>
 *  <p>(3) 字符串长度过长，其他处理(不考虑)</>
 *
 * @date 2021/3/10
 * @author zhangliuwei
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final char EMPTY_STR = ' ';

    public static void main(String[] args) {
        String str = " aaa bb cccc ddddd eeeeeeee ";
        int result = caculateStr(str);
        logger.info("result size:{}",result);
    }

    private static int caculateStr(String str) {
        if (Strings.isNullOrEmpty(str) || Strings.isNullOrEmpty(str.trim())){
            return 0;
        }

        char[] chars = str.toCharArray();
        int left = 0, right = 0, result = 0,temp = 0;

        while (right < chars.length){
            char rCahr = chars[right];
            right++;
            if (' ' == rCahr){
                left = right;
                result = Integer.max(result, temp);
                temp = 0;
            }else{
                temp++;
                if (right == chars.length){
                    result = Integer.max(result, temp);
                }
            }
        }

        return result;
    }
}
