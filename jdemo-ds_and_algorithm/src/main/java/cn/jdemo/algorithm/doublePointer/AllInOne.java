package cn.jdemo.algorithm.doublePointer;

/**
 * 双指针技巧
 *   1 快慢指针常见算法:（slow = 0,fast = 0,步长不同）
 *      判断链表中是否存在还；
 *      已知链表中有环，找到链表中环的起点；
 *      寻找链表的中点；
 *      寻找链表倒数第K个元素；
 *   2 左右指针常见算法:(right = 0,left = nums.length-1)
 *      二分查找(查找的有序数组)
 *      两数之和(处理有序数组)
 *      反转数组
 *
 * 滑动窗口
 *
 * 补充：
 *   1 twosum（非有序数组）
 *      1) 穷举，暴力
 *      2) 增加hash表，key放数组值,value放入数组下标 【处理数组中元素不重复】
 *      3) 排序+双指正技巧
 *
 * @date 2021/2/18
 */
public class AllInOne {
}
