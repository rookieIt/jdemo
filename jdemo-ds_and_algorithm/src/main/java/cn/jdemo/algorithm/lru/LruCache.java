package cn.jdemo.algorithm.lru;

import java.util.LinkedHashMap;

public class LruCache<K,V> {
    /* 头部最先插入的 - 最久未使用的 */
    LinkedHashMap<K,V> map = new LinkedHashMap<>();
    int capcity = 0;

    LruCache(int capcity){
        this.capcity = capcity;
    }

    public void put(K k,V v){
        if (map.containsKey(k)){
            map.put(k,v);
            makeRecently(k);
            return;
        }
        if (capcity <= map.size()){
            // todo:找到头部,然后删除
            K next = map.keySet().iterator().next();
            map.remove(next);
        }
        map.put(k,v);
    }

    public V get(K k){
        V v = null;
        if (map.containsKey(k)){
            v = map.get(k);
            makeRecently(k);
        }
        return v;
    }

    private void makeRecently(K k) {
        V v = map.get(k);
        map.remove(k);
        map.put(k,v);
    }
}
