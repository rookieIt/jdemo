package cn.jdemo.algorithm.lru;

public class App {
    public static void main(String[] args) {
        LruCache<String,String> lruCache = new LruCache<>(2);
        lruCache.put("1","1");
        lruCache.put("2","2");
        lruCache.put("3","3");
        System.out.println(lruCache.get("1"));
        System.out.println(lruCache.get("2"));
        System.out.println(lruCache.get("3"));

    }
}
