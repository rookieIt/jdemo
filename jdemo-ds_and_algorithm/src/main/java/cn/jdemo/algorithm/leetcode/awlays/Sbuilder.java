package cn.jdemo.algorithm.leetcode.awlays;

/**
 * @see StringBuilder
 * @see StringBuilder#append(int) 
 * @see StringBuilder#delete(int start, int end)
 * @see StringBuilder#length()
 * 
 */
public class Sbuilder {
    public static void main(String[] args) {
        StringBuilder sbuilder = new StringBuilder("ABCDEF");
        System.out.println(sbuilder.delete(sbuilder.length()-2,sbuilder.length()-1));
    }
}
