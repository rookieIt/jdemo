package cn.jdemo.algorithm.leetcode.awlays;

import java.util.Deque;
import java.util.concurrent.TimeUnit;

/**
 * @see java.util.Queue#offer(Object)
 * @see java.util.Queue#poll()
 * @see java.util.Queue#peek()
 * @see java.util.Queue#size()
 * @see java.util.Queue#remove(Object)
 * @see java.util.Queue#isEmpty()
 *
 * @see java.util.PriorityQueue 队列
 *
 * @see java.util.Deque#offer(Object)
 * @see java.util.Deque#offerFirst(Object)
 * @see java.util.Deque#offerLast(Object)
 * @see Deque#poll()
 * @see Deque#pollLast()
 * @see Deque#pollFirst()
 * @see Deque#remove(Object)
 * @see Deque#remove()  == removeFirst()
 * @see Deque#removeFirst()
 * @see Deque#removeLast()
 * @see Deque#size()
 * @see Deque#isEmpty()
 *
 * @see java.util.LinkedList 双端队列
 *
 *
 * @see java.util.concurrent.BlockingQueue
 * @see java.util.concurrent.BlockingQueue#offer(Object)
 * @see java.util.concurrent.ArrayBlockingQueue#offer(Object) 添加要么成功,要么失败
 * @see java.util.concurrent.ArrayBlockingQueue#offer(Object, long, TimeUnit) 可等待指定事时间，添加要么成功,要么失败
 * @see java.util.concurrent.LinkedBlockingQueue
 * @see java.util.concurrent.PriorityBlockingQueue
 *
 * @see java.util.concurrent.BlockingDeque
 * @see java.util.concurrent.LinkedBlockingDeque
 */
public class Jqueue {
}
