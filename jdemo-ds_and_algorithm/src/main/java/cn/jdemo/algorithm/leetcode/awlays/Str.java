package cn.jdemo.algorithm.leetcode.awlays;

/**
 * @see String#charAt(int)
 * @see String#substring(int, int)
 * @see String#replace(char, char)
 * @see String#length()
 *
 * 字符转数字
 * System.out.println('9'-'0');// 9
 */
public class Str {
    public static void main(String[] args) {
        System.out.println('9'-'0');// 9
    }
}
