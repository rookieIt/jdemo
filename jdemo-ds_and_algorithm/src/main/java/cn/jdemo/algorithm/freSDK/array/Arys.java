package cn.jdemo.algorithm.freSDK.array;

import java.util.Arrays;

/**
 * @see Arrays#copyOfRange(Object[], int, int)
 * @see Arrays#copyOf(int[], int)
 *
 * @see Arrays#toString(int[])
 * @see Arrays#sort(int[])
 * @see Arrays#fill(int[], int) 
 * @see Arrays#binarySearch(int[], int fromIdx, int toIdx, int key) 
 * @see Arrays#binarySearch(int[], int key)  
 * 
 * @see Arrays#asList(Object[])
 */
public class Arys {
    public static void main(String[] args) {
        test02();
    }

    public static void test01() {
        int[] a = new int[]{1,2,3};
        int[] b = Arrays.copyOfRange(a, 0, 1);
        System.out.println(Arrays.toString(b));// [1]
    }

    public static void test02() {
        int[] a = new int[]{1,2,3};
        int[] b = Arrays.copyOf(a, 2);
        System.out.println(Arrays.toString(b));// [1, 2]
    }

    public static void test03() {
        int[] a = new int[]{1,2,3};
        Arrays.sort(a);
    }
}
