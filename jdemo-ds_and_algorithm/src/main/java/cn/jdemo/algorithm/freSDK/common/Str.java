package cn.jdemo.algorithm.freSDK.common;

/**
 * @see String#length() 
 * @see String#codePointCount(int begin, int end)
 *
 * <a href = "https://juejin.cn/post/6844904036873814023">内码/外码</>
 *
 * 内码：char或String在内存里使用的编码方式。
 * 外码：除了内码都可以认为是“外码”。（包括class文件的编码）
 *
 * 而java内码：unicode（utf-16）中使用的是utf-16.
 * 所以上面的那句话再进一步解释就是：返回字符串的长度，这一长度等于字符串中的UTF-16的代码单元的数目。
 *
 * utf16: U+0000-U+FFFF --> 2个字节
 *        大于U+FFFF --> 4个字节
 *
 */
public class Str {
    public static void main(String[] args) {
        test01();
        test02();
    }
    public static void test01(){
        String str = "@1中€";
        System.out.println(str.length());// 4
    }
    public static void test02(){
        String str = "𝄞"; // 这个就是那个音符字符，只不过由于当前的网页没支持这种编码，所以没显示。
        String str2 = "\uD834\uDD1E";// 这个就是音符字符的UTF-16编码
        System.out.println(str2);// 𝄞
        System.out.println(str.length());// 2
        System.out.println(str2.length());// 2
        System.out.println(str.codePointCount(0,str.length()));// 1
    }
}
