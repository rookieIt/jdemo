package cn.jdemo.algorithm.freSDK.common;

import java.util.Scanner;

/**
 * @see Scanner#hasNext()
 * @see Scanner#hasNextLine()
 * @see Scanner#hasNextInt()
 *
 * @see Scanner#next()
 * @see Scanner#nextLine()
 * @see Scanner#nextInt()
 */
public class Scan {
    public static void main(String[] args) {
       // test01();
       test02();
    }

    /**
     * 输入:1 2
     *
     * 1 2
     */
    public static void test01(){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()){
            System.out.println(scanner.nextLine());
        }
    }

    /**
     * 输入: 1 2
     *
     * 1
     * 2
     */
    public static void test02(){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String next = scanner.next();
            if (next.equals("exit")){
                break;
            }
            System.out.println(next);
        }
    }
}
