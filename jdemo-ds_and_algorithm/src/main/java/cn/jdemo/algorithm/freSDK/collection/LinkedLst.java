package cn.jdemo.algorithm.freSDK.collection;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @see LinkedList#offer(Object)
 * @see LinkedList#peek()
 * @see LinkedList#poll()
 * 
 * @see LinkedList#push(Object) 
 * @see LinkedList#pop()
 *
 */
public class LinkedLst {
    /**
     * [3, 5, 1]
     * 3
     * 5
     * [1]
     * @param args
     */
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>(Arrays.asList(3,5,1));
        System.out.println(list.toString());
        System.out.println(list.poll());
        System.out.println(list.pop());
        System.out.println(list.toString());
    }
}
