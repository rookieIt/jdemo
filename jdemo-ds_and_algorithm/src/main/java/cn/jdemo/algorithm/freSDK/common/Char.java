package cn.jdemo.algorithm.freSDK.common;

public class Char {
    public static void main(String[] args) {
        toChar();
        System.out.println("------------------");
        doCalculate();
        System.out.println("------------------");
        toInt();
    }

    public static void doCalculate(){
        char a = 'a';
        System.out.println('a');
        System.out.println(a - 'a');
    }

    public static void toChar(){
        int num = 65;
        char ch = (char) num;
        System.out.println(ch);// A
    }
    public static void toInt(){
        char ch = 'a';
        int num = (int) ch;
        System.out.println(num); // 输出 97
    }
}
