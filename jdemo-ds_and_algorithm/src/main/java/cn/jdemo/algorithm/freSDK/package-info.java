/**
 * ********************************刷题高频sdk**************************************
 * **************其他总结***************
 * @see cn.jdemo.base.collect
 * **************通用总结***************
 * 集合
 * @see java.util.Collections
 * @see java.util.Map
 * @see java.util.List
 * @see java.util.Set
 *
 * 数组
 * @see java.util.Arrays
 *
 * 堆栈
 * @see java.util.Queue
 * @see java.util.Deque
 * @see java.util.Stack
 *
 * 交互
 * @see java.util.Scanner
 *
 * 工具
 * @see java.lang.Math
 * @see java.lang.String
 * @see java.lang.StringBuilder
 */
package cn.jdemo.algorithm.freSDK;