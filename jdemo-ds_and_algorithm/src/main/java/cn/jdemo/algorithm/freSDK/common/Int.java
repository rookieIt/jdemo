package cn.jdemo.algorithm.freSDK.common;

/**
 * @see Integer#parseInt(String)
 * @see Integer#parseInt(String, int radix) 2/8/16进制 转换 10进制
 * @see Integer#toBinaryString(int)
 * @see Integer#toOctalString(int)
 * @see Integer#toHexString(int)
 *
 * 0x --> 16进制(Hex)
 * 0b --> 2进制(Binary)
 * 0 --> 8进制(Octal)
 *
 */
public class Int {
    public static void main(String[] args) {
        show();
        shift();
    }

    public static void show() {
        int a = 0xa;// 10
        int b = 017;// 15
        int c = 0b10;// 2
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }

    public static void shift() {
        int res = Integer.parseInt("A", 16);
        int res2 = Integer.parseInt("10", 2);
        System.out.println(res);// 16进制转10进制 ==> 10
        System.out.println(res2);// 2进制转10进制 ==> 2

        System.out.println(Integer.toBinaryString(res2));// 10转2 ==> 10
        System.out.println(Integer.toOctalString(res));// 10转8 ==> 12
        System.out.println(Integer.toHexString(res));// 10转16 ==> a
    }


}
