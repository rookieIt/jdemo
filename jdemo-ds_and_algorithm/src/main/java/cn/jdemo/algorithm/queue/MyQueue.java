package cn.jdemo.algorithm.queue;

import java.util.Stack;

/**
 * 请你仅使用两个栈实现先入先出队列。
 * 队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：
 */
public class MyQueue {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.push(1);
        myQueue.push(2);
        System.out.println(myQueue.peek());
        System.out.println(myQueue.pop());
        System.out.println(myQueue.pop());
    }
    private Stack<Integer> in;
    private Stack<Integer> out;
    public MyQueue() {
        in = new Stack();
        out = new Stack();
    }

    public void push(int x) {
        in.push(x);
    }

    public int pop() {
        if (out.isEmpty()){
            int size = in.size();
            for (int i = 0; i < size; i++) {
                out.push(in.pop());
            }
        }
        return out.pop();
    }

    public int peek() {
        if (out.isEmpty()){
            int size = in.size();
            for (int i = 0; i < size; i++) {
                out.push(in.pop());
            }
        }
        return out.peek();
    }

    public boolean empty() {
        return in.isEmpty() && out.isEmpty();
    }
}
