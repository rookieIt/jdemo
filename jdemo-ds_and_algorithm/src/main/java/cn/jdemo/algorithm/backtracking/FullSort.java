package cn.jdemo.algorithm.backtracking;

import java.util.LinkedList;
import java.util.List;

/**
 * [1,2,3]-全排列
 * @date 2021/2/18
 */
public class FullSort {
    public static void main(String[] args) {
        test01();
    }

    private static final List<LinkedList<Integer>> RES = new LinkedList<LinkedList<Integer>>();
    static void test01() {
        Integer[] nums= {1,2,3};
        backTrack(nums,new LinkedList());
        RES.forEach(System.out::println);
    }

    private static void backTrack(Integer[] nums, LinkedList<Integer> track){
        if (track.size() == nums.length){
            RES.add(new LinkedList<>(track));
            return;
        }

        for (Integer num:nums){
            if (track.contains(num)){
                continue;
            }
            track.add(num);
            backTrack(nums,track);
            track.remove(num);
        }
    }
}
