package cn.jdemo.algorithm.backtracking.dfs;

/**
 * 给你一个由'1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。
 * 岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。
 * 此外，你可以假设该网格的四条边均被水包围。
 *
 * 示例 1：
 * 输入：grid = [
 *   ['1','1','1','1','0'],
 *   ['1','1','0','1','0'],
 *   ['1','1','0','0','0'],
 *   ['0','0','0','0','0']
 * ]
 * 输出：1
 * 示例 2：
 * 输入：grid = [
 *   ['1','1','0','0','0'],
 *   ['1','1','0','0','0'],
 *   ['0','0','1','0','0'],
 *   ['0','0','0','1','1']
 * ]
 * 输出：3
 */
public class NumIslands {

    public static void main(String[] args) {
        char[][] grid = new char[][]{{'1','1','0','0','0'},{'1','1','0','0','0'},{'0','0','1','0','0'},{'0','0','0','1','1'}};
        int res = new NumIslands().numIslands(grid);
        System.out.println(res);
    }

    public int numIslands(char[][] grid) {
        int res = 0;
        int xlen = grid.length, ylen = grid[0].length;
        for (int i = 0;i<xlen;i++){
            for (int j = 0;j<ylen;j++){
                // 发现一个陆地，把相连陆地都置为0
                if (grid[i][j] == '1'){
                    res++;
                    fill(grid,i,j);
                }
            }
        }
        return res;
    }

    private void fill(char[][] grid, int x, int y) {
        int xlen = grid.length,ylen = grid[0].length;
        if (x<0 || x>=xlen || y<0 || y>= ylen){
            return;
        }
        if (grid[x][y] == '0'){
            return;
        }
        // 将 (x, y) 变成海水
        grid[x][y] = '0';
        // left
        fill(grid,x - 1,y);
        // down
        fill(grid,x,y-1);
        // right
        fill(grid,x + 1,y);
        // up
        fill(grid,x,y+1);
    }
}
