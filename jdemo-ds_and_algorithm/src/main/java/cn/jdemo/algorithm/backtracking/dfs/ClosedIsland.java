package cn.jdemo.algorithm.backtracking.dfs;

/**
 * 二维矩阵 grid由 0（土地）和 1（水）组成。岛是由最大的4个方向连通的 0组成的群，
 * 封闭岛是一个完全 由1包围（左、上、右、下）的岛。
 * 请返回 封闭岛屿 的数目。
 * 示例 1：
 * 输入：grid = {{1,1,1,1,1,1,1,0},{1,0,0,0,0,1,1,0},{1,0,1,0,1,1,1,0},{1,0,0,0,0,1,0,1},{1,1,1,1,1,1,1,0]]
 * 输出：2
 */
public class ClosedIsland {
    public static void main(String[] args) {
        // int[][] grid= {{1,1,1,1,1,1,1,0},{1,0,0,0,0,1,1,0},{1,0,1,0,1,1,1,0},{1,0,0,0,0,1,0,1},{1,1,1,1,1,1,1,0}};
        int[][] grid= {{0,0,1,1,0,1,0,0,1,0},{1,1,0,1,1,0,1,1,1,0},{1,0,1,1,1,0,0,1,1,0},{0,1,1,0,0,0,0,1,0,1},{0,0,0,0,0,0,1,1,1,0},{0,1,0,1,0,1,0,1,1,1},{1,0,1,0,1,1,0,0,0,1},{1,1,1,1,1,1,0,0,0,0},{1,1,1,0,0,1,0,1,0,1},{1,1,1,0,1,1,0,1,1,0}};
        int res = new ClosedIsland().closedIsland(grid);
        System.out.println(res);
    }

    public int closedIsland(int[][] grid) {
        int res = 0;
        int xlen = grid.length,ylen =grid[0].length;

        // 靠边得陆地不算封闭岛屿--把靠边得陆地置为海水
        for (int i = 0; i < xlen; i++) {
            dfs(grid, i ,0);
            dfs(grid, i ,ylen-1);
        }
        for (int j = 0; j < ylen; j++) {
            dfs(grid,0,j);
            dfs(grid,xlen-1,j);
        }

        // 排除边界
        for (int i = 0;i<xlen;i++){
            for (int j = 0;j<ylen;j++){
                if (grid[i][j] == 0){
                    res++;
                    dfs(grid,i,j);
                }

            }
        }
        return res;
    }

    private void dfs(int[][] grid, int x, int y) {
        int xlen = grid.length,ylen =grid[0].length;
        // 到边界了
        if (x<0 || y<0 || x>=xlen || y>=ylen){
            return;
        }
        // 是水
        if (grid[x][y] == 1){
            return;
        }
        grid[x][y] = 1; //
        dfs(grid,x,y-1);
        dfs(grid,x-1,y);
        dfs(grid,x+1,y);
        dfs(grid,x,y+1);
    }

}
