package cn.jdemo.algorithm.binaryHeap;

/**
 * 1 ⼆叉堆其实就是⼀种特殊的⼆叉树（完全⼆叉树），所以适合存储在【数组】⾥：
 *     堆顶arr[1]，左孩子索引temp*2,右孩子索引 temp*2+1,父索引 temp/2
 *
 * 2 二叉堆还分为最⼤堆和最⼩堆:
 *     最⼤堆的性质是：每个节点都⼤于等于它的两个⼦节点。类似的，最⼩堆的性质是：每个节点都⼩于等于它的⼦节点
 *
 * 3 二叉堆算法应用
 * @see cn.jdemo.pattern.priority_queue.AllInOne 优先级队列
 *
 * floodfill
 *
 * @date 2021/2/18
 */
public class AllInOne {
}
