package cn.jdemo.algorithm.lfu;

import java.util.HashMap;
import java.util.Map;

public class LfuCache<K,V> {

    Map<K,V> map = new HashMap<>();
    Map<K,Integer> kToFreq = new HashMap<>();
    int capcity = 0;

    LfuCache(int capcity){
        this.capcity = capcity;
    }

    public V get(K k){
        V v = null;
        if (map.containsKey(k)){
            v = map.get(k);
            increaseFreq(k);
        }
        return v;
    }

    public void put(K k,V v){
        if (map.containsKey(k)){
            map.put(k,v);
            increaseFreq(k);
            return;
        }
        if (capcity <= map.size()){
            removeMinFreq(k);
        }
        map.put(k, v);
        increaseFreq(k);
    }

    /* 找到频率最小的key，并移除 */
    private void removeMinFreq(K k) {
    }

    /* 增加指定key的频次 */
    private void increaseFreq(K k) {
    }

}
