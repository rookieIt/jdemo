package cn.jdemo.algorithm.stack;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 请你仅使用两个队列实现一个后入先出（LIFO）的栈，
 * 并支持普通栈的全部四种操作（push、top、pop 和 empty）。
 */
public class MyStack {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        System.out.println(myStack.top());
        System.out.println(myStack.pop());
    }
    private Queue<Integer> masterQue = new LinkedBlockingQueue();;
    private Queue<Integer> slaveQue = new LinkedBlockingQueue();;
    public MyStack() {
    }

    public void push(int x) {
        masterQue.offer(x);
    }

    public int pop() {
        int size = masterQue.size();
        if (size > 1){
            for (int i = 0; i < size-1; i++) {
                slaveQue.offer(masterQue.remove());
            }
        }else{
            return masterQue.remove();
        }

        // 当前弹出元素
        Integer poll = masterQue.remove();

        // 从移到主
        this.slaveToMaster();
        return poll;
    }

    public int top() {
        int size = masterQue.size();
        if (size > 1){
            for (int i = 0; i < size-1; i++) {
                slaveQue.offer(masterQue.remove());
            }
        }else{
            return masterQue.element();
        }

        // 当前弹出元素
        Integer poll = masterQue.remove();
        slaveQue.offer(poll);

        // 从移到主
        this.slaveToMaster();
        return poll;
    }

    public boolean empty() {
        return masterQue.isEmpty();
    }

    private void slaveToMaster(){
        int size = slaveQue.size();
        for (int i = 0; i < size; i++) {
            masterQue.offer(slaveQue.remove());
        }
    }
}
