package cn.jdemo.algorithm.dynamicProgramming;

/**
 * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
 * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
 * 问总共有多少条不同的路径？
 * 动规5步
 * 1.定义
 *      dp[m-1][n-1],m x n 网格有dp[m-1][n-1]条不同的路径
 * 2.递推公式
 *      dp[i][j] = dp[i-1][j] + dp[i][j-1]
 * 3.初始化
 *      1 x 1, dp[0][0] = 0
 *      1 x 2, dp[0][1] = 1
 *      2 x 1, dp[1][0] = 1
 *      2 x 2, dp[1][1] = 2
 * 4.顺序遍历
 * 5.样例: 3 x 3
 *          dp[i][0]  dp[i][1]  dp[i][2]
 *     0        0        1          1
 *     1        1        2          3
 *     2        1        3
 */
public class UniquePaths {
    public static void main(String[] args) {
        int res = new UniquePaths().uniquePaths(3, 2);
        System.out.println(res);
    }
    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];
        // m为1或n为1,则都是有1条不同的路径
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }
        for (int i = 0; i < n; i++) {
            dp[0][i] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }
}
