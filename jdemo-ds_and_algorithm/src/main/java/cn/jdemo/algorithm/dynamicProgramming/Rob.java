package cn.jdemo.algorithm.dynamicProgramming;

/**
 * 你是一个专业的小偷，计划偷窃沿街的房屋。
 * 每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，
 * 如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。
 *
 * 动规5步
 * 1.定义
 *      dp[i],不触动警报装置的情况下,偷第i家最高金额
 * 2.递推公式
 *      1)偷,则dp[i-2]+nums[i]
 *      2)不偷,则dp[i-1]
 *      dp[i] = max(dp[i-2]+nums[i],dp[i-1])
 * 3.初始化
 *      dp[0] = stole[0];
 *      dp[1] = max(nums[0], nums[1]);
 * 4.遍历顺序
 * 5.样例:[1,2,3,1]
 *          dp[i]
 *      0     1
 *      1     2
 *      2     4
 *      3     4
 */
public class Rob {
    public static void main(String[] args) {
        int[] nums = {1,2,3,1};
        int rob = new Rob().rob(nums);
        System.out.println(rob);
    }
    public int rob(int[] nums) {
        int length = nums.length;
        int[] dp = new int[length];

        // 初始化
        dp[0] = nums[0];
        if (nums.length == 1){
            return dp[--length];
        }
        dp[1] = Math.max(nums[0],nums[1]);
        if (nums.length == 2){
            return dp[--length];
        }

        for (int i = 2; i < length; i++) {
            dp[i] = Math.max(dp[i-2] + nums[i],dp[i-1]);
        }
        return dp[--length];
    }
}
