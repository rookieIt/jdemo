package cn.jdemo.algorithm.dynamicProgramming;

/**
 * 你是一个专业的小偷，计划偷窃沿街的房屋，每间房内都藏有一定的现金。
 * 这个地方所有的房屋都 围成一圈 ，这意味着第一个房屋和最后一个房屋是紧挨着的。
 * 同时，相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警 。
 *
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你 在不触动警报装置的情况下 ，今晚能够偷窃到的最高金额。
 *
 * 动规5步
 * 1.定义
 *      dp[i],不触动警报装置的情况下,偷第i家最高金额
 *     房屋围城一圈，主要两种情况: (0,n-1),(1,n-1)
 * 2.递推公式
 *      1)偷,则dp[i-2]+nums[i]
 *      2)不偷,则dp[i-1]
 *      dp[i] = max(dp[i-2]+nums[i],dp[i-1])
 * 3.初始化
 *      dp[0] = stole[0];
 *      dp[1] = max(nums[0], nums[1]);
 * 4.遍历顺序
 * 5.样例:nums = [1,2,1,1]
 *  [0,2]
 *          dp[i]
 *      0     1
 *      1     2
 *      2     2
 *  [1,3]
 *          dp[i]
 *      0     2
 *      1     2
 *      2     3
 *
 */
public class Rob2 {
    public static void main(String[] args) {
        int[] nums = {1,2,1,1};
        int rob = new Rob2().rob(nums);
        System.out.println(rob);
    }
    public int rob(int[] nums) {
        int length = nums.length;
        if (length == 0){
            return 0;
        }
        if (length == 1){
            return nums[0];
        }
        if (length == 2){
            return Math.max(nums[0],nums[1]);
        }
        int rec1 = rec(nums, 0, length - 2);
        int rec2 = rec(nums, 1, length-1);
        return Math.max(rec1, rec2);
    }

    private int rec(int[] nums,int start,int end){
        int length = end-start+1;
        int[] dp = new int[length];
        // 初始化
        dp[0] = nums[start];
        dp[1] = Math.max(nums[start],nums[start+1]);

        for (int i = 2; i < length; i++) {
            dp[i] = Math.max(dp[i-2] + nums[start + i],dp[i-1]);
        }
        return dp[--length];
    }
}
