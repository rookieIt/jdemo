package cn.jdemo.algorithm.demo;

import java.util.*;

/**
 * 常用函数
 * @see Arrays#sort(int[], int from, int to)
 * @see Arrays#copyOfRange(Object[], int, int) 
 * @see Arrays#fill(int[], int) 
 * 
 * @see Collections#sort(List) 
 * @see Collections#fill(List, Object) 
 * @see Collections#reverse(List)
 * @see Collections#shuffle(List) 
 * @see Collections#copy(List dest, List src)
 *
 * @see Stack#push(Object)
 * @see Stack#peek()
 * @see Stack#pop()
 * @see Queue#offer(Object)
 * @see Queue#peek()
 * @see Queue#poll()
 *
 * @see Math#max(int, int) 
 * @see Math#min(int, int) 
 *
 */
public class Demo01 {
    public static void main(String[] args) {
        test5();
    }

    public static void test5(){
    }

    public static void test4(){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            System.out.println(scanner.nextLine());
        }
    }

    public static void test3() {
        System.out.println(Arrays.asList(1,2,3));
        System.out.println(Math.max(1,2));
        ArrayList<Integer> integers = new ArrayList<>(Arrays.asList(2, 3, 1));
        Collections.sort(integers);
        System.out.println(integers);

        Collections.reverse(integers);
        System.out.println(integers);

        Collections.fill(integers,0);
        System.out.println(integers);

        Arrays.fill(new int[]{1,2,4},0);
        int[] ints = new int[10];
        Arrays.fill(ints,0);
        System.out.println(Arrays.toString(ints));
        int[] ints1 = Arrays.copyOfRange(new int[]{1,2,4}, 1, 2);
        System.out.println(Arrays.toString(ints1));


        List<Integer> integers1 = Arrays.asList(1, 3, 9, 10, 8, 9);
        Collections.shuffle(integers1);// 随机|打乱
        System.out.println(integers1);
    }

    public static void test2() {
        PriorityQueue<Integer> queue = new PriorityQueue<>((t1, t2) -> {
            return t1.intValue() - t2.intValue();
        });
        queue.offer(2);
        queue.offer(1);
        System.out.println(queue.poll());
    }


    public static void test1(){
        Queue<String> linkedList = new LinkedList<>();
        linkedList.offer("123");
        System.out.println(linkedList.peek());
        System.out.println(linkedList.poll());
        System.out.println(linkedList.size());

        Stack<String> stack = new Stack<>();
        stack.push("123");
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.size());
    }
}
