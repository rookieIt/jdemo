package cn.jdemo.algorithm.demo.p1;

/**
 * 同一个包下,default / protected , 都可以访问
 */
public class T3 extends T1{
    public static void main(String[] args) {
        T1 t3 = new T3();
        System.out.println(t3.name);
        System.out.println(t3.age);

        T1 t1 = new T1();
        System.out.println(t1.name);
        System.out.println(t1.age);
    }
}
