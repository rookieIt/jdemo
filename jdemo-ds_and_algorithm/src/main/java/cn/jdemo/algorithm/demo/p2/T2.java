package cn.jdemo.algorithm.demo.p2;

import cn.jdemo.algorithm.demo.p1.T1;

public class T2 extends T1 {
    public static void main(String[] args) {
        T2 t2 = new T2();
        // System.out.println(t2.name);// 无法访问,T1#name (default)
        System.out.println(t2.age);// 可以访问,T1#age (protected)

        T1 t1 = new T1();
        // System.out.println(t1.name);// 无法访问,T1#name (default)
        // System.out.println(t1.age);// 无法访问,T1#age (protected)
    }
}
