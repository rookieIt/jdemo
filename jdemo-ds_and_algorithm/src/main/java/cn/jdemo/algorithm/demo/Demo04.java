package cn.jdemo.algorithm.demo;

// 顺序打印
public class Demo04 {
    static char fuhao = 'a';
    static int toNum = 0;
    public static void main(String[] args) {
        seqPrint();
    }

    public static void seqPrint(){
        int[] arrs = new int[]{1,2,3,6,4,5,7,8,9,10,10};
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this){
                    Integer threadId = Integer.parseInt(Thread.currentThread().getName());
                    while(toNum < 26){
                        if (toNum % 3 == (threadId-1)) {
                            System.out.println("thread" + threadId+": " + (toNum++) + (fuhao++));
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            notifyAll();
                        }else{
                            try {
                                wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        };
        new Thread(runnable,"1").start();
        new Thread(runnable,"2").start();
        new Thread(runnable,"3").start();
    }

}
