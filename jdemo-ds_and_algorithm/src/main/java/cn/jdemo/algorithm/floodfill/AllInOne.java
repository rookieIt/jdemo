package cn.jdemo.algorithm.floodfill;

/**
 * floodfill：二维矩阵(图片的像素点矩阵)
 *     fill(int x,int y){
 *         // 上 下 左 右
 *         fill(x-1,y);
 *         fill(x+1,y);
 *         fill(x,y-1);
 *         fill(x,y+1);
 *     }
 *
 *  上述框架解决所有二维矩阵的遍历问题；说得⾼端⼀点，这就叫深度优先搜索（Depth First Search，简称 DFS），
 *  说得简单⼀点，这就叫四叉树遍历框架。坐标 (x, y) 就是 root，四个⽅向就是 root 的四个⼦节点。
 *
 *
 * @description
 * @date 2021/2/18
 */
public class AllInOne {
}
