package cn.jdemo.algorithm;

import java.util.ArrayList;

public class Test {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add(0,"a");
        list.add(0,"b");
        System.out.println(list); // [b, a] -- 元素a后移到索引1处了
        list.set(0,"c");
        System.out.println(list); // [c, a]
    }
}
