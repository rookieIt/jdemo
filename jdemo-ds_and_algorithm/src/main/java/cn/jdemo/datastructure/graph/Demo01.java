package cn.jdemo.datastructure.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @date 2021/1/18
 */
public class Demo01 {

    private final static Logger logger = LoggerFactory.getLogger(Demo01.class);

    public static void main(String[] args) {
        logger.info("xxx{}{}", "a","b");
    }
}
