package cn.jdemo.type;

public class Demo10_Instance {
    public static void main(String[] args) {
        A2 a2 = new A2();
        if (a2 instanceof A2){
            System.out.println("a2 instanceof A2");
        }
        if (a2 instanceof A1){
            System.out.println("a2 instanceof A1");
        }

        A1 a1 = new A1();
        if (a1 instanceof A2){
            System.out.println("a1 instanceof A2");
        }
        if (a1 instanceof A1){
            System.out.println("a1 instanceof A1");
        }
    }
}

class A1{
}
class A2 extends A1{
}

