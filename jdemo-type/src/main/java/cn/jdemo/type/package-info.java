/**
 * @see java.lang.reflect.Type#getTypeName()
 *
 * Type是Java编程语言中“所有类型”的公共父接口；
 * 其中，“所有类型”的描述尤为值得关注。它并不仅仅是我们平常工作中经常使用的 int、String、List、Map等数据类型，而是从Java语言角度来说，对基本类型、引用类型向上的抽象；
 *
 * Type体系中类型的包括：原始类型(Class)、参数化类型(ParameterizedType)、数组类型(GenericArrayType)、类型变量(TypeVariable)、基本类型(Class);
 * 原始类型，不仅仅包含我们平常所指的类，还包括枚举、数组、注解等；
 * 参数化类型，就是我们平常所用到的泛型List、Map；
 * 数组类型，并不是我们工作中所使用的数组String[] 、byte[]，而是带有泛型的数组，即T[] ；
 * 基本类型，也就是我们所说的java的基本类型，即int,float,double等
 *
 * 简单介绍了Java-Type的体系；为了解决泛型，JDK1.5版本开始引入Type接口；
 * 在此之前，Java中只有原始类型，所有的原始类型都是通过Class进行抽象；
 * 有了Type以后，Java的数据类型得到了扩展，从原始类型扩展为参数化类型(ParameterizedType)、数组类型(GenericArrayType)、类型变量(TypeVariable);
 *
 * 下面就用代码的方式，对其中的5大类型：原始类型(Class)、
 * 参数化类型(ParameterizedType)、数组类型(GenericArrayType)、类型变量(TypeVariable)、基本类型(Class)
 *
 * 1. Method,Field,Constructor,Class与Type关系
 *    Method，Field，Constructor都是与Type关联的，而Class是Type的子类
 *
 * 著作权归https://www.pdai.tech所有。
 * 链接：https://www.pdai.tech/md/java/basic/java-basic-x-generic.html
 *
 * Java泛型这个特性是从JDK 1.5才开始加入的，因此为了兼容之前的版本，Java泛型的实现采取了“伪泛型”的策略，
 * 即Java在语法上支持泛型，但是在编译阶段会进行所谓的“类型擦除”（Type Erasure），
 * 将所有的泛型表示（尖括号中的内容）都替换为具体的类型（其对应的原生态类型），就像完全没有泛型一样。
 * 理解类型擦除对于用好泛型是很有帮助的，尤其是一些看起来“疑难杂症”的问题，弄明白了类型擦除也就迎刃而解了。
 *
 * GenericDeclaration 通用声明
 *
 *
 *
 * @link https://www.cnblogs.com/javastack/p/12963179.html
 * Java 泛型（generics）是 JDK 5 中引入的一个新特性, 泛型提供了编译时类型安全检测机制，该机制允许开发者在编译时检测到非法的类型。
 *
 * 泛型的本质是参数化类型，也就是说所操作的数据类型被指定为一个参数。
 * 泛型带来的好处
 * 在没有泛型的情况的下，通过对类型 Object 的引用来实现参数的“任意化”，“任意化”带来的缺点是要做显式的强制类型转换，而这种转换是要求开发者对实际参数类型可以预知的情况下进行的。
 *
 * 对于强制类型转换错误的情况，编译器可能不提示错误，在运行的时候才出现异常，这是本身就是一个安全隐患。
 *
 * 那么泛型的好处就是在编译的时候能够检查类型安全，并且所有的强制转换都是自动和隐式的。
 *
 * @date 2020/12/11
 */
package cn.jdemo.type;