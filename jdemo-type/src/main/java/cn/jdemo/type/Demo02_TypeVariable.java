package cn.jdemo.type;

import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/**
 *
 * @see java.lang.reflect.TypeVariable<D extends GenericDeclaration > 泛型变量：获取泛型边界相关（针对Class、Constructor、Method上定义的泛型）
 *      类型变量声明（定义）的时候不能有下限（既不能有super），否则编译报错。为什么？
 *      答: T extends classA表示泛型有上限classA，当然可以，因为这样，每一个传进来的类型必定是classA（具有classA的一切属性和方法），
 *      但若是T super classA，传进来的类型不一定具有classA的属性和方法，当然就不适用于泛型。
 *
 * @see TypeVariable#getBounds() 返回表示此类型变量的上限。请注意，如果没有显式声明上限，则上限为{@code Object}。
 *
 * 针对Class、Constructor、Method，实现了GenericDeclaration接口
 *
 * @date 2020/12/25
 */
public class Demo02_TypeVariable<K extends Number, V> {
    /*public List<K> list = null;
    public Map<K,V> map = null;*/

    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * Name():K
     * Bounds():java.lang.Number
     * GenericDeclaration():class cn.jdemo.type.Demo02
     * Name():V
     * Bounds():java.lang.Object
     * GenericDeclaration():class cn.jdemo.type.Demo02
     */
    public static void test01() {
        TypeVariable<Class<Demo02_TypeVariable>>[] Demo02Parameters = Demo02_TypeVariable.class.getTypeParameters();
        for (TypeVariable typeVariable:Demo02Parameters){
            System.out.println("Name():"+typeVariable.getName());
            Type[] bounds = typeVariable.getBounds();
            for (Type type:bounds){
                System.out.println("Bounds():"+type.getTypeName());
            }
            System.out.println("GenericDeclaration():"+typeVariable.getGenericDeclaration());
        }
    }
}
