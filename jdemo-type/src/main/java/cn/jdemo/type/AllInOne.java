package cn.jdemo.type;

import java.lang.reflect.GenericDeclaration;

/**
 * @see java.lang.reflect.Type#getTypeName() jdk1.8后该接口增加默认方法
 *
 * @see java.lang.Class
 * @see java.lang.reflect.ParameterizedType : 参数化类型
 * @see java.lang.reflect.TypeVariable : 类型变量:获取的定义的参数泛型的上边界信息
 *      类型变量声明（定义）的时候不能有下限（既不能有super），否则编译报错。为什么？
 *      T extends classA表示泛型有上限classA，当然可以，因为这样，每一个传进来的类型必定是classA（具有classA的一切属性和方法），
 *      但若是T super classA，传进来的类型不一定具有classA的属性和方法，当然就不适用于泛型。
 *
 * @see java.lang.reflect.GenericArrayType : 泛型数组类型
 * @see java.lang.reflect.WildcardType : 通配符上下边界类型，获取的是参数泛型的上下边界信息
 *
 * @see java.lang.reflect.GenericDeclaration 只有实现了这个接口的才能在对应“实体”上声明“范型变量”（如Class，Constructor，Method）的公共接口
 * @see GenericDeclaration#getTypeParameters() 返回值TypeVariable<?>[],获取声明的类型变量
 *
 * 泛型的擦除的原因以及Java中Type的作用
 * 其实在jdk1.5之前Java中只有原始类型而没有泛型类型，而在JDK 1.5 之后引入泛型，但是这种泛型仅仅存在于编译阶段，当在JVM运行的过程中，与泛型相关的信息将会被擦除，如List与List都将会在运行时被擦除成为List这个类型。而类型擦除机制存在的原因正是因为如果在运行时存在泛型，那么将要修改JVM指令集，这是非常致命的。
 * 此外，原始类型在会生成字节码文件对象，而泛型类型相关的类型并不会生成与其相对应的字节码文件(因为泛型类型将会被擦除)，因此，无法将泛型相关的新类型与class相统一。因此，为了程序的扩展性以及为了开发需要去反射操作这些类型，就引入了Type这个类型，并且新增了ParameterizedType, TypeVariable, GenericArrayType, WildcardType四个表示泛型相关的类型，再加上Class，这样就可以用Type类型的参数来接受以上五种子类的实参或者返回值类型就是Type类型的参数。统一了与泛型有关的类型和原始类型Class。而且这样一来，我们也可以通过反射获取泛型类型参数。
 *
 * spring提供了更具统一的类型抽象：ResolvableType
 *
 * @description
 * @date 2020/12/25
 */
public class AllInOne {
}
