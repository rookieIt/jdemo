package cn.jdemo.type;

import java.lang.reflect.*;
import java.util.List;

/**
 *
 * @see java.lang.reflect.GenericArrayType 泛型数组类型
 * @see GenericArrayType#getTypeName() 参数全类型名称
 * @see GenericArrayType#getGenericComponentType() 数组的泛型类型
 *
 *
 * @date 2020/12/25
 */
public class Demo03_GenericArrayType<T> {
    public T[] ts;

    public List<T[]> list;


    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * **********ts_GenericArrayType***********
     * TypeName(): T[]
     * GenericComponentType(): T
     * **********list_ParameterizedType***********
     * ActualTypeArguments(): T[]
     */
    public static void test01() {
        Field[] fields = Demo03_GenericArrayType.class.getFields();
        for (Field field:fields){
            Type genericType = field.getGenericType();
            if (genericType instanceof GenericArrayType){// ts
                System.out.println("**********"+field.getName()+"_GenericArrayType***********");
                GenericArrayType genericArrayType = (GenericArrayType) genericType;
                System.out.println("TypeName(): "+genericArrayType.getTypeName());
                System.out.println("GenericComponentType(): "+genericArrayType.getGenericComponentType());
            }
            if (genericType instanceof ParameterizedType){// list
                System.out.println("**********"+field.getName()+"_ParameterizedType***********");
                ParameterizedType parameterizedType = (ParameterizedType) genericType;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                for (Type type:actualTypeArguments){
                    System.out.println("ActualTypeArguments(): "+type.getTypeName());
                }
            }
        }
    }
}
