package cn.jdemo.jsystem.array;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/2/9
 */
public class Demo01 {

    private static final Logger logger = LoggerFactory.getLogger(Demo01.class);

    public static void main(String[] args) {
        test04();
    }

    /**
     * 输出内容:
     * chars length：10
     */
    private static void test01() {
        char[] chars = new char[10];
        logger.info("chars length：{}",String.valueOf(chars.length));
    }

    /**
     * 输出内容:
     * java.lang.ArrayIndexOutOfBoundsException: 5
     */
    private static void test02() {
        char[] chars = new char[]{'a','b'};
        chars[5] = 'e';
        logger.info("chars length：{}",String.valueOf(chars.length));
    }

    /**
     * 输出内容:
     * chars length：2
     */
    private static void test03() {
        char[] chars = new char[]{'a','b'};
        chars[1] = 'e';
        logger.info("chars length：{}",String.valueOf(chars.length));
    }

    /**
     * 输出内容:
     * chars length：1
     */
    private static void test04() {
        char[] chars = new char[]{'a','b'};
        char[] chars2 = new char[]{'a'};
        chars = chars2;
        logger.info("chars length：{}",String.valueOf(chars.length));
        logger.info("chars2 length：{}",String.valueOf(chars2.length));
    }
}
