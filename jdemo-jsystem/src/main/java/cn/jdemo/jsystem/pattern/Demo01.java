package cn.jdemo.jsystem.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo01 {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("^[abcde]");
        Matcher matcher = pattern.matcher("hello");
        System.out.println(matcher.find());
        System.out.println(matcher.group());
    }
}
