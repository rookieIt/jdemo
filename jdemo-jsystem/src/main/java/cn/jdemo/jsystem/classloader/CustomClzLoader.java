package cn.jdemo.jsystem.classloader;

public class CustomClzLoader extends ClassLoader{

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		return Class.forName(name);
	}

}
