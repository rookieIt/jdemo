package cn.jdemo.jsystem.classloader;

/**
 * 
 * 打印结果: 
 *     sun.misc.Launcher$AppClassLoader@18b4aac2
 * 	   class cn.jdemo.jsystem.classloader.Demo01
 * @author will
 */
public class Demo02{

	public static void main(String[] args) throws ClassNotFoundException {
		CustomClzLoader customClzLoader = new CustomClzLoader();
		Class<?> aClass = customClzLoader.loadClass("cn.jdemo.jsystem.classloader.Demo01");
		System.out.println(aClass.getClassLoader());// sun.misc.Launcher$AppClassLoader@18b4aac2
		System.out.println(aClass);// class cn.jdemo.jsystem.classloader.Demo01
	}
}
