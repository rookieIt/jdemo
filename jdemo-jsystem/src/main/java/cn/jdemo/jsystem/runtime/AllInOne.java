package cn.jdemo.jsystem.runtime;

import java.io.File;
import java.io.InputStream;

/**
 * @see Runtime
 *
 * @see Runtime#getRuntime()
 * @see Runtime#addShutdownHook(Thread)
 * @see Runtime#removeShutdownHook(Thread)
 * @see Runtime#gc()
 * @see Runtime#freeMemory()
 * @see Runtime#maxMemory()
 * @see Runtime#totalMemory()
 * @see Runtime#availableProcessors()
 * @see Runtime#halt(int status) 强制终止
 * @see Runtime#exit(int status)
 *
 * @see Runtime#load(String filename) 加载由filename参数指定的本机库;filename参数必须是绝对路径名;（例:Runtime.getRuntime().load（“/home/avh/lib/libX11.so”）;）。
 * @see Runtime#load0(Class, String)  
 * @see Runtime#loadLibrary(String)   
 * @see Runtime#loadLibrary0(Class, String)
 *
 * 命令行执行
 * @see Runtime#exec(String[]) 
 * @see Runtime#exec(String) 
 * @see Runtime#exec(String, String[])  
 * @see Runtime#exec(String[], String[])   
 * @see Runtime#exec(String, String[], File) 
 * @see Runtime#exec(String[], String[], File)
 * 
 * 过期
 * @see Runtime#getLocalizedInputStream(InputStream)
 * @see Runtime#runFinalizersOnExit(boolean) 
 * 
 * @date 2020/12/24
 */
public class AllInOne {
}
