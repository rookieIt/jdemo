package cn.jdemo.jsystem.runtime;

/**
 * @see Runtime#availableProcessors() 返回Java虚拟机可用的处理器数量
 * @see Runtime#maxMemory() 返回Java虚拟机*尝试使用的最大内存量。如果没有固有限制，则将返回值{@link * java.lang.Long＃MAX_VALUE}。
 * @see Runtime#totalMemory() 返回Java虚拟机中的内存总量。 此方法返回的值可能会随时间而变化，具体取决于主机环境。
 * @see Runtime#freeMemory() 返回Java虚拟机中的可用内存量。 调用gc方法可能导致增加freeMemory。
 *
 * @date 2020/12/18
 */
public class Demo05 {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * processors:4
     * maxMemory:3749183488
     * totalMemory:253231104
     * freeMemory:249266928
     */
    public static void test01() {
        int processors = Runtime.getRuntime().availableProcessors();
        long totalMemory = Runtime.getRuntime().totalMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("processors:" + processors);
        System.out.println("maxMemory:" + maxMemory);
        System.out.println("totalMemory:" + totalMemory);
        System.out.println("freeMemory:" + freeMemory);
    }

}
