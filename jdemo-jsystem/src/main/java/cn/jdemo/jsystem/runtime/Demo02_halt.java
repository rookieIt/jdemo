package cn.jdemo.jsystem.runtime;
/**
 * 强制终止当前正在运行的Java虚拟机
 * 此方法永远不会正常返回
 * (关闭钩子也不会执行)
 * @see Runtime#halt(int)
 * @see java.lang.Shutdown#halt(int)
 *
 * @date 2020/12/18
 */
public class Demo02_halt {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * main:start...
     */
    public static void test01() {
        System.out.println(Thread.currentThread().getName()+":start...");
        Thread thread = new Thread(()->{
            System.out.println("shutdownHook01...");
        });
        Runtime.getRuntime().addShutdownHook(thread);
        Runtime.getRuntime().halt(-1);//int值随意
        System.out.println(Thread.currentThread().getName()+":end...");
    }

}
