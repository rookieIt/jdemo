package cn.jdemo.jsystem.runtime;
/**
 * 通过启动其关闭序列来终止当前正在运行的Java虚拟机。
 * 此方法永远不会正常返回。
 * int参数用作状态码；按照惯例，非零状态代码表示异常终止。
 * (关闭钩子会执行)
 * @see Runtime#exit(int)
 * @see java.lang.Shutdown#exit(int)
 *
 * @date 2020/12/18
 */
public class Demo03_exit {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * main:start...
     * shutdownHook01...
     */
    public static void test01() {
        System.out.println(Thread.currentThread().getName()+":start...");
        Thread thread = new Thread(()->{
            System.out.println("shutdownHook01...");
        });
        Runtime.getRuntime().addShutdownHook(thread);
        Runtime.getRuntime().exit(0);// int值随意
        System.out.println(Thread.currentThread().getName()+":end...");
    }
}
