package cn.jdemo.jsystem.system;

/**
 * 程序参数: String[] args
 * @date 2020/12/30
 */
public class Demo03 {
    public static void main(String[] args) {
        /*
         * 系统属性
         * 设置方法: 启动命令中设置, 主类前面, -D开头, 空格分隔
         * 查看所有系统属性: java -XshowSettings:properties
         *
         * 测试:
         * java -DsysPro1=1 -DsysPro2=2 -DsysPro3=3 demo.java.args.Demo
         *
         * VM options 一般以-D 、-X 或者-XX 开头，存在多个参数以空格隔开
         *
         * 例如(-XX:+ 或者-XX:- 某个属性值(+表示开启，-表示关闭)):https://blog.csdn.net/weixin_42416119/article/details/114098671
         *  -DsysPro1=1
         *  -Xms4G -Xmx4G -Xss256k -Xmn2g
         *  -XX:PermSize=128m -XX:+PrintGCDetails -XX:+PrintGCDateStamps
         *  -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDetails -XX:+PrintGCDateStamps
         *  -XX:-OmitStackTraceInFastThrow
         *
         */
        System.getProperties().forEach((k, v) -> {
            System.out.println(k + ": " + v);
            System.out.println("--------------------------");
            if (((String)k).startsWith("sysPro")) {
                System.out.println(k + ": " + v);
            }
        });
    }
}
