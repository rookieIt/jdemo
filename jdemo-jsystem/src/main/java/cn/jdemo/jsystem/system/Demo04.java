package cn.jdemo.jsystem.system;

/**
 * 程序参数
 * VM系统属性
 * 环境变量
 *
 * 如果要获取内存相关信息
 * @see cn.jdemo.jsystem.runtime.AllInOne
 * @see Runtime#exec(String) 执行脚本
 * @see Runtime#maxMemory()
 * @see Runtime#freeMemory()
 * @see Runtime#availableProcessors()
 *
 * 如果还要获取类加载数量之类的信息
 * @see cn.jdemo.jsystem.management.AllInOne
 * @see java.lang.management.ManagementFactory
 */
public class Demo04 {
    /**
     * ----------程序参数----------------
     * ------------VM（虚拟机系统属性---不包括-Xmx这些参数）--------------
     * sun.cpu.isalist: amd64
     * sun.desktop: windows
     * java.vendor.url.bug: http://bugreport.sun.com/bugreport/
     * file.separator: \
     * java.ext.dirs: D:\env\jdk8\jdk1.8.0_251\jre\lib\ext;C:\Windows\Sun\Java\lib\ext
     * java.version: 1.8.0_251
     * java.vm.info: mixed mode
     * user.language: zh
     * java.specification.vendor: Oracle Corporation
     * sun.java.command: cn.jdemo.jsystem.system.Demo04
     * java.home: D:\env\jdk8\jdk1.8.0_251\jre
     * sun.arch.data.model: 64
     * java.vm.specification.version: 1.8
     * ... ... ... ...
     * ------------环境变量--------------
     * JAVA_HOME: D:\env\jdk8\jdk1.8.0_251
     * GRADLE_HOME: D:\package\gradle\gradle-5.6.4
     * MAVEN_HOME: D:\env\apache-maven-3.6.3
     * USERNAME: zhangliuwei
     * Path: C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;D:\env\jdk8\jdk1.8.0_251\bin;D:\env\jdk8\jdk1.8.0_251\jre\bin;D:\env\apache-maven-3.6.3\bin;D:\env\Git\cmd;D:\util\node-v14.19.3-win-x64;D:\log\node\node_cache;D:\log\node\node_global;D:\package\gradle\gradle-5.6.4\bin;C:\Program Files\Docker\Docker\resources\bin;D:\package\MinGW\bin;D:\util\Python\Python27\Scripts\;D:\util\Python\Python27\;C:\Users\zhangliuwei\AppData\Local\Microsoft\WindowsApps;D:\package\MinGW\bin;
     * ... ... ... ...
     */
    public static void main(String[] args) {
        System.out.println("----------程序参数----------------");
        for (String arg:args) {
            System.out.println(arg);
        }
        System.out.println("------------VM（虚拟机系统属性---不包括-Xmx这些参数）--------------");
        System.getProperties().forEach((k, v) -> {
            System.out.println(k + ": " + v);
        });
        System.out.println("------------环境变量--------------");
        System.getenv().forEach((k, v) -> {
            System.out.println(k + ": " + v);
        });
    }
}
