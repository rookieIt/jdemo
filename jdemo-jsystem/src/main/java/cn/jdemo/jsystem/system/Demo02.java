package cn.jdemo.jsystem.system;

/**
 * 程序参数: String[] args
 * @date 2020/12/30
 */
public class Demo02 {
    public static void main(String[] args) {
        /*
         * 程序参数
         * 设置方法: 启动命令中设置, 主类后面, 空格分隔
         *
         * 测试:
         * java demo.java.args.Demo 1 2 3
         */
        for (int i = 0; i < args.length; i++) {
            System.out.println("proArg"+ i + ": " + args[i]);
        }
    }
}
