package cn.jdemo.jsystem.system;

/**
 * @see System
 * @see System#nanoTime()
 * @see System#currentTimeMillis()
 * @see System#arraycopy(Object, int, Object, int, int)
 * @see System#getSecurityManager() 
 * @see System#getProperties() 返回Properties --- VM(虚拟机系统属性：-D -X -XX)
 * @see System#getenv() 返回map ---环境变量
 * @see System#getenv(String) 返回string
 *
 * 优先级: 程序参数 > VM选项 > 环境变量
 *
 * @date 2020/12/30
 */
public class AllInOne {
}
