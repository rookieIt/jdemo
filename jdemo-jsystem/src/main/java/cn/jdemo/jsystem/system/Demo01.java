package cn.jdemo.jsystem.system;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 环境变量-1)读取系统/用户环境变量;2)启动程序时增加
 * @date 2020/12/30
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();

        /*
         * 环境变量
         * 设置方法: 启动命令外设置, 用export设置
         * 查看所有环境变量: export
         *
         * 测试:
         * export envVar1=1; export envVar2=2; export envVar3=3
         * java demo.java.args.Demo
         */


        System.getenv().forEach((k, v) -> {
            if (k.startsWith("envVar")) {
                System.out.println(k + ": " + v);
            }
        });
    }
    public static void test01() {
        Map<String, String> envs = System.getenv();
        Set<Map.Entry<String, String>> entries = envs.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
