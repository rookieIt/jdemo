package cn.jdemo.jsystem.system;

public class SecurityManagerDemo {
    public static void main(String[] args) {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager!=null){
            securityManager.checkExit(0);
        }else{
            System.out.println("securityManager 开启参数:"+"-Djava.security.manager");
        }
    }
}
