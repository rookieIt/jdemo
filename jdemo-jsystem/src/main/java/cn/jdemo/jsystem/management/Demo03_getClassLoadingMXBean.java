package cn.jdemo.jsystem.management;

import javax.management.ObjectName;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

/**
 *
 * @see ManagementFactory#getClassLoadingMXBean() 回Java虚拟机的类加载系统的托管Bean。
 *
 * @see ClassLoadingMXBean#getLoadedClassCount() // 返回Java虚拟机中当前加载的类数
 * @see ClassLoadingMXBean#getObjectName() // 该平台托管对象的对象名称
 * @see ClassLoadingMXBean#getTotalLoadedClassCount() // 加载的类总数
 * @see ClassLoadingMXBean#getUnloadedClassCount() // 返回自Java虚拟机开始执行以来已卸载的类的总数
 *
 * @date 2020/12/21
 */
public class Demo03_getClassLoadingMXBean {
    public static void main(String[] args) {
        test01();
    }

    /**
     * objectName: java.lang:type=ClassLoading
     * loadedClassCount: 534
     * totalLoadedClassCount: 534
     * unloadedClassCount: 0
     */
    public static void test01() {
        // 内存管理器
        ClassLoadingMXBean classLoadingMXBean = ManagementFactory.getClassLoadingMXBean();
        ObjectName objectName = classLoadingMXBean.getObjectName();
        int loadedClassCount = classLoadingMXBean.getLoadedClassCount();
        long totalLoadedClassCount = classLoadingMXBean.getTotalLoadedClassCount();
        long unloadedClassCount = classLoadingMXBean.getUnloadedClassCount();
        System.out.println("objectName: " + objectName);
        System.out.println("loadedClassCount: " + loadedClassCount);
        System.out.println("totalLoadedClassCount: " + totalLoadedClassCount);
        System.out.println("unloadedClassCount: " + unloadedClassCount);
    }
}
