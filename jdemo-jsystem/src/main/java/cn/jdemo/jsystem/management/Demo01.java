package cn.jdemo.jsystem.management;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

/**
 *
 * @see ManagementFactory#getMemoryMXBean() 返回Java虚拟机，内存系统的托管Bean
 *
 * @date 2020/12/21
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
    }
    public static void test01() {
        // 内存管理器
        MemoryMXBean memorymbean = ManagementFactory.getMemoryMXBean();
        System.out.println("HeapMemoryUsage: " + memorymbean.getHeapMemoryUsage());
        System.out.println("NonHeapMemoryUsage: " + memorymbean.getNonHeapMemoryUsage());

    }
}
