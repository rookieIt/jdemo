package cn.jdemo.jsystem.management;


import javax.management.MBeanServerConnection;
import java.lang.management.ManagementFactory;

/**
 * @see java.lang.management.ManagementFactory
 *
 * @see ManagementFactory#getGarbageCollectorMXBeans() // 虚拟机的垃圾回收器管理bean
 * @see ManagementFactory#getClassLoadingMXBean()
 * @see ManagementFactory#getCompilationMXBean()
 * @see ManagementFactory#getMemoryManagerMXBeans()
 * @see ManagementFactory#getMemoryPoolMXBeans()
 * @see ManagementFactory#getMemoryMXBean()
 * @see ManagementFactory#getOperatingSystemMXBean()
 * @see ManagementFactory#getRuntimeMXBean()
 * @see ManagementFactory#getThreadMXBean()
 *
 * @see ManagementFactory#getPlatformManagementInterfaces()
 * @see ManagementFactory#getPlatformMBeanServer()
 * @see ManagementFactory#getPlatformMXBean(Class)
 * @see ManagementFactory#getPlatformMXBean(MBeanServerConnection, Class)
 * @see ManagementFactory#getPlatformMXBeans(Class)
 * @see ManagementFactory#getPlatformMXBeans(MBeanServerConnection, Class)
 *
 * @date 2020/12/24
 */
public class AllInOne {
}
