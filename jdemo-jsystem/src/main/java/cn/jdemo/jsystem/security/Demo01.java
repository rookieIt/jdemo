package cn.jdemo.jsystem.security;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @see java.lang.Math#random() 底层调用Random
 * @see java.util.Random Random是线程安全的; Random不是密码学安全的(伪随机数（线性同余法生成）)
 * @see java.util.concurrent.ThreadLocalRandom 并发性能优于Random
 * @see java.security.SecureRandom 加密相关的推荐使用 SecureRandom (密码学意义上的安全随机数，要求必须保证其不可预测性--真随机数)
 *
 * SecureRandom在java各种组件中使用广泛，可以可靠的产生随机数;但在大量产生随机数的场景下，性能会较低。
 *
 * VM options配置 "-Djava.security.egd=file:/dev/./urandom"加快随机数产生过程。
 *
 * @date 2020/12/21
 */
public class Demo01 {
    public static void main(String[] args) {

    }

    public static void test01() throws NoSuchAlgorithmException {
    }

}
