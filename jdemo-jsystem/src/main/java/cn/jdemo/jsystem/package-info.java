/**
 * java基础系统类
 * @see java.lang.Runtime
 * @see java.lang.System
 * @see java.lang.management.ManagementFactory
 * @see java.lang.management.ManagementPermission
 * @see java.lang.SecurityManager
 *
 * @date 2020/12/18
 */
package cn.jdemo.jsystem;