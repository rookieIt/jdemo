package cn.jdemo.utils.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基于函数接口+::，以build的方式塞入属性值
 * @see cn.jdemo.pattern.builder.custom.App
 * @see cn.jdemo.pattern.builder.custom.Builder
 * @see cn.jdemo.pattern.builder.custom.UserVo
 *
 * @date 2021/1/28
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);


    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * 09:53:39.776 [main] INFO cn.jdemo.pattern.builder.custom.App - UserVo{id=1000, name='test', sex=male}
     */
    public static void test01() {
        UserVo userVo = Builder.of(UserVo::new).with(UserVo::setId, 1000)
                .with(UserVo::setName, "test")
                .with(UserVo::setSex, UserVo.Sex.male)
                .build();
        logger.info(userVo.toString());
    }
}