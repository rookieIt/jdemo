package cn.jdemo.utils.runCallable;

public abstract class MyRunnable implements Runnable{

    private Callable callable;

    MyRunnable(Callable callable){
        this.callable = callable;
    }
    MyRunnable(){
    }

    @Override
    public void run() {
        runBiz();
        if (callable!=null){
            callable.call();// 业务增强
        }
    }
    public abstract void runBiz();
}
