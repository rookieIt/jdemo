package cn.jdemo.utils.runCallable;

public interface Callable {
    void call();
}
