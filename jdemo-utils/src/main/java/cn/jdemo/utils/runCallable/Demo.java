package cn.jdemo.utils.runCallable;

public class Demo {
    public static void main(String[] args) {
        Thread thread = new Thread(new MyRunnable() {
            @Override
            public void runBiz() {
                System.out.println("业务执行");
            }
        });
        thread.start();
        Thread thread2 = new Thread(new MyRunnable(() -> {
            System.out.println("业务扩展");
        }) {
            @Override
            public void runBiz() {
                System.out.println("业务执行");
            }
        });
        thread2.start();

    }
}
