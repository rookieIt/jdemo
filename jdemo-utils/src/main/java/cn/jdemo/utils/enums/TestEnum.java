package cn.jdemo.utils.enums;


import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum TestEnum {

    XianXiaMenDian("1", "线下门店"),
    YiDianYiYe("2", "一店一页"),
    XianShangShangCheng("3", "线上商城"),
    DEFAULT("default", "未知");

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }
    public String getDesc() {
        return desc;
    }

    private TestEnum(String code, String desc){
        this.code = code;
        this.desc = desc;
    }


    private static final Map<String, TestEnum> nameMap = Arrays.stream(values()).collect(Collectors.toMap(TestEnum::name, Function.identity()));

    public static TestEnum getByName(String name) {
        return nameMap.get(name);
    }

    public static TestEnum find(String key) {
        if (key == null || "".equals(key)) {
            return DEFAULT;
        }
        for(TestEnum value:values()){
            if (key.equals(value.getCode())) {
                return value;
            }
        }
        return DEFAULT;
    }
}
