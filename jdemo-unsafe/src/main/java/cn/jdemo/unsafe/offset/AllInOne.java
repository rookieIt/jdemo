package cn.jdemo.unsafe.offset;


import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * 获取相对于内存地址的偏移量
 * @see Unsafe#staticFieldOffset(Field) 静态变量相对于类内存地址的偏移量
 * @see Unsafe#objectFieldOffset(Field) 非静态变量相对于实例化对象的偏移量
 * @see Unsafe#fieldOffset(Field) 过期，不使用
 *
 * 基于偏移量修改(静态/非静态)变量值
 * @see Unsafe#putInt(Object, long offset, int)
 *  示例： theUnasfe.putInt(A.class, valOffset1, 2);
 *  示例： theUnasfe.putInt(a, valOffset2, 4);
 *  (1) putXXX(Object,long offset,int) object是具体对象/类,offset是偏移量,最后参数是要赋的值
 * @see Unsafe#putBoolean(Object, long, boolean)
 * @see Unsafe#putLong(Object, long, long)
 * @see Unsafe#putObject(Object, long, Object)
 * @see Unsafe#putDouble(Object, long, double)
 * @see Unsafe#putFloat(Object, long, float)
 * @see Unsafe#putChar(Object, long, char)
 * @see Unsafe#putByte(Object, long, byte)
 *
 * @date 2021/1/15
 */
public class AllInOne {
}
