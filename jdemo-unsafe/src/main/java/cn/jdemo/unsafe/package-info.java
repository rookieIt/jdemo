/**
 * Unsafe类是java提供的，对系统硬件级别的底层操作；
 *
 * @see sun.misc.Unsafe#getUnsafe()
 *
 * @see sun.misc.Unsafe#compareAndSwapInt(java.lang.Object, long, int, int) 
 * @see sun.misc.Unsafe#compareAndSwapLong(java.lang.Object, long, long, long) 
 * @see sun.misc.Unsafe#compareAndSwapObject(java.lang.Object, long, java.lang.Object, java.lang.Object)
 *
 * 操作的内存,是不归JVM管理的
 * @see sun.misc.Unsafe#allocateMemory(long) 申请内存
 * @see sun.misc.Unsafe#reallocateMemory(long, long) 扩充内存
 * @see sun.misc.Unsafe#freeMemory(long) 释放内存(内存释放后,在put值会JVM崩溃,但此时仍然可以基于内存地址获取数据)
 *
 * 示例,其他类型类似(但boolean型没有针对申请内存的操作)
 * @see sun.misc.Unsafe#putByte(long addr, byte) 基于申请的内存地址,赋值
 * @see sun.misc.Unsafe#putByte(java.lang.Object, long offset, byte) 基于当前对象/类变量 + 内存相对偏移量,赋值
 * @see sun.misc.Unsafe#getByte(long addr) 基于申请的内存地址,获取
 * @see sun.misc.Unsafe#getByte(java.lang.Object, long offset)   基于当前对象/类变量 + 内存相对偏移量,获取
 * 
 * @see sun.misc.Unsafe#park(boolean, long timeout) 
 * @see sun.misc.Unsafe#unpark(java.lang.Object thread)
 *
 * @date 2021/1/4
 */
package cn.jdemo.unsafe;