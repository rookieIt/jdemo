package cn.jdemo.unsafe.park;

/**
 * @see sun.misc.Unsafe#unpark(Object thread)
 * @see sun.misc.Unsafe#park(boolean, long timeout)
 * @date 2021/1/15
 */
public class Demo01 {

    public static void main(String[] args) {

    }
    /**
     * Thread thread = new Thread(()->{
     *         unsafe.park(false, 0);//永远挂起
     *     });
     *         thread.start();
     *
     *         unsafe.unpark(thread);//恢复线程
     */
    public static void test01() {

    }
}
