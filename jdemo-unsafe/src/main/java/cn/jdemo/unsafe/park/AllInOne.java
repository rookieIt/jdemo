package cn.jdemo.unsafe.park;

/**
 * unsafe提供的线程挂起和恢复
 *
 * @see sun.misc.Unsafe#park(boolean abs, long timeout)
 *   参数说明:???
 *      (1) abs为false时，表示timeout以纳秒为单位,若设置timeout为0，表示永远挂起，直到interrupt或则unpark
 *      (2) abs为true时，表示timeout以毫秒为单位(注意，经测试在abs为true时，将timeout设置为0，线程会立即返回)
 *      (3) timeout：指时间点，该时间点从1970计数开始；
 *
 * @see sun.misc.Unsafe#unpark(Object thread)
 *      (1) 将给定的线程从挂起状态恢复到运行状态;
 *      (2) 特殊：unpark可以在park之前使用(使用一次或多次,作为参数的thread线程始终将只获得一个运行许可),
 *              当park方法调用时，若检测到该线程存在一个运行许可，park方法也会立即返回；
 *              （这种方式在多线程中虽然很灵活，相对于notify/wait的方式，但不建议如此使用）
 *
 * @date 2021/1/15
 */
public class AllInOne {
}
