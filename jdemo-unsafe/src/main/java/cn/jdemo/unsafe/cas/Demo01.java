package cn.jdemo.unsafe.cas;

/**
 *
 * //3.0关于并发对变量的原子操作，请查看其它资料；unsafe 提供硬件级别的原子操作CAS方法，如：compareAndSwapInt(Object ,long ,int ,int)
 *         //说明： 第一个参数：需要更新的对象；第二个参数：偏移地址； 第三个对象：预期在该偏移地址上的当前值，即：getInt(obj,偏移地址) == 预期值； 第四个参数：需要更新的值
 *         //此类方法，当且仅当当前偏移量的值等于预期值时，才更新为给定值；否则不做任何改变；
 *         //compareAndSwapObject 和 compareAndSwapLong 与下述示例类似；
 *         long offset = unsafe.allocateMemory(1);
 *
 *         unsafe.putInt(Integer.class, offset, 1);
 *
 *         System.out.println(unsafe.getInt(Integer.class, offset));
 *
 *         boolean updateState = unsafe.compareAndSwapInt(Integer.class, offset, 1, 5);
 *         System.out.println("update state = "+ updateState +" ; value = " + unsafe.getInt(Integer.class,offset));
 *
 *         unsafe.freeMemory(offset);
 * @date 2021/1/15
 */
public class Demo01 {
    public static void main(String[] args) {

    }
    public static void test01() {

    }
}
