package cn.jdemo.unsafe.cas;

/**
 * 硬件级别的原子性更新操作
 * @see sun.misc.Unsafe#compareAndSwapInt(Object, long, int, int) 
 * @see sun.misc.Unsafe#compareAndSwapLong(Object, long, long, long) 
 * @see sun.misc.Unsafe#compareAndSwapObject(Object, long, Object, Object) 
 *
 * @date 2021/1/15
 */
public class AllInOne {
}
