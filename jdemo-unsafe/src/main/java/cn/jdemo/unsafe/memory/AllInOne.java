package cn.jdemo.unsafe.memory;


import sun.misc.Unsafe;

/**
 * 申请的内存地址,不归JVM管理
 *
 * @see Unsafe#allocateMemory(long)
 *  (1)向本地系统申请一块内存地址;(2)返回内存地址的起始地址(3)内存的使用脱离jvm，gc无法管理,故要要手动释放内存，避免内存溢出;
 * @see Unsafe#freeMemory(long)
 *  (1) 释放给定内存地址的内存，若在使用改地址，JVM崩溃报错
 * @see Unsafe#reallocateMemory(long address, long size)
 *  (1) 释放给定内存地址所使用的内存;(2)重新申请给定大小的内存
 *
 * @see Unsafe#copyMemory(long, long, long)
 * @see Unsafe#copyMemory(Object, long, Object, long, long) 
 * @see Unsafe#setMemory(long, long, byte)
 * @see Unsafe#setMemory(Object, long, long, byte)
 *
 * @date 2021/1/15
 */
public class AllInOne {
}
