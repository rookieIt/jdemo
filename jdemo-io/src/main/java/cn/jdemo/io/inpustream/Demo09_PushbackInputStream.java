package cn.jdemo.io.inpustream;


import java.io.*;

/**
 * @see InputStream 抽象基类
 * @see FilterInputStream 基类
 * @see PushbackInputStream
 *
 * // 构造方法
 * @see PushbackInputStream#PushbackInputStream(InputStream) 推回缓冲区的大小默认为1（只能退回一个字节）
 * @see PushbackInputStream#PushbackInputStream(InputStream, int) 推回缓冲区的大小为设置的int值
 *
 * // 个性方法
 * @see PushbackInputStream#unread(int) 退回一个指定字节
 * @see PushbackInputStream#unread(byte[]) 退回一个指定数组
 * @see PushbackInputStream#unread(byte[], int, int)
 *
 *
 * @date 2020/12/21
 */
public class Demo09_PushbackInputStream {
    public static void main(String[] args) throws Exception {
        // test01();
        // test02();
        // test03();
        // test04();
        test05();
    }

    /**
     * @see PushbackInputStream#PushbackInputStream(InputStream) 只能推回一个字节
     * @see java.io.PushbackInputStream#unread(int)
     */
    public static void test01() throws Exception {
        byte[] buf = new String("abc中文efg").getBytes();
        PushbackInputStream in = new PushbackInputStream(new ByteArrayInputStream(buf));
        int read01 = in.read();// 读取一个字节
        System.out.println(new Character((char)read01));// a
        int read02 = in.read();// 读取一个字节
        System.out.println(new Character((char)read02));// b

        in.unread(read01+10);// 退回一个指定字节
        int read03 = in.read();// 读取一个字节
        System.out.println(new Character((char)read03));// k
    }

    /**
     * @see PushbackInputStream#PushbackInputStream(InputStream) 只能推回一个字节
     * @see java.io.PushbackInputStream#unread(int)
     */
    public static void test02() throws Exception {
        byte[] buf = new String("abc中文efg").getBytes();
        PushbackInputStream in = new PushbackInputStream(new ByteArrayInputStream(buf));
        in.unread(new Character('a')+10);// 退回一个指定字节
        int read01 = in.read();// 读取一个字节
        System.out.println(new Character((char)read01));// k
        int read02 = in.read();// 读取一个字节
        System.out.println(new Character((char)read02));// a
    }

    /**
     * @see PushbackInputStream#PushbackInputStream(InputStream) 只能推回一个字节
     * @see java.io.PushbackInputStream#unread(int)
     *
     * 输出结果:
     *   Exception in thread "main" java.io.IOException: Push back buffer is full
     */
    public static void test03() throws Exception {
        byte[] buf = new String("abc中文efg").getBytes();
        PushbackInputStream in = new PushbackInputStream(new ByteArrayInputStream(buf));
        in.unread(new Character('a')+10);// 退回一个指定字节
        in.unread(new Character('a')+11);// 在退回一个指定字节
    }

    /**
     * @see PushbackInputStream#PushbackInputStream(InputStream) 只能推回一个字节
     * @see java.io.PushbackInputStream#unread(byte[])
     *
     * 输出结果:
     *   Exception in thread "main" java.io.IOException: Push back buffer is full
     */
    public static void test04() throws Exception {
        byte[] buf = new String("abc中文efg").getBytes();
        PushbackInputStream in = new PushbackInputStream(new ByteArrayInputStream(buf));
        byte[] b = new byte[]{'x','y','z'};
        in.unread(b);// Exception in thread "main" java.io.IOException: Push back buffer is full
    }

    /**
     * @see PushbackInputStream#PushbackInputStream(InputStream, int) 推回指定数量字节
     * @see java.io.PushbackInputStream#unread(byte[])
     * 输出结果:
     *   xyzabc中文efg
     */
    public static void test05() throws Exception {
        byte[] buf = new String("abc中文efg").getBytes();
        PushbackInputStream in = new PushbackInputStream(new ByteArrayInputStream(buf),5);

        // 推回3个字节
        byte[] b = new byte[]{'x','y','z'};
        in.unread(b);

        byte[] tempBytes = new byte[3];
        int temp;
        while ((temp = in.read(tempBytes, 0, tempBytes.length))!=-1){
            System.out.print( new String(tempBytes, 0, temp));
        }
        in.close();
    }
}
