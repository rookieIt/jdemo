package cn.jdemo.io.inpustream;

import java.io.*;

/**
 * @see InputStream 抽象基类
 * @see ByteArrayInputStream 传入byte[]
 * (1)实例化需要byte[]
 * (2)也支持mark(标记)
 * (3)也支持skip
 *
 * //构造方法
 * @see ByteArrayInputStream#ByteArrayInputStream(byte[])
 * @see ByteArrayInputStream#ByteArrayInputStream(byte[], int, int)
 *
 * 同步对象方法
 * @see ByteArrayInputStream#read()//返回值,读取的一个字节
 * @see ByteArrayInputStream#read(byte[] b, int off, int len) //(1)返回值，读入缓冲区的字节总数 (2)读取多个字节到b
 * @see ByteArrayInputStream#available() //下一字节是否可读
 * @see ByteArrayInputStream#mark(int readlimit) //标记, readlimit为mark后最多可读取的字节数;(打标记，重置当前位置到标记的位置重新读取数据)
 * @see ByteArrayInputStream#reset()//重置回mark位置
 * @see ByteArrayInputStream#skip(long n)//跳过n个字节
 * 对象方法
 * @see ByteArrayInputStream#close() 空方法
 * @see ByteArrayInputStream#markSupported()//是否支持mark, true
 *
 * @date 2020/12/21
 */

/**
 * 补充:多次使用流
 *  InputStream input =  httpconn.getInputStream();
 *
 *  ByteArrayOutputStream baos = new ByteArrayOutputStream();
 *  byte[] buffer = new byte[1024];
 *  int len;
 *  while ((len = input.read(buffer)) > -1 ) {
 *     baos.write(buffer, 0, len);
 *  }
 *  baos.flush();
 *
 *  InputStream stream1 = new ByteArrayInputStream(baos.toByteArray());
 *  //TODO:显示到前台
 *  InputStream stream2 = new ByteArrayInputStream(baos.toByteArray());
 *  //TODO:本地缓存
 */
public class Demo02_ByteArrayInputStream {
    public static void main(String[] args) throws IOException {
        // test01();
        // test02();
        test03();
    }

    public static void test01() throws IOException {
        byte[] buf = new String("abcdef中文123").getBytes();// 默认编码取: file.encoding 的 Vm Options
        ByteArrayInputStream bais = new ByteArrayInputStream(buf);
        int read = bais.read();

        //强转
        Character character = new Character((char) read);
        System.out.println("读取的一个字节(int): "+read);// 97
        System.out.println("读取的一个字节(Character): "+character);// a
        bais.close();
    }

    public static void test02() throws IOException {
        byte[] buf = new String("中abcd123").getBytes();// 默认编码取: file.encoding 的 Vm Options
        ByteArrayInputStream bais = new ByteArrayInputStream(buf);
        int read = bais.read();

        Character character = new Character((char) read);
        System.out.println("读取的一个字节(int): "+ read);// 输出:228
        System.out.println("读取的一个字节(Character): " + character);// 输出:ä
        bais.close();
    }

    public static void test03() throws IOException {
        byte[] buf = new String("中abcd123").getBytes();// 默认编码取: file.encoding 的 Vm Options
        ByteArrayInputStream bais = new ByteArrayInputStream(buf);
        byte[] b = new byte[3];
        int read = bais.read(b);

        System.out.println("读取的字节数量: "+read);// 3
        System.out.println("字节数组b中读取的内容:"+new String(b));// 中
        bais.close();
    }

    /**
     * 其他类似... ...
     * @see cn.jdemo.io.inpustream.Demo01
      */
}
