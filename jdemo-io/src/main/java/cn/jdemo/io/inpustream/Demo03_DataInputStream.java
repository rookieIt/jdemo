package cn.jdemo.io.inpustream;

import java.io.*;

/**
 *
 *
 * @see InputStream 抽象基类
 * @see java.io.DataInput DataInputStream的父接口
 * @see FilterInputStream DataInputStream的父类
 * @see java.io.DataInputStream 数据是格式化的
 * (1)实例化需要byte[]
 * (2)也支持mark(标记)
 * (3)也支持skip
 *
 * // 构造方法
 * @see java.io.DataInputStream#DataInputStream(InputStream in)
 *
 * // 包含inputstream常用方法
 *
 * // 有别于其他的方法，实现了DataInput的方法
 * @see java.io.DataInputStream#readFully(byte[] bytes) bytes过大，则会报异常，和InputStream流有关
 * @see cn.jdemo.io.outputstream.Demo03#test03()
 * final boolean     readBoolean()
 * final byte     readByte()
 * final char     readChar()
 * final double     readDouble()
 * final float     readFloat()
 * final void     readFully(byte[] dst)
 * final void     readFully(byte[] dst, int offset, int byteCount)
 * final int     readInt()
 * final String     readLine()
 * final long     readLong()
 * final short     readShort()
 * final static String     readUTF(DataInput in)
 *
 *
 * final String     readUTF() readUTF()的作用，是从输入流中读取UTF-8编码的数据，并以String字符串的形式返回。
 * final int     readUnsignedByte()
 * final int     readUnsignedShort()
 * final int     skipBytes(int count)
 *
 *
 * @date 2020/12/21
 */
public class Demo03_DataInputStream {
    public static void main(String[] args) throws IOException {
        // test01();
        test02();
    }

    /**
     * 输出结果:
     * Exception in thread "main" java.io.EOFException
     * @see java.io.DataInputStream#readFully(byte[] bytes) bytes过大，则会报异常
     */
    public static void test01() throws IOException {
        InputStream inputStream = new FileInputStream("D:\\test.txt");
        DataInputStream dataIS = new DataInputStream(inputStream);

        System.out.println("-------------readFully()方法-----------");
        byte[] b = new byte[1024];
        dataIS.readFully(b);// Exception in thread "main" java.io.EOFException
        System.out.println(new String(b));
        System.out.println("-------------readLine()方法------------");
        System.out.println(dataIS.readLine());// 乱码

        dataIS.close();
    }

    /**
     * 输出结果:
     * -------------readFully()方法-----------
     * a
     * -------------readLine()方法------------
     *  a
     */
    public static void test02() throws IOException {
        InputStream inputStream = new FileInputStream("D:\\test.txt");
        DataInputStream dataIS = new DataInputStream(inputStream);

        System.out.println("-------------readFully()方法-----------");
        byte[] b = new byte[2];
        dataIS.readFully(b);// Exception in thread "main" java.io.EOFException
        System.out.println(new String(b));
        System.out.println("-------------readLine()方法------------");
        System.out.println(dataIS.readLine());// 乱码
        dataIS.close();
    }

}
