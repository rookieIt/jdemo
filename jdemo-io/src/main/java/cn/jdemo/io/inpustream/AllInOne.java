package cn.jdemo.io.inpustream;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

/**
 * @see java.io.InputStream 抽象基类，针对byte处理
 *
 * 构造方法(默认)
 * @see java.io.InputStream#InputStream()
 *
 * 基本常用方法如下
 * @see java.io.InputStream#read() (抽象方法)从输入流读取数据的下一个字节
 * @see java.io.InputStream#read(byte[]) 
 * @see java.io.InputStream#read(byte[], int, int)
 * @see java.io.InputStream#mark(int readlimit) 标记
 * @see java.io.InputStream#markSupported()
 * @see java.io.InputStream#reset() 重置
 * @see java.io.InputStream#skip(long)  跳过并丢弃此输入流的n个字节
 * @see java.io.InputStream#available() 从下一次调用此输入流的方法返回可从该输入流读取（或跳过）的字节数，而不会阻塞。
 *
 * 常用subclass
 * @see java.io.BufferedInputStream
 *      @see BufferedInputStream#fill() 内部调用了底层InputStream
 * @see java.io.ByteArrayInputStream
 *      @see ByteArrayInputStream#read() 实现了基于内存的底层InputStream
 *      @see ByteArrayInputStream#read(byte[], int, int) 实现了基于内存的底层InputStream
 * @see java.io.DataInputStream
 * @see java.io.FileInputStream
 *      @see FileInputStream#read0() native方法
 *      @see FileInputStream#readBytes(byte[], int, int) native方法
 * @see java.io.FilterInputStream
 * @see java.io.LineNumberInputStream
 * @see java.io.ObjectInputStream
 * @see java.io.PipedInputStream
 * @see java.io.PushbackInputStream
 * @see java.io.SequenceInputStream
 * @see java.io.StringBufferInputStream
 *
 * @date 2020/12/21
 */
public class AllInOne {
}
