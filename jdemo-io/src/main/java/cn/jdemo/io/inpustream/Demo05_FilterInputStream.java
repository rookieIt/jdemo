package cn.jdemo.io.inpustream;

import java.io.FilterInputStream;
import java.io.InputStream;

/**
 * @see InputStream 抽象基类
 * @see FilterInputStream 过滤输入流
 *
 * 其常见的子类有DataInputStream,BufferedInputStream以及PushBackInputStream等.其子类总结为:
 * DataInputStream 数据输入流,以机器无关的方式读取Java的基本类型.
 * BufferedInputStream 缓冲输入流,由于基础输入流一个字节一个字节读取,频繁与磁盘进行交互,造成读取速度较低.缓冲流的存在就是先将数据读取到缓冲流(内存中),然后一次性从内存中读取多个字符.提高读取的效率.
 * PushInputStream 回退输入流,java中读取数据的方式是顺序读取,如果某个数据不需要读取,需要程序处理.PushBackInputStream就可以将某些不需要的数据回退到缓冲中.
 * LineNumberInputStream
 * // 构造方法
 * @see java.io.FilterInputStream#FilterInputStream(InputStream) protected修饰，非子类/非同包下不能访问
 *
 * @date 2020/12/21
 */
public class Demo05_FilterInputStream {
    public static void main(String[] args) throws Exception {
        // test01();
    }
    public static void test01() throws Exception {
        // 无
    }
}
