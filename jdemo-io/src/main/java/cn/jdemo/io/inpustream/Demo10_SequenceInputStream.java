package cn.jdemo.io.inpustream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Enumeration;
import java.util.Vector;

/**
 * @see InputStream 抽象基类
 * @see java.io.SequenceInputStream 基于多个输入流的有序集合，依次读取;
 *
 * // 构造方法
 * @see java.io.SequenceInputStream#SequenceInputStream(InputStream, InputStream) 
 * @see java.io.SequenceInputStream#SequenceInputStream(Enumeration)
 *
 *
 * @date 2020/12/21
 */
public class Demo10_SequenceInputStream {
    public static void main(String[] args) throws Exception {
        // test01();
        test02();
    }
    public static void test01() throws Exception {
        SequenceInputStream in =
                new SequenceInputStream(new FileInputStream("D:\\test.txt"),new FileInputStream("D:\\1.txt"));

        byte[] b = new byte[1024];
        int temp;
        while ((temp = in.read(b, 0, b.length))!=-1){
            System.out.println( new String(b, 0, temp));
        }
        in.close();
    }
    public static void test02() throws Exception {
        Vector<InputStream> vectors = new Vector<InputStream>();
        vectors.add(new FileInputStream("D:\\test.txt"));
        vectors.add(new FileInputStream("D:\\1.txt"));
        Enumeration<InputStream> elements = vectors.elements();
        SequenceInputStream in = new SequenceInputStream(elements);

        byte[] b = new byte[1024];
        int temp;
        while ((temp = in.read(b, 0, b.length))!=-1){
            System.out.println( new String(b, 0, temp));
        }
        in.close();
    }
}
