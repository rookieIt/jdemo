package cn.jdemo.io.inpustream;

import java.io.*;

/**
 * @see InputStream 抽象基类
 * @see java.io.FilterInputStream 基类
 * @see java.io.LineNumberInputStream 增加了跟踪行号的附加功能
 *
 * // 构造方法
 * @see java.io.LineNumberInputStream#LineNumberInputStream(InputStream)
 *
 * // 个性方法
 * @see LineNumberReader#getLineNumber() 获取当前行号
 *
 * @date 2020/12/21
 */
public class Demo06_LineNumberInputStream {
    public static void main(String[] args) throws Exception {
        test01();
    }
    public static void test01() throws Exception {
        LineNumberInputStream lineNumIn = new LineNumberInputStream(new FileInputStream("D:\\test.txt"));

        byte[] b = new byte[8];
        int temp;
        while ((temp = lineNumIn.read(b, 0, b.length))!=-1){
            int lineNumber = lineNumIn.getLineNumber();
            System.out.println("[line"+lineNumber+"]" + new String(b, 0, temp));
        }
    }
}
