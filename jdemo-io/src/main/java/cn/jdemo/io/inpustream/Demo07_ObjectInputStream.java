package cn.jdemo.io.inpustream;

import java.io.*;

/**
 *
 * @see ObjectInput 父接口
 * @see DataInput 父接口
 * @see ObjectStreamConstants 父接口
 * @see InputStream 抽象基类
 *
 * @see ObjectInputStream 基于序列、反序列以及FileOutputStream和FileInputStream 实现持久存储;
 * (1)实例化需要InputStream流对象
 * (2)用于将数据从任何Java原语类型转换为一系列字节
 * (3)ObjectOutputStream与ObjectInputStream配合，可进行文件序列化：
 *        (a)持久到磁盘---与FileOutputStream、FileInputStream配合使用;
 *        @see cn.jdemo.io.outputstream.Demo06_ObjectOutputStream#test03()
 *        (b)保存到内存(深度克隆)---与ByteArrayOutputStream、ByteArrayInputStream配合使用
 *        @see cn.jdemo.io.util.CloneUtil#clongObj(Object)
 *
 * // 构造方法
 * @see ObjectInputStream#ObjectInputStream(InputStream)
 * @see ObjectInputStream#ObjectInputStream() // protected方法，非子类和非同包下无法直接实例化
 *
 * @see ObjectInputStream#readObject()
 *
 * @date 2020/12/21
 */
public class Demo07_ObjectInputStream {
    public static void main(String[] args) {
        // test01();
    }

    /**
     * @see cn.jdemo.io.outputstream.Demo06_ObjectOutputStream
     */
    public static Object test01(String path) {
        Object object = null;
        try(ObjectInputStream objectIs = new ObjectInputStream(new FileInputStream(path));){

            object = objectIs.readObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }
}
