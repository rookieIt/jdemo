package cn.jdemo.io.inpustream;

import java.io.IOException;
import java.io.InputStream;

/**
 * @see InputStream
 *
 * 构造方法
 * @see InputStream#InputStream()
 *
 * 抽象方法
 * @see InputStream#read()
 *
 * 须知:
 *   (1)其子类都是对read()方法的实现
 *   (2)其子类具体对read()方法底层逻辑实现的有:FileInputStream(读取磁盘io)、ByteArrayInputStream(基于内存io)
 *   (3)其他子类，实例化需要传入InputStream参数的，基本是对底层io的进一步封装(不与底层直接交互)，调用的是传入的InputStream参数的read()方法，进行底层调用
 *
 * @date 2020/12/25
 */
public class Demo00_InputStream {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        InputStream inputStream = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };

        try {
            System.out.println(inputStream.read());//0
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
