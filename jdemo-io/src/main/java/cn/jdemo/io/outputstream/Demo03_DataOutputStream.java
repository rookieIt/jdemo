package cn.jdemo.io.outputstream;

import java.io.*;

/**
 *
 * @see OutputStream 抽象基类
 * @see DataOutput DataOutputStream的父接口
 * @see FilterOutputStream DataOutputStream的父类
 * @see DataOutputStream 数据是格式化的
 * (1)实例化需要OutputStream流对象
 * (2)DataOutputStream 用于将数据从任何Java原语类型转换为一系列字节
 * (3)与DataInputStream配合，可进行写入读取
 *
 * // 构造方法
 * @see DataOutputStream#DataOutputStream(OutputStream)
 *
 * @see DataOutputStream#write(int)
 * @see DataOutputStream#write(byte[])
 * @see DataOutputStream#write(byte[], int, int)
 * @see DataOutputStream#incCount(int) (wirte时，计算字节长度)将写入的计数器增加指定的值，直到达到Integer.MAX_VALUE
 *
 * // 有别于其他的方法，实现了DataInput的方法
 * @see DataOutputStream#writeBoolean(boolean)
 * @see DataOutputStream#writeByte(int)
 * @see DataOutputStream#writeBytes(String)
 * @see DataOutputStream#writeChar(int)
 * @see DataOutputStream#writeChars(String)
 * @see DataOutputStream#writeDouble(double)
 * @see DataOutputStream#writeFloat(float)
 * @see DataOutputStream#writeInt(int)
 * @see DataOutputStream#writeLong(long)
 * @see DataOutputStream#writeShort(int)
 * @see DataOutputStream#writeUTF(String)
 *
 * // 包含inputstream常用方法
 *
 * @date 2020/12/21
 */
public class Demo03_DataOutputStream {
    public static void main(String[] args) throws IOException {
        // test01();
        test02();
    }

    public static void test01() throws IOException {
        // 写入
        FileOutputStream fileOS = new FileOutputStream("D:\\test.txt");
        DataOutputStream dataOS = new DataOutputStream(fileOS);
        dataOS.writeDouble(1.11d);
        dataOS.writeDouble(2.22d);
        dataOS.close();

        // 读取
        DataInputStream in = new DataInputStream(new FileInputStream("D:\\test.txt"));
        System.out.println(in.readDouble());// 1.11
        System.out.println(in.readDouble());// 2.22
    }

    /**
     * 输出结果(写入和读取类型一致):
     * true
     * 97
     * a
     * 10
     * 20
     * 97
     * 97.97
     * 97.97
     * abc97中文97def
     */
    public static void test02() throws IOException {
        FileOutputStream fileOS = new FileOutputStream("D:\\test.txt");
        DataOutputStream dataOS = new DataOutputStream(fileOS);
        dataOS.writeBoolean(true);
        dataOS.writeByte(97);
        dataOS.writeChar( 97);

        dataOS.writeShort(10);
        dataOS.writeInt(20);

        dataOS.writeLong(97L);
        dataOS.writeDouble(97.97d);
        dataOS.writeDouble(97.97d);
        dataOS.writeUTF("abc97中文97def");
        dataOS.close();

        DataInputStream in = new DataInputStream(new FileInputStream("D:\\test.txt"));
        System.out.println(in.readBoolean());//true
        System.out.println(in.readByte());// 97
        System.out.println(in.readChar());// a

        System.out.println(in.readShort());// 10
        System.out.println(in.readInt());// 20

        System.out.println(in.readLong());// 97
        System.out.println(in.readDouble());// 97.97
        System.out.println(in.readDouble());// 97.97
        System.out.println(in.readUTF());// abc97中文97def
        in.close();
    }

    /**
     * 输出结果(写入和读取类型不一致时)
     *  true
     * 97
     * a
     * 655360
     * 20
     * 97
     * 4636594442803955630
     * 97.97
     * 2.2779760360442024E-308
     * Exception in thread "main" java.io.EOFException
     *
     */
    public static void test03() throws IOException {
        FileOutputStream fileOS = new FileOutputStream("D:\\test.txt");
        DataOutputStream dataOS = new DataOutputStream(fileOS);
        dataOS.writeBoolean(true);
        dataOS.writeByte(97);
        dataOS.writeChar( 97);

        dataOS.writeShort(10);
        dataOS.writeInt(20);

        dataOS.writeLong(97L);
        dataOS.writeDouble(97.97d);
        dataOS.writeDouble(97.97d);
        dataOS.writeUTF("abc97中文97def");
        dataOS.close();

        DataInputStream in = new DataInputStream(new FileInputStream("D:\\test.txt"));
        System.out.println(in.readBoolean());//true
        System.out.println(in.readByte());// 97
        System.out.println(in.readChar());// a

        System.out.println(in.readInt());// 655360
        System.out.println(in.readShort());// 20

        System.out.println(in.readLong());// 97
        System.out.println(in.readLong());// 4636594442803955630
        System.out.println(in.readDouble());// 97.97
        System.out.println(in.readDouble());// 2.2779760360442024E-308
        System.out.println(in.readUTF());// java.io.EOFException
        in.close();
    }
}
