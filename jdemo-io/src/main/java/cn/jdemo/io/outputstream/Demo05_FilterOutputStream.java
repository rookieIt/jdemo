package cn.jdemo.io.outputstream;

import java.io.*;

/**
 * @see OutputStream
 * @see java.io.FilterOutputStream 过滤输出流,简单重写了OutputStream中的方法
 *
 * // 构造方法
 * @see java.io.FilterOutputStream#FilterOutputStream(OutputStream)
 *
 * @date 2020/12/21
 */
public class Demo05_FilterOutputStream {
    public static void main(String[] args) throws Exception {
        // test01();
    }
    public static void test01() throws Exception {
        // 无
    }
}
