package cn.jdemo.io.outputstream;

import java.io.*;

/**
 * @see OutputStream FilterOutputStream的基类
 * @see FilterOutputStream BufferedOutputStream的基类
 * @see BufferedOutputStream (传入io流，缓存读取):
 * (1)实例化时绑定输出流对象(即OutputStream对象参数)
 * (2)内部有缓存，默认是8192，可以指定；
 * (3)刷新内部buf缓存到输出流对象:(1)write到buf满时调用(2)手动调用flush()(3)close时内部也会调用flush()
 *
 * //构造方法
 * @see BufferedOutputStream#BufferedOutputStream(OutputStream out) DEFAULT_BUFFER_SIZE = 8192 = 8*1024
 * @see BufferedOutputStream#BufferedOutputStream(OutputStream out,int size)  size为内部缓存大小
 *
 * 同步对象方法
 * @see BufferedOutputStream#write(int) 写入一个字节
 * @see BufferedOutputStream#write(byte[]) 写入多个字节
 * @see BufferedOutputStream#write(byte[], int, int) 写入多个字节
 * @see BufferedOutputStream#flush() 刷新内部buf缓存到输出流
 * @see BufferedOutputStream#flushBuffer() 刷新内部buf缓存到输出流:(1)write到buf满时调用(2)手动调用flush()(3)close时内部会调用flush()
 * 对象方法
 * @see BufferedOutputStream#close() 刷新内部缓存并关闭相关系统资源(绑定的输出流对象关闭)
 *
 * @date 2020/12/21
 */
public class Demo01_BufferedOutputStream {
    public static void main(String[] args) throws IOException {
        // test01();
        test02();
    }

    public static void test01() throws IOException {
        BufferedOutputStream bufferedOS = new BufferedOutputStream(new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                System.out.println("自定义OutputStream,write:"+new Character((char) b));
            }
        });
        bufferedOS.write(97);
        bufferedOS.close();
    }

    /**
     * @see BufferedOutputStream#write(byte[])
     * @see BufferedOutputStream#flushBuffer()
     * @see BufferedOutputStream#close()
     *
     * 关闭输出流对象和相关系统资源
     * @see FileOutputStream#close()
     * @see FileOutputStream#close0()
     */
    public static void test02() throws IOException {
        BufferedOutputStream bufferedOS = new BufferedOutputStream(new FileOutputStream("D:\\test.txt"));
        bufferedOS.write(new String("insert \r\n").getBytes());// 直接覆盖了原有test.txt内容
        bufferedOS.close();
    }

}
