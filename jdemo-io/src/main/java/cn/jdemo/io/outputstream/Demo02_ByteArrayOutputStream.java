package cn.jdemo.io.outputstream;

import java.io.*;

/**
 * @see OutputStream 抽象基类
 * @see ByteArrayOutputStream 传入byte[]
 * (1)内置存储数据的缓存区，默认是32字节,可以手动指定;
 * (2)内置存储数据的缓存区，填满扩容
 * (3)基于内存的io流，不需要回收资源，所以close()为空 【gc会回收内存垃圾】
 *
 * //构造方法
 * @see ByteArrayOutputStream#ByteArrayOutputStream()
 * @see ByteArrayOutputStream#ByteArrayOutputStream(int)
 *
 * 同步对象方法
 * @see ByteArrayOutputStream#write(int) 写入一个字节
 * @see ByteArrayOutputStream#write(byte[]) 写入多个字节
 * @see ByteArrayOutputStream#write(byte[], int, int) 写入多个字节
 * @see ByteArrayOutputStream#writeTo(OutputStream)
 * @see ByteArrayOutputStream#reset() 重置，重用已经分配的缓冲区空间
 * @see ByteArrayOutputStream#toByteArray() 获取缓冲区的有效内容字节数组
 *
 * 对象方法
 * @see ByteArrayOutputStream#ensureCapacity(int) 扩容字节缓存
 * @see ByteArrayOutputStream#grow(int)
 * @see ByteArrayOutputStream#hugeCapacity(int)
 * @see ByteArrayOutputStream#flush() 继承自OutputStream的空方法
 * @see ByteArrayOutputStream#close() 空方法
 *
 *
 * @date 2020/12/21
 */
public class Demo02_ByteArrayOutputStream {
    public static void main(String[] args) throws IOException {
        // test01();
        test02();
    }

    public static void test01() throws IOException {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream(32);
        byteArrayOS.write(97);

        byte[] bytes = byteArrayOS.toByteArray();
        System.out.println(bytes+" length:"+bytes.length);//[B@47089e5f length:1
        byteArrayOS.close();// 空方法
    }

    public static void test02() throws IOException {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream(32);
        byteArrayOS.write(new String("文本内容为:").getBytes());
        byteArrayOS.write(97);

        byteArrayOS.writeTo(new FileOutputStream("D:\\test.txt"));// 文本内容为:a
        byteArrayOS.close();// 空方法
    }
}
