package cn.jdemo.io.outputstream;

import java.io.OutputStream;

/**
 * @see java.io.OutputStream 抽象基类，针对byte处理
 *
 * 构造方法(默认)
 * @see OutputStream#OutputStream()
 *
 * 基本常用方法如下
 * @see java.io.OutputStream#write(int) 抽象方法
 * @see java.io.OutputStream#write(byte[])
 * @see java.io.OutputStream#write(byte[], int, int)
 * @see OutputStream#flush() 空方法
 * @see OutputStream#close() 空方法
 *
 * 常用subclass
 * @see java.io.BufferedOutputStream
 * @see java.io.ByteArrayOutputStream
 * @see java.io.DataOutputStream
 * @see java.io.FileInputStream
 * @see java.io.FilterOutputStream
 * @see java.io.ObjectOutputStream
 *
 * @date 2020/12/21
 */
public class AllInOne {
}
