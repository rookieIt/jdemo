package cn.jdemo.io.outputstream;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @see OutputStream
 * @see FileOutputStream
 *
 * // 构造方法
 * @see FileOutputStream#FileOutputStream(File file) 要读取的文件
 * @see FileOutputStream#FileOutputStream(String name) 与系统有关的文件名
 * @see FileOutputStream#FileOutputStream(FileDescriptor fdObj) FileDescriptor是"文件描述符";
 * @see FileOutputStream#FileOutputStream(File, boolean append) append是否追加
 * @see FileOutputStream#FileOutputStream(String, boolean append)
 *
 *
 * @see FileOutputStream#write(int)
 * @see FileOutputStream#write(byte[], int, int)
 * @see FileOutputStream#write(byte[])
 * @see FileOutputStream#write(int, boolean append)
 * @see FileOutputStream#writeBytes(byte[], int, int, boolean append)
 * @see FileOutputStream#open(String, boolean)
 * @see FileOutputStream#open0(String, boolean)  反射调用open0,可以打开新的文件流
 * @see FileOutputStream#getFD() 获取文件描述符
 *
 * @date 2020/12/21
 */
public class Demo04_FileOutputStream {
    public static void main(String[] args) throws Exception {
        // test01();
        test02();
    }

    /**
     * (1)实例化FileOutputStream，是否追加设置为false
     * (2)基于反射，是否追加设置为true,不生效？？？
     * @see FileOutputStream#write(int, boolean append)
     * @throws Exception
     */
    public static void test01() throws Exception {
        FileOutputStream fileOS = new FileOutputStream("D:\\test.txt");
        Class<? extends FileOutputStream> fileOSClass = fileOS.getClass();
        Method writeNative = fileOSClass.getDeclaredMethod("write", int.class, boolean.class);
        writeNative.setAccessible(true);
        writeNative.invoke(fileOS, 97,true);
        fileOS.close();
    }

    /**
     * (1)实例化FileOutputStream，是否追加设置为true
     * (2)基于反射，是否追加设置为false,生效了
     * @throws Exception
     */
    public static void test02() throws Exception {
        FileOutputStream fileOS = new FileOutputStream("D:\\test.txt",true);
        Class<? extends FileOutputStream> fileOSClass = fileOS.getClass();
        Method writeNative = fileOSClass.getDeclaredMethod("write", int.class, boolean.class);
        writeNative.setAccessible(true);
        writeNative.invoke(fileOS, 97,false);
        fileOS.close();
    }
}
