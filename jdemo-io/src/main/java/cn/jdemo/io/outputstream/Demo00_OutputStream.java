package cn.jdemo.io.outputstream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @see OutputStream
 *
 * 构造方法
 * @see OutputStream#OutputStream()
 *
 * 抽象方法
 * @see OutputStream#write(int)
 *
 * @date 2020/12/25
 */
public class Demo00_OutputStream {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        try (OutputStream outputStream = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                System.out.println(new Character((char) b));
            }})
        {
            outputStream.write(97);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
