/**
 *
 * 字节流的抽象基类：
 * @see java.io.InputStream
 * @see cn.jdemo.io.inpustream.AllInOne
 *
 * @see java.io.OutputStream
 * @see cn.jdemo.io.outputstream.AllInOne
 *
 * 字符流的抽象基类：
 * @see java.io.Reader
 * @see cn.jdemo.io.reader.AllInOne
 *
 * @see java.io.Writer
 * @see cn.jdemo.io.writer.AllInOne
 *
 * -Dfile.encoding=utf-8
 *
 * 特殊点:
 * @see java.io.ByteArrayOutputStream#close()
 * @see java.io.ByteArrayOutputStream#ensureCapacity(int) 扩容
 * @see java.io.ByteArrayInputStream#close()
 * 1 ByteArrayInputStream 与 ByteArrayOutputStream的close()是空方法？
 *   答：(1)ByteArrayOutputStream或ByteArrayInputStream是内存读写流，没有占用网络、磁盘文件等资源,Java的垃圾回收机制会将它回收，所以不需要关流；
 *      (2)添加元素, 容量超了就扩容
 *
 * @date 2020/12/16
 */
package cn.jdemo.io;