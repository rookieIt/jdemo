package cn.jdemo.io.reader;


import java.io.LineNumberReader;
import java.io.Reader;

/**
 *  @see Reader 抽象基类
 *  @see LineNumberReader
 *
 *  构造方法
 * @see LineNumberReader#LineNumberReader(Reader) 默认char[]缓存大小8192
 * @see LineNumberReader#LineNumberReader(Reader, int sz)
 *
 * @see AllInOne#基本常用方法
 * @see LineNumberReader#readLine()
 * @see LineNumberReader#readLine(boolean) 如果为true，则将跳过下一个'\n'
 *
 * @date 2020/12/23
 */
public class Demo02_LineNumberReader {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
    }
}
