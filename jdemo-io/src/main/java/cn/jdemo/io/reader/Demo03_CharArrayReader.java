package cn.jdemo.io.reader;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

/**
 *  @see Reader 抽象基类
 *  @see BufferedReader (传入io流，缓存读取):
 *  (1)实例化需要封装ioStream对象，内部有缓存，默认是8192；
 *  (2)支持mark(标记)，并标记readlimit,可以reset重置会mark位置重新读取
 *  (3)支持skip.跳过并丢弃此输入流中的n个字节的数据
 *
 *  构造方法
 * @see BufferedReader#BufferedReader(Reader) 默认char[]缓存大小8192
 * @see BufferedReader#BufferedReader(Reader, int sz)
 *
 * @see AllInOne#基本常用方法
 * @see BufferedReader#readLine()
 * @see BufferedReader#readLine(boolean) 如果为true，则将跳过下一个'\n'
 *
 * @date 2020/12/23
 */
public class Demo03_CharArrayReader {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        try {
            new BufferedReader(new FileReader(""), 256);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            
        }
    }
}
