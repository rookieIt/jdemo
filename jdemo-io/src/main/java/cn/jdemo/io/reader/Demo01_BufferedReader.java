package cn.jdemo.io.reader;


import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *  @see java.io.Reader 抽象基类
 *  @see java.io.BufferedReader (传入io流，缓存读取):
 *  (1)实例化需要封装ioStream对象，内部有缓存，默认是8192；
 *  (2)支持mark(标记)，并标记readlimit,可以reset重置会mark位置重新读取
 *  (3)支持skip.跳过并丢弃此输入流中的n个字节的数据
 *
 *  构造方法
 * @see java.io.BufferedReader#BufferedReader(Reader) 默认char[]缓存大小8192
 * @see java.io.BufferedReader#BufferedReader(Reader, int sz)
 *
 * @see cn.jdemo.io.reader.AllInOne#基本常用方法
 * @see BufferedReader#readLine()
 * @see BufferedReader#readLine(boolean) 如果为true，则将跳过下一个'\n'
 *
 * @date 2020/12/23
 */
public class Demo01_BufferedReader {
    public static void main(String[] args) {
        // test01();
        test02();
    }

    public static void test01() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\test.txt"), 8)){
            String readLine = bufferedReader.readLine();
            System.out.println(readLine);//abc+中文字符串123
            System.out.println(bufferedReader.readLine());//abc+第二行内容123
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

    public static void test02() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("D:\\test.txt"), 8)){
            Class<? extends BufferedReader> aClass = bufferedReader.getClass();
            Method readLineAndParm = aClass.getDeclaredMethod("readLine", boolean.class);
            readLineAndParm.setAccessible(true);
            Object invoke = readLineAndParm.invoke(bufferedReader, true);
            System.out.println(invoke.toString());//abc+中文字符串123
        } catch (FileNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
