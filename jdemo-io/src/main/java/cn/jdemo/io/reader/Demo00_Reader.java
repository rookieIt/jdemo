package cn.jdemo.io.reader;

import java.io.IOException;
import java.io.Reader;

/**
 * @see Reader
 *
 * 构造方法
 * @see Reader#Reader()
 * @see Reader#Reader(Object lock)  
 *
 * 抽象方法
 * @see Reader#read(char[], int, int)
 * @see Reader#close()
 *
 * @date 2020/12/25
 */
public class Demo00_Reader {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        Reader reader = new Reader() {
            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                System.out.println("Reader#read(char[], int, int)");
                return 0;
            }
            @Override
            public void close() throws IOException {
                System.out.println("Reader#close()");
            }
        };
    }
}
