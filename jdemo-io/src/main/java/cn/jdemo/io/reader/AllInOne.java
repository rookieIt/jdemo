package cn.jdemo.io.reader;

import java.io.Reader;
import java.nio.CharBuffer;

/**
 * @see java.io.Closeable 父接口
 * @see java.lang.Readable 父接口
 * @see java.io.Reader 抽象基类，针对char（char占2字节，16位）处理
 *
 *
 * 构造方法
 * @see Reader#Reader() 默认锁是当前对象
 * @see Reader#Reader(Object lock) 设置锁对象
 *
 * 基本常用方法如下Se
 * @see Reader#read()
 * @see Reader#read(char[])
 * @see Reader#read(CharBuffer)
 * @see Reader#read(char[], int, int)
 * @see Reader#ready()
 * @see Reader#mark(int readAheadLimit)
 * @see Reader#markSupported()
 * @see Reader#reset() 重置
 * @see Reader#skip(long n) 当前方法中使用了同步锁
 * @see Reader#lock 锁对象
 *
 * 常用subclass
 * @see java.io.BufferedReader
 * @see java.io.LineNumberReader
 * @see java.io.CharArrayReader
 * @see java.io.InputStreamReader
 * @see java.io.FileReader
 * @see java.io.FilterReader
 * @see java.io.PushbackReader
 * @see java.io.PipedReader
 * @see java.io.StringReader
 *
 * @date 2020/12/21
 */
public class AllInOne {
}
