package cn.jdemo.io.writer;

/**
 * @see java.lang.Appendable 父接口
 * @see java.io.Closeable 父接口
 * @see java.io.Flushable 父接口
 * @see java.io.Writer 抽象基类，针对char（char占2字节，16位）处理
 *
 * @see java.io.Writer#Writer() protected
 * @see java.io.Writer#Writer(Object lock)
 *
 * 基本常用方法如下
 * @see java.io.Writer#write(int)
 * @see java.io.Writer#write(String)
 * @see java.io.Writer#write(char[])
 * @see java.io.Writer#write(String, int, int)
 * @see java.io.Writer#append(char)
 * @see java.io.Writer#append(CharSequence)
 * @see java.io.Writer#append(CharSequence, int, int)
 * @see java.io.Writer#flush() 抽象方法
 * @see java.io.Writer#close() 抽象方法
 * @see java.io.Writer#writeBuffer char临时缓冲区，用于保存字符串和单个字符的写入，默认是1024
 *
 *
 *
 * 常用subclass
 * @see java.io.BufferedWriter
 * @see java.io.CharArrayWriter
 * @see java.io.FilterWriter
 * @see java.io.OutputStreamWriter
 * @see java.io.FileWriter
 * @see java.io.PipedWriter
 * @see java.io.PrintWriter
 * @see java.io.StringWriter
 *
 * @date 2020/12/21
 */
public class AllInOne {
}
