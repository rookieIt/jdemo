package cn.jdemo.io.writer;

import java.io.IOException;
import java.io.Writer;

/**
 * @see java.io.Writer
 *
 * 构造方法
 * @see java.io.Writer#Writer() 默认锁，this对象
 * @see java.io.Writer#Writer(Object lock) 指定对象锁
 *
 * 抽象方法
 * @see Writer#write(char[], int, int)
 * @see Writer#flush()
 * @see Writer#close()
 *
 * @date 2020/12/25
 */
public class Demo00_Writer {
    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        Writer writer = new Writer() {
            @Override
            public void write(char[] cbuf, int off, int len) throws IOException {
                System.out.println("Writer#write(char[], int, int)");
            }
            @Override
            public void flush() throws IOException {
                System.out.println("Writer#flush()");
            }
            @Override
            public void close() throws IOException {
                System.out.println("Writer#close()");
            }
        };
    }
}
