package cn.jdemo.juc.pattern.component.demo;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合模式
 * 
 * 	组合模式，就是在一个对象中包含其他对象，这些被包含的对象可能是终点对象（不再包含别的对象），也有可能是非终点对象（其内部还包含其他对象，或叫组对象）；
 * 
 *  我们将对象称为节点，即一个根节点包含许多子节点，这些子节点有的不再包含子节点，而有的仍然包含子节点，以此类推。很明显，这是树形结构，终结点叫叶子节点，非终节点（组节点）叫树枝节点，第一个节点叫根节点。
 *  
 *  同时也类似于文件目录的结构形式：文件可称之为终节点，目录可称之为非终节点（组节点）。
 *
 */
public class Demo01 {
	
	public static void main(String[] args) {
		Demo01 demo = new Demo01();
		
		MarketBranch rootBranch = demo.new MarketBranch("总店");
		MarketBranch qhdBranch = demo.new MarketBranch("秦皇岛分店");
		
		MarketJoin hgqJoin = demo.new MarketJoin("秦皇岛分店一海港区加盟店");
		MarketJoin btlJoin = demo.new MarketJoin("秦皇岛分店二白塔岭加盟店");
		
		qhdBranch.add(hgqJoin);
		qhdBranch.add(btlJoin);
		rootBranch.add(qhdBranch);
		
		rootBranch.PayByCard();// 消费
	}
	
	public abstract class Market {
		String name;
 
		public abstract void add(Market m);
 
		public abstract void remove(Market m);
 
		public abstract void PayByCard();
	}
 
	// 分店 下面可以有加盟店
	public class MarketBranch extends Market {
		// 加盟店列表
		List<Demo01.Market> list = new ArrayList<Demo01.Market>();
		
		public MarketBranch(String s) {
			this.name = s;
		}
		@Override
		public void add(Market m) {
			list.add(m);
		}
		@Override
		public void remove(Market m) {
			list.remove(m);
		}
		// 消费之后，该分店下的加盟店自动累加积分
		@Override
		public void PayByCard() {
			System.out.println(name + "消费,积分已累加入该会员卡");
			for (Market m : list) {
				m.PayByCard();
			}
		}
	}
 
	// 加盟店 下面不在有分店和加盟店，最底层
	public class MarketJoin extends Market {
		public MarketJoin(String s) {
			this.name = s;
		}
		@Override
		public void add(Market m) {
		}
 
		@Override
		public void remove(Market m) {
		}
 
		@Override
		public void PayByCard() {
			System.out.println(name + "消费,积分已累加入该会员卡");
		}
	}
}
