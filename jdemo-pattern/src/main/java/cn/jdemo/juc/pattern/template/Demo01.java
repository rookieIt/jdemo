package cn.jdemo.juc.pattern.template;

/**
 * 模板方法模式
 */
public class Demo01 {
	public static void main(String[] args) {
		Cookie one = new Demo01().new One();
		one.isC();
		one.cook();
		System.out.println("**************************");
		Cookie two = new Demo01().new Two();
		two.cook();
	}
	
	abstract class Cookie{
		public abstract void a();
		public abstract void b();
		public abstract void c();
		public boolean isC() {
			return true;
		};
		final public  void cook(){
			this.a();
			this.b();
			if (this.isC()) {
				this.c();
			}
		};
	}
	
	class One extends Cookie{
		@Override
		public void a() {
			System.out.println("One: a");
		}

		@Override
		public void b() {
			System.out.println("One: b");
		}

		@Override
		public void c() {
			System.out.println("One: c");
		}
		
		@Override
		public boolean isC() {
			return false;
		}
		
	}
	
	class Two extends Cookie{

		@Override
		public void a() {
			System.out.println("Two: a");
			
		}

		@Override
		public void b() {
			System.out.println("Two: b");
			
		}

		@Override
		public void c() {
			System.out.println("Two: c");
		}
		
	}
	
}
