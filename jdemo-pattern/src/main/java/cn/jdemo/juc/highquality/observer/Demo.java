package cn.jdemo.juc.highquality.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * Description
 * ProjectName parent_will
 * Created by 张刘伟 on 2018/8/14 11:27.
 * Version 1.0
 */
public class Demo {
    public static void main(String[] args) {
        Observable observableo = new CaoCaoObservable();

        Observer observer = new LiuBangObserver();

        observableo.addObserver(observer);

        System.out.println("xxxxxxxxxxxxx");
        observableo.notifyObservers("xxhhhhhhx");

    }
}
