package cn.jdemo.pattern.facade;

/**
 * @description
 * @date 2021/1/19
 */
public class HeroAshe extends HeroWorker {
    @Override
    String name() {
        return "Ashe";
    }
}
