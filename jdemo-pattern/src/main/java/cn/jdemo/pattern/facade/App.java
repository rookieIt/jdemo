package cn.jdemo.pattern.facade;

import java.util.Arrays;
import java.util.List;

/**
 * 案例说明;
 * @see HeroWorkerFacade 门面系统，提供简单操作界面
 * @see HeroWorkerFacade#compose01() 走A
 * @see HeroWorkerFacade#compose02() 回城
 *
 * @see HeroWorker 平A 移动 回城，作为基础操作子系统
 * @see HeroAshe 艾希基础信息子系统
 * @see HeroEkko 艾克基础信息子系统
 *
 * @date 2021/1/20
 */
public class App {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * 09:57:58.464 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ekko MOVING...
     * 09:57:58.468 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ekko A...
     * 09:57:58.468 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ashe MOVING...
     * 09:57:58.468 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ashe A...
     * 09:57:58.468 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ekko GO_HOME...
     * 09:57:58.469 [main] INFO cn.jdemo.pattern.facade.HeroWorker - Ashe GO_HOME...
     */
    public static void test01() {
        HeroWorkerFacade facade = new HeroWorkerFacade(new HeroEkko(), new HeroAshe());
        facade.compose01();
        facade.compose02();
    }

}
