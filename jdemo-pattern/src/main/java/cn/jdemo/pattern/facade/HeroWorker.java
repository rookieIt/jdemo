package cn.jdemo.pattern.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 *
 * @date 2021/1/19
 */
public abstract class HeroWorker {

    private static final Logger logger = LoggerFactory.getLogger(HeroWorker.class);

    void a(){
        logger.info("{} A...",name());
    }

    void go_home(){
        logger.info("{} GO_HOME...",name());
    }

    void moving(){
        logger.info("{} MOVING...",name());
    }

    private void action(Action action){
        switch (action){
            case A:
                a();
                break;
            case MOVING:
                moving();
                break;
            case GO_HOME:
                go_home();
                break;
            default:
                logger.info("{}wrong action...",name());
        }
    }

    /**
     * Perform actions.
     * @param actions
     */
    public void action(Action... actions){
        Arrays.stream(actions).forEach(this::action);
    }

    abstract String name();

    enum Action{
        A,GO_HOME,MOVING
    }
}
