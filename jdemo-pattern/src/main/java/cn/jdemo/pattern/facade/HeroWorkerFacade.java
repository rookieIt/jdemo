package cn.jdemo.pattern.facade;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class HeroWorkerFacade {
    private final List<HeroWorker> heroWorkers;

    HeroWorkerFacade(HeroWorker ... workers){
        this.heroWorkers = workers!=null? Arrays.asList(workers):null;
    }

    // 走 A
    public void compose01(){
       doWork(heroWorkers, HeroWorker.Action.MOVING, HeroWorker.Action.A);
    }

    // 回城
    public void compose02(){
        doWork(heroWorkers, HeroWorker.Action.GO_HOME);
    }

    void doWork(Collection<HeroWorker> heroWorkers, HeroWorker.Action... actions){
        if (heroWorkers!=null){
            heroWorkers.stream().forEach(temp -> temp.action(actions));
        }
    }

}
