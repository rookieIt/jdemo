package cn.jdemo.pattern.singleton;

import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * 1.直接加载
 * 2.自动支持序列化机制，绝对防止多次实例化
 * 3.防止反序列化重新创建新的对象，绝对防止多次实例化
 *
 * 编译后是一个 final 类，所以它是不能被继承的
 * 枚举类实例 INSTANCE 是一个static final 静态常量，仅有一份；所以可以直接引用
 *
 * 继承自 Enum 类(保证不会被反射)
 * 默认为私有构造器(可以被暴力反射)
 * @see java.lang.reflect.Constructor#newInstance() (clazz.getModifiers() & Modifier.ENUM) != 0
 *
 * 枚举类如何避免了反射问题？
 *  -> 反射在通过newInstance创建对象时，会检查该类是否ENUM修饰，如果是则抛出异常，反射失败
 *
 * 枚举类如何避免了反序列化问题？
 *  -> 实际是Enum抽象类的static final 成员变量，而一切static变量反序列化是直接使用的当前JVM中对应static变量的值，故还是单例的
 *  -> 通过Enum#valueOf获取的
 * @see ObjectInputStream#readEnum(boolean)
 * @see Enum#valueOf(java.lang.Class, java.lang.String)
 */
public enum Demo03 implements Serializable {
    INSTANCE;
}
