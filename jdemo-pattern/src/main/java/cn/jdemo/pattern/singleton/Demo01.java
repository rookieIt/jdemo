package cn.jdemo.pattern.singleton;

/**
 * 懒加载
 * 1.利用了 classloader 机制来保证初始化 instance 时只有一个线程
 * 2.只有通过显式调用 getInstance 方法时，才会显式装载 SingletonHolder 类，从而实例化 instance
 *
 * 缺点
 * 还存在反射攻击或者反序列化攻击
 */
public class Demo01 {
    private Demo01 (){}

    private static class SingletonHolder {
        private final static Demo01 INSTANCE = new Demo01();
    }

    public static Demo01 getInstance(){
        return SingletonHolder.INSTANCE;
    }
}
