package cn.jdemo.pattern.singleton;

/**
 * 直接加载
 * 1.利用了 classloader 机制来保证初始化 instance 时只有一个线程
 * 缺点：还存在反射攻击或者反序列化攻击
 *
 * @see Demo05 避免了反射
 */
public class Demo02 {
    private static final Demo02 INSTANCE = new Demo02();
    public static Demo02 getInstance() {
        return INSTANCE;
    }
}
