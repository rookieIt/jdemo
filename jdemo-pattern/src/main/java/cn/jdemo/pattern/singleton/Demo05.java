package cn.jdemo.pattern.singleton;

/**
 * 直接加载
 *
 * 避免了反射
 */
public class Demo05 {

    private static final Demo05 instance = new Demo05();

    // 私有化
    private Demo05() {
        if (instance!=null){
            throw new RuntimeException("禁止实例化！");
        }
    }

    public static Demo05 getInstance(){
        return instance;
    }
}
