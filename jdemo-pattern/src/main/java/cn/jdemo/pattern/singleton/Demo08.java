package cn.jdemo.pattern.singleton;

import java.io.Serializable;

/**
 * 直接加载
 *
 * 避免了反射
 * 避免了克隆
 * 避免了序列化
 *
 * transit反序列化没用:
 *      其是将不需要序列化的属性前添加关键字transient，序列化对象的时候，这个属性就不会序列化到指定的目的地中
 *      transient关键字只能修饰变量
 *      一个静态变量不管是否被transient修饰，均不能被序列化
 *        -- 反序列化后类中static型变量的值为当前JVM中对应static变量的值，这个值是JVM中的不是反序列化得出的
 */
public class Demo08 implements Cloneable, Serializable {
    public static final Demo08 instance = new Demo08();

    // 私有化
    private Demo08() {
        if (instance!=null){
            throw new RuntimeException("禁止实例化！");
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return instance;
    }

    private Object readResolve(){
        return instance;
    }
}
