package cn.jdemo.pattern.singleton;

/**
 * 双重检测懒加载
 *
 * 1) 反射时有校验；
 * 2) 但若在懒加载前,则可以被反射无数次 -- 故这种情况只能使用饿汉式,然后在私有构造器中校验
 * @see Demo05
 */
public class Demo04Vo {
    private static volatile Demo04Vo instance = null;

    // 私有化
    private Demo04Vo() throws Exception {
        if (instance!=null){
            throw new Exception("禁止实例化！");
        }
    }

    public static Demo04Vo getInstance() throws Exception {
        if (instance == null) {
            synchronized (Demo04Vo.class) {
                if (instance == null){
                    instance = new Demo04Vo();
                }
            }
        }
        return instance;
    }
}
