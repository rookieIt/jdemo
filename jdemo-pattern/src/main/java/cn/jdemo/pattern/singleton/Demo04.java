package cn.jdemo.pattern.singleton;

/**
 * 双重检测懒加载
 *
 * 1) 会被反射
 */
public class Demo04 {
    private static volatile Demo04 instance = null;

    // 私有化
    private Demo04(){}

    public static Demo04 getInstance(){
        if (instance == null) {
            synchronized (Demo04.class) {
                if (instance == null){
                    instance = new Demo04();
                }
            }
        }
        return instance;
    }
}
