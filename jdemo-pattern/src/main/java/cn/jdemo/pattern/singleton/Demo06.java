package cn.jdemo.pattern.singleton;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * 直接加载
 *
 * 避免了反射
 * 避免了克隆
 * 避免了序列化
 */
public class Demo06 implements Cloneable, Serializable {

    private static final Demo06 instance = new Demo06();

    // 私有化
    private Demo06() {
        if (instance!=null){
            throw new RuntimeException("禁止实例化！");
        }
    }

    public static Demo06 getInstance(){
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return instance;
        // throw new RuntimeException("禁止实例化！");
    }

    private Object readResolve(){
        System.out.println("readResolve");
        return instance;
        // throw new RuntimeException("禁止实例化！");
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        // in.defaultReadObject();
        System.out.println("readObject");
        // throw new RuntimeException("禁止实例化！");
    }
}
