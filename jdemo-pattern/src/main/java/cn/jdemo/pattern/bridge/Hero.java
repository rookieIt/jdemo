package cn.jdemo.pattern.bridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/19
 */
public interface Hero {
    Logger logger = LoggerFactory.getLogger(Hero.class);

    void Q();

    void W();

    void E();

    void R();

    default void A(){
        logger.info("平A...");
    }

}
