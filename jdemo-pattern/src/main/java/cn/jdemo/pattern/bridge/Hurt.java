package cn.jdemo.pattern.bridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/19
 */
public interface Hurt{

    Logger logger = LoggerFactory.getLogger(Hurt.class);

    void magic();

    void physical();

}
