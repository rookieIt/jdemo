package cn.jdemo.pattern.bridge;

/**
 * bridge pattern
 *
 * Composition over inheritance.<code>组合(解耦)重于继承}</>
 * The Bridge pattern can also be thought of as two layers of abstraction.
 * With Bridge, you can decouple an abstraction from its implementation so that the two can vary independently.
 *
 * 基于多态，业务拆分，组合使用
 *
 * @date 2021/1/18
 */
public class AllInOne {
}
