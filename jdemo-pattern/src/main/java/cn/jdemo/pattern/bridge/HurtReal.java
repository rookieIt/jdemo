package cn.jdemo.pattern.bridge;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class HurtReal implements Hurt{

    @Override
    public void magic() {
        logger.info("HurtReal maggir normal hurt");
    }

    @Override
    public void physical() {
        logger.info("HurtReal physical normal hurt");
    }
}
