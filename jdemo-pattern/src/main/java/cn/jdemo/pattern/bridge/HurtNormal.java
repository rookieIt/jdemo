package cn.jdemo.pattern.bridge;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class HurtNormal implements Hurt{

    @Override
    public void magic() {
        logger.info("HurtNormal maggir normal hurt");
    }

    @Override
    public void physical() {
        logger.info("HurtNormal physical normal hurt");
    }
}
