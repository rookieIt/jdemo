package cn.jdemo.pattern.bridge;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class HeroAshe implements Hero{

    Hurt hurt = null;

    HeroAshe(Hurt hurt){
        this.hurt = hurt;
    }

    @Override
    public void Q() {
        logger.info("HeroAshe Q physical hurt ...");
        hurt.physical();
    }

    @Override
    public void W() {
        logger.info("HeroAshe W physical hurt ...");
        hurt.physical();
    }

    @Override
    public void E() {
        logger.info("HeroAshe E no hurt ...");
    }

    @Override
    public void R() {
        logger.info("HeroAshe R magic hurt ...");
        hurt.magic();
    }
}
