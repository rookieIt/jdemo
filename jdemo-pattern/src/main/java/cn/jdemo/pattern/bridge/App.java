package cn.jdemo.pattern.bridge;

/**
 *
 * @date 2021/1/19
 */
public class App {

    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        HeroAshe heroAshe = new HeroAshe(new HurtReal());
        heroAshe.A();
        heroAshe.Q();
        heroAshe.W();
        heroAshe.E();
        heroAshe.R();
    }

}
