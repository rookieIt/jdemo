package cn.jdemo.pattern.caching;

/**
 *
 * caching模式
 *
 * 缓存模式：通过在使用资源后不立即释放资源来避免昂贵的资源重新获取。
 *
 * 资源保留其身份，保留在某些快速访问的存储中，并被重新使用以避免再次获取它们。
 *
 * 在这种模式下，有四种主要的缓存策略/技术；每个都有自己的优点和缺点。他们是:
 * <code>write-through</code> 将数据通过单个事务写入cache和DB;
 * <code>write-around</code> 将数据写入DB;
 * <code>write-behind</code> 将数据写入cache,只有在cache已满时才将数据写入DB;
 * <code>cache-aside</code>则负责保持两个数据源中的数据都同步到应用程序本身。
 *
 * <code> read-through-</code>策略也包含在上述四个策略中-
 * 将数据从缓存返回给调用方<b>，如果</b>存在，则为<b> else </b>来自DB的查询并将存储到高速缓存中以备将来使用。
 * 这些策略确定何时应将缓存中的数据写回到后备存储（即数据库），并帮助保持两个数据源同步/最新。
 * 此模式可以提高性能，还有助于维护缓存中保存的数据与基础数据存储中的数据之间的一致性。
 *
 *
 * Least-Recently-Used (LRU)
 *
 *
 * @date 2021/1/18
 */
public class AllInOne {
}
