package cn.jdemo.pattern.monostate;

/**
 * The MonoState pattern ensures that all instances of the class will have the same state. This can
 * be used a direct replacement of the Singleton pattern.
 *
 * MonoState pattern : 所有实例将具有相同的状态
 *
 * @description
 * @date 2021/2/5
 */
public class AllInOne {
}
