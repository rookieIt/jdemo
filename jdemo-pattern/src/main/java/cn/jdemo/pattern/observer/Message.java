package cn.jdemo.pattern.observer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class Message {

    private List<Observer> observers;

    Message(){
        observers = new ArrayList<>();
    }

    void pushMsg(MessageType... messageTypes){
        notifyObservers(messageTypes);
    }

    private void notifyObservers(MessageType... messageTypes){
        Optional.ofNullable(observers)
                .ifPresent(observers -> observers.forEach(observer -> observer.onEvent(messageTypes)));
    }

    enum MessageType{
        warn,barn,mark,disappear
    }

    public List<Observer> getObservers() {
        return observers;
    }

    public void setObservers(Observer... observers) {
        this.observers.addAll(Arrays.asList(observers));
    }

    public void removeObservers(Observer... observers){
        this.observers.removeAll(Arrays.asList(observers));
    }
}
