package cn.jdemo.pattern.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 *
 * @date 2021/1/28
 */
public class ObserverAshe implements Observer{

    private static final Logger logger = LoggerFactory.getLogger(ObserverAshe.class);

    @Override
    public void onEvent(Message.MessageType... type) {
        Arrays.asList(type).stream().forEach(this::onEvent0);
    }

    private void onEvent0(Message.MessageType type) {
        switch (type){
            case barn:
                logger.info("barn Ashe");
                break;
            case mark:
                logger.info("mark Ashe");
                break;
            case warn:
                logger.info("warn Ashe");
                break;
            case disappear:
                logger.info("disappear Ashe");
                break;
            default:
                logger.info("default Ashe");
                break;
        }
    }
}
