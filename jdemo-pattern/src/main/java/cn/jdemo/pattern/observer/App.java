package cn.jdemo.pattern.observer;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class App {
    public static void main(String[] args) {
        // test01();
        test02();
    }

    /**
     * 输出内容:
     * 11:21:57.546 [main] INFO cn.jdemo.pattern.observer.ObserverAshe - disappear Ashe
     * 11:21:57.549 [main] INFO cn.jdemo.pattern.observer.ObserverAshe - mark Ashe
     */
    public static void test01() {
        Message message = new Message();
        message.setObservers(new ObserverAshe());
        message.pushMsg(Message.MessageType.disappear, Message.MessageType.mark);
    }

    /**
     * 输出内容
     * 11:24:57.453 [main] INFO cn.jdemo.pattern.observer.ObserverAshe - disappear Ashe
     * 11:24:57.455 [main] INFO cn.jdemo.pattern.observer.ObserverAshe - mark Ashe
     * 11:24:57.456 [main] INFO cn.jdemo.pattern.observer.ObserverAshe - barn Ashe
     * 11:24:57.457 [main] INFO cn.jdemo.pattern.observer.ObserverEkko - barn Ekko
     */
    public static void test02() {
        Message message = new Message();
        message.setObservers(new ObserverAshe());
        message.pushMsg(Message.MessageType.disappear, Message.MessageType.mark);

        message.setObservers(new ObserverEkko());
        message.pushMsg(Message.MessageType.barn);
    }
}
