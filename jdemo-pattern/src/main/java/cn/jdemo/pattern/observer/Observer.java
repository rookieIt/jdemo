package cn.jdemo.pattern.observer;

/**
 *
 * @date 2021/1/28
 */
public interface Observer {

    void onEvent(Message.MessageType... type);

}
