package cn.jdemo.pattern.builder.extend;

/**
 *
 * @description
 * @date 2021/1/20
 */
public class Demo
{
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        /*
            类名::(非静态)方法,返回函数接口需要，有个参数为类实例对象
         */
        Demo demo= Builder.of(Demo::new).with(Demo::setName,"Ashe").build();
        System.out.println(demo.getName());// Ashe

        Demo demo2= Builder.of(()->new Demo()).with(Demo::setName,"Ekko").build();
        System.out.println(demo2.getName());// Ekko
    }
}
