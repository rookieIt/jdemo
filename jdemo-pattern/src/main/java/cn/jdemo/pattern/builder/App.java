package cn.jdemo.pattern.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/1/21
 */
public class App {

    public static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        Hero ashe = new Hero.Builder("Ashe")
                .withHeroGender(HeroGender.female)
                .withHeroType(HeroType.ADC)
                .withHeroWeapon(HeroWeapon.BOW)
                .build();
        logger.info(ashe.toString());
    }
}
