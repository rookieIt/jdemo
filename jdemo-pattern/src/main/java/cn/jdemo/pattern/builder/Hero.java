/*
 * The MIT License
 * Copyright © 2014-2019 Ilkka Seppälä
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package cn.jdemo.pattern.builder;

/**
 * Hero, the class with many parameters.
 */
public final class Hero {


  private final String name;
  private final HeroType heroType;
  private final HeroGender heroGender;
  private final HeroWeapon heroWeapon;

  private Hero(Builder builder) {
    this.name = builder.name;
    this.heroType = builder.heroType;
    this.heroGender = builder.heroGender;
    this.heroWeapon = builder.heroWeapon;
  }

  public String getName() {
    return name;
  }

  public HeroType getHeroType() {
    return heroType;
  }

  public HeroGender getHeroGender() {
    return heroGender;
  }

  public HeroWeapon getHeroWeapon() {
    return heroWeapon;
  }

  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();
    sb.append("This is a ")
        .append(heroType)
        .append(" named ")
        .append(name);
    if (heroType != null || heroGender != null) {
      sb.append(" with ");
      if (heroType != null) {
        sb.append(heroType).append(' ');
      }
      if (heroGender != null) {
        sb.append(heroGender).append(' ');
      }
      sb.append(heroGender != HeroGender.male ? "xy" : "xx");
    }
    if (heroWeapon != null) {
      sb.append(" and wielding a ").append(heroWeapon);
    }
    sb.append('.');
    return sb.toString();
  }

  /**
   * The builder class.
   */
  public static class Builder {

    private final String name;
    private HeroType heroType;
    private HeroGender heroGender;
    private HeroWeapon heroWeapon;

    /**
     * Constructor.
     */
    public Builder(String name) {
      if (name == null) {
        throw new IllegalArgumentException("profession and name can not be null");
      }
      this.name = name;
    }
    public Builder withHeroType(HeroType heroType) {
      this.heroType = heroType;
      return this;
    }

    public Builder withHeroGender(HeroGender heroGender) {
      this.heroGender = heroGender;
      return this;
    }
    public Builder withHeroWeapon(HeroWeapon heroWeapon) {
      this.heroWeapon = heroWeapon;
      return this;
    }

    public Hero build() {
      return new Hero(this);
    }
  }
}
