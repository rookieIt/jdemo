package cn.jdemo.pattern.priority_queue;

import java.util.PriorityQueue;

/**
 *
 * @description
 * @date 2021/2/18
 */
public class AllInOne {
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>((x,y)->{
            return y.compareTo(x);
        });
        queue.add(2);
        queue.add(1);
        queue.add(3);
        System.out.println(queue.peek());
        System.out.println(queue.peek());
        System.out.println(2/10);
    }
}
