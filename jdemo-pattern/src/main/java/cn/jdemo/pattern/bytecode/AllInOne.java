package cn.jdemo.pattern.bytecode;

/**
 * @what
 * The intention of Bytecode pattern is to give behavior the flexibility of data by encoding it as
 * instructions for a virtual machine.
 * An instruction set defines the low-level operations that can be performed.
 * A series of instructions is encoded as a sequence of bytes.
 * A virtual machine executes these instructions one at a time, using a stack for intermediate values.
 * By combining instructions, complex high-level behavior can be defined.
 *
 * @how
 * <p>This pattern should be used when there is a need to define high number of behaviours and
 * implementation engine is not a good choice because It is too lowe level Iterating on it takes too
 * long due to slow compile times or other tooling issues. It has too much trust. If you want to
 * ensure the behavior being defined can’t break the game, you need to sandbox it from the rest of
 * the codebase.
 *
 * bytecode模式，使行为动作具有数据的灵活性
 *
 * @date 2021/1/18
 */
public class AllInOne {
}
