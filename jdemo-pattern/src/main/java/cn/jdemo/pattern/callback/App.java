package cn.jdemo.pattern.callback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/1/18
 */
public class App {
    private final  static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        Task simpleTask = new Task() {
            @Override
            public void execute() {
                logger.info("executing...");
            }
        };
        simpleTask.executeWith(()->logger.info("call..."));
    }
}
