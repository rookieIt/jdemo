package cn.jdemo.pattern.callback;

/**
 * @date 2021/1/18
 */
@FunctionalInterface
public interface Callback {
    void call();
}
