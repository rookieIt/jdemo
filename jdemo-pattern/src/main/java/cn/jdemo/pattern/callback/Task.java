package cn.jdemo.pattern.callback;


/**
 *
 * @date 2021/1/18
 */
public abstract class Task {

    public final void executeWith(Callback call){
        execute();
        if (call!=null){
            call.call();
        }
    }

    public abstract void execute();

}
