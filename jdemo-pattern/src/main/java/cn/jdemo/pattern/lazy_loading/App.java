package cn.jdemo.pattern.lazy_loading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        test01();
    }

    public static void test01() {
        HolderJdk8 holderJdk8 = new HolderJdk8();
        Heavy heavy = holderJdk8.getHeavy();
        Heavy heavy2 = holderJdk8.getHeavy();
        logger.info("heavy:"+heavy);//cn.jdemo.pattern.lazy_loading.Heavy@6e0e048a
        logger.info("heavy2"+heavy2);//cn.jdemo.pattern.lazy_loading.Heavy@6e0e048a

        HolderJdk8 holderJdk82 = new HolderJdk8();
        Heavy heavy22 = holderJdk82.getHeavy();
        logger.info("heavy22"+heavy22);//cn.jdemo.pattern.lazy_loading.Heavy@5bc79255
    }
}
