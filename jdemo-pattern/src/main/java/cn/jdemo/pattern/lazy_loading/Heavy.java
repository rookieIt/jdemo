package cn.jdemo.pattern.lazy_loading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class Heavy {
    private static final Logger logger = LoggerFactory.getLogger(Heavy.class);

    Heavy(){
        logger.info("Creating Heavy ...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.error("Exception caught.", e);
        }
        logger.info("... Heavy created");
    }
}
