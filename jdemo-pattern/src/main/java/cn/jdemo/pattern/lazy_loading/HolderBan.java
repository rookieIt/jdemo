package cn.jdemo.pattern.lazy_loading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/1/19
 */
public class HolderBan {
    private static  final  Logger logger = LoggerFactory.getLogger(HolderBan.class);
    private Heavy heavy;
    HolderBan(){
        logger.info("HolderBan created");
    }
    public Heavy getHeavy(){
        if (heavy == null){
            heavy = new Heavy();
        }
        return heavy;
    }
}
