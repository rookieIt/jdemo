package cn.jdemo.pattern.marker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/1/19
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        test01();

    }

    /**
     * 输出内容:
     * 14:23:46.224 [main] INFO cn.jdemo.pattern.marker.Guarder - Guarder has Permission enter...
     * 14:23:46.226 [main] INFO cn.jdemo.pattern.marker.App - thief no permission
     */
    public static void test01() {
        Guarder guarder = new Guarder();
        Thief thief = new Thief();

        if (guarder instanceof Permission){
            guarder.enter();
        }else{
            logger.info("guarder no permission");
        }

        if (thief instanceof Permission){
            thief.steal();
        }else{
            logger.info("thief no permission");
        }
    }
}
