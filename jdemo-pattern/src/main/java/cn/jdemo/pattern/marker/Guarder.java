package cn.jdemo.pattern.marker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2021/1/19
 */
public class Guarder implements Permission{
    private static final Logger logger = LoggerFactory.getLogger(Guarder.class);
    public void enter(){
        logger.info("Guarder has Permission enter...");
    }
}
