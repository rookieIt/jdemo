package cn.jdemo.pattern.marker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class defining Thief.
 *
 * @date 2021/1/19
 */
public class Thief {
    private static final Logger logger = LoggerFactory.getLogger(Guarder.class);
    protected void steal(){
        logger.info("Thief steal...");
    }
}
