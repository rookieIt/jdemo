/**
 * Creational pattern and a Structural pattern
 *
 *
 * adapter pattern
 *
 * apiGateway pattern:
 *
 * bridge pattern: 同一接口下的多实现的拆解（同一接口即为桥梁）;
 *
 * builder pattern: 伸缩式扩展构造器;
 *
 * bytecode pattern: 字节码模式，简化行为动作，量化为具体的bytecode(定义大量的行为，使其沙箱化)
 *
 * caching pattern:
 *
 * Callback pattern:
 *
 * adapter 与 bridge相同点:
 *  桥接和适配器都是让两个东西配合工作;
 * adapter 与 bridge区别点：
 * 适配器：改变已有的两个接口，让他们相容。
 * 桥接模式：分离抽象化和实现，使两者的接口可以不同，目的是分离。
 * 所以说，如果你拿到两个已有模块，想让他们同时工作，那么你使用的适配器。
 * 如果你还什么都没有，但是想分开实现，那么桥接是一个选择。
 * 桥接是先有桥，才有两端的东西
 * 适配是先有两边的东西，才有适配器
 *
 * @date 2021/1/18
 */
package cn.jdemo.pattern;