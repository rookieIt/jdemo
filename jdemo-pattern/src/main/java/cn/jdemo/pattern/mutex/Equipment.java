package cn.jdemo.pattern.mutex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Equipment(装备): 云顶之弈抢装备
 *
 * @description
 * @date 2021/1/28
 */
public class Equipment {

    private Lock mutex;
    private List<String> equipName;

    public Equipment(Lock mutex, String... equipName){
        this.mutex = mutex;
        this.equipName = new ArrayList<>(Arrays.asList(equipName)); // 处理为大家熟知的arraylist，不然remove方法不能好好用
    }

    public String grapEqiup(){
        String grapResult = "no equip";
        try {
            mutex.acquire();
            if (!equipName.isEmpty()){
                grapResult = equipName.iterator().next();
                equipName.remove(grapResult);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            mutex.release();
        }
        return grapResult;
    }

}
