package cn.jdemo.pattern.mutex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class Hero extends Thread{

    private static final Logger logger = LoggerFactory.getLogger(Hero.class);

    private String name;
    private Equipment equipment;

    public Hero(String name,Equipment equipment){
        this.name = name;
        this.equipment = equipment;
    }

    @Override
    public void run() {
        String eqiup = equipment.grapEqiup();
        logger.info(name+" grap "+eqiup);
    }
}
