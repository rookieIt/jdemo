package cn.jdemo.pattern.mutex;

/**
 *
 * @description
 * @date 2021/1/28
 */
public interface Lock {

    void acquire() throws InterruptedException;

    void release();
}
