package cn.jdemo.pattern.mutex;

/**
 * 案例: 云顶之弈,互斥抢装备
 *
 * @description
 * @date 2021/1/28
 */
public class App {
    public static void main(String[] args) {
        test01();
    }


    /**
     * 输出内容:
     * 17:52:18.718 [Thread-4] INFO cn.jdemo.pattern.mutex.Hero - other grap 水滴
     * 17:52:18.717 [Thread-1] INFO cn.jdemo.pattern.mutex.Hero - ekko grap 斗篷
     * 17:52:18.718 [Thread-3] INFO cn.jdemo.pattern.mutex.Hero - alistar grap 拳套
     * 17:52:18.719 [Thread-2] INFO cn.jdemo.pattern.mutex.Hero - ashe grap no equip
     * 17:52:18.716 [Thread-0] INFO cn.jdemo.pattern.mutex.Hero - will grap 大剑
     * 17:52:18.719 [Thread-5] INFO cn.jdemo.pattern.mutex.Hero - pyy grap no equip
     */
    public static void test01() {
        Mutex mutex = new Mutex();
        Equipment equipment = new Equipment(mutex, "大剑","斗篷","拳套","水滴");

        Hero will = new Hero("will", equipment);
        Hero ekko = new Hero("ekko", equipment);
        Hero ashe = new Hero("ashe", equipment);
        Hero alistar = new Hero("alistar", equipment);
        Hero other = new Hero("other", equipment);
        Hero pyy = new Hero("pyy", equipment);

        will.start();
        ekko.start();
        ashe.start();
        alistar.start();
        other.start();
        pyy.start();
    }
}
