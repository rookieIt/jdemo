package cn.jdemo.pattern.mutex.cas;

import cn.jdemo.pattern.mutex.Equipment;
import cn.jdemo.pattern.mutex.Hero;

/**
 *
 * @description
 * @date 2021/1/29
 */
public class App {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * 09:39:35.309 [Thread-2] INFO cn.jdemo.pattern.mutex.Hero - will grap 大剑
     * 09:39:35.315 [Thread-1] INFO cn.jdemo.pattern.mutex.Hero - ekko grap 斗篷
     * 09:39:35.316 [Thread-0] INFO cn.jdemo.pattern.mutex.Hero - aseh grap no equip
     */
    public static void test01() {
        CAS cas = new CAS();
        Equipment equipment = new Equipment(cas,"大剑","斗篷");
        Hero aseh = new Hero("aseh", equipment);
        Hero ekko = new Hero("ekko", equipment);
        Hero will = new Hero("will", equipment);

        aseh.start();
        ekko.start();
        will.start();
    }
}
