package cn.jdemo.pattern.mutex;

/**
 * 互斥锁，使用<code>wait</code>方法和<code>notiyf</code>方法
 *
 * @see Object#wait()
 * @see Object#notify()
 *
 * @description
 * @date 2021/1/28
 */
public class Mutex implements Lock{

    private Object owner;

    @Override
    public synchronized void acquire() throws InterruptedException {
        if (owner != null){
            wait();
        }
        owner = Thread.currentThread();
    }

    @Override
    public synchronized void release() {
        if (Thread.currentThread() == owner){
            owner = null;
            notify();
        }
    }

}
