package cn.jdemo.pattern.null_object;

/**
 *
 * @description
 * @date 2021/1/29
 */
public interface Node {

    int getSize();

    String getName();

    Node getLeft();

    Node getRight();

    default void work(){
        // do nothing
    }
}
