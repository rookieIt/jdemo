package cn.jdemo.pattern.null_object;


/**
 *
 * @description
 * @date 2021/1/29
 */
public class App {
    public static void main(String[] args) {
        // test01();
        test02();
    }

    /**
     * 输出内容:
     * 11:13:33.275 [main] INFO cn.jdemo.pattern.null_object.NodeImpl - Ashe is working with null,null
     */
    public static void test01() {
        Node node = new NodeImpl("Ashe",  NullNode.getInstance(), NullNode.getInstance());
        node.work();
    }

    /**
     * 输出内容:
     * 15:10:20.178 [main] INFO cn.jdemo.pattern.null_object.NodeImpl - Ashe is working with null,Ekko
     * 15:10:20.182 [main] INFO cn.jdemo.pattern.null_object.NodeImpl - Ekko is working with null,null
     */
    public static void test02() {
        Node node = new NodeImpl("Ashe"
                , NullNode.getInstance()
                , new NodeImpl("Ekko", NullNode.getInstance(), NullNode.getInstance()));
        node.work();

        node.getRight().work();
    }
}
