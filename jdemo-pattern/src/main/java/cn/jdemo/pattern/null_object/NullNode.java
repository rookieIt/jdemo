package cn.jdemo.pattern.null_object;

/**
 *
 * @description
 * @date 2021/1/29
 */
public class NullNode implements Node{

    private static final NullNode instance = new NullNode();

    private NullNode (){
    }

    public static NullNode getInstance(){
        return instance;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public String getName() {
        return "null";
    }

    @Override
    public Node getLeft() {
        return null;
    }

    @Override
    public Node getRight() {
        return null;
    }
}
