package cn.jdemo.pattern.null_object;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @description
 * @date 2021/1/29
 */
public class NodeImpl implements Node{

    private static final Logger logger = LoggerFactory.getLogger(NodeImpl.class);

    private Node left;
    private Node right;
    private String name;

    NodeImpl(String name, Node left,Node right){
        this.left = left;
        this.right = right;
        this.name = name;
    }

    @Override
    public int getSize() {
        return 1+left.getSize()+right.getSize();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Node getLeft() {
        return left;
    }

    @Override
    public Node getRight() {
        return right;
    }

    @Override
    public void work() {
        logger.info("{} is working with {},{}",name,left.getName(),right.getName());
    }
}
