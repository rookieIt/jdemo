package cn.jdemo.pattern.adapter;

/**
 * 适配渔船业务到划艇中
 * @description
 * @date 2021/1/19
 */
public class FishingBoatAdapter implements RowingBoat{

    // 渔船
    private FishingBoat fishingBoat;

    FishingBoatAdapter(){
        this.fishingBoat = new FishingBoat();
    }

    @Override
    public void row() {
        fishingBoat.sail();
    }
}
