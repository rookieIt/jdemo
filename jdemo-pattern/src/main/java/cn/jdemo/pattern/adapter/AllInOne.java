package cn.jdemo.pattern.adapter;

/**
 * adapter pattern(适配器)：
 *     (1)现有业务模块，渔船；
 *     (2)新业务模块，划艇；
 *     (3)增加中间适配器，把先有业务适配到新业务
 *
 * 基于封装，处理原有数据；
 * 基于继承，是配到新业务；
 *
 * @date 2021/1/18
 */
public class AllInOne {
}
