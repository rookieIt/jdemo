package cn.jdemo.pattern.adapter;

/**
 *
 * @description
 * @date 2021/1/19
 */
public class App {

    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * 13:33:42.239 [main] INFO cn.jdemo.pattern.adapter.FishingBoat - fishingBoat sail...
     *
     */
    public static void test01() {
        Captain captain = new App().new Captain(new FishingBoatAdapter());
        captain.row();
    }



    class Captain{
        // 划艇
        private RowingBoat rowingBoat;

        Captain(RowingBoat rowingBoat){
            this.rowingBoat = rowingBoat;
        }

        public void row(){
            rowingBoat.row();
        }
    }
}
