package cn.jdemo.pattern.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 渔船业务
 * @description
 * @date 2021/1/19
 */
public final class FishingBoat {
    private static final Logger logger = LoggerFactory.getLogger(FishingBoat.class);

    void sail(){
        logger.info("fishingBoat sail...");
    }
}
