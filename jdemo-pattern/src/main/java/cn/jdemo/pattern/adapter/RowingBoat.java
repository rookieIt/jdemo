package cn.jdemo.pattern.adapter;

/**
 * 划艇业务接口
 * @description
 * @date 2021/1/19
 */
public interface RowingBoat {
    void row();
}
