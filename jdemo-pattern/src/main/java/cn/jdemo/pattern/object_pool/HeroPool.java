package cn.jdemo.pattern.object_pool;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class HeroPool extends ObjectPool<Hero>{

    @Override
    protected Hero create() {
        return new Hero();
    }
}
