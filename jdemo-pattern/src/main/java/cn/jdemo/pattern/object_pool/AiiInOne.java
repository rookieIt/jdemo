package cn.jdemo.pattern.object_pool;

/**
 * object_pool
 *
 * When it is necessary to work with a large number of objects that are particularly expensive to
 * instantiate and each object is only needed for a short period of time, the performance of an
 * entire application may be adversely affected. An object pool design pattern may be deemed
 * desirable in cases such as these.
 *
 * 使用条件:
 *
 * 大量对象，极短时间使用，实例化代价高，性能影响了整体应用
 *
 * @description
 * @date 2021/1/28
 */
public class AiiInOne {
}
