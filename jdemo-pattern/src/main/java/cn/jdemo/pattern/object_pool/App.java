package cn.jdemo.pattern.object_pool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

/**
 * @see App#test03() 实例化耗能，可反复利用已初始化对象
 * @description
 * @date 2021/1/28
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static void main(String[] args) {
        // test01();
        // test02();
        test03();
    }

    /**
     * 输出内容:
     * spend time 2s :init str:2021-01-28T15:41:19.681
     */
    public static void test01() {
        HeroPool pool = new HeroPool();
        int before = LocalDateTime.now().getSecond();
        Hero hero01 = pool.checkOut();
        int after = LocalDateTime.now().getSecond();
        logger.info("spend time " + (after-before) +"s :"+hero01.getStr());
    }

    /**
     * 输出内容:
     * 15:43:22.236 [main] INFO cn.jdemo.pattern.object_pool.App - spend time 4s :
     * 15:43:22.239 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:43:20.231
     * 15:43:22.239 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:43:22.231
     */
    public static void test02() {
        HeroPool pool = new HeroPool();
        int before = LocalDateTime.now().getSecond();
        Hero hero01 = pool.checkOut();
        Hero hero02 = pool.checkOut();
        int after = LocalDateTime.now().getSecond();
        logger.info("spend time " + (after-before) +"s :");
        logger.info(hero01.getStr());
        logger.info(hero02.getStr());
    }


    /**
     * 输出内容:
     * 15:46:19.220 [main] INFO cn.jdemo.pattern.object_pool.App - spend time 4s :
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:46:17.215
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:46:19.215
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - -----------------------------------------
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - spend time 0s :
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:46:17.215
     * 15:46:19.223 [main] INFO cn.jdemo.pattern.object_pool.App - init str:2021-01-28T15:46:19.215
     */
    public static void test03() {
        HeroPool pool = new HeroPool();
        int before = LocalDateTime.now().getSecond();
        Hero hero01 = pool.checkOut();
        Hero hero02 = pool.checkOut();
        int after = LocalDateTime.now().getSecond();
        logger.info("spend time " + (after-before) +"s :");
        logger.info(hero01.getStr());
        logger.info(hero02.getStr());

        logger.info("-----------------------------------------");

        int before02 = LocalDateTime.now().getSecond();
        pool.putIn(hero01);
        pool.putIn(hero02);
        Hero hero03 = pool.checkOut();
        Hero hero04 = pool.checkOut();
        int after02 = LocalDateTime.now().getSecond();
        logger.info("spend time " + (after02-before02) +"s :");
        logger.info(hero03.getStr());
        logger.info(hero04.getStr());
    }
}
