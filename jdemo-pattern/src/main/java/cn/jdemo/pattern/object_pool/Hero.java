package cn.jdemo.pattern.object_pool;

import java.time.LocalDateTime;

/**
 *
 * @description
 * @date 2021/1/28
 */
public class Hero {

    private String str = "";

    {
        // init
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        str = "init str:"+ LocalDateTime.now().toString();
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
