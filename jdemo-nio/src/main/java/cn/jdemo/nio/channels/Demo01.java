package cn.jdemo.nio.channels;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 *
 * @see java.nio.channels.Channel  channel接口定义
 * @see java.nio.channels.Channels final类,定义了一些实用的静态方法
 *
 * @see java.io.RandomAccessFile
 *
 * @date 2020/12/24
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
    }
    public static void test01() {
        try {

            RandomAccessFile randomAccessFile = new RandomAccessFile("D:\\test.txt", "rw");
            FileChannel channel = randomAccessFile.getChannel();

            ByteBuffer buf = ByteBuffer.allocate(48);

            int tempSize = 0;
            while ((tempSize = channel.read(buf))!=-1){
                buf.flip();

                while(buf.hasRemaining()){
                    System.out.print((char) buf.get());
                }

                buf.clear();
            }

            randomAccessFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
