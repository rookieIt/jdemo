package cn.jdemo.base.wild;

public class Processor<T> {
    private T item;

    Processor(){
    }

    Processor(T t){
        this.item = t;
    }

    public void processor(T t){
        System.out.println(t);
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
