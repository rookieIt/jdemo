/**
 * 通配符
 *  <? super T> ?匹配的类型,是T的父类以及T本身
 *  <? extends T> ?匹配的类型,是T的子类以及T本身
 *
 * Java泛型 PECS（Producer Extends, Consumer Super）
 * 1.了解PECS前需要了解 java继承、泛型擦除、里氏替换原则。
 * @see cn.jdemo.base.clazz.TypeClear3#main(java.lang.String[]) 泛型擦除
 * https://www.zhihu.com/question/20400700
 * https://cloud.tencent.com/developer/article/1554858
 */
package cn.jdemo.base.wild;