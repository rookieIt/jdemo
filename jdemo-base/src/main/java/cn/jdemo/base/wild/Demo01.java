package cn.jdemo.base.wild;

/**
 * PECS（Producer Extends Consumer Super）原则:
 *
 * 频繁往外读取内容的，适合用上界Extends
 * 经常往里插入的，适合用下界Super
 */
public class Demo01 {
    public static void main(String[] args) {
        test04();
    }

    /**
     * cn.jdemo.base.wild.Obj2@4d591d15
     */
    public static void test04(){
        Processor<? extends Obj1> processor = new Processor<Obj1>(new Obj2());
        // Processor<? extends Obj1> processor1 = new Processor<Obj1>(new Obj1());
        // Processor<? extends Obj1> processor2 = new Processor<Obj1>(new Obj());// 报错
        // Processor<? extends Obj1> processor2 = new Processor<Obj2>(new Obj1());// 报错
        System.out.println(processor.getItem());
        // processor.setItem(new Obj1());// 报错
        // processor.setItem(new Obj2());// 报错
        // processor.setItem(new Obj());// 报错
    }

    /**
     * cn.jdemo.base.wild.Obj2@4d591d15
     * cn.jdemo.base.wild.Obj1@65ae6ba4
     */
    public static void test03(){
        Processor<? super Obj1> processor = new Processor<Obj1>(new Obj2());
        System.out.println(processor.getItem());
        processor.setItem(new Obj2());
        processor.setItem(new Obj1());
        // processor.setItem(new Obj()); 报错
        System.out.println(processor.getItem());
    }

    public void test01(){
        // Processor<? extends Obj1> processor = new Processor<Obj>();// 报错
        Processor<? extends Obj1> processor1 = new Processor<Obj1>();
        Processor<? extends Obj1> processor2 = new Processor<Obj2>();
    }
    public void test02(){
        Processor<? super Obj> processor = new Processor<Obj>();
        // Processor<? super Obj> processor1 = new Processor<Obj1>();// 报错
        // Processor<? super Obj> processor2 = new Processor<Obj2>();// 报错
    }
}
