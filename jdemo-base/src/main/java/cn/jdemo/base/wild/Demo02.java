package cn.jdemo.base.wild;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 */
public class Demo02 {
    public static void main(String[] args) {
        test01();
        test02();
    }

    /**
     * cn.jdemo.base.wild.Obj1@65ae6ba4
     * cn.jdemo.base.wild.Obj2@48cf768c
     */
    public static void test01(){
        List<? extends Obj1> list1 = new ArrayList<>();
        // list1.add(new Obj());// 报错
        // list1.add(new Obj1());// 报错
        // list1.add(new Obj2());// 报错

        List<? extends Obj1> list1_1 = new ArrayList<Obj1>(Collections.singletonList(new Obj1()));
        List<? extends Obj1> list1_2 = new ArrayList<Obj1>(Collections.singletonList(new Obj2()));
        // List<? extends Obj1> list1_2 = new ArrayList<Obj1>(Collections.singletonList(new Obj()));// 报错
        System.out.println(list1_1.get(0));
        System.out.println(list1_2.get(0));
    }

    /**
     * cn.jdemo.base.wild.Obj1@59f95c5d
     * cn.jdemo.base.wild.Obj2@5ccd43c2
     */
    public static void test02(){
        List<? super Obj1> list1 = new ArrayList<>();
        list1.add(new Obj1());
        list1.add(new Obj2());
        // list1.add(new Obj());// 报错

        System.out.println(list1.get(0));
        System.out.println(list1.get(1));
    }
}
