package cn.jdemo.base.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * VM options:-Xms20M -Xmx20M -Xmn10M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath="D:\oom"
 *
 * cd D:\oom
 *
 * jhat ./java_pidxxxx.hprof
 *
 * @description
 * @date 2021/3/25
 */
public class Demo01 {
    public static void main(String[] args) {
        Demo01 demo01 = new Demo01();
        demo01.test01();
    }

    private void test01() {

        List<Object> list = new ArrayList<Object>();
        while(true){
            list.add(new Object());
        }
    }
}
