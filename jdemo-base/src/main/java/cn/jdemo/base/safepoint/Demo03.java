package cn.jdemo.base.safepoint;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class Demo03 {

    public static AtomicInteger num = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        // test01();
        // test02();
        // test03();
        // test04();
        test05();
    }

    /**
     * 输出结果:
     * num=35775810,cost time:512
     * ... long time ...
     * Thread-0执行结束!
     * Thread-1执行结束!
     *
     * -XX:GuaranteedSafepointInterval 默认是1s
     */
    private static void test05() throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为可数循环
                for (int i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        // 调用其他静态方法
        long start = System.currentTimeMillis();
        Thread.sleep(500);
        System.out.println("num="+num +",cost time:"+ (System.currentTimeMillis() - start));
    }

    /**
     * 输出结果:
     *   num=0,cost time:1
     *
     *   ... long time ...
     *
     *   Thread-0执行结束!
     *   Thread-1执行结束!
     */
    private static void test01() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为可数循环
                for (int i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        long start = System.currentTimeMillis();
        // 调用其他静态方法
        Integer.parseInt("1");
        System.out.println("num="+num +",cost time:"+ (System.currentTimeMillis() - start));
    }

    /**
     * 输出结果:
     * num=0
     * ... long time ...
     * Thread-0执行结束!
     * Thread-1执行结束!
     */
    private static void test02() throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为可数循环
                for (int i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        // 调用其他静态方法
        Thread.sleep(0);
        // Thread.sleep(1000);
        System.out.println("num="+num);
    }

    /**
     * 输出结果:
     * num=29192
     * ... long time ...
     * Thread-0执行结束!
     * Thread-1执行结束!
     */
    private static void test03() throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为可数循环
                for (int i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        // 调用其他静态方法
        Thread.sleep(1);
        // Thread.sleep(1000);
        System.out.println("num="+num);
    }

    /**
     * 输出结果:
     * num=12056205
     * ... long time ...
     * Thread-0执行结束!
     * Thread-1执行结束!
     */
    private static void test04() throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为可数循环
                for (int i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        // 调用其他静态方法
        Thread.sleep(100);
        // Thread.sleep(1000);
        System.out.println("num="+num);
    }


}
