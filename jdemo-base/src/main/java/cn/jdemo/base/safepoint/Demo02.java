package cn.jdemo.base.safepoint;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class Demo02 {
    public static AtomicInteger num = new AtomicInteger(0);

    /**
     * 设置为不可数循环--不用等循环结束，在循环期间就能进入 Safepoint
     *
     * 输出结果:
     *   num=103864078,cost time:1001
     *
     *   ... ...(long long time...)
     *
     *   Thread-1执行结束!
     *   Thread-0执行结束!
     */
    public static void main(String[] args) throws InterruptedException {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 设置为不可数循环,int-->long
                for (long i = 0; i < 1000000000; i++) {
                    num.getAndAdd(1);
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        System.out.println("num="+num +",cost time:"+ (System.currentTimeMillis() - start));
    }


}
