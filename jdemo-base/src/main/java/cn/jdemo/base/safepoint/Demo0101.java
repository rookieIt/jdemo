package cn.jdemo.base.safepoint;

/**
 *
 * 总线风暴：volatile 和CAS 的操作导致BUS总线缓存一致性流量激增所造成
 */
public class Demo0101 {

    public static volatile int num = 0;

    /**
     * 输出内容:
     * Thread-1执行结束!
     * num=1157591296,cost time:29904
     * Thread-0执行结束!
     *
     */
    public static void main(String[] args) throws InterruptedException {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000000000; i++) {
                    num++;
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        System.out.println("num="+num +",cost time:"+ (System.currentTimeMillis() - start));
    }


}
