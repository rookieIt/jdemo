/**
 * safePoint（安全点）
 *     垃圾回收前即准备STW前,线程会检测safePoint(标志位),来确实是否挂起
 *     StopTheWorld指的是全局safepoint(针对hotspot),即要求所有线程都处于safepoint状态
 *
 * 那什么对象被可以作为“GC Root”呢？
 *  虚拟机栈中引用的对象
 *  方法区中引用的对象
 *  方法区中常量引用的对象
 *  本地方法栈中JNI引用的对象
 *  Java虚拟机内部的引用（类型对应的Class对象，常驻的异常对象等等）
 *  所有被synchronized持有的对象
 *  反映 Java 虚拟机内部情况的 JMXBean、JVMTI中注册的回调、本地代码缓存等等
 *
 * mutator:运行在JVM上的用户Java程序称为mutator
 * <a href="https://zhuanlan.zhihu.com/p/461298916"></>
 * <a href="https://www.ancii.com/ap2uauueq/"></>
 * <a href="https://juejin.cn/post/7139741080597037063"></>
 */
package cn.jdemo.base.safepoint;