package cn.jdemo.base.safepoint;

/**
 *
 * <a href="https://xie.infoq.cn/article/a80542aca7ad53efaaab1a27a">没有发生 GC 也进入了安全点？</>
 * 只要满足以下 3 个条件，也是会进入安全点的：
 *  1.VMThread 处于正常运行状态 (VMThread 肯定能正常运行)
 *  2.设计了进入安全点的间隔时间 (-XX:GuaranteedSafepointInterval默认是1s)
 *  3.SafepointALot 是否为 true 或者是否需要清理 (是否有代码缓存要清理)
 *
 * 总结:
 *  a.睡眠时间设置小于1s，也不会导致这个问题
 * @see Demo03#test05() 可以看这个
 *  b.SafepointALot-->这个可能是和inflation/代码缓存什么有关，普通int型++,这条件貌似不会触发
 *
 *
 *
 * <br/>总线风暴：volatile 和CAS 的操作导致BUS总线缓存一致性流量激增所造成
 */
public class Demo01 {

    public static int num = 0;

    /**
     * Thread-1执行结束!
     * Thread-0执行结束!
     * num=-2147392311,cost time:1014
     *
     */
    public static void main(String[] args) throws InterruptedException {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < Integer.MAX_VALUE; i++) {
                    num++;
                }
                System.out.println(Thread.currentThread().getName()+"执行结束!");
            }
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
        long start = System.currentTimeMillis();
        Thread.sleep(1000);
        System.out.println("num="+num +",cost time:"+ (System.currentTimeMillis() - start));
    }


}
