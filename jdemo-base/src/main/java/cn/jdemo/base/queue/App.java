package cn.jdemo.base.queue;

import cn.jdemo.base.Node;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @see cn.willbj.brief.queue 参考
 *
 * <p> 队列
 * @see java.util.Queue
 * @see java.util.Queue#offer(java.lang.Object) 添加
 * @see java.util.Queue#add(java.lang.Object) 添加-容量满异常
 * @see java.util.Queue#peek() 检索
 * @see java.util.Queue#element() 检索-容量空异常
 * @see java.util.Queue#poll() 检索并删除
 * @see java.util.Queue#remove() 检索并删除-容量空异常
 *
 * <p> 阻塞队列
 * @see java.util.concurrent.BlockingQueue
 * @see LinkedBlockingQueue
 * @see LinkedBlockingQueue#put(Object) 容量满,阻塞入队
 * @see LinkedBlockingQueue#take() 容量空,阻塞出队
 * @see ArrayBlockingQueue 必须初始化容量
 * @see ArrayBlockingQueue#put(Object) 容量满,阻塞入队
 * @see ArrayBlockingQueue#take() 容量空,阻塞出队
 *
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        test02();
        testBase();
    }

    public static void testBase() {
        Queue<Node> linkedBlockingQueue = new LinkedBlockingQueue<>();
    }



    public static void test02() throws InterruptedException {
        ArrayBlockingQueue<Node> nodes = new ArrayBlockingQueue<Node>(2);
        nodes.put(Node.builder().num(1).build());
        nodes.put(Node.builder().num(2).build());
        nodes.put(Node.builder().num(3).build());// 阻塞入队
        System.out.println("插入成功~");
    }

    public static void test01(){
        Queue<Node> nodes = new ArrayBlockingQueue<Node>(2);
        nodes.offer(Node.builder().num(1).build());
        nodes.add(Node.builder().num(2).build());
        Node element = nodes.element();
        System.out.println(element);
        Node peek = nodes.peek();
        System.out.println(peek);
        Node poll = nodes.poll();
        System.out.println(poll);
        Node remove = nodes.remove();
        System.out.println(remove);
    }

    public static void test03() throws InterruptedException {
        LinkedBlockingQueue<Node> objects = new LinkedBlockingQueue<>(2);
        objects.add(Node.builder().num(1).build());
        objects.offer(Node.builder().num(2).build());
        objects.put(Node.builder().num(3).build());
        System.out.println(objects.size());
    }
}
