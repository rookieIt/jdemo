package cn.jdemo.base.operator;

import java.util.Arrays;

/**
 *
 * @description
 * @date 2021/2/18
 */
public class Demo02 {
    public static void main(String[] args) {
        test01();
    }

    public static void test01(){
        int[] arr = {5,1};
        arr[0] = arr[0] ^ arr[0]; // a = a + b
        arr[0] = arr[0] ^ arr[0]; // b = a - b
        arr[0] = arr[0] ^ arr[0]; // a = a - b
        System.out.println(Arrays.toString(arr));
    }

}
