package cn.jdemo.base.operator;

/**
 *
 * @description
 * @date 2021/2/18
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
        test02();
        test03();
        test04();
        System.out.println("-----------------------------------");
        test05();
    }

    public static void test05(){
        System.out.println(4 >>> 1);// 2
        System.out.println(-4 >>> 1);// 2147483646
        System.out.println(Integer.toBinaryString(4 ));// 100
        System.out.println(Integer.toBinaryString(4 >>> 1));// 10
        System.out.println(Integer.toBinaryString(-4 ));// 1111 1111 1111 1111 1111 1111 1111 1100
        System.out.println(Integer.toBinaryString(-4 >>> 1));// 111 1111 1111 1111 1111 1111 1111 1110
    }
    public static void test04(){
        int a = 13;
        int b = 3;
        System.out.println(a/b);// 4
        System.out.println((double)a/b);// 4.333333333333333
        System.out.println((float)a/b);// 4.3333335
        System.out.println(Math.ceil((double)a/b));// 5.0
        System.out.println(Math.ceil((float)a/b));// 5.0
        System.out.println(Math.floor((double)a/b));// 4.0
        System.out.println(Math.floor((float)a/b));// 4.0
    }

    private static void test03() {
        int[] arr = {1,2};
        arr[0] = arr[0] ^ arr[1]; // a = a + b
        arr[1] = arr[0] ^ arr[1]; // b = a - b
        arr[0] = arr[0] ^ arr[1]; // a = a - b
        System.out.println(arr);
    }

    public static void test01() {
        // x与x-1,2进制会消除一位1;
        System.out.println(6 & 5);// 4 <== 110 & 101 = 100
        System.out.println(5 & 4);// 4 <== 101 & 100 = 100
        System.out.println(4 & 3);// 0 <== 100 & 011 = 000
        returnCount(6); // 2
        returnCount(5); // 2
        returnCount(4); // 1

    }

    /**
     * 返回其二进制中对应1的数量
     * @param n
     * @return 当前数对应二进制数,1的个数
     */
    public static int returnCount(int n){
        int c = 0;
        while(n!=0){
            n &= (n-1);
            c++;
        }
        System.out.println(c);
        return c;
    }

    public static void test02(){
        String s1 = new String("a");
        String s2 = "a";
        System.out.println(s1 == s2);// false
        System.out.println(s2 == s1.intern());// true
        System.out.println("-----------------------------------------");
        String s3 = new String("c") +new String("c");
        s3.intern();
        String s4 = "cc";
        System.out.println(s3 == s4);// true ,s3.intern直接存储对象引用到 字符串常量池中
        System.out.println(s3.intern() == s4);// true
        System.out.println("-----------------------------------------");
    }
}
