package cn.jdemo.base.clazz.load;

public class Other {
    private int i = method();
    private static int j = staticMethod();
    static {
        System.out.println("1、其他静态代码块");
    }

    Other() {
        System.out.println("2、其他构造器");
    }

    {
        System.out.println("3、其他代码块");
    }

    private int method() {
        System.out.println("4、其他方法");
        return 1;
    }

    private static int staticMethod() {
        System.out.println("5、其他静态方法");
        return 1;
    }
}
