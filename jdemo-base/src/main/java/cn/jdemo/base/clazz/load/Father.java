package cn.jdemo.base.clazz.load;

public class Father {
    private int i = method();
    private static int j = staticMethod();
    private int k = ovveride();
    static {
        System.out.println("1、父类静态代码块");
    }
    Father() {
        System.out.println("2、父类构造器");
    }
    {
        System.out.println("3、父类代码块");
    }
    private int method() {
        System.out.println("4、父类方法");
        return 1;
    }
    private static int staticMethod() {
        System.out.println("5、父类静态方法");
        return 1;
    }
    public int ovveride(){
        System.out.println("4、父类被重写方法");
        return 1;
    }
}
