package cn.jdemo.base.clazz;


public class DemoArr {
    public static void main(String[] args) throws InterruptedException {
        test03();
    }

    /**
     * Requested array size exceeds VM limit
     */
    private static void test01() {
        long[] arr = new long[Integer.MAX_VALUE];// 8B * Integer.MAX_VALUE -1 = 2的34次方字节 = 16G
        System.out.println(arr.length);
    }

    /**
     * Requested array size exceeds VM limit
     */
    private static void test02() {
        char[] arr = new char[Integer.MAX_VALUE];// 2B * Integer.MAX_VALUE -1 = 2的32次方字节 = 4G
        System.out.println(arr.length);
    }

    private static void test03() throws InterruptedException {
        boolean[] arr = new boolean[Integer.MAX_VALUE >> 20];// 1B * Integer.MAX_VALUE = 2的11次方字节-1 = 2KB-1
        System.out.println(arr.length);// 2047
        Thread.sleep(50*1000);
    }
}
