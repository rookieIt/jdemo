package cn.jdemo.base.clazz;

import java.util.ArrayList;

public class TypeClear2 {
    /* 不同泛型，但是Class对象同一个 */
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        System.out.println(list1.getClass() == list2.getClass());// true
    }
}
