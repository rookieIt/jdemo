package cn.jdemo.base.clazz.load;

/**
 * @see ClassLoader#loadClass(String)
 * 类仅加载了
 */
public class App3 {

    /**
     * 输出:
     * sun.misc.Launcher$AppClassLoader@18b4aac2
     * cn.jdemo.base.clazz.load.Other
     * ------------------
     * ------------------
     * 5、其他静态方法
     * 1、其他静态代码块
     * 4、其他方法
     * 3、其他代码块
     * 2、其他构造器
     * @param args
     */
    public static void main(String[] args) throws ClassNotFoundException {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);
        String name = Other.class.getName();
        System.out.println(name);
        System.out.println("------------------");
        systemClassLoader.loadClass(name);
        System.out.println("------------------");
        Other other = new Other();
    }
}
