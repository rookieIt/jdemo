package cn.jdemo.base.clazz.load;

/**
 * @see Class#forName(String)
 * 类加载、连接、静态初始化都执行了
 */
public class App2Vo {

    private static Son son;

    /**
     * 输出:
     * 5、其他静态方法
     * 1、其他静态代码块
     * -------相当于第二次-----------
     * 4、其他方法
     * 3、其他代码块
     * 2、其他构造器
     * @param args
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName(Other.class.getName());
        System.out.println("--------相当于第二次----------");
        Other other = new Other();
    }
}
