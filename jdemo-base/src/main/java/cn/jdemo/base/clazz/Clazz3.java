package cn.jdemo.base.clazz;


/**
 * @see Demojol#test05()
 */
public class Clazz3 {
    private long h = 1024L;
    private long[] i = new long[16];
    private char j = 'a';
    private boolean k = false;
    private byte l = 10;
    private long[] m = new long[]{1,2,3,4,5,6,7,8,9,10,11,13};
    private Clazz clazz;
    private Clazz clazz2= new Clazz();
}
