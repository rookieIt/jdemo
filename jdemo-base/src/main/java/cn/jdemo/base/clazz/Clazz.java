package cn.jdemo.base.clazz;


/**
 * @see Demojol#test01()
 * @see Demojol#test02()
 */
public class Clazz {
    private int a;
    private Integer b;
    private int[] c = new int[1024];// 4B*1024 = 4KB
    private long[] d = new long[1024];// 8B*1024 = 8KB
}
