package cn.jdemo.base.clazz.load;

public class Son extends Father{

    private int i = method();

    private static int j = staticMethod();

    private int k =ovveride();

    static {
        System.out.println("6、子类静态代码块");
    }

    Son() {
        System.out.println("7、子类构造器");
    }
    {
        System.out.println("8、子类代码块");
    }
    public int method() {
        System.out.println("9、子类方法");
        return 2;
    }
    private static int staticMethod() {
        System.out.println("10、子类静态方法");
        return 2;
    }
    @Override
    public int ovveride(){
        System.out.println("4、子类重写方法");
        return 1;
    }

    /**
     * 5、父类静态方法
     * 1、父类静态代码块
     * 10、子类静态方法
     * 6、子类静态代码块
     * 123
     * @param args
     */
    public static void main(String[] args) {

    }
}
