package cn.jdemo.base.clazz.load;

/**
 * @see Class#forName(String)
 * 类加载、连接、静态初始化都执行了
 */
public class App2 {

    private static Son son;

    /**
     * 输出:
     * 5、其他静态方法
     * 1、其他静态代码块
     * @param args
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName(Other.class.getName());
    }
}
