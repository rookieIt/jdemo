package cn.jdemo.base.demo;

/**
 * <a href="https://blog.csdn.net/zyc1234576/article/details/119297400"> new String()到底创建了几个对象？看这一篇就够了 </>
 */
public class StringDemo0103 {
	
	public static void main(String[] args) {
		// test01();// false
		// test02();// false
		test03();// false
	}

	public static void test01(){
		// String s1 = new StringBuilder().append("a").append("b").toString();
		char[] a =new char[]{'a','b'};
		String s1 = new String(a, 0, 2);
		// s1.intern();
		String str2 = "ab";
		System.out.println(str2 == s1);// false
	}
	public static void test02(){
		String s1 = new String("ab");
		// s1.intern();
		String str2 = "ab";
		System.out.println(str2 == s1);// false
	}
	public static void test03(){
		String str2 = "ab";// ldc
		String s1 = new String("ab");// new
		// s1.intern();
		System.out.println(str2 == s1);// false
	}



}
