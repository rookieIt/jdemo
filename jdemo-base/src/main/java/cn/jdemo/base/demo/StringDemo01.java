package cn.jdemo.base.demo;

public class StringDemo01 {
	
	public static void main(String[] args) {
		
		StringDemo01.test01();// true

		StringDemo01.test0101();// false
		StringDemo01.test0102();// true
		StringDemo01.test0103();// true

		StringDemo01.test02();// false
		StringDemo01.test0202();// false

		StringDemo01.test03();// true
		StringDemo01.test0301();// false
		StringDemo01.test0302();// true
		
		StringDemo01.test04();// false
		
		StringDemo01.test05();// false
	}
	
	public static void test01(){
		String str2 = new String("str")+new String("01");
		
		/* intern()方法 :
		 * (1)如果字符串常量池中已经包含一个等于此String对象的字符串，则返回代表池中这个字符串的String对象；
		 * (2)否则，将此String对象包含的字符添加到常量池中，并返回此String对象的引用。
		 * (a) When the intern method is invoked, if the pool already contains a
		 * string equal to this {@code String} object as determined by
		 * the {@link #equals(Object)} method, then the string from the pool is
		 * returned. 
		 * (b) Otherwise, this {@code String} object is added to the
		 * pool and a reference to this {@code String} object is returned.
		 */
		String intern = str2.intern();
		
		String str1 = "str01";
		System.out.println(intern==str1);
	}

	public static void test0101(){
		String str2 = new String("str")+new String("01");
		str2.intern();
		String str1 = "str01";
		System.out.println(str2==str1);// false,前面已经有了str01
	}

	public static void test0102(){
		String str2 = new String("1") + new String("1");
		str2.intern();
		String str1 = "11";
		System.out.println(str2 == str1);// true,???
	}
	public static void test0103(){
		String str2 = new String("str") + new String("a");
		str2.intern();
		String str1 = "stra";
		System.out.println(str2 == str1);// true,???
	}

	public static void test02(){
		String str2 = new String("str") + new String("01");
		String str1 = "str01";
		System.out.println(str2==str1);// false
	}
	public static void test0202(){
		String str2 = new String("1") + new String("1");
		String str1 = "11";
		System.out.println(str2==str1);// false
	}

	public static void test03(){
		String s1 = "hello" + "word";//true 编译后优化为: String s1 = "helloword";
		String s2 = "helloword";
		System.out.println(s1 == s2);
	}
	public static void test0301(){
		String s1 = "abc";
		String s2 = "a";
		String s3 = "bc";
		String s4 = s2 + s3;// false，因为s2+s3实际上是使用StringBuilder.append来完成，会生成不同的对象。
		System.out.println(s1 == s4);
	}
	public static void test0302(){
		String s1 = "abc";
		final String s2 = "a";
		final String s3 = "bc";
		String s4 = s2 + s3;// true，因为final变量在编译后会直接替换成对应的值，所以实际上等于s4=”a”+”bc”，而这种情况下，编译器会直接合并为s4=”abc”，所以最终s1==s4。
		System.out.println(s1 == s4);
	}
	
	public static void test04(){
		String t1 = new String("hello");
		String t2 = new String("word");
		String s1 = t1 + t2;
		String s2 = "helloword";
		System.out.println(s1 == s2);// false
	}
	
	public static void test05(){
		final String t1 = new String("hello");
		final String t2 = new String("word");
		String s1 = t1 + t2;
		String s2 = "helloword";
		System.out.println(s1 == s2);// false
	}
	

		
}
