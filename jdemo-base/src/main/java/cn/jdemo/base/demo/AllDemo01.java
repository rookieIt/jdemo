package cn.jdemo.base.demo;

/**
 * 自动装箱
 * @see Integer#valueOf(int) 
 * @see Character#valueOf(char) 
 * @see String#valueOf(Object/char[]/char/int/short/double/long)
 * @see Long#valueOf(long)
 * 自动拆箱
 * @see Integer#intValue()
 * @see Character#charValue()
 * @see Long#longValue()
 * ... ..
 */
public class AllDemo01 {
    public static void main(String[] args) {
        Integer x = 10; // ==> Integer x = Integer.valueOf(10);
    }
}
