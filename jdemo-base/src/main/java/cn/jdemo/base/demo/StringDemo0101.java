package cn.jdemo.base.demo;

/**
 * <a href="https://blog.csdn.net/zzpdbk/article/details/100046278">  字符串与三个常量池 </>
 *
 * 常量池
 * 运行期常量池
 * 字符串常量池
 *
 * ldc指令 : 从运行时常量池中推送至栈顶
 *
 */
public class StringDemo0101 {
	
	public static void main(String[] args) {
		test01();// true
		test01();// false
		// test0101();// false
	}

	/**
	 * @see StringBuilder#toString() 底层是new String(char[], int, int)
	 * @see String#String(char[], int, int)
	 * 只执行了new指令,没有执行ldc
	 */
	public static void test01(){
		String str2 = new String("str")+new String("01");// StringBuilder.append().toString --
		str2.intern();
		String str1 = "str01";
		System.out.println(str2 == str1);// true
	}
	public static void test0101(){
		String str2 = new String("str")+new String("01");
		str2.intern();
		String str1 = "str01";
		System.out.println(str2 == str1);// false
	}
}
