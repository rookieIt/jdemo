package cn.jdemo.base.demo;

public class StringDemo0102 {
	
	public static void main(String[] args) {
		test01();// false
		test0101();// false
	}

	public static void test01(){
		String str2 = new String("str");
		str2.intern();
		String str1 = "str";
		System.out.println(str2 == str1);// false
	}
	public static void test0101(){
		String str2 = new String("12");
		str2.intern();
		String str1 = "12";
		System.out.println(str2 == str1);// false
	}


}
