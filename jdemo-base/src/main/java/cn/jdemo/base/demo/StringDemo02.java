package cn.jdemo.base.demo;

/**
 * 
 * @author user
 *
 */
public class StringDemo02 {

	public static void main(String[] args) {
		/*
		 * 
		 * 字符串连接 - 使用 + 操作符 : 0 ms
		 * 字符串连接 - 使用 StringBuffer : 4 ms
		 */
		StringDemo02.test01();
		
		/*
		 * 字符串连接 - 使用 + 操作符 : 2959 ms
		 * 字符串连接 - 使用 StringBuffer : 2 ms
		 */
		StringDemo02.test02();
	}

	/**
	 * + : string 长度定长
	 * 
	 * + 性能优于 StringBuffer
	 */
	public static void test01() {
		// +"为每个字符串变量赋值，公用一个内值，占用一份内存空间
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			String result = "This is" + "testing the" + "difference" + "between" + "String" + "and" + "StringBuffer";
		}
		long endTime = System.currentTimeMillis();
		System.out.println("字符串连接" + " - 使用 + 操作符 : " + (endTime - startTime) + " ms");// 字符串连接 - 使用 + 操作符 : 0 ms

		// "StringBuffer"每次新建一个新对象，内存分配新的空间，新分配10000份内存空间
		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			StringBuffer result = new StringBuffer();
			result.append("This is");
			result.append("testing the");
			result.append("difference");
			result.append("between");
			result.append("String");
			result.append("and");
			result.append("StringBuffer");
		}
		long endTime1 = System.currentTimeMillis();
		System.out.println("字符串连接" + " - 使用 StringBuffer : " + (endTime1 - startTime1) + " ms");// 字符串连接 - 使用StringBuffer : 5 ms
	}
	
	/**
	 * + : string长度变长
	 * 
	 * StringBuffer 性能优于 +
	 */
	public static void test02() {
		// +"为每个字符串变量赋值，公用一个内值，占用一份内存空间
		long startTime = System.currentTimeMillis();
		String result = null;
		for (int i = 0; i < 10000; i++) {
			result += "This is" + "testing the" + "difference" + "between" + "String" + "and" + "StringBuffer";
		}
		long endTime = System.currentTimeMillis();
		System.out.println("字符串连接" + " - 使用 + 操作符 : " + (endTime - startTime) + " ms");

		// "StringBuffer"每次新建一个新对象，内存分配新的空间，新分配10000份内存空间
		long startTime1 = System.currentTimeMillis();
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < 10000; i++) {
			sBuffer.append("This is");
			sBuffer.append("testing the");
			sBuffer.append("difference");
			sBuffer.append("between");
			sBuffer.append("String");
			sBuffer.append("and");
			sBuffer.append("StringBuffer");
		}
		long endTime1 = System.currentTimeMillis();
		System.out.println("字符串连接" + " - 使用 StringBuffer : " + (endTime1 - startTime1) + " ms");
	}
}
