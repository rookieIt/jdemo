package cn.jdemo.base.demo.str;

public class Str01 {
    public static void main(String[] args) {
        String str1 = new String("1") + new String("a");
        String str2 = "1a";
        System.out.println(str1 == str2);
    }
}
