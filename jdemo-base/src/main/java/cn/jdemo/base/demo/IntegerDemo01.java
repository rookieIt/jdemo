package cn.jdemo.base.demo;

public class IntegerDemo01 {
    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE == Integer.MAX_VALUE);// true

        Integer temp01 = Integer.MAX_VALUE;
        Integer temp02 = Integer.MAX_VALUE;
        System.out.println(temp01 == temp02);//false

        temp01 = 127;
        temp02 = 127;
        System.out.println(temp01 == temp02);//true Integer中缓存

        temp01 = 128;
        temp02 = 128;
        System.out.println(temp01 == temp02);//false
    }
}
