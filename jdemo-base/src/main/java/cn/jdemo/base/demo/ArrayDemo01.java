package cn.jdemo.base.demo;

/**
 * 数组扩容
 *
 */
public class ArrayDemo01 {
	public static void main(String[] args) {
		String[] names = new String[] {"a","b","c"};
		String[] copy = new String[4];
		copy[3] = "d";
		System.arraycopy(names, 0, copy, 0, names.length);
		for (String str : copy) {
			System.out.println(str);
		}
		// Collections.reverse(list);
	}
}
