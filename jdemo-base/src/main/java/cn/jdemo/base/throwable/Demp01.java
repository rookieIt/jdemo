package cn.jdemo.base.throwable;

/**
 * @date 2021/3/23
 */
public class Demp01 {
    public static void main(String[] args) {
        test01();// Exception in thread "main" java.lang.ArithmeticException: / by zero

        try {
            test02();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void test01() throws RuntimeException{
        System.out.println(1/0);
    }

    private static void test02() throws Exception{
        System.out.println(1/0);
    }
}
