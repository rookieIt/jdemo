package cn.jdemo.base.throwable;

/**
 * @see java.lang.Throwable
 *
 * @see java.lang.Exception
 *
 * @see java.lang.RuntimeException 其及其子类，未经检查的异常，不需要在方法或构造函数的声明中声明
 * @see java.lang.UnsupportedOperationException
 *
 * @description
 * @date 2021/1/11
 */
public class AllInOne {
}
