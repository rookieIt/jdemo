package cn.jdemo.base.binary;
public class Demo01 {
    public static void main(String[] args) {

        System.out.println(Integer.MIN_VALUE >> 2);// -536870912
        System.out.println(Integer.MIN_VALUE >>> 2);// 536870912 // 无符号右移

        System.out.println(Integer.MAX_VALUE << 2);//0x7fffffff -4
        System.out.println(((long)Integer.MAX_VALUE) << 2);//0x7fffffff 8589934588
        System.out.println(Integer.MIN_VALUE);// 0x80000000 -2147483648
        System.out.println(Integer.MIN_VALUE << 2);// 0x80000000 0

        System.out.println(0xffffffff);// -1
        System.out.println(0xffffffff >>  2);// -1
        System.out.println(0xffffffff <<  2);// -4

        System.out.println(0xffffffff >>> 2); // 1073741823

    }
}
