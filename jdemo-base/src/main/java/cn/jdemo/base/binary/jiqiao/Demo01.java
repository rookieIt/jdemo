package cn.jdemo.base.binary.jiqiao;

/**
 * 求最近的2的整次幂
 */
public class Demo01 {
    public static void main(String[] args) {
        System.out.println(test01(27));// 32
        System.out.println(test01(32));// 32
        System.out.println(test01(33));// 64
        System.out.println(test01(2));// 2
        System.out.println(test01(3));// 4
    }

    public static int test01(int cap){
        int n = cap - 1;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        return n+1;
    }

}
