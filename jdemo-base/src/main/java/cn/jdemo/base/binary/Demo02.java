package cn.jdemo.base.binary;

public class Demo02 {
    public static void main(String[] args) {
        test01();
    }

    private static void test01() {
        System.out.println(-2>>1);// -1 算术右移,高位原为0补0,原为1补充1
        System.out.println(-2>>>1);// 2147483647  逻辑右移,高位补0
        System.out.println(Integer.MAX_VALUE);// 2147483647
    }
}
