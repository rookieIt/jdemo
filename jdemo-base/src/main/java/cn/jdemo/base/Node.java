package cn.jdemo.base;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Node {
    Integer num;
    String name;
}
