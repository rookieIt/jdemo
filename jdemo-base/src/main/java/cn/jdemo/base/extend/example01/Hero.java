package cn.jdemo.base.extend.example01;

/**
 *
 * @date 2021/2/15
 */
public class Hero {
    private String id;
    private String name;
    private String sex;

    Hero(){
        System.out.println("Hero() init...");
    }

    Hero(String id,String name,String sex){
        this.id = id;
        this.name = name;
        this.sex = sex;
    }
}
