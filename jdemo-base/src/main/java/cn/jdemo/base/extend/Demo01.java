package cn.jdemo.base.extend;

import cn.jdemo.base.extend.example01.Hero;
import cn.jdemo.base.extend.example01.HeroEkko;

/**
 *
 * @date 2021/2/15
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出内容:
     * Hero() init...
     */
    public static void test01() {
        HeroEkko heroEkko = new HeroEkko();
    }
}
