package cn.jdemo.base.stack;

import cn.jdemo.base.Node;

import java.util.Stack;

/**
 * <p> 1) Stack extends Vector
 * <p> 2) Stack 线程安全的
 * <p> 3) 底层数组自动扩容
 *
 * @see Stack#push(Object) 底层调用父类的addElement方法 - 容量满,自动扩容
 * @see Stack#pop() 容量空异常
 * @see Stack#peek() 查看,不移除 - 容量空异常
 * @see Stack#empty() 判空
 * @see Stack#search(Object)  从1~n,栈顶元素为1
 *
 */
public class App {
    public static void main(String[] args) {
        test01();
        System.out.println("-------------------------");
        test02();
    }

    public static void test02() {
        Stack<Node> stack = new Stack<>();
        stack.push(Node.builder().num(1).build());
        System.out.println(stack.peek());// Node(num=1, name=null)
        System.out.println(stack.pop());// Node(num=1, name=null)
        System.out.println(stack.peek());// EmptyStackException
        System.out.println(stack.pop());// EmptyStackException
    }

    public static void test01() {
        Stack<Node> stack = new Stack<>();
        Node node1 = Node.builder().num(1).build();
        Node node2 = Node.builder().num(2).build();
        stack.push(node1);
        stack.push(node2);
        System.out.println(stack.search(node1));// 2
        System.out.println(stack.search(Node.builder().num(1).build()));// 2
        System.out.println(stack.search(node2));// 1
    }
}
