package cn.jdemo.base.new8;

/**
 *
 * 常说的五种创建对象的方式
 *
 * 1 new创建，通过new关键字调用构造方法
 * 2 反射创建对象，Class中的newInstance()方法和Constructor中的newInstance()，注意区分两者的区别。
 * 3 Object中的clone方法创建，记得实现Cloneable接口
 * 4 反序列化创建，首先要有列化之后的对象资源，实现Serializable接口
 * 5 Unsafe.getUnsafe().allocateInstance(Class.class)  rt.jar中sun.misc包中的native方法。
 *
 * 另外还有几种特殊的创建方式
 *
 * 6 jdk动态代理  java.lang.reflect.Proxy
 * 7 cglib代理创建
 * 8 Objenesis类库 提供了多种创建对象的方式
 *
 * @date 2021/2/9
 */
public class AllInOne {
}
