package cn.jdemo.base.collect;

import java.util.LinkedHashMap;
import java.util.Map;

public class Demo01 {
    public static void main(String[] args) throws InterruptedException {
        test1();
        test2();
        while (true){
            Thread.sleep(1000);
            System.out.println("运行中...");
        }
    }

    /**
     * key-value添加顺序
     * 访问前...
     * 1
     * 2
     * 3
     * 访问后...
     * 1
     * 2
     * 3
     */
    public static void test1(){
        Map<Integer,Integer> map = new LinkedHashMap();
        map.put(1,2);
        map.put(2,2);
        map.put(3,2);
        System.out.println("访问前...");
        map.keySet().forEach(t->{
            System.out.println(t);
        });

        System.out.println("访问后...");
        map.get(2);
        map.keySet().forEach(t->{
            System.out.println(t);
        });
    }

    /**
     * key-value访问顺寻
     * 访问前...
     * 1
     * 2
     * 3
     * 访问后...
     * 1
     * 3
     * 2
     */
    public static void test2(){
        Map<Integer,Integer> map = new LinkedHashMap(8,0.75f,true);

        map.put(1,2);
        map.put(2,2);
        map.put(3,2);
        System.out.println("访问前...");
        map.keySet().forEach(t->{
            System.out.println(t);
        });

        System.out.println("访问后...");
        map.get(2);
        map.keySet().forEach(t->{
            System.out.println(t);
        });
    }
}
