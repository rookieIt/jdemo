/**
 *
 * ####### 刷题集合(常用) ##########
 * @see cn.jdemo.base.queue.App 队列
 *
 * @see java.util.Set#iterator()
 * @see java.util.Set#add(java.lang.Object)
 * @see java.util.Set#remove(java.lang.Object)
 * @see java.util.Set#contains(java.lang.Object)
 * @see java.util.Set#toArray()
 * @see java.util.Set#size()
 *
 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
 * @see java.util.Map#get(java.lang.Object)
 * @see java.util.Map#getOrDefault(java.lang.Object, java.lang.Object)
 * @see java.util.Map#remove(java.lang.Object)
 * @see java.util.Map#remove(java.lang.Object, java.lang.Object)
 * @see java.util.Map#containsKey(java.lang.Object)
 * @see java.util.Map#containsValue(java.lang.Object)
 * @see java.util.Map#size()
 * @see java.util.Map#keySet()
 * @see java.util.Map#entrySet()
 * @see java.util.Map#values()
 *
 * @see java.util.List#add(java.lang.Object)
 * @see java.util.List#add(int idx, java.lang.Object)
 * @see java.util.List#set(int, java.lang.Object)
 * @see java.util.List#get(int idx)
 * @see java.util.List#indexOf(java.lang.Object)
 * @see java.util.List#remove(java.lang.Object)
 * @see java.util.List#remove(int idx)
 *
 * @see java.util.LinkedHashMap 有序hashmap(默认添加顺序)
 * @see java.util.LinkedHashMap#LinkedHashMap(int initCap, float loadFactor, boolean accessOrder) 访问顺序
 * @see java.util.LinkedHashSet 有序set
 *
 * @see java.util.LinkedList 双端队列 + 列表
 * @see java.util.LinkedList#offer(java.lang.Object)
 * @see java.util.LinkedList#offerFirst(java.lang.Object)
 * @see java.util.LinkedList#offerLast(java.lang.Object)
 * @see java.util.LinkedList#poll()
 * @see java.util.LinkedList#pollFirst()
 * @see java.util.LinkedList#pollLast()
 *
 *
 * ####### 线程安全集合 ##########
 *
 *
 */
package cn.jdemo.base.collect;