package cn.jdemo.base.deque;

import cn.jdemo.base.Node;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @see cn.willbj.brief.queue 参考
 *
 * <p>双端队列
 * @see Deque
 * @see Deque#offer(Object) 添加
 * @see Deque#offerFirst(Object)
 * @see Deque#offerLast(Object)
 * @see Deque#peek() 检索
 * @see Deque#peekFirst()
 * @see Deque#peekLast()
 * @see Deque#poll() 检索并删除
 * @see Deque#pollFirst()
 * @see Deque#pollLast()
 *
 * <p>双端队列-非阻塞
 * @see java.util.ArrayDeque 初始化数组容量默认16
 *
 * <p>阻塞双端队列
 * @see java.util.concurrent.BlockingDeque
 * @see LinkedBlockingDeque
 * @see LinkedBlockingDeque#put(Object) 阻塞添加
 * @see LinkedBlockingDeque#putFirst(Object)
 * @see LinkedBlockingDeque#putLast(Object)
 * @see LinkedBlockingDeque#take() 阻塞检出并删除
 * @see LinkedBlockingDeque#takeFirst()
 * @see LinkedBlockingDeque#takeLast()
 *
 */
public class App {
    public static void main(String[] args) {
        testBase();
        test01();
    }

    public static void testBase(){
        // 基于数组的双端队列
        Deque<Node> deque = new ArrayDeque<>();
        deque.offer(Node.builder().num(1).build());
        deque.offerFirst(Node.builder().num(2).build());
        deque.offerLast(Node.builder().num(3).build());
        System.out.println(deque.pollFirst());// Node(num=2, name=null)
        System.out.println(deque.pollFirst());// Node(num=1, name=null)
        System.out.println(deque.pollFirst());// Node(num=3, name=null)
    }

    public static void test01(){
        // 基于链表的阻塞双端队列
        Deque<Node> deque = new LinkedBlockingDeque<>();
        deque.offer(Node.builder().num(1).build());
        deque.offerFirst(Node.builder().num(2).build());
        deque.offerLast(Node.builder().num(3).build());
        System.out.println(deque.pollFirst());// Node(num=2, name=null)
        System.out.println(deque.pollFirst());// Node(num=1, name=null)
        System.out.println(deque.pollFirst());// Node(num=3, name=null)
    }
}
