package cn.jdemo.base.test;

import java.util.HashMap;
import java.util.Map;

/**
 * 方法重写
 * 1) 父子类之间（方法名）相同，（参数列表）相同。
 * 2) 子类重写方法的（返回值类型以及抛出的异常）,要比父类的更严格;
 * 3) 子类重写方法的（权限修饰符）,要比父类的要宽松或者相同
 *
 * 构造方法
 * 1) 子类是无法继承父类构造方法的
 * 2) 子类的初始化过程中，必须先执行父类的初始化动作
 * 3) super()和super(实参列表)都只能出现在子类构造器的首行
 */
public class D extends E{

    @Override
    public HashMap test(int x, String y, HashMap map) throws RuntimeException {
        return null;
    }
}

 abstract class E{
    protected abstract Map test(int x, String y, HashMap map) throws Exception;
 }