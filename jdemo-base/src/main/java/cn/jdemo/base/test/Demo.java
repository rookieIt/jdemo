package cn.jdemo.base.test;

import java.util.*;

public class Demo {
    public static void main(String[] args) {

        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(1);
        set.add(2);
        System.out.println(set.size());

        Integer x = 1000;
        Integer y = 1000;
        System.out.println(x==y);
        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>(10);
        map.put(2,2);
        map.put(1,2);
        map.put(3,2);

        map.remove(1);
        map.keySet().forEach(t-> System.out.println(map.get(t)));

        System.out.println(map.containsKey(2));
        System.out.println(map.containsValue(3));

        List<Integer> list = new LinkedList<>();
        System.out.println(list.contains(1));

    }
}
