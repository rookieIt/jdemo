/**
 * Guava工程包含了若干被Google的Java项目广泛依赖 的核心库
 * 例如：集合 [collections] 、缓存 [caching] 、原生类型支持 [primitives support] 、
 *      并发库 [concurrency libraries] 、通用注解 [common annotations] 、字符串处理 [string processing] 、I/O 等等。
 *
 * 们经常使用System.currentTimeMillis()来计算某段代码执行的实现，Stopwatch 可以很方便的实现同样的功能。
 *
 *   Stopwatch stopwatch = Stopwatch.createStarted();
 *                 for(int i =0; i <10000; i++){
 *                  //do something test
 *                 }
 *   long nanos = stopwatch.elapsed(TimeUnit.NANOSECONDS);
 *   System.out.println(nanos);
 *
 *
 *
 *
 * 4. 线程
 * com.google.common.util.concurrent目录下是各种线程工具类。
 * 这里介绍一下：
 * ListenableFuture：顾名思义就是可以监听的Future，它是对java原生Future的扩展增强。
 * MoreExecutors：该工具类，提供了很多静态方法。其中listeningDecorator方法初始化ListeningExecutorService方法，使用此实例submit方法即可初始化ListenableFuture对象。
 * ListeningExecutorService的invokeAny继承自Jdk原生类，率先返回线程组中首个执行完毕的。
 * ListeningExecutorService的invokeAll并行执行线程组，等待所有线程执行完毕，适用于批量处理。
 *
 *    public final List<String> list = Lists.newArrayList("a", "b", "c", "d", "e", "f");
 *
 *     @Test
 *     public void testGuava() {
 *         ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));
 *         List<Callable<String>> allThread = Lists.newArrayList();
 *         for (final String value : list) {
 *             Callable<String> callable = new Callable<String>() {
 *                 @Override
 *                 public String call() {
 *                     return value + " 休息了 " + sleepRandom();
 *                 }
 *             };
 *             allThread.add(callable);
 *         }
 *         try {
 *             //1. 返回首个执行完毕的值
 *             //  String date = listeningExecutorService.invokeAny(allThread);
 *             // 2. 并行执行完所有线程
 *             List<Future<String>> valueList = listeningExecutorService.invokeAll(allThread);
 *             for (Future<String> future : valueList) {
 *                 System.out.println(future.get());
 *             }
 *         } catch (Exception e) {
 *             e.printStackTrace();
 *         } finally {
 *             listeningExecutorService.shutdown();
 *         }
 *     }
 *
 * 在Java 8中可以这样实现
 *
 *  @Test
 *     public void testJava8() throws Exception {
 *
 *         ExecutorService executor = Executors.newFixedThreadPool(10);
 *         List<CompletableFuture> futures = list.stream()
 *                 .map(value -> CompletableFuture.completedFuture(value).thenApplyAsync(s -> s + " 休息了 " + sleepRandom(), executor))
 *                 .collect(Collectors.toList());
 *         //1. 返回首个执行完毕的值
 *         // CompletableFuture valueFuture = CompletableFuture.anyOf(futures.toArray(new CompletableFuture[futures.size()]));
 *         // 2. 并行执行完所有线程
 *         CompletableFuture valueFutureList = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
 *         futures.stream().forEach(f -> {
 *             try {
 *                 System.out.println(f.get());
 *             } catch (Exception e) {
 *
 *             }
 *         });
 *
 *     }
 *
 * @date 2020/12/31
 */
package cn.jdemo.guava;