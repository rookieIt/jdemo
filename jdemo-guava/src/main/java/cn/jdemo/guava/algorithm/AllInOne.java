package cn.jdemo.guava.algorithm;

/**
 * @see com.google.common.hash.BloomFilter
 * @see com.google.common.util.concurrent.RateLimiter
 *  * 6.1 布隆过滤
 *  * 布隆过滤器用于判断一个元素是否在一个超大的集合中。
 *  * 哈希表也能用于判断元素是否在集合中，但是布隆过滤器只需要哈希表的1/8或1/4的空间复杂度就能完成同样的功能。
 *  * Guava中的布隆过滤实现:
 *  * com.google.common.hash.BloomFilter
 *  *
 *  * 6.2 限流算法
 *  * Guava 中提供了RateLimiter类，它经常用于限制对一些物理资源或者逻辑资源的访问速率。
 *  * com.google.common.util.concurrent.RateLimiter
 * @date 2021/1/6
 */
public class AllInOne {
}
