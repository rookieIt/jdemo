package cn.jdemo.guava.collections;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @date 2021/1/7
 */
public class Demo01 {
    public static void main(String[] args) {
        test02();
    }

    public static void test01() {
        CopyOnWriteArrayList<Object> copyOnWriteArrayList = Lists.newCopyOnWriteArrayList();
        LinkedList<Object> linkedList = Lists.newLinkedList();
    }

    public static void test02() {
        Lists.newArrayList("ashe", "ekko").forEach(t-> System.out.println(t));
    }

    public static void test03() {
        HashMap<Object, Object> map = Maps.newHashMap();
    }

}
