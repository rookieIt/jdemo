package cn.jdemo.guava.collections;

/**
 *
 * 3.1 集合创建
 * 各种以S结尾的工厂类简化了集合的创建。在创建泛型实例的时候，它们使代码更加简洁
 * List<String> list =Lists.newArrayList();
 * List<String> list2 = Lists.newArrayList("a","b","c");
 * Set<Integer> set = Sets.newHashSet();
 *
 * 类型推导的功能，在Java 7中已经得到支持。
 * Map<String,List<String>> m=new HashMap<>();
 *
 * 可以返回原返回类型的任何子类型的对象
 * Maps.newHashMap();
 * Maps.newLinkedHashMap();
 *
 * 3.2 集合转换、查找、过滤、分割
 * Lists.transform可以很方便将一个集合转换成另外一种类型的集合。
 *  List<String> listStr = Lists.newArrayList("1", "2", "3");
 *  //将字符串集合转换为Integer集合
 *  List<Integer> listInteger = Lists.transform(listStr, new Function<String, Integer>() {
 *             @Nullable
 *             @Override
 *             public Integer apply(@Nullable String s) {
 *                 return Integer.valueOf(s);
 *             }
 *         });
 *
 * 在Java 8中的实现为
 *
 *   List<Integer> listInteger2 = listStr.stream().map(Integer::valueOf).collect(Collectors.toList());
 *
 * Iterables.find查找集合首个匹配的元素
 *         List<String> listStr = Lists.newArrayList("hello", "world", "hehe");
 *         //查找首个以h开头的值
 *         String value = Iterables.find(listStr, new Predicate<String>() {
 *             @Override
 *             public boolean apply(String input) {
 *                 return input.startsWith("h");
 *             }
 *         });
 *
 *  在Java 8中的实现为
 *  String value2 = listStr.stream().findFirst().filter(input -> input.startsWith("h")).get();
 *
 *  Collections2.filter过滤集合中所有符合特定条件的元素。
 *    List<String> listWithH = Lists.newArrayList(Collections2.filter(listStr, new Predicate<String>() {
 *             @Override
 *             public boolean apply(@Nullable String s) {
 *                 return s.startsWith("h");
 *             }
 *         }));
 *
 *  在Java 8中的实现为
 * List<String> listWithH2 = listStr.stream().filter(input -> input.startsWith("h")).collect(Collectors.toList());
 *
 *  Lists.partition可以将一个大的集合分割成小集合，适用于分批查询、插入等场景。
 *  List<String> listStr = Lists.newArrayList("1", "2", "3","4","5","6","7");
 *  List<List<String>>  batchList = Lists.partition(listStr,3);
 *  //被分割成了： [[1, 2, 3], [4, 5, 6], [7]]
 *
 * 3.3 分组
 * Maps.uniqueIndex可以根据集合中的唯一键把集合转换为以唯一键为key，以元素为value的Map.
 * Multimaps.index可以根据集合中的相同的值把集合转换为以相同值为key，以List<元素>为value的Map. 相当于一键多值Map。
 * class Apple {
 *         int id;
 *         String color;
 *         public Apple(int id, String color) {
 *             this.id = id;
 *             this.color = color;
 *         }
 *         public int getId() {
 *             return id;
 *         }
 *         public String getColor() {
 *             return color;
 *         }
 *         @Override
 *         public String toString() {
 *             return MoreObjects.toStringHelper(Apple.class).add("id", id).add("color", color).toString();
 *         }
 *     }
 *
 *     @Test
 *     public void test1() {
 *         List<Apple> appleList = Lists.newArrayList(new Apple(1, "red"), new Apple(2, "red"), new Apple(3, "green"), new Apple(4, "green"));
 *         // 以主键为key，生成键值对：Map<id,Apple>
 *         Map<Integer, Apple> appleMap = Maps.uniqueIndex(appleList, new Function<Apple, Integer>() {
 *             @Nullable
 *             @Override
 *             public Integer apply(@Nullable Apple apple) {
 *                 return apple.getId();
 *             }
 *         });
 *       // 相当于根据颜色分类：转为Map<颜色，Collection<Apple>>
 *      Multimap<String, Apple> multiMap = Multimaps.index(appleList,
 *                 new Function<Apple, String>() {
 *                     @Nullable
 *                     @Override
 *                     public String apply(@Nullable Apple apple) {
 *                         return apple.getColor();
 *                     }
 *                 });
 *
 *     }
 *
 * 在Java 8中的实现为
 *
 *  List<Apple> appleList = Lists.newArrayList(new Apple(1, "red"), new Apple(2, "red"), new Apple(3, "green"), new Apple(4, "green"));
 *   Map<Integer, Apple> appleMap = appleList.stream().collect(Collectors.toMap(Apple::getId, apple -> apple));
 *
 *   Map<String, List<Apple>> groupsByColor =
 *                 appleList.stream().collect(Collectors.groupingBy(Apple::getColor));

 * 3.4 其他
 * 排序：
 *
 * Collections.sort
 * Ordering.natural()
 *
 * 集合的交集、并集和差集:
 * Sets.intersection(setA, setB);
 * Sets.union(setA, setB);
 * Sets.difference(setA, setB);

 * 创建不可变的集合
 *  ImmutableList<String> list = ImmutableList.of("1", "2", "3");
 *
 * @date 2020/12/31
 */
public class AllInOne {
}
