package cn.jdemo.guava.math;

/**
 * 1.    IntMath
 * 该类是int类型的值的算术运算。
 * 1.1 isPowerOfTwo(int x)
 * public static boolean isPowerOfTow(int x)
 * 该方法是检查一个整数是不是可以换算成2的指数，也就是检查转换成2进制之后是不是只有一个1。其中Java中的Integer.bitCount(x)也可以实现这样的功能，但是bitCount(x)是检查二进制里边有多少个1，如果有一个就返回true，不过对于Integer.MIN_VALUE来说，bitCount(x)==1，但是他并不能转换成2的指数。
 * 1.2 log2(int x, RoundingMode mode)
 * public static int log2(int x, RoundingModemode)
 * 该方法是返回一个整数基于2的对数，使用mode来取整。其中mode常用的有7种，UP是指总是取整离他最近的整数的最大值；DOWN是指总是取整离他最近的整数的最小值；CEILING是指在趋向正无穷的方向上取离他最近的整数；FLOOR是指在趋向负无穷方向上取离他最近的整数；HALF_UP是四舍五入取整数，如果出现0.5的小数，就向上取整；HALF_DOWN是四舍五入取整数，如果出现0.5的小数，就向下取整；HALF_EVEN是四舍五入取整数，如果出现0.5的小数，就向着偶数的方向取整。
 * 1.3 log10(int x, RoundingMode mode)
 * public static ing log10(int x,RoundingMode mode)
 * 该方法是返回一个整数的基于10的对数，使用mode来取整，mode和上面的用法一样。
 * 1.4 pow(int b, int k)
 * public static int pow(int b, int k)
 * 该方法返回的是以b为底，以k为指数的值；如果他们大小超过了整数范围，它就相当于BigInteger.valueOf(b).pow(k).intValue()。
 * 1.5 sqrt(int x, RoundingMode mode)
 * public static int sqrt(int x, RoundingModemode)
 * 该方法整数的开平方根，根据mode模式取整。
 * 1.6 divide(int p, int q, RoundingMode mode)
 * public static int divide(int p, int q,RoundingMode mode)
 * 该方法返回p除以q的值，按照mode模式取整。
 * 1.7 mod(int x, int m)
 * public static int mode(int x, int m)
 * 该方法返回的是x%m的值，但是和他的区别是，这个方法返回的总是非负数。
 * 譬如说：mod(7, 4) == 3
 *               mod(-7, 4) == 1
 *               mod(-1, 4) == 3
 *               mod(-8, 4) == 0
 *               mod(8, 4) == 0
 * 他使用的方法是((x%m)+m)%m，比如说-7，-7%4 = -3，然后-3 + 4 = 1
 * 1.8 gcd(int a, int b)
 * public static int gcd(int a, int b)
 * 该方法返回的是两个整数a和b的最大公因数，如果a == b == 0，那么返回0.
 * 1.9 checkedAdd(int a, int b)
 * public static int checkedAdd(int a, int b)
 * 该方法返回的是整数a和整数b的和，如果和不超过整数范围，就返回这个值。
 * 1.10 checkedSubtract(int a, int b)
 * public static int checkedSubtract(int a,int b)
 * 该方法是返回整数a和整数b的差，如果不超过整数范围，就返回这个值。
 * 1.11 checkedMultiply(int a, int b)
 * public static int checkedMultiply(int a,int b)
 * 该方法返回整数a和整数b的乘积，如果不超过整数返回，就返回这个值。
 * 1.12 checkedPow(int b, int k)
 * public static int checkedPow(int b, int k)
 * 该方法返回以b为底，以k为指数的幂，如果不超过整数范围，就返回这个值。
 * 1.13 factorial(int n)
 * public static int factorial(int n)
 * 该方法返回的是n的阶乘，如果n == 0，则返回1，如果最后结果已经超过了整数最大值，就返回整数的最大值。
 * 1.14 binomial(int n, int k)
 * public static int binomial(int n, int k)
 * 该方法是返回排列组合中的组合的值，相当于，如果超过整数的最大值，将会返回整数的最大值。
 *
 * 2.    LongMath
 * 该类是针对long类型的数进行算术运算。该类中的方法和IntMath中的方法一样，只不过把IntMath中的int类型改为long就可以了。具体的方法解析不再一一介绍。
 *
 * 3.    BigIntegerMath
 * 该类是针对BigInteger类型的数进行操作运算的，该类中包含的方法有binomial(), divide(), factorial(), isPowerOfTwo(), log10(), log2(),sqrt()，这些方法在IntMath类中都出现过，把所有的int类型替换成BigInteger类型就可以了，不在做一一介绍。
 *
 * 4.    DoubleMath
 * 该类的操作对象是double类型的数值。
 * 4.1             roundToInt(double x,RoundingMode mode)
 * public static int roundToInt(double x,RoundingMode mode)
 * 该方法是将double类型的数按照mode方式取整。
 * 4.2      roundToLong(double x, RoundingMode mode)
 * publicstatic long roundToLong(double x, RoundingMode mode)
 * 该方法是将double类型的数x按照mode方式取整，返回long类型的数。
 * 4.3 roundToBigInteger(double x,RoundingMode mode)
 * public static BigIntegerroundToBigInteger(double x, RoundingMode mode)
 * 该方法是将double类型的数x按照mode的方式取整，然后返回BigInteger类型的数。
 * 4.4 isPowerOfTwo(double x)
 * public static boolean isPowerOfTwo(doublex)
 * 该方法功能是，如果x严格等于2的k次幂，就返回true，否则返回false。
 * 4.5 log2(double x)
 * public static double log2(double x)
 * 该方法返回以2为底的对数。
 * 4.6 log2(double x, RoundingModemode)
 * public static int log2(double x,RoundingMode mode)
 *
 * 该方法功能是，对于x，计算他以2为底的对数，然后按照mode方式取整，并返回。
 *
 * 4.7 isMathematicalInteger(doublex)
 * public static booleanisMathematicalInteger(double x)
 *
 * 该方法是判断当前的double 类型的值x是否也是一个整数，如果是返回true，否则返回false，当前前提这个数必须是有穷的和有限的。
 *
 * 4.8 factorial(int n)
 * public static double factorial(int n)
 *
 * 该方法返回的是n的阶乘，如果最后的值超过Double的最大值，就会返回Double.POSITIVE.INFINITY。
 *
 * 4.9 fuzzyEquals(double a, doubleb, double tolerance)
 * public static boolean fuzzyEquals(doublea, double b, double tolerance)
 *
 * 该方法是比较两个double类型的数是否在一定范围内相等，即a和b的差的绝对值是否小于tolerance，如果小于，返回true，否则返回false。
 *
 * 4.10 fuzzyCompare(double a, doubleb, double tolerance)
 * public static int fuzzyCompare(double a,double b, double tolerance)
 *
 * 该方法是比较两个double类型的数，如果在一定范围内相等，就返回0，如果a > b，就返回1， 如果a < b就返回-1，否则返回Booleans.compare(Double.isNaN(a),Double.isNaN(b));这是针对所有的NaN的值。
 *
 * @date 2021/1/7
 */
public class AllInOne {
}
