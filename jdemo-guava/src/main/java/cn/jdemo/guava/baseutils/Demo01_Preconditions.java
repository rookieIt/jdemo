package cn.jdemo.guava.baseutils;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @see com.google.common.base.Preconditions
 * @see Preconditions#checkArgument(boolean exp, String errorMessageTemplate, int, int)
 * @see Preconditions#checkNotNull(Object reference)
 *
 * 1 .checkArgument(boolean) ：
 * 功能描述：检查boolean是否为真。 用作方法中检查参数
 * 失败时抛出的异常类型: IllegalArgumentException
 * 2.checkNotNull(T)：
 * 功能描述：检查value不为null， 直接返回value；
 * 失败时抛出的异常类型：NullPointerException
 * 3.checkState(boolean)：
 * 功能描述：检查对象的一些状态，不依赖方法参数。 例如， Iterator可以用来next是否在remove之前被调用。
 * 失败时抛出的异常类型：IllegalStateException
 * 4.checkElementIndex(int index, int size)：
 * 功能描述：检查index是否为在一个长度为size的list， string或array合法的范围。 index的范围区间是[0, size)(包含0不包含size)。无需直接传入list， string或array， 只需传入大小。返回index。
 * 失败时抛出的异常类型：IndexOutOfBoundsException
 * 5.checkPositionIndex(int index, int size)：
 * 功能描述：检查位置index是否为在一个长度为size的list， string或array合法的范围。 index的范围区间是[0， size)(包含0不包含size)。无需直接传入list， string或array， 只需传入大小。返回index。
 * 失败时抛出的异常类型：IndexOutOfBoundsException
 * 6.checkPositionIndexes(int start, int end, int size)：
 * 功能描述：检查[start, end)是一个长度为size的list， string或array合法的范围子集。伴随着错误信息。
 * 失败时抛出的异常类型：IndexOutOfBoundsException
 *
 * @date 2020/12/31
 */
public class Demo01_Preconditions {
    public static void main(String[] args) {
        // test01();
        // test02();
        // test03();
        test04();
    }

    /**
     * 输出内容
     * Exception in thread "main" java.lang.IllegalArgumentException: this must have the same number(1 != 2)
     */
    public static void test01() {
        // Preconditions.checkArgument(1==2);
        Preconditions.checkArgument(1==2, "this must have the same number(%s != %s)", 1, 2);
    }

    /**
     * Exception in thread "main" java.lang.NullPointerException
     */
    public static void test02() {
        Object obj = Preconditions.checkNotNull(null);
    }

    public static void test03() {
        // TODO
        Preconditions.checkState(1==2);
    }

    /**
     * 输出内容
     * Exception in thread "main" java.lang.IndexOutOfBoundsException: index (10) must be less than size (0)
     */
    public static void test04() {
        Preconditions.checkElementIndex(10, new ArrayList<>().size());
    }
}
