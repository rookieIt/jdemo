package cn.jdemo.guava.caches;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 * cache的常用参数：
 * 　　1. 缓存最大大小的设置：CacheBuilder.maximumSize(long)
 * 　　2. 过期时间：expireAfterAccess(long, TimeUnit) expireAfterWrite(long, TimeUnit)
 * 　　3. 引用类型：CacheBuilder.weakKeys() CacheBuilder.weakValues() CacheBuilder.softValues()
 * 　　4. 自动刷新cache：CacheBuilder.refreshAfterWrite
 * 　　5. 过期时间：CacheBuilder.expireAfterWrite
 *
 * 1 定时回收（Timed Eviction）
 * CacheBuilder提供两种定时回收的方法：
 * expireAfterAccess(long, TimeUnit)：缓存项在给定时间内没有被读/写访问，则回收。请注意这种缓存的回收顺序和基于大小回收一样。
 * expireAfterWrite(long, TimeUnit)：缓存项在给定时间内没有被写访问（创建或覆盖），则回收。如果认为缓存数据总是在固定时候后变得陈旧不可用，这种回收方式是可取的。
 * 如下文所讨论，定时回收周期性地在写操作中执行，偶尔在读操作中执行。
 * 测试定时回收
 * 对定时回收进行测试时，不一定非得花费两秒钟去测试两秒的过期。你可以使用Ticker接口和CacheBuilder.ticker(Ticker)方法在缓存中自定义一个时间源，而不是非得用系统时钟。
 *
 * 2 基于引用的回收（Reference-based Eviction）
 * 通过使用弱引用的键、或弱引用的值、或软引用的值，Guava Cache可以把缓存设置为允许垃圾回收：
 * CacheBuilder.weakKeys()：使用弱引用存储键。当键没有其它（强或软）引用时，缓存项可以被垃圾回收。因为垃圾回收仅依赖恒等式（==），使用弱引用键的缓存用==而不是equals比较键。
 * CacheBuilder.weakValues()：使用弱引用存储值。当值没有其它（强或软）引用时，缓存项可以被垃圾回收。因为垃圾回收仅依赖恒等式（==），使用弱引用值的缓存用==而不是equals比较值。
 * CacheBuilder.softValues()：使用软引用存储值。软引用只有在响应内存需要时，才按照全局最近最少使用的顺序回收。考虑到使用软引用的性能影响，我们通常建议使用更有性能预测性的缓存大小限定（见上文，基于容量回收）。使用软引用值的缓存同样用==而不是equals比较值。
 *
 * 3 显式清除
 * 任何时候，你都可以显式地清除缓存项，而不是等到它被回收：
 * 个别清除：Cache.invalidate(key)
 * 批量清除：Cache.invalidateAll(keys)
 * 清除所有缓存项：Cache.invalidateAll()
 *
 * 1 移除监听器
 *  通过CacheBuilder.removalListener(RemovalListener)，你可以声明一个监听器，以便缓存项被移除时做一些额外操作。
 *  缓存项被移除时，RemovalListener会获取移除通知[RemovalNotification]，其中包含移除原因[RemovalCause]、键和值。
 *  请注意，RemovalListener抛出的任何异常都会在记录到日志后被丢弃[swallowed]。
 * 警告：默认情况下，监听器方法是在移除缓存时同步调用的。因为缓存的维护和请求响应通常是同时进行的，
 * 代价高昂的监听器方法在同步模式下会拖慢正常的缓存请求。
 * 在这种情况下，你可以使用RemovalListeners.asynchronous(RemovalListener, Executor)把监听器装饰为异步操作
 * 2 清理什么时候发生？
 * 使用CacheBuilder构建的缓存不会”自动”执行清理和回收工作，也不会在某个缓存项过期后马上清理，也没有诸如此类的清理机制。相反，它会在写操作时顺带做少量的维护工作，或者偶尔在读操作时做——如果写操作实在太少的话。
 * 这样做的原因在于：如果要自动地持续清理缓存，就必须有一个线程，这个线程会和用户操作竞争共享锁。此外，某些环境下线程创建可能受限制，这样CacheBuilder就不可用了。
 * 相反，我们把选择权交到你手里。如果你的缓存是高吞吐的，那就无需担心缓存的维护和清理等工作。如果你的 缓存只会偶尔有写操作，而你又不想清理工作阻碍了读操作，那么可以创建自己的维护线程，以固定的时间间隔调用Cache.cleanUp()。ScheduledExecutorService可以帮助你很好地实现这样的定时调度。
 *
 * 刷新
 * 刷新和回收不太一样。正如LoadingCache.refresh(K)所声明，刷新表示为键加载新值，这个过程可以是异步的。在刷新操作进行时，缓存仍然可以向其他线程返回旧值，而不像回收操作，读缓存的线程必须等待新值加载完成。
 * 如果刷新过程抛出异常，缓存将保留旧值，而异常会在记录到日志后被丢弃[swallowed]。
 * 重载CacheLoader.reload(K, V)可以扩展刷新时的行为，这个方法允许开发者在计算新值时使用旧的值。
 * @date 2021/1/6
 */
public class Demo01 {
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 输出内容
     * ************刷新前***********
     * key01_value
     * key02_value
     * key03_value
     * ************刷新后***********
     * key01_value
     * key02_value
     * key03_value
     *
     * @throws InterruptedException
     */
    public static void test01() throws InterruptedException {
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder();
        cacheBuilder.maximumSize(100);
       /* cacheBuilder.expireAfterAccess(1, TimeUnit.SECONDS);
        cacheBuilder.expireAfterWrite(1, TimeUnit.SECONDS);*/
        cacheBuilder.refreshAfterWrite(1, TimeUnit.SECONDS);

        cacheBuilder.softValues();//软引用
        LoadingCache<String, Object> loadingCache = cacheBuilder.build(new CacheLoader<String, Object>() {
            @Override
            public Object load(String key) throws Exception {
                return test01Helper(key);
            }

            // 异步刷新,手动重写???
            @Override
            public ListenableFuture<Object> reload(String key, Object oldValue) throws Exception {
                ListenableFutureTask<Object> task = ListenableFutureTask.create(() -> test01Helper2(key));
                return task;
            }
        });

        System.out.println("************刷新前***********");
        try {
            System.out.println(loadingCache.get("key01"));
            System.out.println(loadingCache.get("key02"));
            System.out.println(loadingCache.get("key03"));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("************刷新后***********");
        loadingCache.refresh("key03");
        Thread.sleep(3000);
        try {
            System.out.println(loadingCache.get("key01"));
            System.out.println(loadingCache.get("key02"));
            System.out.println(loadingCache.get("key03"));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    private static Object test01Helper(String key) {
        return key+"_value";
    }
    private static Object test01Helper2(String key) {
        return key+"_valueVo";
    }
}
