package cn.jdemo.guava.eventbus;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

public class MqListen {

    @Subscribe
    public void listen(MqEvent event) {
        System.out.println(event);
    }
    @Subscribe
    public void listen(DeadEvent event) {
        System.out.println("guava自定义的接收没有订阅的事件:"+event);
    }


}
