package cn.jdemo.guava.eventbus;

/**
 * EventBus是Guava提供的消息发布-订阅类库，它的工作机制类似于观察者模式，通过通知者去注册观察者，最后由通知者向观察者发布消息。
 * 如果你需要订阅某种类型的消息，只需要在指定的方法上加上@Subscribe注解即可。
 * 代码例子：
 * 一个观察者可以订阅多个发布者，一个发布者可以推送给多个观察者，实现了解耦。
 *
 *
 * public class MqEvent {
 * }
 * public class StatusEvent {
 * }
 *  //第一个观察者，订阅了两种类型的发布
 *  public class EventHandler {
 *
 *     @Subscribe
 *     public void mq(MqEvent mq) {
 *         System.err.println(mq.getClass().getCanonicalName()+" work");
 *     }
 *
 *     @Subscribe
 *     public void status(StatusEvent statusEvent) {
 *         System.err.println(statusEvent.getClass().getCanonicalName() +" work");
 *     }
 * }
 * // 第二个观察者只订阅了一种类型的发布
 * public class OtherHandler {
 *
 *     @Subscribe
 *     public void mq(MqEvent mq) {
 *         System.err.println( "OtherHandler work");
 *     }
 * }
 *
 * public class EventTest {
 *
 *     public static void main(String[] args) {
 *         //初始化消息总线
 *         EventBus eventBus = new EventBus();
 *         // 注册订阅者
 *         eventBus.register(new EventHandler());
 *         eventBus.register(new OtherHandler());
 *         //MqEvent推送给了两个订阅者
 *         MqEvent mqEvent = new MqEvent();
 *         StatusEvent statusEvent = new StatusEvent();
 *         //发布消息
 *         eventBus.post(mqEvent);
 *         eventBus.post(statusEvent);
 *     }
 *
 * EventBus 另有异步实现： AsyncEventBus
 * 另一个更加专业、更加强大、更加高效，基于内存的消息处理框架是来自LMAX(伦敦外汇黄金交易所)的：Disruptor
 *
 * @description
 * @date 2021/1/6
 */
public class AllInOne {
}
