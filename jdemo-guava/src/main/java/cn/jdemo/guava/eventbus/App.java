package cn.jdemo.guava.eventbus;

import com.google.common.eventbus.EventBus;

public class App {
    public static void main(String[] args) {
        //初始化消息总线
        EventBus eventBus = new EventBus();
        eventBus.register(new MqListen());
        eventBus.post(new MqEvent());
        eventBus.post(new NoCareEvent());
    }
}
