package cn.jdemo.guava.strings;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Demo {
    public static void main(String[] args) {
        test01();
        test02();
        test03();
    }

    /**
     * 截取文件类型
     */
    private static void test03() {
        String last = Iterables.getLast(Splitter.on(".").split("xxx.zip"));
        System.out.println(last);
    }

    /**
     * 字符串分割
     */
    private static void test02() {
        Splitter splitter = Splitter.on("-").omitEmptyStrings();// 切割规则
        List<String> collect = splitter.splitToStream("123-abc-中文").collect(Collectors.toList());
        System.out.println(collect);
    }

    /**
     * 字符串拼接
     *
     * 输出内容:
     * 123-abc-中文
     */
    private static void test01() {
        ArrayList<String> objects = Lists.newArrayList();
        objects.add("123");
        objects.add("abc");
        objects.add("中文");
        String join = Joiner.on("-").join(objects);
        System.out.println(join);
    }
}
