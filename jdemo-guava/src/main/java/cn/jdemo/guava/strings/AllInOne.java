package cn.jdemo.guava.strings;

/**
 *
 * 2.字符串处理
 * 2.1 拼接
 * Joiner 可以快速地把多个字符串或字符串数组连接成为用特殊符号连接的字符串。
 *
 *   List<String> list = Lists.newArrayList("a","b","c");
 *   String value =Joiner.on("-").skipNulls().join(list);
 *   System.out.println(value);
 *  //输出为： a-b-c
 *
 * 2.2 分割
 * Splitter用来分割字符串
 *   String testString = "Monday,Tuesday,,Thursday,Friday,,";
 *   //英文分号分割；忽略空字符串
 *   Splitter splitter = Splitter.on(",").omitEmptyStrings().trimResults();
 *   System.out.println(splitter.split(testString).toString());
 *   //转换为了：[Monday, Tuesday, Thursday, Friday]
 *
 * 2.3 匹配
 * CharMatcher常用来从字符串里面提取特定字符串。
 * 比如想从字符串中得到所有的数字.
 * String value = CharMatcher.DIGIT.retainFrom("some text 2046 and more");
 * //value=2046
 *
 * @description
 * @date 2021/1/6
 */
public class AllInOne {
}
