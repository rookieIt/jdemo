package cn.jdemo.guava.mapdb;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.IndexTreeList;
import org.mapdb.Serializer;

import java.util.concurrent.ConcurrentMap;

/**
 * @see org.mapdb.DBMaker
 */
public class Demo01 {
    public static void main(String[] args) {
        // todo
    }

    public static void create(){
        // jvm堆空间
        DB db = DBMaker.memoryDB().make();
        // direct内存
        DB db2 = DBMaker.memoryDirectDB().make();
        // 磁盘随机
        DB db3 = DBMaker.fileDB("/Users/cdqiushengsen/Documents/helloTreeMapDb").make();
        // 磁盘mmap
        DB db4 = DBMaker.fileDB("/Users/cdqiushengsen/Documents/helloTreeMapDb")
                .fileMmapEnableIfSupported().make();
    }

    /**
     * 最佳实践
     */
    public static void createGreat(){
        DB db = DBMaker.fileDB("/Users/cdqiushengsen/Documents/helloTreeMapDb")
                //.checksumHeaderBypass()
                .fileMmapEnableIfSupported()//1 mmap
                .fileMmapPreclearDisable()//2 使用mmap的优化
                .cleanerHackEnable()//3 针对使用mmap时，jvm所出现的bug所做的处理
                .closeOnJvmShutdown()//4 jvm正常关闭时，将会关闭数据库
                .transactionEnable()//5 开启事务，写的速度下降，但是数据安全了
                .concurrencyScale(128)//6 数据库内部本质还是读写锁，因此更高的并发度设置在并发写的时候可以提供写性能
                .make();
        ConcurrentMap<Long, String> map = db.treeMap("words")
                .keySerializer(Serializer.LONG)
                .valueSerializer(Serializer.STRING)
                .valuesOutsideNodesEnable()
                .createOrOpen();
    }
}
