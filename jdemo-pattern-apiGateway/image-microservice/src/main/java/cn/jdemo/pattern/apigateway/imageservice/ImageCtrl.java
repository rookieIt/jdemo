package cn.jdemo.pattern.apigateway.imageservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @date 2021/1/19
 */
@RestController
public class ImageCtrl {

    @RequestMapping("/image")
    public String index(){
        return "image.png";
    }

}
