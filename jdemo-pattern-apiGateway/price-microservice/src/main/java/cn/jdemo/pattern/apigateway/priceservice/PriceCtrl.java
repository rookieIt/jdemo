package cn.jdemo.pattern.apigateway.priceservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @date 2021/1/19
 */
@RestController
public class PriceCtrl {

    @RequestMapping("/price")
    public String index(){
        return "price";
    }

}
