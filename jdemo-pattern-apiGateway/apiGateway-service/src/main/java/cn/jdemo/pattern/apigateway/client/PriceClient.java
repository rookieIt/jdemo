package cn.jdemo.pattern.apigateway.client;


/**
 *
 * @date 2021/1/19
 */
public interface PriceClient {
    String getPrice();
}
