package cn.jdemo.pattern.apigateway.client;

import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Stream;

/**
 * lru get put
 *
 * 1 2 3 4 5
 * 1 3 4 5 6 7
 */
public class Demo {

    Deque<Integer> queue = new LinkedBlockingDeque<>();
    Map<Integer,String> map = new LinkedHashMap<Integer,String>();
    int cap;
    int count;
    public Demo(int cap){
        cap = this.cap;
    }


    public Integer get(int req){
        final Stream<Integer> parallel = queue.stream().parallel();
        Integer poll = queue.poll();
        queue.offer(poll);
        return poll;
    }

    public void put(Integer val){
        while(count > cap){
            count--;
            queue.pollFirst();
        }
        count++;
        queue.offer(val);
    }
}