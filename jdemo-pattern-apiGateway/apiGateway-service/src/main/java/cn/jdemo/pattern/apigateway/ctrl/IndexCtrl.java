package cn.jdemo.pattern.apigateway.ctrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @date 2021/1/18
 */
@RestController
public class IndexCtrl {

    private final static Logger logger = LoggerFactory.getLogger(IndexCtrl.class);

    @RequestMapping("/index")
    public String index(){
        logger.info("index...");
        return "index";
    }
}
