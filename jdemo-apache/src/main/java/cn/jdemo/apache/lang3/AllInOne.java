package cn.jdemo.apache.lang3;

/**
 * @see org.apache.commons.lang3.arch
 * @see org.apache.commons.lang3.builder
 * @see org.apache.commons.lang3.concurrent
 * @see org.apache.commons.lang3.event
 * @see org.apache.commons.lang3.exception
 * @see org.apache.commons.lang3.math
 * @see org.apache.commons.lang3.mutable 可变
 * @see org.apache.commons.lang3.reflect
 * @see org.apache.commons.lang3.text
 * @see org.apache.commons.lang3.time
 * @see org.apache.commons.lang3.tuple 元组
 *
 *
 * @see org.apache.commons.lang3.AnnotationUtils
 * @see org.apache.commons.lang3.ArchUtils
 * @see org.apache.commons.lang3.ArrayUtils
 * @see org.apache.commons.lang3.BitField
 * @see org.apache.commons.lang3.BooleanUtils
 * @see org.apache.commons.lang3.CharEncoding 过时，被Java自己的java.nio.charset.StandardCharsets取代
 * @see org.apache.commons.lang3.CharSequenceUtils
 * @see org.apache.commons.lang3.CharSet
 * @see org.apache.commons.lang3.CharSetUtils
 * @see org.apache.commons.lang3.CharUtils
 * @see org.apache.commons.lang3.ClassPathUtils
 * @see org.apache.commons.lang3.ClassUtils
 * @see org.apache.commons.lang3.Conversion
 * @see org.apache.commons.lang3.EnumUtils
 * @see org.apache.commons.lang3.JavaVersion
 * @see org.apache.commons.lang3.LocaleUtils
 * @see org.apache.commons.lang3.NotImplementedException
 * @see org.apache.commons.lang3.ObjectUtils
 * @see org.apache.commons.lang3.RandomStringUtils
 * @see org.apache.commons.lang3.RandomUtils
 * @see org.apache.commons.lang3.Range
 * @see org.apache.commons.lang3.RegExUtils
 * @see org.apache.commons.lang3.SerializationException
 * @see org.apache.commons.lang3.SerializationUtils
 * @see org.apache.commons.lang3.StringEscapeUtils 过时
 * @see org.apache.commons.lang3.StringUtils
 * @see org.apache.commons.lang3.SystemUtils
 * @see org.apache.commons.lang3.ThreadUtils
 * @see org.apache.commons.lang3.Validate
 *
 * @date 2021/1/14
 */
public class AllInOne {
}
