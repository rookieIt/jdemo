/**
 *  reactive(响应式编程)框架（推-模式）
 *      spring-webflux -> Reactor -> reactiveStream
 *      Reactor -> reactiveStream
 *      Rxjava -> reactiveStream
 *  <a href="http://projectreactor.mydoc.io/"> Reactor文档 </>
 *  <a href="https://zhuanlan.zhihu.com/p/129889972"> Rxjava文档 </>
 *  <a herf="https://blog.csdn.net/crazymakercircle/article/details/124120506"> flux和Mono</>
 *
 *
 *
 *  jdk9 Flow
 *  <a href="https://blog.csdn.net/qq_45380083/article/details/124462576"> 响应式+jdk9的Flow </>
 *
 *
 *
 *  reactive stream协议详解（-->解决了，异步系统中的背压问题）
 *  <a href="https://blog.csdn.net/superfjj/article/details/106153648"></>
 *  1.异步【stream】处理的标准(非阻塞的back pressure)
 *  2.只是一个标准
 *
 *  实现：Publisher，Subscriber，Subscription，Processor
 *
 *  既然 Reactive Stream 和 Java 8 引入的 Stream 都叫做流，它们之间有什么关系呢？
 *    有一点关系，Java 8 的 Stream 主要关注在流的过滤，映射，合并。
 *    而 Reactive Stream 更进一层，侧重的是流的产生与消费，即流在生产与消费者之间的协调。
 *
 *  在进行异步消息处理时，Reactive Streams 和  Actor 是两种不同的编程模式选择。
 *  Reactive Streams 规范相比 Actor 更简单，只是说收发消息异步，有流量控制。
 *  而 Actor 编程模式涉及到 Actor 容错管理，消息路由，集群，并支持远程消息等。
 */
package cn.jdemo.reactive;