package cn.jdemo.reactive.wrapper;

public interface MySubscriber<T> {
    void onNext(T t);
}
