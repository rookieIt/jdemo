package cn.jdemo.reactive.wrapper;

public interface MyPublisher<T> {
    void subscribe(MySubscriber<? super T> subscriber);
}
