package cn.jdemo.reactive.reactor;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

/**
 * @see Flux
 * @see reactor.core.publisher.Mono
 */
public class Demo01 {
    public static void main(String[] args) {
        Disposable result = Flux.just(1, 2, 3)
                .publishOn(Schedulers.parallel()) //指定在parallel线程池中执行
                .map(i -> {
                    System.out.println("map1: " + Thread.currentThread().getName());
                    return i;
                })
                .publishOn(Schedulers.elastic()) // 指定下游的执行线程
                .map(i -> {
                    System.out.println("map2: " + Thread.currentThread().getName());
                    return i;
                })
                .subscribeOn(Schedulers.single())
                .subscribe(i -> System.out.println("subscribe: " + Thread.currentThread().getName()));
        result.dispose();
    }
}
