package cn.jdemo.core.flink;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.Arrays;

public class StreamExe {

    public void work() throws Exception {

        StreamExecutionEnvironment streamExe = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<Integer> dataStreamSource = streamExe.addSource(new SourceFunction<Integer>() {
            @Override
            public void run(SourceContext<Integer> sourceContext) throws Exception {
            }

            @Override
            public void cancel() {

            }
        });
        dataStreamSource.print();
        streamExe.execute();

    }
}
