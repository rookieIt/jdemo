/**
 * 并发锁
 * @see java.util.concurrent.locks.Lock
 * @see java.util.concurrent.locks.Lock#lock()
 * @see java.util.concurrent.locks.Lock#tryLock()
 * @see java.util.concurrent.locks.Lock#tryLock(long, java.util.concurrent.TimeUnit)
 * @see java.util.concurrent.locks.Lock#unlock()
 * @see java.util.concurrent.locks.Lock#lockInterruptibly()
 * @see java.util.concurrent.locks.Lock#newCondition()
 *
 * @see java.util.concurrent.locks.Condition
 * @see java.util.concurrent.locks.Condition#await()
 * @see java.util.concurrent.locks.Condition#await(long, java.util.concurrent.TimeUnit)  
 * @see java.util.concurrent.locks.Condition#awaitNanos(long) 
 * @see java.util.concurrent.locks.Condition#awaitUninterruptibly() 
 * @see java.util.concurrent.locks.Condition#awaitUntil(java.util.Date)
 * @see java.util.concurrent.locks.Condition#signal()
 * @see java.util.concurrent.locks.Condition#signalAll()
 *
 * @see java.lang.Object#wait()
 * @see java.lang.Object#wait(long)
 * @see java.lang.Object#wait(long, int)
 * @see java.lang.Object#notify()
 * @see java.lang.Object#notifyAll()
 *
 * 工具类
 * @see java.util.concurrent.locks.LockSupport
 * @see java.util.concurrent.locks.LockSupport#LockSupport() 不能被初始化
 * @see java.util.concurrent.locks.LockSupport#park() 
 * @see java.util.concurrent.locks.LockSupport#park(java.lang.Object blocker)
 * @see java.util.concurrent.locks.LockSupport#parkNanos(long nanos)
 * @see java.util.concurrent.locks.LockSupport#parkNanos(java.lang.Object blocker, long nanos)
 * @see java.util.concurrent.locks.LockSupport#parkUntil(long deadline)
 * @see java.util.concurrent.locks.LockSupport#parkUntil(java.lang.Object blocker, long deadline)
 * @see java.util.concurrent.locks.LockSupport#getBlocker(java.lang.Thread) 
 * @see java.util.concurrent.locks.LockSupport#unpark(java.lang.Thread)
 *
 *
 * @date 2021/1/12
 */
package cn.jdemo.juc.lock;