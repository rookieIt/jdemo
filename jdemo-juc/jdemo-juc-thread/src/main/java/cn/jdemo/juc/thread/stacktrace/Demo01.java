package cn.jdemo.juc.thread.stacktrace;

/**
 * StackTraceElement[]存储了，从线程运行期开始(表现为run()方法的调用)到getStackTrace()方法为止的堆栈信息
 * @see java.lang.Thread#getStackTrace() 对象方法，返回表示该线程的堆栈转储的堆栈跟踪元素数组
 *
 *  @see java.lang.Thread#getAllStackTraces() 静态方法，返回所有活动线程的堆栈跟踪的映射。
 * @date 2020/12/21
 */
public class Demo01 {
    public static void main(String[] args) throws InterruptedException {
        test01();
        runAdd();
    }

    public static void test01() throws InterruptedException {
        Thread thread = new Thread(() -> {
            temp01 temp01 = new temp01(5,5);
            // temp01.runAdd();
        });
        thread.start();
    }

    public static void runAdd() {
        StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
        for (StackTraceElement element:stackTraces){
            System.out.println(element.getClassName()+"#"+element.getMethodName()+"#"+element.getFileName()
                    +"---"+element.getLineNumber());
        }
    }
}


class temp01{
    private int i;
    private int j;
    public temp01(){
        init(0,0);
    }
    public temp01(int i,int j){
        init(i,j);
    }
    private void init(int i,int j) {
        i=i;
        j=j;
    }

    /**
     * 输出内容:
     * java.lang.Thread#getStackTrace---1559
     * cn.jdemo.juc.thread.stacktrace.temp01#runAdd---44
     * cn.jdemo.juc.thread.stacktrace.Demo01#lambda$test01$0---16
     * java.lang.Thread#run---748
     */
    public int runAdd() {
        StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
        for (StackTraceElement element:stackTraces){
            System.out.println(element.getClassName()+"#"+element.getMethodName()
                    +"---"+element.getLineNumber());
        }
        return i+j;
    }
}
