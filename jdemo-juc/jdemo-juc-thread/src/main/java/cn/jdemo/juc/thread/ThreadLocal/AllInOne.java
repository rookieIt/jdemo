package cn.jdemo.juc.thread.ThreadLocal;

/**
 *
 * 使用场景
 *
 * 1、在进行对象跨层传递的时候，使用ThreadLocal可以避免多次传递，打破层次间的约束。
 * 2、线程间数据隔离
 * 3、进行事务操作，用于存储线程事务信息。
 * 4、数据库连接，Session会话管理。
 */
/**
 *
 * @see ThreadLocal#set(Object)
 * @see ThreadLocal#createMap(Thread, Object) 线程初次初始化一个ThreadLocalMap
 * @see ThreadLocal#get()
 * @see ThreadLocal#getMap(Thread) 每个线程持有一个ThreadLcalMap对象
 * @see ThreadLocal#remove()
 * @see ThreadLocal#initialValue()
 *
 * @see ThreadLocal#HASH_INCREMENT 0x61c88647
 *
 * @see ThreadLocal.ThreadLocalMap#Entry[] ThreadLcalMap(为每个Thread都维护了一个Entry数组)
 * @see ThreadLocal.ThreadLocalMap#getEntry(ThreadLocal)
 * @see ThreadLocal.ThreadLocalMap.Entry
 *
 * @see Thread#threadLocals 此线程有关的ThreadLocal的ThreadLocalMap值。该映射由ThreadLocal类维护
 *
 *
 * 自我总结
 * 1 一个线程下可以有多个ThreadLocal对象，但是它们公用一个ThreadLcalMap对象；
 * 2 多个线程下，ThreadLocal隔离,本质是各个线程下的ThreadLcalMap对象隔离；
 * 3 一个线程初始化后，第一次调用ThreadLocal#set(Object)时，会调用ThreadLocal#createMap(Thread, Object)方法初始化ThreadLcalMap对象；
 * 4 一个线程下的多个ThreadLocal对象，公用一个ThreadLcalMap存储数据，如何做到不同ThreadLocal对象的数据隔离？
 *      ThreadLcalMap#Entry[] 的索引，基于ThreadLocal#threadLocalHashCode与斐波那契散列乘数进行散列(hash),所以不同ThreadLocal对象的索引位置不同。
 * 5 为什么ThreadLocal.ThreadLocalMap.Entry的key使用弱引用？
 *      答:1）ThreadLocalMap与thread的生命周期一样长，若使用强引用，即thradLocal的外部存在强引用，导致thradLocal不会被回收，从而导致Entry内存泄漏；
 *      2）若使用弱引用，ThreadLocal会被GC回收，会导致ThreadLocalMap中key为null， 而value还存在着强引用，只有thead线程退出以后,value的强引用链条才会断掉，
 *      但如果当前线程再迟迟不结束的话，这些key为null的Entry的value就会一直存在一条强引用链，永远无法回收，造成内存泄漏。
 *      3）所以，强引用，弱引用都可能导致内存泄漏。但，弱引用优势在于，key不会内存泄漏;
 *      同时，针对弱引用，可能导致的内存泄漏问题处理方案是：当key为null，在下一次ThreadLocalMap调用set(),get()，remove()方法的时候会清除相关value值。
 * 6 threadLocal导致的内存泄漏，如何避免？
 *      答:(1)使用完ThreadLocal后，执行remove操作;
 *        (2)将ThreadLocal变量定义成private static，这样就一直存在ThreadLocal的强引用，也就能保证任何时候都能通过ThreadLocal的弱引用访问到Entry的value值，进而手动清除掉；
 *
 *
 * ThreadLocal和Synchronized都是为了解决多线程中相同变量的访问冲突问题，不同的点是
 * Synchronized是通过线程等待，牺牲时间来解决访问冲突
 * ThreadLocal是通过每个线程单独一份存储空间，牺牲空间来解决冲突，并且相比于Synchronized，ThreadLocal具有线程隔离的效果，只有在线程内才能获取到对应的值，线程外则不能访问到想要的值。
 *
 * @link https://zhuanlan.zhihu.com/p/102571059
 * @date 2021/1/7
 */
public class AllInOne {
}
