package cn.jdemo.juc.core.util.async.worker;

/**
 *
 * @description
 * @date 2020/11/30
 */
public class WorkResult<V> {

    private V result;

    private ResultStateEnum resultState;

    private Exception ex;

    public WorkResult(V result,ResultStateEnum resultState){
        this(result,resultState,null);
    }

    public WorkResult(V result,ResultStateEnum resultState,Exception ex){
        this.result = result;
        this.resultState = resultState;
        this.ex = ex;
    }

    public static <V> WorkResult<V> defaultResult() {
        return new WorkResult<>(null, ResultStateEnum.DEFAULT);
    }


    public V getResult() {
        return result;
    }

    public void setResult(V result) {
        this.result = result;
    }

    public ResultStateEnum getResultState() {
        return resultState;
    }

    public void setResultState(ResultStateEnum resultState) {
        this.resultState = resultState;
    }

    public Exception getEx() {
        return ex;
    }

    public void setEx(Exception ex) {
        this.ex = ex;
    }
}
