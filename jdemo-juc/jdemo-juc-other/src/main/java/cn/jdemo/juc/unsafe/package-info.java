/**
 * park(false,0) 表示永不到期，一直挂起，直至被唤醒
 * 第一个参数如果是true，表示绝对时间，则var2为绝对时间值，单位是毫秒
 * 第一个参数如果是false，表示相对时间，则var2为相对时间值，单位是纳秒
 * @see sun.misc.Unsafe#park(boolean, long)
 * @see sun.misc.Unsafe#unpark(java.lang.Object thread)
 * 
 * @see sun.misc.Unsafe#wait() 
 * @see sun.misc.Unsafe#wait(long) 
 * @see sun.misc.Unsafe#wait(long, int) 
 * 
 * @see sun.misc.Unsafe#allocateInstance(java.lang.Class) 
 * @see sun.misc.Unsafe#allocateMemory(long)  
 * @see sun.misc.Unsafe#reallocateMemory(long, long)
 * 
 * @see sun.misc.Unsafe#compareAndSwapObject(java.lang.Object, long, java.lang.Object, java.lang.Object) 
 * @see sun.misc.Unsafe#compareAndSwapLong(java.lang.Object, long, long, long)  
 * @see sun.misc.Unsafe#compareAndSwapInt(java.lang.Object, long, int, int)   
 */
package cn.jdemo.juc.unsafe;