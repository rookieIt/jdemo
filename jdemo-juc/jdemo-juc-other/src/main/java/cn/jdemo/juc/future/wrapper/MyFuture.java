package cn.jdemo.juc.future.wrapper;

public interface MyFuture<R> {

    R get() throws InterruptedException;

    boolean done();
}
