package cn.jdemo.juc.AQS.alorigthm;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 *
 * @date 2020/12/16
 */
public class CLH {
    public static void main(String[] args) {

    }

    class ClhLock implements Lock{
        private AtomicReference<CLHNode> tail;
        private ThreadLocal<CLHNode> threadLocal;

        public ClhLock() {
            this.tail = new AtomicReference<>();
            this.threadLocal = new ThreadLocal<>();
        }

        @Override
        public void lock() {

        }

        @Override
        public void lockInterruptibly() throws InterruptedException {
        }

        @Override
        public boolean tryLock() {
            return false;
        }

        @Override
        public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
            return false;
        }

        @Override
        public void unlock() {
        }

        @Override
        public Condition newCondition() {
            return null;
        }
    }

    class CLHNode {
        private volatile boolean locked = true;
        public boolean getLocked() {
            return locked;
        }
        public void setLocked(boolean locked) {
            this.locked = locked;
        }
    }
}
