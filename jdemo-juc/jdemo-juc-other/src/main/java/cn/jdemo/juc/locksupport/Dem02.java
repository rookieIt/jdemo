package cn.jdemo.juc.locksupport;

import java.util.concurrent.locks.LockSupport;

/**
 *
 */
public class Dem02 {
    public static void main(String[] args) throws InterruptedException {
        test1();
    }

    /**
     * 执行结果:
     * 业务执行start...
     * quit...
     * 业务执行end...
     */
    public static void test1() throws InterruptedException {
        WorkThread thread = new WorkThread();
        thread.start();

        Thread.sleep(1000);

        thread.quit();
    }

}

class WorkThread extends Thread{
    public void quit(){
        System.out.println("quit...");
        LockSupport.unpark(this);
    }

    @Override
    public void run() {
        System.out.println("业务执行start...");
        LockSupport.park();
        System.out.println("业务执行end...");
    }
}
