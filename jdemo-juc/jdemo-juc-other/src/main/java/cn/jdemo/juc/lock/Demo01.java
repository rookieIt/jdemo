package cn.jdemo.juc.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Demo01 {
    static Lock lock = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    public static void test01() throws InterruptedException {
        Thread thread = new Thread(() -> {
            if (lock.tryLock()){
                try {
                    System.out.println("biz1...");
                    while(true){
                        System.out.println("执行中...");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }else{
                System.out.println("获取失败");
            }

        });
        thread.start();

        Thread.sleep(2 * 1000);

        Thread thread1 = new Thread(() -> {
            try {
                lock.lockInterruptibly();
                System.out.println("biz2...");
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });
        thread1.start();
        Thread.sleep(2 * 1000);
        System.out.println("准备中断。。。");
        thread.interrupt();
    }
}
