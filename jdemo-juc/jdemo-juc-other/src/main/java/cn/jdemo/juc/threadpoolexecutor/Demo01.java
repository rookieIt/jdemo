package cn.jdemo.juc.threadpoolexecutor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Demo01 {

    private static ThreadPoolExecutor executor = new ThreadPoolExecutorAdapter(1,2, 10,
            TimeUnit.SECONDS,new ArrayBlockingQueue<Runnable>(2),new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger(0);
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r, "currThread_" + counter.getAndIncrement());
            return thread;
        }
    },new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        test01();
    }

    /**
     * 输出数据:
     * beforeExecute, thread's:currThread_0
     * currThread_0:执行开始...
     * currThread_0:执行结束...
     * afterExecute...
     * beforeExecute, thread's:currThread_0
     * currThread_0:执行开始...
     * currThread_0:执行结束...
     * afterExecute...
     */
    public static void test01(){
        executor.execute(() -> {
            try {
                System.out.println(Thread.currentThread().getName()+":执行开始...");
                Thread.sleep(3000);
                System.out.println(Thread.currentThread().getName()+":执行结束...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.execute(() -> {
            try {
                System.out.println(Thread.currentThread().getName()+":执行开始...");
                Thread.sleep(5000);
                System.out.println(Thread.currentThread().getName()+":执行结束...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println(executor.getCompletedTaskCount());
        executor.shutdown();
    }
}
