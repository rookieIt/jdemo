package cn.jdemo.juc.future.callbackWrapper;

import cn.jdemo.juc.future.wrapper.MyFuture;
import cn.jdemo.juc.future.wrapper.MyTask;

public interface MyFutureService2<T,R> {

    void commit(MyTask<T,R> task, T t, CallBack<R> callBack);

}
