package cn.jdemo.juc.thread;

/**
 *
 * 调用interrupt()可以打断阻塞，打断阻塞并不等于线程的生命周期结束，仅仅是打断了当前线程的阻塞状态
 * 一旦阻塞状态被打断，就会抛出一个InterruptedException，像一个信号通知当前线程
 */
public class interruptDemo03 {
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 结果分析，synchronized获取锁的过程不响应中断请求
     *
     * 执行结果:
     * Thread-0前1679920605294
     * java.lang.InterruptedException: sleep interrupted
     * 	at java.lang.Thread.sleep(Native Method)
     * 	at cn.jdemo.juc.thread.interruptDemo03.lambda$test01$0(interruptDemo03.java:37)
     * 	at java.lang.Thread.run(Thread.java:748)
     * 线程阻塞状态被打断！
     * 后续业务~
     */
    private static void test01() throws InterruptedException {
        // (3)
        Thread thread2 = new Thread(() -> {
            synchronized (interruptDemo03.class) {
                try {
                    System.out.println(Thread.currentThread().getName()+"前"+System.currentTimeMillis());
                    Thread.sleep(1000L);
                    System.out.println(Thread.currentThread().getName()+"后"+System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("线程阻塞状态被打断！");
                }
                System.out.println("后续业务~");
            }
        });
        thread2.start();
        // (4)
        Thread.sleep(10L);
        thread2.interrupt();
    }
}
