package cn.jdemo.juc.future.wrapper;

public interface MyFutureService<T,R> {
    MyFuture<R> commit(MyTask<T,R> task,T t);
}
