package cn.jdemo.juc.thread;

/**
 * @see Thread#yield() 让出cpu，然后大家一起重新抢，该线程如果再次获取，则继续执行
 * @description
 * @date 2020/12/7
 */
public class YieldDemo {
    public static void main(String[] args) throws InterruptedException {
        test01();
        // test02();
    }

    /**
     * 执行结果如下1:
     * Thread-2 : 开始
     * Thread-1 : 开始
     * Thread-2 : 结束
     * Thread-1 : 结束
     * Thread-0 : 开始
     * Thread-0 : 结束
     *
     *  @throws InterruptedException
     *
     * 执行结果如下2:
     * Thread-0 : 开始
     * Thread-2 : 开始
     * Thread-1 : 开始
     * Thread-1 : 结束
     * Thread-2 : 结束
     * Thread-0 : 结束
     *
     * 其他执行结果...
     */
    public static void test01() throws InterruptedException {
        for (int i = 1; i <= 3; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+" : 开始");
                // Thread.yield();
                System.out.println(Thread.currentThread().getName()+" : 结束");
            }).start();
        }
    }

    /**
     * 执行结果如下:
     * Thread-2 : 开始
     * Thread-2 : 结束
     * Thread-0 : 开始
     * Thread-0 : 结束
     * Thread-1 : 开始
     * Thread-1 : 结束
     * @throws InterruptedException
     */
    public static void test02() throws InterruptedException {
        for (int i = 1; i <= 3; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+" : 开始");
                // Thread.yield();
                System.out.println(Thread.currentThread().getName()+" : 结束");
            }).start();
        }
    }

}
