package cn.jdemo.juc.noshare;

/**
 * 共享
 */
public class App2 {

    /**
     * 不填充--输出如下:
     * Thread num 1 duration = 403
     * Thread num 2 duration = 1243
     * Thread num 3 duration = 987
     * Thread num 4 duration = 1412
     * Thread num 5 duration = 2809
     * Thread num 6 duration = 3035
     * Thread num 7 duration = 3875
     * Thread num 8 duration = 4064
     * Thread num 9 duration = 2210
     *
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        for(int i=1;i<10;i++){
            System.gc();
            final long start = System.currentTimeMillis();
            Sharing.runTest(i);
            System.out.println("Thread num "+i+" duration = " + (System.currentTimeMillis() - start));
        }
    }
}
