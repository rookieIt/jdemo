/**
 * 减法计数器
 * @see java.util.concurrent.CountDownLatch
 *
 * 应用场景是：有一个任务想要往下执行，但必须要等到其他的任务执行完毕后才可以继续往下执行
 *
 * @date 2020/12/7
 */
package cn.jdemo.juc.CountDownLatch;