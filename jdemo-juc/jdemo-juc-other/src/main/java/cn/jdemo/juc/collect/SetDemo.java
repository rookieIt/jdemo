package cn.jdemo.juc.collect;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 
 *
 * @date 2020/12/7
 */
public class SetDemo {
    public static void main(String[] args) {
        test01(args);
    }

    public static void test01(String[] args) {
        // Set的替代方案1: jdk1.5
        CopyOnWriteArraySet set = new CopyOnWriteArraySet<String>();
        set.add("1");
    }
}
