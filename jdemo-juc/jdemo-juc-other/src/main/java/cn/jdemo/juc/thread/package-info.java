/**
 * 类方法
 * @see java.lang.Thread#yield() 让出cpu，然后大家一起重新抢，之后继续执行后续
 * @see java.lang.Thread#sleep(long) ： 状态变为：TIME_WAITING
 *
 * 对象方法
 * @see java.lang.Thread#join()
 * @see java.lang.Thread#join(long)
 * A线程调用B线程的join()方法，将会使A等待B执行完才继续执行
 *
 * @see java.lang.Thread#interrupt()
 * 总:当对处于阻塞状态的线程调用interrupt方法时（处于阻塞状态的线程是调用thread#sleep(),thread#join(),Object#wait()的线程)，
 *      会抛出InterruptException异常,并且会清除阻塞状态
 *
 * 使用场景：
 *     前置条件:线程的start方法启动一个线程后，线程开始执行run方法，run方法运行结束后线程退出,那为什么还需要结束一个线程呢？
 *     例如:在生产者/消费者模式中，消费者主体就是一个死循环，它不停的从队列中接受任务，执行任务，在停止程序时，我们需要一种”优雅”的方法以关闭该线程。
 *          在一些图形用户界面程序中，线程是用户启动的，完成一些任务，比如从远程服务器上下载一个文件，在下载过程中，用户可能会希望取消该任务。
 *          在一些场景中，比如从第三方服务器查询一个结果，我们希望在限定的时间内得到结果，如果得不到，我们会希望取消该任务。
 *          有时，我们会启动多个线程做同一件事，比如类似抢火车票，我们可能会让多个好友帮忙从多个渠道买火车票，只要有一个渠道买到了，我们会通知取消其他渠道。
 *     @see java.lang.Thread#stop() 过时，不使用
 *     Java中，停止一个线程的主要机制是中断，中断并不是强迫终止一个线程，它是一种协作机制，是给线程传递一个取消信号，但是由线程来决定如何以及何时退出；
 *
 * @see java.lang.Thread#interrupted() 静态方法 == Thread.currentThread().isInterrupted(true);
 * 该静态方法返回当前线程的中断状态，然后清除当前线程的中断状态
 * @see java.lang.Thread#isInterrupted() 实例方法
 * 该实例方法返回当前线程的中断状态
 *
 * 补充:synchronized关键字获取锁的过程中不响应中断请求，这是synchronized的局限性。
 * 如果这对程序是一个问题，应该使用显式锁，后面章节我们会介绍显式锁Lock接口，它支持以响应中断的方式获取锁。
 *
 * 线程中止方法
    1.  使用退出标志（volatile修饰），使线程正常退出，也就是当run方法完成后线程终止。
    2.  使用stop方法强行终止线程（这个方法不推荐使用，因为stop和suspend、resume一样，也可能发生不可预料的结果）。
    3.  使用interrupt方法中断线程。
   Java并发库的一些代码就提供了单独的取消/关闭方法，比如说，
    (1) Future接口提供了如下方法以取消任务：
        boolean cancel(boolean mayInterruptIfRunning);
    (2) ExecutorService提供了如下两个关闭方法：
        void shutdown();
        List shutdownNow();
    本节主要介绍Java中如何取消/关闭线程，主要依赖的技术是中断，但它是一种协作机制，不会强迫终止线程，
    作为线程的实现者，应该提供明确的取消/关闭方法，并用文档描述清楚其行为;
    作为线程的调用者，应该使用其取消/关闭方法，而不是贸然调用interrupt。
 *
 *
 * 准备/初始 就绪 运行 阻塞 终止
 * ①初始(NEW)：新创建了一个线程对象，但还没有调用start()方法。
 * ②运行(RUNNABLE)：Java线程中将就绪（ready）和运行中（running）两种状态笼统的成为“运行”。
 *                 该线程对象的start()后，该状态的线程位于可运行线程池中，等待被线程调度选中，获取cpu 的使用权，此时处于就绪状态（ready）；
 *                 就绪状态的线程在获得cpu 时间片后变为运行中状态（running）。
 * ③阻塞(BLOCKED)：表示线程阻塞于***锁***。
 * ④等待(WAITING)：进入该状态的线程需要等待其他线程做出一些特定动作（通知或中断）。
 * ⑤超时等待(TIME_WAITING)：该状态不同于WAITING，它可以在指定的时间内自行返回。
 * ⑥终止(TERMINATED)：表示该线程已经执行完毕。
 *
 *
 * 主内存与工作内存：
 * (1)JAVA内存模型规定了所有的变量都存储在主内存（Main Memory）中。
 * (2)所有的线程都有自己的工作内存，工作内存中保存了被该线程使用到的变量的主内存副本拷贝，
 * (3)线程对变量的所有操作（读取、赋值）都必须在工作内存中执行，而不能直接读写主内存中的变量。
 * 补充:直接读写主内存变量(volatile)
 *
 *
 *
 * @date 2020/12/7
 */
package cn.jdemo.juc.thread;