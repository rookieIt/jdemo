/**
 * suspend与resume
 * (1)Java废弃 suspend() 去挂起线程的原因，是因为 suspend() 在导致线程暂停的同时，并不会去释放任何锁资源。其他线程都无法访问被它占用的锁。
 * 直到对应的线程执行 resume() 方法后，被挂起的线程才能继续，从而其它被阻塞在这个锁的线程才可以继续执行。
 * (2)但是，如果 resume() 操作出现在 suspend() 之前执行，那么线程将一直处于挂起状态，
 * 同时一直占用锁，这就产生了死锁。而且，对于被挂起的线程，它的线程状态居然还是 Runnable。
 *
 * wait与notify
 * Object类提供，wait与notify必须配合synchronized使用，因为调用之前必须持有锁，wait会立即释放锁，notify则是同步块执行完了才释放
 *
 * await与singal
 * Condition类提供，而Condition对象由new ReentLock().newCondition()获得，与wait和notify相同，因为使用Lock锁后无法使用wait方法
 *
 * park与unpark
 * LockSupport是一个非常方便实用的线程阻塞工具，它可以在线程任意位置让线程阻塞。
 * 和Thread.suspenf()相比，它弥补了由于resume()在前发生，导致线程无法继续执行的情
 */
package cn.jdemo.juc;
/**
 * wait() 和notify() 或者 notifyAll() 进行搭配的时候必须在synchronized（obj）模块里  (注：这里的obj为任意一对象)，
 * 切调用wait() notify()，notifyAll()的对象要同一（注：为相同的对象。）
 *
 * wait() notify()，notifyAll()这三个方法的作用范围：必须在同一个main方法里，否者无效。（请看demoZH01，demoZH02，demoZH02）.
 *
 * 调用notify()时：恢复第一个调用wait（）方法的线程，其他的线程不恢复。（注：每个调用wait()方法的对象要同一，即都是相同的一个对象。调用notify()方法的对象  和  调用wait()方法的对象    要一样）
 *
 * 调用notifyAll()时：恢复所有调用wait()方法的线程。（注：每个调用wait()方法的对象要同一，即都是相同的一个对象。调用notifyAll()方法的对象  和  调用wait()方法的对象    要一样）
 *
 * 调用wait方法时：会释放当前的锁对象并加入该对象的等待池中。只有针对此对象调用notify()或者notifyAll()方法后本线程才进入对象锁池准备竞争对象锁 当锁池中的线程获得锁后，才开始准备恢复执行操作（这也是必须在synchronized（obj）模块里的原因，当线程唤醒后已经获得了对象锁，并且此时的线程处于synchronized（obj）模块里，假如线程没有在synchronized（obj）模块里，那锁对象所对应的线程进入到锁池中并获得锁 就没有意义了。同时java语言规定 wait() notify()，notifyAll()要在 在synchronized（obj）模块里使用，否者会报错的）。在java中，每个对象都有两个池，锁(monitor)池和等待池。
 *
 * 3：注意： 线程的等待与唤醒都与所关联的对象有关， 因为他们依赖 关联对象的两个池，锁(monitor)池和等待池。
 *
 */