package cn.jdemo.juc.AQS;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自选锁
 * @description
 * @date 2020/12/16
 */
public class SpinLock02 {

    private final AtomicReference<Thread> reference =  new AtomicReference();

    public void lock(){
        while (!reference.compareAndSet(null, Thread.currentThread())){
        }
    }
    public void unlock(){
        while (!reference.compareAndSet(Thread.currentThread(), null)){

        }
    }
}
