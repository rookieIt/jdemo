package cn.jdemo.juc.core.util.async.callback;


import cn.jdemo.juc.core.util.async.wrapper.WorkerWrapper;

import java.util.Map;

/**
 * 被注解的接口可以有默认方法/静态方法，或者重写Object的(public)方法
 *
 * 有且仅有一个未实现的抽象方法
 */
@FunctionalInterface
public interface IWorker<P,V> {

    V action(P p, Map<String, WorkerWrapper> allWrappers);

    default V defaultValue(){
        System.out.println("IWorker_defaultVlue");
        return null;
    }
}
