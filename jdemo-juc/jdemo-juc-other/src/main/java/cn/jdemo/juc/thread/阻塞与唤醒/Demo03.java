package cn.jdemo.juc.thread.阻塞与唤醒;

/**
 * 有锁，但是不是同一把锁
 * @see Thread#sleep(long) 让出cpu,其他线程可以使用cpu
 *
 */
public class Demo03 {
    private static Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 执行结果如下【同一把锁】:thread#sleep()---sleep方法只让出了CPU，而并不会释放同步资源锁
     * Thread-0start...
     * Thread-0end...
     * Thread-1start...
     * Thread-1end...
     */
    public static void test01() throws InterruptedException {
        new Thread(()->{
            try {
                synchronized (lock){
                    System.out.println(Thread.currentThread().getName()+"start...");
                    Thread.sleep(2000L,999999);//{@code 0-999999} additional nanoseconds to sleep
                    System.out.println(Thread.currentThread().getName()+"end...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        Thread.sleep(200L);// 保障线程1优先获取cpu

        new Thread(()->{
            try {
                synchronized (lock){
                    System.out.println(Thread.currentThread().getName()+"start...");
                    Thread.sleep(500L,999999);//{@code 0-999999} additional nanoseconds to sleep
                    System.out.println(Thread.currentThread().getName()+"end...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
