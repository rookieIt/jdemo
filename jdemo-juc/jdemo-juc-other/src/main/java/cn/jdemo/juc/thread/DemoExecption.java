package cn.jdemo.juc.thread;

public class DemoExecption {
    public static void main(String[] args) {
        try {
            test01();
        }catch (Exception e){
            System.out.println(e.getMessage());// 没有执行
        }finally {
            System.out.println("exit");
        }

    }

    /*正常情况下，如果不做特殊的处理，在主线程中是不能够捕获到子线程中的异常的*/
    private static void test01() {
        Thread thread = new Thread(() -> {
            Thread thread1 = new Thread(() -> {
                throw new RuntimeException("exception...");
            },"thread_a");
            thread1.start();
        },"thread_b");
        thread.start();
    }
}
