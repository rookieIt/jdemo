package cn.jdemo.juc.future.callbackWrapper;

import cn.jdemo.juc.future.wrapper.MyTask;

public class MyFutureServiceImpl2<T,R> implements MyFutureService2<T,R> {

    @Override
    public void commit(MyTask<T,R> task, T t,CallBack<R> callBack) {
        Thread thread = new Thread(() -> {
            R execute = task.execute(t);
            /* 调用回调函数 */
            callBack.call(execute);
        });
        thread.start();
    }
}
