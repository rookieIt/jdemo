package cn.jdemo.juc.CompletableFuture.wrapper;

public interface IHandler<T,V> {
    /**
     * 任务开始的监听
     */
    default void begin(T t) {}

    /**
     * 任务开始的监听
     */
    default void end(T t,WorkResult<V> workResult) {}

    /**
     * 任务开始的监听
     */
    default void exception(T t,WorkResult<V> workResult) {}

}
