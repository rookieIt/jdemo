package cn.jdemo.juc.semaphore;

import java.util.concurrent.Semaphore;

public class HystrixDemo {

    static volatile int count = 0;

    public static void main(String[] args) throws InterruptedException {

        Semaphore semaphore = new Semaphore(10);
        while (count < 20) {

            Thread thread = new Thread(()->{
                Task task1 = new Task();
                try {
                    semaphore.acquire(5);
                    task1.execute();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                semaphore.release(5);
            });
            thread.setName("cuur_" + count++);
            thread.start();
        }
    }

    public static class Task{
        public void execute() throws InterruptedException {
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName());
        }
    }
}
