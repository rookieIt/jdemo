package cn.jdemo.juc.thread;

/**
 * @see Thread#join() 
 * @see Thread#join(long) 
 * @see Thread#join(long millis, int nanos)
 *
 * @description
 * @date 2020/12/7
 */
public class joinDemo {
    public static void main(String[] args) {
        test01();
    }

    /**
     * Thread-1:02执行前
     * Thread-0:01执行前
     * Thread-0:01执行后
     * Thread-1:02执行后
     */
    public static void test01() {
        Thread thread01 = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+":01执行前");
            System.out.println(Thread.currentThread().getName()+":01执行后");
        });
        thread01.start();

        Thread thread02 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+":02执行前");
            try {
                thread01.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+":02执行后");
        });
        thread02.start();
    }
}
