package cn.jdemo.juc.collect;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * java.util包下List的实现
 * @see java.util.ArrayList 数组
 * @see java.util.LinkedList 链表
 * @see java.util.Vector 数组、synchronized
 * @see java.util.Stack 继承Vector、synchronized
 *
 *
 * @date 2020/12/7
 */
public class ListDemo {
    public static void main(String[] args) {
        test01(args);
    }

    public static void test01(String[] args) {
        // Arraylist的替代方案1: jdk1.2
        List<String> syncList = Collections.synchronizedList(new ArrayList<String>());
        syncList.add("1");
        syncList.get(0);

        // Arraylist的替代方案2: jdk1.5
        CopyOnWriteArrayList<String> str = new CopyOnWriteArrayList<>();
        str.add("2");
        str.get(0);

        // LinkedList的替代方案1: jdk1.2
        List<String> syncList3 = Collections.synchronizedList(new LinkedList<String>());
        syncList3.add("3");
        syncList3.get(0);
    }
}
