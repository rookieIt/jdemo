package cn.jdemo.juc.thread.阻塞与唤醒;

/**
 * 有锁，但是不是同一把锁
 * @see Thread#sleep(long) 让出cpu,其他线程可以使用cpu
 *
 */
public class Demo02 {
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 执行结果如下【不是同一把锁】:thread#sleep()会让出cpu
     * Thread-0start...
     * Thread-1start...
     * Thread-1end...
     * Thread-0end...
     *
     */
    public static void test01() throws InterruptedException {
        new Thread(()->{
            try {
                synchronized (new Object()){
                    System.out.println(Thread.currentThread().getName()+"start...");
                    Thread.sleep(2000L,999999);//{@code 0-999999} additional nanoseconds to sleep
                    System.out.println(Thread.currentThread().getName()+"end...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        Thread.sleep(200L);// 保障线程1优先获取cpu

        new Thread(()->{
            try {
                synchronized (new Object()){
                    System.out.println(Thread.currentThread().getName()+"start...");
                    Thread.sleep(500L,999999);//{@code 0-999999} additional nanoseconds to sleep
                    System.out.println(Thread.currentThread().getName()+"end...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
