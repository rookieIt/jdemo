package cn.jdemo.juc.core.util.async.callback;

import cn.jdemo.juc.core.util.async.worker.WorkResult;

/**
 *
 * @description
 * @date 2020/11/30
 */
@FunctionalInterface
public interface ICallback<P,V> {
    // 至少一个抽象方法
    void result(boolean success, P p, WorkResult<V> workResult);

    // 可以default、可以static、可以Object的public方法
    default void begin(){
    }
}
