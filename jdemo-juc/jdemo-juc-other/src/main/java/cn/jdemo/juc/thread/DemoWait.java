package cn.jdemo.juc.thread;

/**
 *
 */
public class DemoWait {
    public static void main(String[] args) throws InterruptedException {
            test01();
    }

    /**
     * hotspot里面对notify方法的实现是顺序唤醒的
     *
     * 输出:
     * thread1 start ...
     * thread2 start ...
     * thread3 start ...
     * thread3 end ...
     * thread1 end ...
     *
     *
     * @throws InterruptedException
     */
    private static void test01() throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            synchronized (DemoWait.class){
                System.out.println("thread1 start ...");
                try {
                    DemoWait.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("thread1 end ...");
            }
        });
        thread1.start();
        Thread.sleep(100L);

        Thread thread2 = new Thread(() -> {
            synchronized (DemoWait.class){
                System.out.println("thread2 start ...");
                try {
                    DemoWait.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("thread2 end ...");
            }
        });
        thread2.start();
        Thread.sleep(100L);

        Thread thread3 = new Thread(() -> {
            synchronized (DemoWait.class){
                System.out.println("thread3 start ...");

                DemoWait.class.notify();

                System.out.println("thread3 end ...");
            }
        });
        thread3.start();
        Thread.sleep(100L);
    }
}
