package cn.jdemo.juc.CompletableFuture.wrapper;

public interface ICallback<T,V> {
    /**
     * 耗时操作执行完毕后，就给value注入值
     */
    void result(T param, WorkResult<V> workResult);
}
