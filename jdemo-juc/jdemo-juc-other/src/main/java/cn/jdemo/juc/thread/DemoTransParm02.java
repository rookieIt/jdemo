package cn.jdemo.juc.thread;

/**
 * InheritableThreadLocal方式: 主子线程数据传递
 *
 * 问题1: 必须新建子线程，才可以主子传递 -- ttl框架(TransmittableThreadLocal)-阿里的
 */
public class DemoTransParm02 {
    private static final ThreadLocal<String> threadLocal = new InheritableThreadLocal<>();
    public static void main(String[] args) {
        threadLocal.set("cuurThread:" + Thread.currentThread().getName());
        test01();
    }

    private static void test01() {
        // String mainThreadVal = threadLocal.get();
        Thread thread = new Thread(() -> {
            // threadLocal.set(mainThreadVal);
            System.out.println("cuurThread:"+ Thread.currentThread().getName()+ ", value:" +threadLocal.get());
        },"thread_b");
        thread.start();
    }
}
