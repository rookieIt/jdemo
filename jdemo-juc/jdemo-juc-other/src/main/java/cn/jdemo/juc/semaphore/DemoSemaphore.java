package cn.jdemo.juc.semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Semaphore 计数信号量(类似于许可证管理器)
 *
 * acquire()方法都会阻塞，直到获取一个可用的许可证。
 * release()方法都会释放持有许可证的线程，并且归还Semaphore一个可用的许可证。
 *
 * @description
 * @date 2020/12/4
 */
public class DemoSemaphore {
    public static void main(String[] args) {
        test();
    }

    /**
     * 输出内容:
     * 汽车1抢到了车位...
     * 汽车3抢到了车位...
     * 汽车3离开了车位...
     * 汽车1离开了车位...
     * 汽车2抢到了车位...
     * 汽车5抢到了车位...
     * 汽车5离开了车位...
     * 汽车2离开了车位...
     * 汽车6抢到了车位...
     * 汽车4抢到了车位...
     * 汽车4离开了车位...
     * 汽车6离开了车位...
     */
    public static void test(){
        Semaphore semaphore = new Semaphore(2);

        for (int i = 1; i <= 6; i++) {
            final int temp = i;
            new Thread(()->{
                try {
                    //得到资源
                    semaphore.acquire();
                    System.out.println("汽车" + temp + "抢到了车位...");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("汽车" + temp + "离开了车位...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    //释放资源
                    semaphore.release();
                }
            },"汽车" + i).start();
        }

    }
}
