package cn.jdemo.juc.CompletableFuture.wrapper;

import java.util.Map;

public interface IWorker<T,V> {
    V action(T object, Map<String, WorkerWrapper> allWrappers);
}
