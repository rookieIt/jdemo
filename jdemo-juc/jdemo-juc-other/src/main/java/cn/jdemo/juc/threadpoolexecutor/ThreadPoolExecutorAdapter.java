package cn.jdemo.juc.threadpoolexecutor;

import java.util.concurrent.*;

public class ThreadPoolExecutorAdapter extends ThreadPoolExecutor {
    private static final ThreadLocal<Long> threadLocal = new ThreadLocal<>();
    public ThreadPoolExecutorAdapter(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        threadLocal.set(System.currentTimeMillis());
        System.out.println("beforeExecute, thread's:" + t.getName());
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        System.out.println("消耗时间: " + (System.currentTimeMillis() - threadLocal.get()));
        System.out.println("runnable's : "+ r.toString());
        if (t == null){
            System.out.println("afterExecute...");
        }else{
            System.out.println("afterExecute, throwable's:" + t.getMessage());
        }
        threadLocal.remove();
    }
}
