package cn.jdemo.juc.noshare;

/**
 * 伪共享:
 *  1.Cache是由很多个cache line组成的。每个cache line通常是64字节，并且它有效地引用主内存中的一块儿地址
 *  2.CPU每次从主存中拉取数据时，会把相邻的数据也存入同一个cache line
 *  3.每次修改某一项，都会使之前缓存的数据失效，从而不能完全达到共享的效果
 *
 * 解决方案:
 *  增大数组元素的间隔使得由不同线程存取的元素位于不同的缓存行上，以空间换时间
 */
public class App {

    /**
     * 通过填充:处理伪共享--输出如下:
     * Thread num 1 duration = 391
     * Thread num 2 duration = 544
     * Thread num 3 duration = 505
     * Thread num 4 duration = 489
     * Thread num 5 duration = 534
     * Thread num 6 duration = 601
     * Thread num 7 duration = 662
     * Thread num 8 duration = 759
     * Thread num 9 duration = 840
     * @param args
     * @throws Exception
     */
    public static void main(final String[] args) throws Exception {
        for(int i=1;i<10;i++){
            System.gc();
            final long start = System.currentTimeMillis();
            FalseSharing.runTest(i);
            System.out.println("Thread num "+i+" duration = " + (System.currentTimeMillis() - start));
        }
    }
}
