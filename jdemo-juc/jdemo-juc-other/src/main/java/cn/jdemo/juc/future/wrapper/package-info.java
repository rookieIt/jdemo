/**
 * 1.线程任务，基于结果进行任务回调
 *
 * 2.异步转同步(类似dubbo的req-resp的异步转同步) lock+condition + Map<reqId,Future>
 *     1) get()时条件等待;
 *     2) received(channel, response)后条件唤醒;
 */
package cn.jdemo.juc.future.wrapper;