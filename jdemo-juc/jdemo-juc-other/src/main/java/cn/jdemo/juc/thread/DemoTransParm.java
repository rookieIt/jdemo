package cn.jdemo.juc.thread;

/**
 * 主子线程数据传递
 */
public class DemoTransParm {
    private static final ThreadLocal<String> threadLocal = new ThreadLocal<>();
    public static void main(String[] args) {
        threadLocal.set("cuurThread:" + Thread.currentThread().getName());
        test01();
    }

    private static void test01() {
        String mainThreadVal = threadLocal.get();
        Thread thread = new Thread(() -> {
            threadLocal.set(mainThreadVal);
            System.out.println("cuurThread:"+ Thread.currentThread().getName()+ ", value:" +threadLocal.get());
        },"thread_b");
        thread.start();
    }
}
