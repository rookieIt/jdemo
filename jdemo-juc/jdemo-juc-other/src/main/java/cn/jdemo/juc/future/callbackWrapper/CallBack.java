package cn.jdemo.juc.future.callbackWrapper;

/**
 * 可在提交任务的时候,将回调接口一并注入
 */
public interface CallBack<T> {
    void call(T t);

    /**
     * 提供一个默认实现
     * @param <T>
     */
    static class DefaultCallBack<T> implements CallBack<T> {

        @Override
        public void call(T t) {
            // do nothing
            System.out.println(t);
        }
    }
}
