package cn.jdemo.juc.future.wrapper;

public interface MyTask<T,R>{
    R execute(T t);
}
