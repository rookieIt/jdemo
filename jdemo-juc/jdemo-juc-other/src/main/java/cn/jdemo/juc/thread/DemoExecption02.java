package cn.jdemo.juc.thread;

/**
 * 子线程处理异常
 */
public class DemoExecption02 {
    public static void main(String[] args) {
        try {
            test01();
        }catch (Exception e){
            System.out.println("currentThread: "+Thread.currentThread() + ", error:"+e.getMessage());
        }finally {
            System.out.println("exit");
        }

    }


    /**
     * 输出
     * exit
     * currentThread: thread_b, error:exception...
     */
    private static void test01() {
        Thread thread = new Thread(() -> {
            throw new RuntimeException("exception...");
        },"thread_b");
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("currentThread: "+Thread.currentThread().getName() + ", error:"+e.getMessage());
            }
        });
        thread.start();
    }
}
