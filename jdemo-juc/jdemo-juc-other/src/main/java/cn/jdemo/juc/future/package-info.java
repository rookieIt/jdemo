/**
 * future设计模式
 * 1.基于runnable + 线程间通信
 * 2.接口设计 -> future#get() future#done()
 * 3. future的get()方法会阻塞，可以在任务完成后，基于事件监听机制，触发get调用 （Google Guava的Future类似这种）
 * <a href="https://blog.csdn.net/wyaoyao93/article/details/115569068">多线程设计模式：Future设计模式</>
 *
 * Future的不足之处（现有juc）
 * 1.无法被动接收异步任务的计算结果 (通过事件监听处理，类似Google Guava的Future)
 * 2.Future间彼此孤立，例如利用它返回的结果再做进一步的运算 (可以通过'任务编码'进行关联)
 * 3.Future没有很好的错误处理机制,必须通过捕获get方法的异常才能知道 (事件监听处理)
 *
 * <a href="https://blog.csdn.net/wyaoyao93/article/details/116164555">还行-总结</>
 */
package cn.jdemo.juc.future;