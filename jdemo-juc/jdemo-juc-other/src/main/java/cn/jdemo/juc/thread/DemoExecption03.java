package cn.jdemo.juc.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 主线程可以捕获异常
 */
public class DemoExecption03 {
    public static void main(String[] args) {
        try {
            test01();
        }catch (Exception e){
            System.out.println(e.getClass());
            System.out.println("currentThread: "+Thread.currentThread().getName() + ", error:"+e.getMessage());
        }finally {
            System.out.println("exit");
        }

    }


    /**
     * 输出
     * class java.util.concurrent.ExecutionException
     * currentThread: main, error:java.lang.RuntimeException: exception...
     * exit
     */
    private static void test01() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Future<?> submit = executorService.submit(new Runnable() {
            @Override
            public void run() {
                throw new RuntimeException("exception...");
            }
        });
        Object o = submit.get();
        System.out.println(o);
    }
}
