package cn.jdemo.juc.CompletableFuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 【static方法】
 *
 * runAsync方法不支持返回值。
 * supplyAsync可以支持返回值
 *
 * link：https://www.jianshu.com/p/6bac52527ca4
 * @date 2020/12/1
 *Demo01
 */
public class Demo01 {

    private static final ExecutorService es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);

    public static void main(String[] args) throws Exception {
        // Demo01.test01();
        Demo01.test02();
    }

    /*
     * 打印内容如下(get()方法会阻塞):
     *  **************************************************
        pool-1-thread-2 : num2
        pool-1-thread-1 : num1
        runAsync()方法：null
        supplyAsync()方法：num2
     *
     *
     * @throws Exception
     */
    public static void test01()throws Exception{

        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : num1");

        }, es);

        CompletableFuture<String> future2= CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : num2");
            return "num2";
        }, es);

        System.out.println("**************************************************");

        Void aVoid = future1.get();
        System.out.println("runAsync()方法："+aVoid);

        String str= future2.get();
        System.out.println("supplyAsync()方法："+str);
    }

    /**
     * 打印内容如下:
     * **************************************************
     * pool-1-thread-2 : num2
     * supplyAsync()方法：num2
     * pool-1-thread-1 : num1
     * runAsync()方法：null
     */
    public static void test02()throws Exception{

        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : num1");

        }, es);

        CompletableFuture<String> future2= CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : num2");
            return "num2";
        }, es);

        System.out.println("**************************************************");

        String str= future2.get();
        System.out.println("supplyAsync()方法："+str);

        Void aVoid = future1.get();
        System.out.println("runAsync()方法："+aVoid);
    }
}
