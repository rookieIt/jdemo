package cn.jdemo.juc.AQS;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自选锁
 * @description
 * @date 2020/12/16
 */
public class SpinLock {

    private static final AtomicReference<Thread> reference =  new AtomicReference();

    public static void lock(){
        while (!reference.compareAndSet(null, Thread.currentThread())){
        }
    }
    public static void unlock(){
        while (!reference.compareAndSet(Thread.currentThread(), null)){

        }
    }
}
