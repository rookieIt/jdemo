package cn.jdemo.juc.core.util.async.callback;

/**
 *
 * @description
 * @date 2020/11/30
 */
public interface ITimeoutWorker<P, V> extends IWorker<P,V> {

    long timeout(long time);

    boolean enableTimeOut();
}
