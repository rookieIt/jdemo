package cn.jdemo.juc.CompletableFuture.wrapper;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) {
        WorkerWrapper<String,String> workerWrapper = new WorkerWrapper<>();
        workerWrapper.setParam("abc");
        workerWrapper.setWorker(new IWorker<String, String>() {
            @Override
            public String action(String object, Map<String, WorkerWrapper> allWrappers) {
                throw new NullPointerException("异常了~");
            }
        });

        workerWrapper.setHandler(new IHandler<String, String>() {
            @Override
            public void begin(String s) {
                System.out.println("begin:" + s);
            }

            @Override
            public void end(String s, WorkResult<String> workResult) {
                System.out.println("end:" + s + " result:" + workResult.getResult());
            }

            @Override
            public void exception(String s, WorkResult<String> workResult) {
                System.out.println(workResult.getEx().getMessage());
            }
        });
        new App().run(workerWrapper);
    }

    public void run(WorkerWrapper workerWrapper){
        ExecutorService es = Executors.newFixedThreadPool(8);
        Map<String, WorkerWrapper> forParamUseWrappers = new ConcurrentHashMap<>();
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            workerWrapper.work(es,forParamUseWrappers);
        },es);
        CompletableFuture.allOf(future);
        es.shutdown();
    }
}
