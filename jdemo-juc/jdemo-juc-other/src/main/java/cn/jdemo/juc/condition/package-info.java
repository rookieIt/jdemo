/**
 * @see java.util.concurrent.locks.Condition
 * java.util.concurrent.locks.Condition#await() 类似于 ava.lang.Object#wait()
 * java.util.concurrent.locks.Condition#signal() 类似于 java.lang.Object#notify()
 * java.util.concurrent.locks.Condition#signalAll() 类似于 java.lang.Object#notifyAll()
 *
 * 使用Condition的await()、signal()这种方式实现线程间协作更加安全和高效
 *
 * 应用场景: 阻塞队列 (生产者消费者模式)
 *
 *
 * ondition可以通俗的理解为条件队列。当一个线程在调用了await方法以后，直到线程等待的某个条件为真的时候才会被唤醒。
 * 这种方式为线程提供了更加简单的等待/通知模式。
 * Condition必须要配合锁一起使用，因为对共享状态变量的访问发生在多线程环境下。
 * 一个Condition的实例必须与一个Lock绑定，因此Condition一般都是作为Lock的内部实现。
 *
 * ConditionObject实现了Condition接口，是AbstractQueuedSynchronizer的内部类（因为Condition的操作都需要获取想关联的锁）
 *
 * @date 2020/12/3
 */
package cn.jdemo.juc.condition;