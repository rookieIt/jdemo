package cn.jdemo.juc.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * InheritableThreadLocal方式: 主子线程数据传递
 *
 * 问题2: 浅拷贝问题
 */
public class DemoTransParm03 {
    private static final ThreadLocal<Map<String,String>> threadLocal = new InheritableThreadLocal<>();
    public static void main(String[] args) throws InterruptedException {
        Map<String, String> map = new HashMap<>();

        map.put("main",Thread.currentThread().getName());
        threadLocal.set(map);
        test01();

        Thread.sleep(1000L);
        System.out.println(map.get("main"));// thread_b
    }

    private static void test01() {
        Thread thread = new Thread(() -> {
            Map<String, String> map = threadLocal.get();
            map.put("main", Thread.currentThread().getName());
        },"thread_b");
        thread.start();
    }
}
