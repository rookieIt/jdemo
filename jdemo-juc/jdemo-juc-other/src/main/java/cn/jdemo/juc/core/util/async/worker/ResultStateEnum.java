package cn.jdemo.juc.core.util.async.worker;

/**
 * 结果状态
 */
public enum ResultStateEnum {
    SUCCESS,
    TIMEOUT,
    EXCEPTION,
    DEFAULT  //默认状态
}
