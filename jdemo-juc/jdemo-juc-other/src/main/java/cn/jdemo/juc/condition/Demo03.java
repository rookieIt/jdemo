package cn.jdemo.juc.condition;

/**
 * 启动线程，线程内部递归调用启动线程，Thread-557637还没打满
 *
 * @description
 * @date 2020/12/4
 */
public class Demo03 {
    public static void main(String[] args) {
        Demo03 demo03 = new Demo03();
        demo03.test01();
    }

    public void test01(){
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
            Demo03 demo03 = new Demo03();
            demo03.test01();
        }).start();
    }
}
