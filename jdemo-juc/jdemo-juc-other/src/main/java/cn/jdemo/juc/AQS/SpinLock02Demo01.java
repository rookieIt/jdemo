package cn.jdemo.juc.AQS;

/**
 *
 * @description
 * @date 2020/12/16
 */
public class SpinLock02Demo01 {
    public static void main(String[] args) throws InterruptedException {
        test01();
    }

    /**
     * 运行结果
     * Thread-0start...
     * Thread-0end...
     * Thread-1start...
     * Thread-1end...
     * @throws InterruptedException
     */
    public static void test01() throws InterruptedException {
        SpinLock02 spinLock02 = new SpinLock02();

        new Thread(()->{
            spinLock02.lock();
            System.out.println(Thread.currentThread().getName()+"start...");
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"end...");
            spinLock02.unlock();
        }).start();

        Thread.sleep(500L);
        new Thread(()->{
            spinLock02.lock();
            System.out.println(Thread.currentThread().getName()+"start...");
            System.out.println(Thread.currentThread().getName()+"end...");
            spinLock02.unlock();
        }).start();


    }
}
