package cn.jdemo.juc.longaddr;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;

public class App {

    public static void main(String[] args) throws InterruptedException {
        int threadCount = 100;
        int times = 1000 * 1000;
        LongAdder longAdder = new LongAdder();
        List<Thread> list = new ArrayList();
        for (int i = 0; i < threadCount; i++) {
            list.add(new Thread(() -> {
                for (int j = 0; j < times; j++) {
                      longAdder.increment();
                }
            }));
        }
        for (Thread thread : list) {
            thread.start();
        }
        for (Thread thread : list) {
            thread.join();
        }
        System.out.println(longAdder.longValue());
    }
}
