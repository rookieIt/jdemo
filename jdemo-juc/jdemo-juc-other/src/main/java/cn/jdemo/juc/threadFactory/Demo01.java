package cn.jdemo.juc.threadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @see java.util.concurrent.ThreadFactory ： 根据需要创建新线程的对象
 * @description
 * @date 2020/12/10
 */
public class Demo01 {
    public static void main(String[] args) {
        test01();
    }

    /**
     * 执行结果(最多生成2个线程，然后放入线程池中调度)
     * demo1_runnable...
     * demo1_runnable...
     * demo1_runnable...
     * demo1_runnable...
     * demo0_runnable...
     * ... ...
     */
    public static void test01() {
        ThreadFactory threadFactory = new ThreadFactory() {
            AtomicInteger atomicInteger = new AtomicInteger(0);
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r,"demo"+atomicInteger.getAndIncrement());
            }
        };

        ExecutorService executorService = Executors.newFixedThreadPool(1000, threadFactory);

        for (int i= 0;i<1000;i++){
            executorService.execute(()->{
                System.out.println(Thread.currentThread().getName()+"_runnable...");
            });
        }
    }
}
