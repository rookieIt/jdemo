package cn.jdemo.juc.future.wrapper;

public class MyFutureServiceImpl<T,R> implements MyFutureService<T,R>{

    @Override
    public MyFuture<R> commit(MyTask<T,R> task,T t) {
        MyFutureImpl<R> future = new MyFutureImpl<>();
        Thread thread = new Thread(() -> {
            R execute = task.execute(t);
            future.finish(execute);
        });
        thread.start();
        return future;
    }
}
