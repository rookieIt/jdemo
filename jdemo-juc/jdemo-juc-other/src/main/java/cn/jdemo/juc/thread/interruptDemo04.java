package cn.jdemo.juc.thread;

/**
 * 调用interrupt()后不会终止，只是一个终止信号
 */
public class interruptDemo04 {
    public static void main(String[] args) throws InterruptedException {
        // test01();
        test02();
    }

    private static void test02() throws InterruptedException {
        Thread thread2 = new Thread(() -> {
            synchronized (interruptDemo04.class) {
                while (!Thread.interrupted()){
                    System.out.println("后续业务~");
                }
            }
        });
        thread2.start();
        Thread.sleep(2000L);
        thread2.interrupt();
    }

    /**
     * 结果分析，synchronized获取锁的过程不响应中断请求
     *
     * 后续业务~
     * ... ...
     */
    private static void test01() throws InterruptedException {
        Thread thread2 = new Thread(() -> {
            synchronized (interruptDemo04.class) {
                for (;;){
                    System.out.println("后续业务~");
                }
            }
        });
        thread2.start();
        Thread.sleep(3000L);
        thread2.interrupt();
    }
}
