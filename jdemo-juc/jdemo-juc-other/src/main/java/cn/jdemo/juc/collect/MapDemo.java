package cn.jdemo.juc.collect;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 *
 * @date 2020/12/7
 */
public class MapDemo {
    public static void main(String[] args) {
        test01(args);
    }

    public static void test01(String[] args) {
        // HashMap的替代方案1: jdk1.5
        ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
        map.put("key", "01");
        map.get("key");
    }
}
