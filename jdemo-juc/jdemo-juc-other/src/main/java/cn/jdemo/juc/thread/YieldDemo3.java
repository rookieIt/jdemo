package cn.jdemo.juc.thread;

/**
 * @see Thread#yield() 让出cpu，然后大家一起重新抢，该线程如果再次获取，则继续执行
 * @description
 * @date 2020/12/7
 */
public class YieldDemo3 {
    private static final Object lock = new Object();
    public static void main(String[] args) throws InterruptedException {
        test01();
    }


    public static void test01() throws InterruptedException {
        new Thread(()->{
            synchronized (lock){
                System.out.println(Thread.currentThread().getName()+" : 开始");
                Thread.yield();
                System.out.println(Thread.currentThread().getName()+" : 结束");
            }
        }).start();

        new Thread(()->{
            synchronized (lock){
                System.out.println(Thread.currentThread().getName()+" : 开始");
                System.out.println(Thread.currentThread().getName()+" : 结束");
            }
        }).start();

        new Thread(()->{
            synchronized (lock){
                System.out.println(Thread.currentThread().getName()+" : 开始");
                System.out.println(Thread.currentThread().getName()+" : 结束");
            }
        }).start();
    }

}
