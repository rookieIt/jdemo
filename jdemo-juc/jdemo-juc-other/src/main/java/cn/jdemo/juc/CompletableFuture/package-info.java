/**
 * @see java.util.concurrent.CompletableFuture 解决线程间任务的调度问题
 * @see java.util.concurrent.CompletionStage 代表异步计算过程中的某一个阶段，一个阶段完成以后可能会触发另外一个阶段
 * ---静态方法---
 * @see java.util.concurrent.CompletableFuture#runAsync(java.lang.Runnable) 无返回值
 * @see java.util.concurrent.CompletableFuture#supplyAsync(java.util.function.Supplier) 有返回值
 * 
 * @see java.util.concurrent.CompletableFuture#allOf(java.util.concurrent.CompletableFuture[])
 * --- 实例方法 ---
 *
 * 【静态方法】
 * completedFuture
 * runAsync    方法不支持返回值。
 * supplyAsync 可以支持返回值
 * allOf
 * anyOf
 *
 * 【实例方法】
 * runAfterEither 方法
 * 两个CompletionStage，任何一个完成了都会执行下一步的操作
 * runAfterBoth
 * 两个CompletionStage，都完成了计算才会执行下一步的操作
 *
 * acceptEither【没有返回值】
 * 两个CompletionStage，谁执行返回的结果快，我就用那个CompletionStage的结果进行下一步的消耗；
 * applyToEither【有返回值】
 * 两个CompletionStage，谁执行返回的结果快，我就用那个CompletionStage的结果进行下一步的处理；
 *
 * thenAcceptBoth【没有返回值】
 * 当两个CompletionStage都执行完成后，把结果一块交给thenAcceptBoth来进行消耗；
 * thenCombine【有返回值】
 * 当两个CompletionStage都执行完成后，把结果一块交给thenAcceptBoth来进行处理；
 *
 * thenRun
 * 不会接收上个CompletionStage的处理结果，只是处理完上个任务后，执行的后续操作。
 * thenAccept
 * 接收上个CompletionStage的处理结果，并消费，无返回结果。
 *
 * thenCompose 方法
 * 对CompletionStage进行流水线操作，第一个操作完成时，将其结果作为参数传递给第二个操作。
 * thenApply 方法
 * 当一个线程依赖另一个线程时，可以使用 thenApply 方法来把这两个线程串行化。
 * handle
 * 对CompletionStage进行流水线操作，第一个操作完成时，将其结果或throwable作为参数传递给第二个操作。
 *
 * whenComplete 和 whenCompleteAsync 的区别：
 * whenComplete：是执行当前任务的线程执行继续执行 whenComplete 的任务。
 * whenCompleteAsync：是执行把 whenCompleteAsync 这个任务继续提交给线程池来进行执行。
 *
 * @date 2020/12/1
 */
package cn.jdemo.juc.CompletableFuture;