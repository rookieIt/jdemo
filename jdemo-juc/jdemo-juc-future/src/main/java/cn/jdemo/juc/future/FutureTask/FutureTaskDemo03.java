package cn.jdemo.juc.future.FutureTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @description
 * @date 2020/12/3
 */
public class FutureTaskDemo03 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test0();
    }
    /**
     *
     * 执行结果：
     * 当前线程: pool-1-thread-1开始
     * 当前线程: pool-1-thread-1结束
     * futureTask返回值:abc
     * pool-1-thread-1:start...
     * pool-1-thread-1:end...
     *
     * 结论:
     *     sleep让出了cpu，但所使用的线程仍然占用
     */
    public static void test0() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        FutureTask<String> futureTask = new FutureTask<String>(() -> {
            System.out.println("当前线程: "+Thread.currentThread().getName() + "开始");
            Thread.sleep(10000L);//java.lang.Thread.State: TIMED_WAITING
            System.out.println("当前线程: "+Thread.currentThread().getName() + "结束");
            return "abc";
        });
        executorService.execute(futureTask);
        executorService.execute(()->{
            System.out.println(Thread.currentThread().getName() + ":start...");
            System.out.println(Thread.currentThread().getName() + ":end...");
        });
        String s = futureTask.get();
        System.out.println("futureTask返回值:"+s);

        // 线程池的单个线程执行完后，线程处于java.lang.Thread.State: WAITING
    }
}
