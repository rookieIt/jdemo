/**
 * @see java.util.concurrent.FutureTask 实现了Runnable和Future接口,封装了Callable对象
 *
 * Callable接口是一个泛型接口，也是函数式接口，异常为Exception，不用强制捕获，
 * Callable接口和Runnable接口相比功能更强大，
 * 但是有个问题就是Callable接口实例不能作为Thread线程实例的target来使用，
 * 而Runnable接口可以直接作为Thread的构造参数传入开启线程。
 * 而且Java线程类中只有Thread，为了解决这个问题，就有了我们今天的FutureTask类
 *
 * @date 2020/12/3
 */
package cn.jdemo.juc.future;