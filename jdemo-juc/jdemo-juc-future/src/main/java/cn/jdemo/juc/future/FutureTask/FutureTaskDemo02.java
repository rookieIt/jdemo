package cn.jdemo.juc.future.FutureTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * // 获取任务执行结果，如果任务没有执行完成，调用次方法线程会一直阻塞，知道任务完成
 * V get()
 * // 和上面一样功能，不同的是多了阻塞时间限制，在限制时间内没有完成则抛出异常
 * public V get(long timeout, TimeUnit unit)
 * // 获取任务执行状态，执行完成返回true
 * public boolean isDone()
 * // 获取任务的取消状态，如果任务完成前被取消，则返回true
 * public boolean isCancelled()
 * // 取消任务执行
 * public boolean cancel(boolean mayInterruptIfRunning)
 * @description
 * @date 2020/12/3
 */
public class FutureTaskDemo02 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test0();
    }
    private static volatile boolean isSucc = false;
    /**
     * TODO
     *
     * 执行结果：
     * 当前线程: Thread-0开始
     * 当前线程: Thread-0 1607409204246
     * 当前线程: Thread-0结束
     * 是否取消:false
     * abc
     */
    public static void test0() throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<String>(() -> {
            System.out.println("当前线程: "+Thread.currentThread().getName() + "开始");
            int i = 0;
            if (i<1){
                System.out.println("当前线程: "+Thread.currentThread().getName() +" "+System.currentTimeMillis());
                isSucc = true;
            }
            System.out.println("当前线程: "+Thread.currentThread().getName() + "结束");
            return "abc";
        });
        new Thread(futureTask).start();// FutureTask -- 对runnable + future 进一步实现包装

        Thread.sleep(1000L);
        boolean cancel = futureTask.cancel(true);
        System.out.println("是否取消:"+futureTask.isCancelled());

        String s = futureTask.get();
        System.out.println(s);
    }

}
