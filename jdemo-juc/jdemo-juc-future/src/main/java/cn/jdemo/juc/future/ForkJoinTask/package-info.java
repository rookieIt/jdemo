/**
 * Fork/Join框架
 *
 * 从JDK1.7开始，Java提供Fork/Join框架用于并行执行任务，它的思想就是讲一个大任务分割成若干小任务，最终汇总每个小任务的结果得到这个大任务的结果。
 * 这种思想和MapReduce很像（input --> split --> map --> reduce --> output）
 *
 * 主要有两步：
 * 第一、任务切分；
 * 第二、结果合并
 * 它的模型大致是这样的：
 *   线程池中的每个线程都有自己的工作队列（PS：这一点和ThreadPoolExecutor不同，ThreadPoolExecutor是所有线程公用一个工作队列，所有线程都从这个工作队列中取任务），
 * 当自己队列中的任务都完成以后，会从其它线程的工作队列中偷一个任务执行，这样可以充分利用资源。
 *
 * ForkJoinTask代表运行在ForkJoinPool中的任务。
 * ForkJoinPool与其它的ExecutorService区别主要在于它使用“工作窃取”：线程池中的所有线程都企图找到并执行提交给线程池的任务。
 * 当大量的任务产生子任务的时候，或者同时当有许多小任务被提交到线程池中的时候，这种处理是非常高效的。特别的，当在构造方法中设置asyncMode为true的时候这种处理更加高效。
 */
package cn.jdemo.juc.future.ForkJoinTask;